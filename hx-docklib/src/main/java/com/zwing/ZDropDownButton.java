package com.zwing;

import javax.swing.*;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import com.zwing.ui.jgoodies.DropDownButtonUI;

/**
 * Created: 22.06.2006 17:24:36
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZDropDownButton extends JButton implements PopupMenuListener,
        ActionSelectedListener {

    private ZPopupMenu popupMenu;

    private DefaultActionHandler dapListener;

    private DropDownModel dropDownModel;

    public ZDropDownButton(String text) {
        this(text, null);
    }

    public Insets getInsets() {
        DropDownButtonUI dui = (DropDownButtonUI) getUI();
        Insets insets = super.getInsets();
        return new Insets(insets.top, insets.left,
                insets.bottom, insets.right + dui.getExtraWidth());
    }

    public ZDropDownButton(Icon icon) {
        this(null, icon);
    }

    public ZDropDownButton(Action action) {
        super(action);
        localInit();
    }

    public ZDropDownButton(String text, Icon icon) {
        super(text, icon);
        localInit();
    }

    public void setPopupMenu(ZPopupMenu p) {
        this.popupMenu = p;
        popupMenu.addPopupMenuListener(this);
    }

    public void setPopupComponent(JComponent c) {
        if (c instanceof ZPopupMenu) {
            popupMenu = (ZPopupMenu) c;
        } else {
            popupMenu = new ZPopupMenu(this);
        popupMenu.removeAll();
        popupMenu.setBorder(BorderFactory.createEmptyBorder());
        popupMenu.add(c);
        popupMenu.pack();
        popupMenu.addPopupMenuListener(this);
    }
    }

    private boolean isPopupVisible() {
        return popupMenu != null && popupMenu.isVisible();
    }

    public void setDropDownModel(DropDownModel dropDownModel) {
        this.dropDownModel = dropDownModel;
    }

    public DropDownModel getDropDownModel() {
        return dropDownModel;
    }

    private void localInit() {
        dapListener = new DefaultActionHandler();
        dropDownModel = new DefaultDropDownModel();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void actionSelected(Action action) {
        setDefaultAction(action);
    }

    public void executeDefaultAction() {
        int modifiers = 0;
        AWTEvent currentEvent = EventQueue.getCurrentEvent();
        if (currentEvent instanceof InputEvent) {
            modifiers = ((InputEvent)currentEvent).getModifiers();
        } else if (currentEvent instanceof ActionEvent) {
            modifiers = ((ActionEvent)currentEvent).getModifiers();
        }
        ActionEvent ae = new ActionEvent(
                    this, ActionEvent.ACTION_PERFORMED,
                    getActionCommand(),
                    EventQueue.getMostRecentEventTime(),
                    modifiers);

        dapListener.action.actionPerformed(ae);
        fireActionPerformed(ae);
    }

    public void setDefaultAction(Action action) {
        dapListener.attachAction(action);
        if (action != null) {
            setIcon((Icon) action.getValue(Action.SMALL_ICON));
            setText((String) action.getValue(Action.NAME));
        } else {
            setIcon(null);
            setText(null);
        }
    }

    private void defaultActionEnabledChanged(boolean enabled) {
        repaint();
    }

    private boolean isDefaultActionEnabled() {
        return dapListener.getAction() != null && dapListener.getAction().isEnabled();
    }

    public void stopPopup() {
        if (popupMenu != null) {
            popupMenu.setVisible(false);
        }
    }

    public void showPopup() {
        Rectangle r = getBounds();
        Point ps = getLocationOnScreen();
        ps.translate(0, getHeight());
        if (popupMenu != null) {
            popupMenu.show(this, 0, getHeight());
        }
    }

    private class DefaultActionHandler implements PropertyChangeListener {
        private Action action;

        public void propertyChange(PropertyChangeEvent evt) {
            if (action == null) {
                throw new NullPointerException("Default action is null.");
            }
            final String prop = evt.getPropertyName();
            if (Action.NAME.equals(prop)) {
                setText((String) action.getValue(Action.NAME));
            } else if (Action.SMALL_ICON.equals(prop)) {
                setIcon((Icon) action.getValue(Action.SMALL_ICON));
            } else if (ActionHolder.PROP_ENABLED.equals(prop)) {
                defaultActionEnabledChanged(action.isEnabled());
            }
        }

        void executeAction() {
        }

        void attachAction(Action action) {
            detachAction();
            if (action != null) {
                action.addPropertyChangeListener(this);
            }
            this.action = action;
        }

        void detachAction() {
            if (action != null) {
                action.removePropertyChangeListener(this);
            }
        }

        public Action getAction() {
            return action;
        }
    }

    private static final String UI_CLASS_ID = "DropDownButtonUI";

    public String getUIClassID() {
        return UI_CLASS_ID;
    }
}
