package com.zwing;

import com.zwing.docking.*;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * Внутренняя рабочая область
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockingPane extends JDesktopPane
        implements ComponentListener {

    private DragCanvas dc;
    private SplittersCanvas sc;

    private DockManager dm;

    public DockingPane() {
        init();
    }

    public DragCanvas getDragCanvas() {
        return dc;
    }

    public SplittersCanvas getSplittersCanvas() {
        return sc;
    }

    protected void init() {
        setLayout(null);

        UIDefaults ud = UIManager.getLookAndFeel().getDefaults();
        Color cBg = ud.getColor("Button.shadow");

        setBackground(cBg);

        dc = new DragCanvas();
        sc = new SplittersCanvas();
        add(dc, DockingConst.ZO_DRAG);
        add(sc, DockingConst.ZO_SPLIT);

        addComponentListener(this);

        this.setFocusable(true);
        this.setEnabled(true);
        this.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);

        dc.setVisible(false);
        sc.setVisible(true);
    }

    public void componentHidden(ComponentEvent e) {
    }

    public void componentShown(ComponentEvent e) {
        dm.adjustAfterResize();
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void doLayout() {
        super.doLayout();
        Dimension sz = getSize();
        setSizeSafe(dc, sz);
        setSizeSafe(sc, sz);
        dm.adjustAfterResize();
    }

    private void setSizeSafe(JComponent child, Dimension sz) {
        if (!sz.equals(child.getSize())) {
            child.setBounds(0, 0, sz.width, sz.height);
        }
    }

    public void componentResized(ComponentEvent e) {
    }

    public void setCurrent(JComponent c) {
    }

    public void setDockManager(DockManager dm) {
        this.dm = dm;
    }
}
