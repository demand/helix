package com.zwing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import com.zwing.blink.Blinkable;
import com.zwing.docking.DockingConst;
import com.zwing.utils.ZUIManager;

/**
 * ZSideButton
 *
 * @author А.Дёмин
 * @since 01.12.2004
 */
public class ZSideButton extends JToggleButton implements Blinkable, MouseListener {

    static final double NINETY_DEGREES = Math.toRadians(90.0);

    private int location;
    private String name;

    private Image imSelected;
    private Image imNormal;
    private Image imPressed;
    private Image imDisabled;
    private Image imBlink;
    private ImageIcon icon;

    private JPopupMenu contextMenu;

    private ContextMenuProvider contextMenuProvider;

    private boolean isBlinkPhaseOn;

    public ZSideButton(int location, String name, ImageIcon icon) {
        super();

        this.location = location;
        this.name = name;
        this.icon = icon;
        isBlinkPhaseOn = false;
        addMouseListener(this);
        init();
    }

    private void init() {
        int height = (Integer) ZUIManager.getProperty("sideButton.height");
        Insets insets = (Insets) ZUIManager.getProperty("sideButton.insets");
        int iconSpace = (Integer) ZUIManager.getProperty("sideButton.iconSpace");
        int borderSize = (Integer) ZUIManager.getProperty("sideButton.borderSize");
        Color cBgBlink = (Color) ZUIManager.getProperty("sideButton.bgBlink");
        Color cFrBlink = (Color) ZUIManager.getProperty("sideButton.frBlink");

        UIDefaults ud = UIManager.getLookAndFeel().getDefaults();
        Color cBgNormal = ud.getColor("Button.background");
        Color cFrNormal = ud.getColor("Button.foreground");
        Color cBgSelected = ud.getColor("Button.shadow");
        Color cFrSelected = ud.getColor("Button.foreground");
        Color cBorder = ud.getColor("Button.darkShadow");

        Image im = (Image) ZUIManager.getProperty("service.image");
        Graphics2D g = (Graphics2D) im.getGraphics();
        Font f = (Font) ZUIManager.getProperty("sideButton.font");
        FontMetrics fm = g.getFontMetrics(f);

        SBData sbd = new SBData();
        sbd.insets = insets;
        sbd.borderSize = borderSize;
        sbd.iconSpace = iconSpace;

        imNormal = drawWhole(cBgNormal, cFrNormal, cBorder, f, fm, height, sbd);
        imSelected = drawWhole(cBgSelected, cFrSelected, cBorder, f, fm, height, sbd);
        imBlink = drawWhole(cBgBlink, cFrBlink, cBorder, f, fm, height, sbd);
    }

    private static void drawBorder(Graphics2D g, int width, int height,
            Color cBorder, Color cBgNormal) {
        g.setColor(cBorder);
        g.drawRect(0, 0, width - 1, height - 1);
        g.setColor(cBgNormal);
        g.drawLine(0, 0, 0, 0);
        g.drawLine(width - 1, 0, width - 1, 0);
        g.drawLine(width - 1, height - 1, width - 1, height - 1);
        g.drawLine(0, height - 1, 0, height - 1);
    }

    private Image drawWhole(Color bg, Color fr, Color brd, Font f, FontMetrics fm, int h,
            SBData sbd) {
        int width = 0;
        int height = h;
        int nameWidth = 0;
        Image nameImage = null;
        if (name != null) {
            nameWidth = fm.stringWidth(name);
            if (nameWidth > 0) {
                int descent = fm.getDescent();
                int y = height - sbd.borderSize - sbd.insets.bottom - descent;

                nameImage = new BufferedImage(nameWidth, height, BufferedImage.TYPE_INT_ARGB);
                Graphics2D g = (Graphics2D) nameImage.getGraphics();
//                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//                        RenderingHints.VALUE_ANTIALIAS_ON);
                g.setColor(bg);
                g.fillRect(0, 0, nameWidth, height);
                g.setFont(f);
                g.setColor(fr);
                g.drawString(name, 0, y);

                width += nameWidth;
            }
        }

        width += sbd.insets.left + sbd.insets.right + sbd.borderSize * 2;

        boolean isVertical = ZSideBar.isVertical(location);
        if (icon != null) {
            width += isVertical ? icon.getIconWidth() : icon.getIconHeight();
            if (nameWidth > 0) {
                width += sbd.iconSpace;
            }
        }

        if (isVertical) {
            int t = width;
            width = height;
            height = t;
        }
        Image im = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) im.getGraphics();

        g.setColor(bg);
        g.fillRect(0, 0, width, height);

        drawIconAndText(width, height, sbd, nameWidth, nameImage, g);

        drawBorder(g, width, height, brd, bg);

        setPreferredSize(new Dimension(width, height));
        setMinimumSize(new Dimension(width, height));
        setMaximumSize(new Dimension(width, height));

        return im;
    }

    private void drawIconAndText(int width, int height, SBData sbd,
            int nameWidth, Image nameImage, Graphics2D g) {
        if (location == DockingConst.DK_WEST) {
            int x = -nameWidth - sbd.insets.right - sbd.borderSize;
            if (icon != null) {
                icon.paintIcon(this, g, (width - icon.getIconWidth()) / 2, -x + sbd.iconSpace);
            }
            int y = 0;
            g.rotate(-Math.PI / 2.0);
            g.drawImage(nameImage, x, y, nameWidth, width, this);
            g.rotate(Math.PI / 2.0);
        } else if (location == DockingConst.DK_EAST) {
            int x = sbd.insets.left + sbd.borderSize;
            if (icon != null) {
                icon.paintIcon(this, g, (width - icon.getIconWidth()) / 2, x);
                x += icon.getIconHeight() + sbd.iconSpace;
            }
            int y = -width;
            g.rotate(Math.PI / 2.0);
            g.drawImage(nameImage, x, y, nameWidth, width, this);
            g.rotate(-Math.PI / 2.0);
        } else if (location == DockingConst.DK_NORTH || location == DockingConst.DK_SOUTH) {
            int x = sbd.insets.left + sbd.borderSize;
            int y = 0;

            if (icon != null) {
                icon.paintIcon(this, g, x, (height - icon.getIconHeight()) / 2);
                x += icon.getIconWidth() + sbd.iconSpace;
            }
            g.drawImage(nameImage, x, y, nameWidth, height, this);
        }
    }

    static class SBData {
        Insets insets;
        int borderSize;
        int iconSpace;
    }

    public int getDockLocation() {
        return location;
    }

    public void setDockLocation(int loc) {
        location = loc;
    }

    protected void drawButton(Color bgColor, Color frColor) {
    }

    public void paint(Graphics g) {
        Dimension sz = getSize();
        Image im = isBlinkPhaseOn ? imBlink : isSelected() ? imSelected : imNormal;
        g.drawImage(im, 0, 0, sz.width, sz.height, this);
    }

    public void blinkPhaseOn() {
        isBlinkPhaseOn = true;
        repaint();
    }

    public void blinkPhaseOff() {
        isBlinkPhaseOn = false;
        repaint();
    }

    public void blinkFinished() {
    }

    public JPopupMenu getContextMenu() {
        return contextMenu;
    }

    public void setContextMenu(JPopupMenu contextMenu) {
        this.contextMenu = contextMenu;
    }

    public ContextMenuProvider getContextMenuProvider() {
        return contextMenuProvider;
    }

    public void setContextMenuProvider(ContextMenuProvider contextMenuProvider) {
        this.contextMenuProvider = contextMenuProvider;
    }

    private void showContextMenu(Point p) {
        JPopupMenu m = contextMenu != null ?
                contextMenu :
                contextMenuProvider != null ?
                        contextMenuProvider.getContextMenu(this) : null;
        if (m!= null) {
            m.show(this, p.x, p.y);
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3) {
            showContextMenu(e.getPoint());
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
}
