package com.zwing;

import javax.swing.JComponent;

/**
 * Created: 01.09.2006 18:14:07
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface DockingCargo {
    /**
     * @return компонент, который вставляется внутрь окна
     */
    JComponent getVisualComponent();

    /**
     * При загрузке конфигурации окон для восстановления соответствия между
     * окном и его содержимым, необходимо чтобы каждый экземпляр
     * внутреннего компонента окна имел уникальное служебное имя.    
     * @return уникальное имя
     */
    String getUniqueName();

    /**
     * @param propName название свойства
     * @return значение свойства
     */
    Object getProperty(String propName);

    /**
     * Добавить контроль изменения свойств содержимомго окна
     * @param cargoListener объект, получающий уведомления
     */
    void addCargoListener(CargoListener cargoListener);

    /**
     * Отменить контроль изменения свойств содержимомго окна
     * @param cargoListener объект, получающий уведомления
     */
    void removeCargoListener(CargoListener cargoListener);
}
