package com.zwing.colorcombo;

import com.zwing.ColorIcon;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.Component;
import java.awt.Color;
import java.awt.Dimension;

/**
 * Created: 22.11.2006 18:34:21
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ColorComboRenderer extends DefaultListCellRenderer {
    private ColorIcon icon;

    public static final Dimension ICON_SIZE = new Dimension(16, 12);

    public ColorComboRenderer() {
        icon = new ColorIcon(ICON_SIZE, null);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
        JLabel rslt = (JLabel) super.getListCellRendererComponent(
                list, value, index, isSelected, cellHasFocus);

//        rslt.setIcon(new ColorIcon(new Dimension(16, 12), (Color) value));
        icon.setColor((Color) value);

        rslt.setIcon(icon);
        rslt.setText(null);

        return rslt;
    }

}
