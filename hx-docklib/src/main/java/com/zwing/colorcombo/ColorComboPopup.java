package com.zwing.colorcombo;

import com.zwing.ZColorComboBox;
import com.zwing.ZColorChooserDialog;
import com.zwing.colorchooser.ColorBoxes;

import javax.swing.plaf.basic.ComboPopup;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

/**
 * Created: 30.10.2006 12:13:27
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ColorComboPopup extends JPopupMenu implements ComboPopup {

    private ZColorComboBox combo;
    private ColorBoxes cb;

    private Handler handler;
    private PropertyChangeListener propertyChangeListener;
    private static final int COLUMNS = 8;
    private static final int INITIAL_BOXES_COUNT = 32;
    private static final Dimension BOX_SIZE = new Dimension(22, 18);

    public ColorComboPopup(ZColorComboBox combo) {
        super();
        this.combo = combo;
        handler = new Handler();
        setLightWeightPopupEnabled(combo.isLightWeightPopupEnabled());
        configurePopup();
        installComboBoxListeners();
        putClientProperty(com.jgoodies.looks.Options.NO_MARGIN_KEY, Boolean.FALSE);
    }

    protected void installComboBoxListeners() {
        propertyChangeListener = createPropertyChangeListener();
        if (propertyChangeListener != null) {
            combo.addPropertyChangeListener(propertyChangeListener);
        }
//        installComboBoxModelListeners(comboBox.getModel());
    }


    private void configurePopup() {
//        setLayout( new BoxLayout( this, BoxLayout.Y_AXIS ) );
        setLayout( new BorderLayout() );
        setBorderPainted(true);
        setOpaque(false);
        intializeComponent();
        setDoubleBuffered( true );
        setFocusable( false );
    }

    private void intializeComponent() {
        setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.black),
                        BorderFactory.createLineBorder(Color.white)));
        setLayout(new BorderLayout());

        ColorComboModel model = (ColorComboModel) combo.getModel();
        cb = new ColorBoxes(
                COLUMNS, 0, BOX_SIZE);
        if (model != null) {
            cb.setColorElements(model.getColorElements());
            cb.setColorNames(model.getColorElements());
        }
        cb.addColorSelectedListener(combo);
        add(cb, BorderLayout.CENTER);
        JButton comp = new JButton(new AbstractAction("More colors...") {
            public void actionPerformed(ActionEvent e) {
                Object[] rcs = ZColorChooserDialog.showColorChooserDialog(null, "Select Color", null);
                if (((Integer) rcs[0]).intValue() == JOptionPane.OK_OPTION) {
                    combo.colorSelected((Color) rcs[1], this);
                }
            }
        });
        comp.setFocusable(false);
        add(comp, BorderLayout.SOUTH);
    }

    /**
     * Implementation of ComboPopup.show().
     */
    public void show() {
        MenuSelectionManager manager = MenuSelectionManager.defaultManager();
        manager.clearSelectedPath();
        Point location = getPopupLocation();
            show(combo, location.x, location.y );
    }

    /**
     * Calculate the placement and size of the popup portion of the combo box based
     * on the combo box location and the enclosing screen bounds. If
     * no transformations are required, then the returned rectangle will
     * have the same values as the parameters.
     *
     * @param px starting x location
     * @param py starting y location
     * @param pw starting width
     * @param ph starting height
     * @return a rectangle which represents the placement and size of the popup
     */
    protected Rectangle computePopupBounds(int px,int py,int pw,int ph) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
            Rectangle screenBounds;

            // Calculate the desktop dimensions relative to the combo box.
            GraphicsConfiguration gc = combo.getGraphicsConfiguration();
            Point p = new Point();
            SwingUtilities.convertPointFromScreen(p, combo);
            if (gc != null) {
            Insets screenInsets = toolkit.getScreenInsets(gc);
            screenBounds = gc.getBounds();
            screenBounds.width -= (screenInsets.left + screenInsets.right);
            screenBounds.height -= (screenInsets.top + screenInsets.bottom);
            screenBounds.x += (p.x + screenInsets.left);
            screenBounds.y += (p.y + screenInsets.top);
        } else {
            screenBounds = new Rectangle(p, toolkit.getScreenSize());
        }
        Rectangle rect = new Rectangle(px,py,pw,ph);
        if (py+ph > screenBounds.y+screenBounds.height && ph < screenBounds.height) {
            rect.y = -rect.height;
        }
        return rect;
    }

    private Point getPopupLocation() {
        Dimension popupSize = combo.getSize();
        Insets insets = getInsets();

        Dimension psz = getPreferredSize();
        // reduce the width of the scrollpane by the insets so that the popup
        // is the same width as the combo box.
        Rectangle popupBounds = computePopupBounds(
                0, combo.getBounds().height, psz.width, psz.height);
        Dimension scrollSize = popupBounds.getSize();
        return popupBounds.getLocation();
    }

    /**
     * Implementation of ComboPopup.hide().
     */
    @SuppressWarnings("deprecated")
    public void hide() {
        MenuSelectionManager manager = MenuSelectionManager.defaultManager();
        MenuElement [] selection = manager.getSelectedPath();
//        for ( int i = 0 ; i < selection.length ; i++ ) {
//            if ( selection[i] == this ) {
                manager.clearSelectedPath();
//                break;
//            }
//        }
        if (selection.length > 0) {
            combo.repaint();
        }
    }

    JList list;
    public JList getList() {
        if (list == null) {
            list = new JList();
        }
        return list;
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return getHandler();
    }

    private Handler getHandler() {
        if (handler == null) {
            handler = new Handler();
        }
        return handler;
    }

    public MouseListener getMouseListener() {
        return getHandler();
    }

    public MouseMotionListener getMouseMotionListener() {
        return getHandler();
    }

    public KeyListener getKeyListener() {
        return null;
    }

    public void uninstallingUI() {
        if (propertyChangeListener != null) {
            combo.removePropertyChangeListener( propertyChangeListener );
        }
//        uninstallComboBoxModelListeners(combo.getModel());
//            uninstallKeyboardActions();
//        uninstallListListeners();
    }

    protected void togglePopup() {
        if (isVisible()) {
            hide();
        } else {
            show();
        }
    }

    private class Handler implements MouseListener, MouseMotionListener,
            PropertyChangeListener {
        //
        // MouseListener
        // NOTE: this is added to both the JList and JComboBox
        //
        public void mouseClicked(MouseEvent e) {
        }

        public void mousePressed(MouseEvent e) {
            if (!SwingUtilities.isLeftMouseButton(e) || !combo.isEnabled()) {
                return;
            }
            if (!combo.isEditable() && combo.isRequestFocusEnabled()) {
                combo.requestFocus();
            }
            togglePopup();
        }

        public void mouseReleased(MouseEvent e) {
//            if (e.getSource() == list) {
//                // JList mouse listener
//                combo.setSelectedIndex( list.getSelectedIndex() );
//                combo.setPopupVisible(false);
//                // workaround for cancelling an edited item (bug 4530953)
//                if (combo.isEditable() && comboBox.getEditor() != null) {
//                    combo.configureEditor(comboBox.getEditor(),
//                                             comboBox.getSelectedItem());
//                }
//                return;
//            }
            // JComboBox mouse listener
            Component source = (Component) e.getSource();
            Dimension size = source.getSize();
            Rectangle bounds = new Rectangle(0, 0, size.width - 1, size.height - 1);
//            if ( !bounds.contains( e.getPoint() ) ) {
//                MouseEvent newEvent = convertMouseEvent( e );
//                Point location = newEvent.getPoint();
//                Rectangle r = new Rectangle();
//                list.computeVisibleRect( r );
//                if ( r.contains( location ) ) {
//                    comboBox.setSelectedIndex( list.getSelectedIndex() );
//                }
//                combo.setPopupVisible(false);
//            }
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        public void mouseMoved(MouseEvent anEvent) {
        }

        public void mouseDragged( MouseEvent e ) {
        }

        //
        // PropertyChangeListener
        //
        public void propertyChange(PropertyChangeEvent e) {
            JComboBox comboBox = (JComboBox)e.getSource();
            String propertyName = e.getPropertyName();

            if ( propertyName == "model" ) {
                ComboBoxModel oldModel = (ComboBoxModel)e.getOldValue();
                ComboBoxModel newModel = (ComboBoxModel)e.getNewValue();
                cb.setColorElements(((ColorComboModel) newModel).getColorElements());
                cb.setColorNames(((ColorComboModel) newModel).getColorElements());
                if ( isVisible() ) {
                    hide();
                }
            } else if ( propertyName == "renderer" ) {
                ; //
            } else if (propertyName == "componentOrientation") {
                ; //
            } else if (propertyName == "lightWeightPopupEnabled") {
                ; //setLightWeightPopupEnabled(comboBox.isLightWeightPopupEnabled());
            }
        }
    }
}
