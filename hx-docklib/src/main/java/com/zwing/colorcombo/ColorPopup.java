package com.zwing.colorcombo;

import com.zwing.ZColorChooserDialog;
import com.zwing.color.ColorElement;
import com.zwing.utils.ZUIManager;
import com.zwing.utils.ZwingUtils;
import com.zwing.utils.ResourceManager;
import com.zwing.colorchooser.ColorBoxes;
import com.zwing.colorchooser.ColorSelectedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created: 19.07.2006 13:13:20
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ColorPopup extends JComponent {
    private JComponent invoker;
    private Color color;

    public ColorPopup(ColorSelectedListener listener, Color color) {
        this.color = color;
        localInit(listener);
    }

    private static final int COLS = 8;
    private static final int BOX_COUNT = 32;
    private static final Dimension BOX_SIZE = new Dimension(22, 18);

    private void localInit(final ColorSelectedListener listener) {
        setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.black),
                        BorderFactory.createLineBorder(Color.white)));
        setLayout(new BorderLayout());

        ColorBoxes cb = new ColorBoxes(COLS, BOX_COUNT, BOX_SIZE);
        ColorElement[] ces =
                (ColorElement[]) ZUIManager.getProperty(ZUIManager.KEY_DEFAULT_COLORS);
        cb.setColorElements(ces);
        cb.setColorNames(ces);
        cb.addColorSelectedListener(listener);
        add(cb, BorderLayout.CENTER);
        add(new JButton(new AbstractAction(ResourceManager.getString("moreColors.label")) {
            public void actionPerformed(ActionEvent e) {
                Object[] rcs = ZColorChooserDialog.showColorChooserDialog(
                        ZwingUtils.findFrame(invoker, null), ResourceManager.getString("selectColor.label"), color);
                if (((Integer)rcs[0]).intValue() == JOptionPane.OK_OPTION) {
                    listener.colorSelected((Color)rcs[1], this);
                }
            }
        }), BorderLayout.SOUTH);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public JComponent getInvoker() {
        return invoker;
    }

    public void setInvoker(JComponent invoker) {
        this.invoker = invoker;
    }
}
