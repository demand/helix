package com.zwing.colorcombo;

import com.zwing.color.ColorElement;
import com.zwing.utils.ZUIManager;
import com.zwing.utils.ResourceManager;

import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.EventListenerList;
import java.awt.Color;
import java.util.List;
import java.util.ArrayList;

/**
 * Created: 27.11.2006 16:07:17
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ColorComboModel implements ComboBoxModel {

    protected EventListenerList listenerList = new EventListenerList();

    /**
     * Таблица содержит N цветов.
     *     0..(N-1) - индекс цвета в таблице (включая цвет "undefined",
     *                если его выбор разрешён)
     *     N        - выбран произвольный цвет, например при помощи
     *                дополнительного диалога
     *     -1       - цвет не выбран
     * Количество доступных и индексируемых элементов:
     *     N    - если запрещён выбор произвольного цвета
     *     N+1  - если разрешён выбор произвольного цвета
     */
    private ColorElement[] colorElements;
    private boolean canSelectDifferent;
    private ColorElement selectedElement;
    private static final String DIFFERENT_COLOR_NAME =
            ResourceManager.getString("different.color.label");

    public ColorComboModel() {
        this(true, true);
    }

    public ColorComboModel(boolean hasUndefinedColor, boolean canSelectDifferent) {
        this.canSelectDifferent = canSelectDifferent;
        selectedElement = ColorElement.getUndefinedColorElement();
        createElements((ColorElement[])
                ZUIManager.getProperty(ZUIManager.KEY_DEFAULT_COLORS), hasUndefinedColor);
    }

    public ColorComboModel(ColorElement[] colorElements) {
        this(colorElements, true, true);
    }

    public ColorComboModel(ColorElement[] colorElements,
            boolean hasUndefinedColor, boolean canSelectDifferent) {
        this.canSelectDifferent = canSelectDifferent;
        selectedElement = ColorElement.getUndefinedColorElement();
        createElements(colorElements, hasUndefinedColor);
    }

    private void createElements(ColorElement[] cs,
            boolean hasUndefinedColor) {
        if (cs == null) {
            colorElements = new ColorElement[0];
        } else {
            List<ColorElement> list = new ArrayList<ColorElement>(cs.length);
            for (ColorElement ce : cs) {
                if (ce != null) {
                    if (hasUndefinedColor || !ce.isUndefined()) {
                        list.add(ce);
                    }
                }
            }
            colorElements = list.toArray(new ColorElement[list.size()]);
        }
    }

    public void setSelectedItem(Object anItem) {
        Color color = (Color) anItem;
        ColorElement found = null;
        for (ColorElement ce : colorElements) {
            if (ce.getColor() == color) {
                found = ce;
                break;
            }
        }
        if (found == null) {
            found = color == null ?
                    ColorElement.getUndefinedColorElement() :
                    new ColorElement(color, DIFFERENT_COLOR_NAME);
        }
        selectedElement = found;
//        fire(this, -1, -1);
    }

    public Object getSelectedItem() {
        return selectedElement.getColor();
    }

    public int getSize() {
        return canSelectDifferent ?
                getTableSize() + 1 : getTableSize();
    }

    public Object getElementAt(int index) {
        return getColorElementAt(index).getColor();
    }

    public void addListDataListener(ListDataListener l) {
        listenerList.add(ListDataListener.class, l);
    }

    public void removeListDataListener(ListDataListener l) {
        listenerList.remove(ListDataListener.class, l);
    }

    public int getTableSize() {
        return colorElements.length;
    }

    public ColorElement getSelectedColorElement() {
        return selectedElement;
    }

    public ColorElement getColorElementAt(int index) {
        ColorElement rslt = null;
        if (index >= 0 && index < colorElements.length) {
            rslt = colorElements[index];
        } else if (canSelectDifferent && index == colorElements.length) {
            rslt = selectedElement;
        } else {
            throw new IllegalArgumentException();
        }
        return rslt;
    }

    public ColorElement[] getColorElements() {
        return colorElements;
    }
}
