package com.zwing;

import com.zwing.utils.ResourceManager;

import javax.swing.*;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;
import java.awt.*;
import java.awt.event.*;

/**
 * ZToolbar
 * Created: 20.05.2009, 12:52:45
 *
 * @author A.Demin (demin@genego.com)
 */
public class ZToolBar extends JPanel
        implements ComponentListener,
        MouseListener, MouseMotionListener, PopupMenuListener {

    public static final int EXPAND_NONE = 0;
    /**
     * количество непоместившихся элементов
     */
    private int expandCount = EXPAND_NONE;

    public static final int EXPAND_BUTTON_SIZE = 9;

    private Rectangle effectiveRect;

    private ImageIcon expandIcon;
    private boolean expandButtonState = false;

    private JToolBar toolBar;
    private JViewport viewport;
    private JPopupMenu jpopup;

    private MouseExitListener mouseExitListener;

    public ZToolBar() {
        init();
    }

    public ZToolBar(int orientation) {
        toolBar = new JToolBar(orientation);
        init();
    }

    public ZToolBar(String name) {
        toolBar = new JToolBar(name);
        init();
    }

    public ZToolBar(String name, int orientation) {
        toolBar = new JToolBar(name, orientation);
        init();
    }

    private void init() {
        setLayout(null);
        viewport = new JViewport();
        expandIcon = ResourceManager.getImageIcon("expand.icon");
        effectiveRect = new Rectangle();

        setBorder(toolBar.getBorder());

        viewport.setView(toolBar);
        super.add(viewport);

        mouseExitListener = new MouseExitListener();

        addComponentListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);

        toolBar.addMouseListener(mouseExitListener);

//        setBackground(Color.GREEN);
//        toolBar.setBackground(Color.PINK);

        viewport.setSize(100, 30);
    }

    @Override
    public Dimension getMinimumSize() {
        Dimension msz = super.getMinimumSize();
        Dimension tbMinSize = toolBar.getPreferredSize();
        if (toolBar.getOrientation() == JToolBar.HORIZONTAL) {
            msz.width = expandIcon.getIconWidth();
            msz.height = Math.max(msz.height, tbMinSize.height);
        } else {
            msz.width = Math.max(msz.width, tbMinSize.width);
            msz.height = expandIcon.getIconHeight();
        }
        return msz;
    }

    @Override
    public Dimension getPreferredSize() {
        return toolBar.getPreferredSize();
    }

    public void setRollover(boolean b) {
        toolBar.setRollover(b);
    }

    @Override
    public Insets getInsets() {
        Insets insets = super.getInsets();
        insets.right = 10;
        return insets;
    }

    private void recalcButtons() {
        Dimension sz = getSize();
        int count = toolBar.getComponentCount();
        if (count == 0) {
            effectiveRect.setBounds(0, 0, sz.width, sz.height);
            expandCount = EXPAND_NONE;
        } else {
            Insets insets = toolBar.getInsets();
            int innerW = Math.max(0, sz.width - insets.left - insets.right);
            int innerH = Math.max(0, sz.height - insets.top - insets.bottom);
            Rectangle inner = new Rectangle(insets.left,
                    insets.top, innerW, innerH);
            int iVisible = getLastVisibleComponentIndex(toolBar, inner);
            if (iVisible == (toolBar.getComponentCount() - 1)) {
                effectiveRect.setBounds(0, 0, sz.width, sz.height);
                expandCount = EXPAND_NONE;
            } else {
                if (toolBar.getOrientation() == JToolBar.HORIZONTAL) {
                    inner.width -= expandIcon.getIconWidth();
                } else {
                    inner.height -= expandIcon.getIconWidth();
                }
                iVisible = getLastVisibleComponentIndex(toolBar, inner);
                if (iVisible >= 0) {
                    Component c = toolBar.getComponent(iVisible);
                    if (toolBar.getOrientation() == JToolBar.HORIZONTAL) {
                        effectiveRect.setBounds(0, 0,
                                c.getX() + c.getWidth(),
                                sz.height);
                    } else {
                        effectiveRect.setBounds(0, 0,
                                sz.width,
                                c.getY() + c.getHeight());
                    }
                } else {
                    effectiveRect.setBounds(0, 0, 0, 0);
                }
            }
            expandCount = toolBar.getComponentCount() - iVisible - 1;
        }


        viewport.setSize(effectiveRect.getSize());
        viewport.setViewSize(effectiveRect.getSize());
        viewport.setExtentSize(effectiveRect.getSize());
        
        setExpBtnState(false);

        validateRoot();
    }

    private void validateRoot() {
        Component c = this;
        while (c.getParent() != null) {
            c = c.getParent();
        }
        c.validate();
        c.repaint();
    }

    private int getLastVisibleComponentIndex(Container container,
            Rectangle vrect) {
        int rslt = -1;
        for (int i = container.getComponentCount() - 1; i >= 0; i--) {
            Component c = container.getComponent(i);
            Rectangle cb = c.getBounds();
            if (vrect.contains(cb)) {
                rslt = i;
                break;
            }
        }
        return rslt;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (expandCount != EXPAND_NONE) {
            paintExpandIcon(g);
            if (expandButtonState) {
                g.setColor(Color.GRAY);
                Rectangle r = getExpandIconBounds();
                g.drawRect(r.x, r.y, r.width - 1, r.height - 1);
            }
        }
    }

    @Override
    protected void paintBorder(Graphics g) {
        super.paintBorder(g);
        if (expandCount != EXPAND_NONE) {
            paintExpandIcon(g);
        }
    }

    private void paintExpandIcon(Graphics g) {
        Rectangle r = getExpandIconBounds();
        expandIcon.paintIcon(this, g, r.x, r.y);
    }

    private Rectangle getExpandIconBounds() {
        Dimension sz = getSize();
        int x, y, width, height;
        if (toolBar.getOrientation() == JToolBar.HORIZONTAL) {
            x = Math.max(0, sz.width - expandIcon.getIconWidth());
            y = Math.max(0, (sz.height - expandIcon.getIconHeight()) / 2);
            width = Math.min(sz.width, expandIcon.getIconWidth());
            height = Math.min(sz.height, expandIcon.getIconHeight());
        } else {
            x = Math.max(0, (sz.width - expandIcon.getIconWidth()) / 2);
            y = Math.max(0, sz.height - expandIcon.getIconHeight());
            width = Math.min(sz.width, expandIcon.getIconWidth());
            height = Math.min(sz.height, expandIcon.getIconHeight());
        }
        return new Rectangle(x, y, width, height);
    }

    @Override
    protected void paintChildren(Graphics g) {
        Shape oldClip = g.getClip();
        g.setClip(effectiveRect);
        super.paintChildren(g);
        g.setClip(oldClip);
    }

    public void componentResized(ComponentEvent e) {
        recalcButtons();
    }

    public void componentMoved(ComponentEvent e) {
        ;
    }

    public void componentShown(ComponentEvent e) {
        ;
    }

    public void componentHidden(ComponentEvent e) {
        Component c = e.getComponent();
        System.out.println("toolbar hidden");
    }

    public void mouseClicked(MouseEvent e) {
        // doExpand();
    }

    private void doExpand() {
        jpopup = new JPopupMenu() {
            @Override
            public Insets getInsets() {
                return new Insets(0, 0, 0, 0);
            }

            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.setColor(Color.LIGHT_GRAY);
                Dimension sz = getSize();
                g.drawRect(0, 0, sz.width-1, sz.height-1);
            }
        };
        jpopup.addComponentListener(this);
        jpopup.addPopupMenuListener(this);
        jpopup.add(toolBar);
        jpopup.show(null, 10, 10);
        repaint();
    }

    private void setExpBtnState(boolean b) {
        expandButtonState = b;
        repaint();
    }

    public void mousePressed(MouseEvent e) {
        ;
    }

    public void mouseReleased(MouseEvent e) {
        ;
    }

    public void mouseEntered(MouseEvent e) {
        ;
    }

    public void mouseExited(MouseEvent e) {
        setExpBtnState(false);
    }

    public void setFloatable(boolean b) {
        toolBar.setFloatable(b);
    }

    public JButton add(Action a) {
        JButton button = toolBar.add(a);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                 closePopup();
            }
        });
        button.addMouseListener(mouseExitListener);
        return button;
    }

    public Component add(Component c) {
        c.addMouseListener(mouseExitListener);
        return toolBar.add(c);
    }

    public void addSeparator() {
        toolBar.addSeparator();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        ;
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        restoreToolbar();
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
        restoreToolbar();
    }

    private void closePopup() {
        if (jpopup != null) {
            jpopup.setVisible(false);
            restoreToolbar();
        }
    }

    private void restoreToolbar() {
        viewport.setView(toolBar);
        doLayout();
        repaint();
        jpopup = null;
        setExpBtnState(false);
    }

    public void mouseDragged(MouseEvent e) {
        ;
    }

    public void mouseMoved(MouseEvent e) {
        testExpButton(e.getPoint());
    }

    private void testExpButton(Point p) {
        if (expandCount > 0 ) {
            Rectangle expandIconBounds = getExpandIconBounds();
            setExpBtnState(expandIconBounds.contains(p));
            if (expandIconBounds.contains(p)) {
                doExpand();
            }
        }
    }

    private void testClosePopup(Component c, Point p) {
        Point pp = new Point(p);
        Component cc = c;
        while (cc != jpopup) {
            Rectangle bounds = cc.getBounds();
            pp.x += bounds.x;
            pp.y += bounds.y;
            cc = cc.getParent();
        }
        Dimension sz = jpopup.getSize();
        if (pp.x < 0 || pp.y < 0 || pp.x >= sz.width || pp.y >= sz.height) {
            closePopup();
        }    
    }

    class MouseExitListener implements MouseListener {
        public void mouseClicked(MouseEvent e) {
            ;
        }

        public void mousePressed(MouseEvent e) {
            ;
        }

        public void mouseReleased(MouseEvent e) {
            ;
        }

        public void mouseEntered(MouseEvent e) {
            ;
        }

        public void mouseExited(MouseEvent e) {
            if (jpopup != null) {
                testClosePopup(e.getComponent(), e.getPoint());
            }
        }
    }
}