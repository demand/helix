package com.zwing;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * @author Andrey Demin
 * @since 17.02.2004
 */
public class ZColorChooserDialog extends JDialog implements ActionListener {
    /** команда кнопки "Ok" */
    private static final String CMD_OK  = "OK";
    /** кнопка "Прекратить" */
    private static final String CMD_CN  = "CN";

    ZColorChooser colorChooser;
    protected int retValue;


    public ZColorChooserDialog(Frame frame, Component parentComp, String title, Color color) {
        super(frame, title, true);
        retValue = JOptionPane.CANCEL_OPTION;
        init();
        colorChooser.setColor(color);
        pack();
        setLocationRelativeTo(parentComp);
        setResizable(false);
    }

    public static final int IV_4 = 4;
    public static final int IV_6 = 6;

    private void init() {
        Container contentPane = getContentPane();
        contentPane.setLayout(new GridBagLayout());
        contentPane.setLayout(new GridBagLayout());

        colorChooser = new ZColorChooser();
        contentPane.add(colorChooser,
                new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE,
                new Insets(IV_6, IV_6, IV_4, IV_6), 0, 0));

        JButton btnOk = new JButton("Ok");
        btnOk.setActionCommand(CMD_OK);
        btnOk.addActionListener(this);
        JButton btnCn = new JButton("Cancel");
        btnCn.setActionCommand(CMD_CN);
        btnCn.addActionListener(this);

        JPanel buttons = new JPanel();
        GridLayout grid = new GridLayout(1, 2);
        grid.setHgap(IV_6);
        buttons.setLayout(grid);
        buttons.add(btnOk);
        buttons.add(btnCn);

        contentPane.add(buttons,
                new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE,
                new Insets(IV_6, IV_6, IV_4, IV_6), 0, 0));
  }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o instanceof JButton) {
            String cmd = e.getActionCommand();
            if (CMD_OK.equals(cmd)) {
                retValue = JOptionPane.OK_OPTION;
                setVisible(false);
            } else if (CMD_CN.equals(cmd)) {
                retValue = JOptionPane.CANCEL_OPTION;
                setVisible(false);
            }
        }
    }

    public static Object[] showColorChooserDialog(
            Component parentComp, String title, Color color) {
        Frame frame = JOptionPane.getFrameForComponent(parentComp);
        ZColorChooserDialog dialog =
                new ZColorChooserDialog(frame, parentComp, title, color);
        dialog.setVisible(true);
        return new Object[]{
                new Integer(dialog.retValue),
                dialog.colorChooser.getColor()};
    }

}
