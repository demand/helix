package com.zwing;

import javax.swing.Action;

/**
 * Created: 09.08.2006 16:29:13
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DefaultDropDownModel implements DropDownModel {
    private Action currentAction;
    private int pressedType = 0;

    DefaultDropDownModel() {
        currentAction = null;
        pressedType = PRESSED_NO;
    }

    public Action getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(Action action) {
        currentAction = action;
    }

    public boolean isPressedArrow() {
        return pressedType == PRESSED_ARROW;
    }

    public boolean isPressedAction() {
        return pressedType == PRESSED_ACTION;
    }

    public void setPressed(int pressedType) {
        if (pressedType >= PRESSED_NO && pressedType <= PRESSED_ACTION) {
            this.pressedType = pressedType;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
