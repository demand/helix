package com.zwing;

import com.zwing.docking.DockingConst;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ZSideBar
 *
 * @author А.Дёмин
 * @since 23.11.2004
 */
public class ZSideBar extends JToolBar implements ActionListener {

    /**
     * Позиция боковой панели. Допустимые значения: <code>DK_WEST</code>, <code>DK_EAST</code>,
     * <code>DK_SOUTH</code>.
     *
     * @see DockingConst#DK_WEST
     * @see DockingConst#DK_EAST
     * @see DockingConst#DK_SOUTH
     */
    protected int location;
    protected int space;
    private Insets insets;

    /**
     * Оконная панель (боковая панель)
     *
     * @param location
     */

    public ZSideBar(int location) {
        this(location, 0);
    }

    public ZSideBar(int location, int space) {
        super();
        this.location = location;
        this.space = space;
        init();
    }

  protected void init() {
    int orientation;
    // посчитать ориентацию (верт./гориз.) в зависимости
    // от положения оконной панели
    if (location == DockingConst.DK_WEST || location == DockingConst.DK_EAST) {
      orientation = JToolBar.VERTICAL;
    } else if (location == DockingConst.DK_NORTH || location == DockingConst.DK_SOUTH) {
      orientation = JToolBar.HORIZONTAL;
    } else {
      throw new IllegalArgumentException();
    }

    insets = new Insets(0, 0, 0, 0);

    setOrientation(orientation);
    setRollover(true);
    setFloatable(false);
    setBorder(BorderFactory.createEmptyBorder());
    setLayout(new ZSidebarLayout(location, space));
  }

    public void captureActionListener(ActionListener a) {
        Component[] childs = getComponents();
        for (int i = 0; i < childs.length; i++) {
            if (childs[i] instanceof ZSideButton) {
                ZSideButton ib = (ZSideButton) childs[i];
                ib.addActionListener(a);
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if (o instanceof ZSideButton) {
            ZSideButton btn = (ZSideButton) o;
            if (btn.isSelected()) {
                swithButtonOn(btn);
            } else {
                switchButtonOff(btn);
            }
        }
    }

    public void swithButtonOn(ZSideButton btn) {
        Component[] childs = getComponents();
        for (int i = 0; i < childs.length; i++) {
            if (childs[i] instanceof ZSideButton) {
                ZSideButton ib = (ZSideButton) childs[i];
                if (ib != btn) {
                    ib.setSelected(false);
                }
            }
        }
        if (!btn.isSelected()) {
            btn.setSelected(true);
        }
        // показать окно для кнопки btn
    }

    public void switchButtonOff(ZSideButton btn) {
        if (btn.isSelected()) {
            btn.setSelected(false);
        }
        // скрыть окно для кнопки btn
    }

    public static boolean isVertical(int location) {
        return
                location == DockingConst.DK_WEST ||
                location == DockingConst.DK_EAST;
    }

    public Insets getInsets() {
        return insets;
    }

    public void setInsets(Insets insets) {
        this.insets = insets;
    }

    public int getDockLocation() {
        return location;
    }

    public void setDockLocation(int loc) {
        location = loc;
    }

    /** */
    public static class ZSidebarLayout implements LayoutManager2 {
        private int orientation;
        private Dimension minSize;
        private Dimension maxSize;
        private Dimension prfSize;

        // расстояние между кнопками
        private int space;

        public ZSidebarLayout(int location) {
            this(location, 0);
        }

        public ZSidebarLayout(int location, int space) {
        this.space = space;

        // посчитать ориентацию (верт./гориз.) в зависимости
        // от положения оконной панели
        if (location == DockingConst.DK_WEST || location == DockingConst.DK_EAST) {
            orientation = JToolBar.VERTICAL;
        } else if (location == DockingConst.DK_NORTH || location == DockingConst.DK_SOUTH) {
            orientation = JToolBar.HORIZONTAL;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void addLayoutComponent(Component comp, Object constraints) {
      // ничего делать не обязательно
    }

    public void addLayoutComponent(String name, Component comp) {
      // ничего делать не обязательно
    }

    public void removeLayoutComponent(Component comp) {
      // ничего делать не обязательно
    }

    public float getLayoutAlignmentX(Container parent) {
        return 0;
    }

    public float getLayoutAlignmentY(Container parent) {
        return 0;
    }

    public synchronized void invalidateLayout(Container parent) {
        minSize = null;
        maxSize = null;
        prfSize = null;
    }


    private boolean needRecalcSizes() {
        return minSize == null || prfSize == null || maxSize == null;
    }

    public synchronized void calcSizes(Container parent) {
        if (needRecalcSizes()) {
            Insets insets = parent.getInsets();
            minSize = new Dimension();
            maxSize = new Dimension();
            prfSize = new Dimension();

            calcSizesInner(parent);

            int wi = insets.left + insets.right;
            int hi = insets.top + insets.bottom;
            minSize.width += wi;
            maxSize.width += wi;
            prfSize.width += wi;
            minSize.height += hi;
            maxSize.height += hi;
            prfSize.height += hi;

            Dimension trMin = parent.getMinimumSize();
            if (minSize.width < trMin.width) {
                minSize.width = trMin.width;
                prfSize.width = trMin.width;
            }
            if (minSize.height < trMin.height) {
                minSize.height = trMin.height;
                prfSize.height = trMin.height;
            }
        }
    }

    private void calcSizesInner(Container parent) {
        int n = parent.getComponentCount();
        if (orientation == JToolBar.HORIZONTAL) {
            for (int i = 0; i < n; i++) {
                Component c = parent.getComponent(i);
                Dimension cMinSize = c.getMinimumSize();
                Dimension cMaxSize = c.getMaximumSize();
                Dimension cPrfSize = c.getPreferredSize();
                minSize.height = Math.max(minSize.height, cMinSize.height);
                maxSize.height = Math.max(maxSize.height, cMaxSize.height);
                prfSize.height = Math.max(prfSize.height, cPrfSize.height);
                minSize.width += space + cMinSize.width;
                maxSize.width += space + cMaxSize.width;
                prfSize.width += space + cPrfSize.width;
            }
            if (n > 0) {
                minSize.width -= space;
                maxSize.width -= space;
                prfSize.width -= space;
            }
        } else {
            for (int i = 0; i < n; i++) {
                Component c = parent.getComponent(i);
                Dimension cMinSize = c.getMinimumSize();
                Dimension cMaxSize = c.getMaximumSize();
                Dimension cPrfSize = c.getPreferredSize();
                minSize.width = Math.max(minSize.width, cMinSize.width);
                maxSize.width = Math.max(maxSize.width, cMaxSize.width);
                prfSize.width = Math.max(prfSize.width, cPrfSize.width);
                minSize.height += space + cMinSize.height;
                maxSize.height += space + cMaxSize.height;
                prfSize.height += space + cPrfSize.height;
            }
            if (n > 0) {
                minSize.height -= space;
                maxSize.height -= space;
                prfSize.height -= space;
            }
        }
    }

    public void layoutContainer(Container parent) {
      int n = parent.getComponentCount();

      Component c;
      Insets insets = parent.getInsets();
      Dimension alloc = parent.getSize();
      int x = insets.left;
      int y = insets.top;
      if (orientation == JToolBar.HORIZONTAL) {
        for (int i = 0; i < n; i++) {
          c = parent.getComponent(i);
          Dimension cSize = c.getPreferredSize();
          c.setBounds(x,
              Math.max(0, (alloc.height - cSize.height) / 2),
              cSize.width, cSize.height);
          x += cSize.width + space;
        }
      } else {
        for (int i = 0; i < n; i++) {
          c = parent.getComponent(i);
          Dimension cSize = c.getPreferredSize();
          c.setBounds(Math.max(0, (alloc.width - cSize.width) / 2),
              y, cSize.width, cSize.height);
          y += cSize.height + space;
        }
      }
    }

    public Dimension minimumLayoutSize(Container parent) {
        calcSizes(parent);
        return minSize;
    }

    public Dimension maximumLayoutSize(Container parent) {
        calcSizes(parent);
        return maxSize;
    }

    public Dimension preferredLayoutSize(Container parent) {
        calcSizes(parent);
        return prfSize;
    }

    public String toString() {
        return "ZSidebarLayout";
    }
  }
}
