package com.zwing;

import com.zwing.colorcombo.ColorComboModel;
import com.zwing.colorcombo.ColorComboRenderer;

import javax.swing.*;

/**
 * ZNestedComboBox
 * Created: 19.10.2009, 15:08:21
 *
 * @author A.Demin (demin@genego.com)
 */
public class ZTreeComboBox extends JComboBox {

    private static final String UI_CLASS_ID = "TreeComboBoxUI";

    public String getUIClassID() {
        return UI_CLASS_ID;
    }

    public ZTreeComboBox() {
        setEditable(false);
  //      model = new ColorComboModel();
    //    setModel(model);
        setRenderer(new ColorComboRenderer());
    }

    public ComboBoxModel getModel() {
//        return model;
        return null;
    }

    public boolean selectWithKeyChar(char keyChar) {
        return false;
    }
}
