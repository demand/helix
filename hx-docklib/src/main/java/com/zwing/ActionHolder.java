package com.zwing;

import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

/**
 * Created: 18.07.2006 19:17:07
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ActionHolder extends AbstractAction implements PropertyChangeListener {
    public static final String PROP_ENABLED = "enabled";

    private Action original;
    private ActionSelectedListener asListener;

    public ActionHolder(Action action, ActionSelectedListener asListener) {
        original = action;
        this.asListener = asListener;
        Object[] keys = (action instanceof AbstractAction) ?
                ((AbstractAction) action).getKeys() :
                new Object[] {Action.NAME, Action.SMALL_ICON,
                        Action.SHORT_DESCRIPTION, Action.LONG_DESCRIPTION,
                        Action.MNEMONIC_KEY, Action.ACCELERATOR_KEY};
        for(Object key : keys) {
            putValue((String) key, original.getValue((String) key));
        }
        setEnabled(action.isEnabled());
        original.addPropertyChangeListener(this);
    }

    public Action getOriginal() {
        return original;
    }

    public void actionPerformed(ActionEvent e) {
        asListener.actionSelected(original);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        final String prop = evt.getPropertyName();
        if (PROP_ENABLED.equals(prop)) {
            setEnabled(original.isEnabled());
        } else {
            putValue(prop, original.getValue(prop));
        }
    }
}
