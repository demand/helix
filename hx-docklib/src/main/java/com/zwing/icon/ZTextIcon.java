package com.zwing.icon;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Created: 04.09.2006 10:42:20
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZTextIcon implements Icon {

    private String text;
    private Font font;
    private Color textColor;
    private Image image;
    private boolean textAntialiasing;
    private Dimension size;
    private int align;

    private static final int FMISIZE = 20;
    private static final BufferedImage FONT_METRIX_IMAGE =
            new BufferedImage(FMISIZE, FMISIZE, BufferedImage.TYPE_4BYTE_ABGR);
    private static final Color DEFAULT_COLOR = Color.BLACK;

    public  ZTextIcon(String text, Font font, Color textColor, boolean textAntialiasing, int align) {
        this.text = text;
        this.font = font;
        this.textColor = textColor;
        this.textAntialiasing = textAntialiasing;
        this.align = align;
        initialize();
    }

    public  ZTextIcon(String text, int fontSizeModificator, Color textColor, boolean textAntialiasing, int align) {
        this.text = text;
        UIDefaults ud = UIManager.getLookAndFeel().getDefaults();
        Font f = ud.getFont("Menu.font");
        if (fontSizeModificator != 0) {
            f = new Font(f.getFontName(), f.getStyle(),
                    Math.max(1, f.getSize() + fontSizeModificator));
        }
        this.font = f;
        this.textColor = textColor;
        this.textAntialiasing = textAntialiasing;
        this.align = align;
        initialize();
    }

    public  ZTextIcon(String text, Font font, Color textColor) {
        this(text, font, textColor, false, SwingConstants.LEFT);
    }

    public  ZTextIcon(String text, int fontSizeModificator, Color textColor) {
        this(text, fontSizeModificator, textColor, false, SwingConstants.LEFT);
    }

    public  ZTextIcon(String text, int fontSizeModificator, boolean textAntialiasing) {
        this(text, fontSizeModificator, DEFAULT_COLOR, textAntialiasing, SwingConstants.LEFT);
    }

    public  ZTextIcon(String text, int fontSizeModificator) {
        this(text, fontSizeModificator, DEFAULT_COLOR);
    }

    public  ZTextIcon(String text, Color textColor) {
        this(text, 0, textColor);
    }

    public  ZTextIcon(String text, Color textColor, boolean textAntialiasing) {
        this(text, 0, textColor, textAntialiasing, SwingConstants.LEFT);
    }

    public  ZTextIcon(String text, boolean textAntialiasing) {
        this(text, 0, DEFAULT_COLOR, textAntialiasing, SwingConstants.LEFT);
    }

    public  ZTextIcon(String text) {
        this(text, 0, DEFAULT_COLOR);
    }

    public ZTextIcon(String text, int fontModifier, boolean textAntialiasing, int align) {
        this(text, fontModifier, DEFAULT_COLOR, textAntialiasing, align);
    }

    private void initialize() {
        Graphics fmg = FONT_METRIX_IMAGE.getGraphics();
        if (textAntialiasing) {
            ((Graphics2D) fmg).setRenderingHint(
                    RenderingHints.KEY_TEXT_ANTIALIASING ,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        FontMetrics fm = fmg.getFontMetrics(font);
        Rectangle2D r = fm.getStringBounds(text, fmg);
        int textWidth = (int) r.getWidth();
        int textHeight = fm.getHeight();

        int w, h;
        if (size == null) {
            w = textWidth;
            h = textHeight;
        } else {
            w = size.width;
            h = size.height;
        }

        image = new BufferedImage(w, h, BufferedImage.TYPE_4BYTE_ABGR);

        Graphics g = image.getGraphics();
        if (textAntialiasing) {
            ((Graphics2D) g).setRenderingHint(
                    RenderingHints.KEY_TEXT_ANTIALIASING ,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        }
        g.setFont(font);
        g.setColor(textColor);

        int x = align == SwingConstants.RIGHT ? w - textWidth :
                align == SwingConstants.CENTER ? (w - textWidth) / 2 :
                        0; // align left

        JLabel lbl = new JLabel(text);
        lbl.setBorder(BorderFactory.createEmptyBorder());
        lbl.setFont(font);
        lbl.setForeground(textColor);
        lbl.setOpaque(false);
        lbl.setBounds(x, getIconHeight() - fm.getDescent(), 200, 10);
//        g.translate(-x, -(getIconHeight() - fm.getDescent()));
        lbl.paint(g);
//        g.translate(x, getIconHeight() - fm.getDescent());

//        g.drawString(text, x, getIconHeight() - fm.getDescent());
    }

    public void setText(String text) {
        this.text = text;
        initialize();
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.drawImage(image, x, y, c);
    }

    public int getIconWidth() {
        return image.getWidth(null);
    }

    public int getIconHeight() {
        return image.getHeight(null);
    }

    public Dimension getSize() {
        return size;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }
}
