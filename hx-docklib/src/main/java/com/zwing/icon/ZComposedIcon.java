package com.zwing.icon;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 04.09.2006 10:42:20
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZComposedIcon implements Icon {

    private Icon baseIcon;
    private Icon coverIcon;

    private Dimension size;

    private Point basePos;
    private Point coverPos;

    private ComposeType composeType;

    public ZComposedIcon(Icon baseIcon, Icon coverIcon, ComposeType composeType) {

        this.baseIcon = baseIcon;
        this.coverIcon = coverIcon;

        basePos = new Point();
        coverPos = new Point();

        if (baseIcon == null && coverIcon == null) {
            throw new NullPointerException("Both source icons are null.");
        }
        Dimension sz = new Dimension();
        if (baseIcon != null) {
            sz.width = baseIcon.getIconWidth();
            sz.height = baseIcon.getIconHeight();
        }
        if (coverIcon != null) {
            sz.width = Math.max(sz.width, coverIcon.getIconWidth());
            sz.height = Math.max(sz.height, coverIcon.getIconHeight());
        }

        setSize(sz);

        this.composeType = composeType;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (baseIcon != null) {
            baseIcon.paintIcon(c, g, basePos.x + x, basePos.y + y);
        }
        
        if (coverIcon != null) {
            coverIcon.paintIcon(c, g, coverPos.x + x, coverPos.y + y);
        }
    }

    public int getIconWidth() {
        return size.width;
    }

    public int getIconHeight() {
        return size.height;
    }

    public Dimension getSize() {
        return new Dimension(size);
    }

    public void setSize(Dimension size) {
        this.size = new Dimension(size);
        recalulatePos();
    }

    private void recalulatePos() {
    }

    /**
     * enums
     */
    public enum ComposeType {
        CENTER,
        NORTH,
        SOUTH,
        WEST,
        EAST,
        NORTHWEST,
        NORTHEAST,
        SOUTHWEST,
        SOUTHEAST
    }
}
