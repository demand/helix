package com.zwing.icon;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 04.09.2006 10:42:20
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZJointIcon implements Icon {
    private Icon baseIcon;
    private Icon jointIcon;
    private int indent;
    private JointType jointType;

    public ZJointIcon(Icon baseIcon, Icon jointIcon, JointType jointType, int indent) {
        this.baseIcon = baseIcon;
        this.jointIcon = jointIcon;
        this.jointType = jointType;
        this.indent = indent;
    }

    public ZJointIcon(Icon baseIcon, Icon jointIcon, JointType jointType) {
        this(baseIcon, jointIcon, jointType, 0);
    }

    public ZJointIcon(Icon baseIcon, Icon jointIcon, int indent) {
        this(baseIcon, jointIcon, JointType.EAST, indent);
    }

    public ZJointIcon(Icon baseIcon, Icon jointIcon) {
        this(baseIcon, jointIcon, JointType.EAST, 0);
    }

    public int getIndent() {
        return indent;
    }

    public void setIndent(int indent) {
        this.indent = indent;
    }

    public Icon getBaseIcon() {
        return baseIcon;
    }

    public void setBaseIcon(Icon baseIcon) {
        this.baseIcon = baseIcon;
    }

    public Icon getJointIcon() {
        return jointIcon;
    }

    public void setJointIcon(Icon jointIcon) {
        this.jointIcon = jointIcon;
    }

    public JointType getJointType() {
        return jointType;
    }

    public void setJointType(JointType jointType) {
        this.jointType = jointType;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (isHorizontal()) {
            int h = getIconHeight();
            int baseY = (h - baseIcon.getIconHeight()) / 2;
            int jointY = (h - jointIcon.getIconHeight()) / 2;
            if (jointType == JointType.EAST) {
                baseIcon.paintIcon(c, g, x, y+baseY);
                jointIcon.paintIcon(c, g, x+baseIcon.getIconWidth() + indent, y+jointY);
            } else {
                jointIcon.paintIcon(c, g, x, y+jointY);
                baseIcon.paintIcon(c, g, x+jointIcon.getIconWidth() + indent, y+baseY);
            }
        } else {
            int w = getIconWidth();
            int baseX = (w - baseIcon.getIconWidth()) / 2;
            int jointX = (w - jointIcon.getIconWidth()) / 2;
            if (jointType == JointType.SOUTH) {
                baseIcon.paintIcon(c, g, x+baseX, y);
                jointIcon.paintIcon(c, g, x+jointX, y+baseIcon.getIconHeight() + indent);
            } else {
                jointIcon.paintIcon(c, g, x+jointX, y);
                baseIcon.paintIcon(c, g, x+baseX, y+jointIcon.getIconHeight() + indent);
            }
        }
    }

    public int getIconWidth() {
        return isHorizontal() ?
                (baseIcon.getIconWidth() + jointIcon.getIconWidth() + indent) :
                (Math.max(baseIcon.getIconWidth(), jointIcon.getIconWidth()));
    }

    public int getIconHeight() {
        return isHorizontal() ?
                (Math.max(baseIcon.getIconHeight(), jointIcon.getIconHeight())) :
                (baseIcon.getIconHeight() + jointIcon.getIconHeight() + indent);
    }

    private boolean isHorizontal() {
        return jointType == JointType.WEST || jointType == JointType.EAST;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ZJointIcon that = (ZJointIcon) o;

        if (indent != that.indent) {
            return false;
        }
        if (baseIcon != null ? !baseIcon.equals(that.baseIcon) : that.baseIcon != null) {
            return false;
        }
        if (jointIcon != null ? !jointIcon.equals(that.jointIcon) : that.jointIcon != null) {
            return false;
        }
        if (jointType != that.jointType) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        return super.hashCode();
    }

    /**
     * enums
     */
    public enum JointType {
        WEST,
        EAST,
        NORTH,
        SOUTH
    }
}
