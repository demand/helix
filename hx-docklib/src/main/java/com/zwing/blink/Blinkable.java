package com.zwing.blink;

/**
 * Blinkable
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Blinkable {
    public void blinkPhaseOn();
    public void blinkPhaseOff();
    public void blinkFinished();
}
