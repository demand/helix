package com.zwing.blink;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Blinker
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Blinker {

    public static final int BLINK_INTERVAL = 400;
    public static final int FOREVER = -100;

    private BlinkEngine blinkEngine;

    public Blinker() {
        blinkEngine = new BlinkEngine();
        new Thread(blinkEngine).start();
    }

    static class BlinkItem implements Comparable {
        private Blinkable component;
        private int count;

        BlinkItem(Blinkable component, int count) {
            this.component = component;
            this.count = count;
        }

        public boolean equals(Object obj) {
	        return this == obj || compareTo(obj) == 0;
        }

        public int hashCode() {
            return super.hashCode();
        }

        public int compareTo(Object o) {
            Blinkable co = ((BlinkItem) o).component;
            if (co == null && component == null) {
                return 0;
            }
            int ht = component.hashCode();
            int ho = co.hashCode();
            return
                    ht < ho ? -1 :
                    ht > ho ? 1 : 0;
        }
    }

    static class BlinkEngine implements Runnable {
        private ArrayList<BlinkItem> blinkComponents; // BlinkItem*
        private ArrayList<BlinkItem> toRemove;        // BlinkItem*
        private boolean blinkPhase;        // false - off, true - on
        private boolean blinkActive;       // true - active, false - to be terminated

        BlinkEngine() {
            blinkPhase = false;
            blinkActive = true;
            blinkComponents = new ArrayList<BlinkItem>();
            toRemove = new ArrayList<BlinkItem>();
        }

        public void run() {
            while (isActive() || blinkPhase) {
                try {
                    Thread.sleep(BLINK_INTERVAL);
                } catch (InterruptedException e) {
                    ; // do nothing
                }
                int i = 0;
                while (i < blinkComponents.size()) {
                    BlinkItem b = blinkComponents.get(i);
                    if (blinkPhase) {
                        // выключить мигание компонента
                        b.component.blinkPhaseOff();
                        i++;
                    } else {
                        if (b.count > 0 || b.count == FOREVER) {
                            b.component.blinkPhaseOn();
                            if (b.count > 0) {
                                b.count--;
                            }
                            i++;
                        } else {
                            blinkComponents.remove(i);
                        }
                    }
                }
                if (blinkPhase) {
                    removeArray();
                }
                blinkPhase = !blinkPhase;
            }
        }

        public synchronized void add(Blinkable b, int count) {
            BlinkItem newItem = new BlinkItem(b, count);
            int oldItemNo = blinkComponents.indexOf(newItem);
            if (oldItemNo < 0) {
                blinkComponents.add(newItem);
            } else {
                (blinkComponents.get(oldItemNo)).count = count;
            }
        }

        private synchronized void removeArray() {
            Iterator<BlinkItem> itr = toRemove.iterator();
            while (itr.hasNext()) {
                BlinkItem b = itr.next();
                if (blinkComponents.contains(b)) {
                    ; // do nothing
                }
                b.component.blinkFinished();
                blinkComponents.remove(b);
            }
            toRemove.clear();
        }

        public synchronized void remove(Blinkable b) {
            toRemove.add(new BlinkItem(b, 0));
        }

        public synchronized void requestStop(){
            blinkActive = false;
        }

        private synchronized boolean isActive() {
            return blinkActive;
        }
    }

    public void add(Blinkable b, int count) {
        blinkEngine.add(b, count);
    }

    public void remove(Blinkable b) {
        blinkEngine.remove(b);
    }

    public void requestStop() {
        blinkEngine.requestStop();
    }

    private synchronized boolean isActive() {
        return blinkEngine.isActive();
    }
}
