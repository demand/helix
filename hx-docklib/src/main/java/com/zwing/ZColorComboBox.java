package com.zwing;

import com.zwing.colorchooser.ColorSelectedListener;
import com.zwing.colorcombo.ColorComboModel;
import com.zwing.colorcombo.ColorComboRenderer;

import javax.swing.JComboBox;
import javax.swing.ComboBoxModel;
import java.awt.Color;

/**
 * Created: 26.10.2006 16:07:19
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZColorComboBox extends JComboBox implements ColorSelectedListener {

    private static final String UI_CLASS_ID = "ColorComboBoxUI";
    protected ComboBoxModel model;

    public String getUIClassID() {
        return UI_CLASS_ID;
    }

    public ZColorComboBox() {
        setEditable(false);
        model = new ColorComboModel();
        setModel(model);
        setRenderer(new ColorComboRenderer());
    }

    public ComboBoxModel getModel() {
        return model;
    }

    public boolean selectWithKeyChar(char keyChar) {
        return false;
    }

    public Color getSelectedColor() {
        return (Color) model.getSelectedItem();
    }

    public void setSelectedColor(Color selectedColor) {
        model.setSelectedItem(selectedColor);
    }

    public Color colorSelected(Color newColor, Object source) {
        model.setSelectedItem(newColor);
        hidePopup();
        repaint();
        return null;
    }
}
