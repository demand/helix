package com.zwing;

import javax.swing.Action;

/**
 * Created: 18.07.2006 19:44:10
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ActionSelectedListener {
    public void actionSelected(Action action);
}
