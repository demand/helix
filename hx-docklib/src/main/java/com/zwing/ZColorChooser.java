package com.zwing;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;

import com.zwing.colorchooser.*;
import com.zwing.color.ColorElement;
import com.zwing.utils.ZUIManager;
import com.zwing.utils.ResourceManager;
import com.zwing.utils.ZwingUtils;

/**
 * @author Andrey Demin
 * @since 09.02.2004
 */
public class ZColorChooser extends JPanel
    implements ActionListener, ColorSelectedListener {

    public static final int FIXED_HUE = 1;
    public static final int FIXED_SAT = 2;
    public static final int FIXED_BRI = 3;
    public static final int FIXED_RED = 4;
    public static final int FIXED_GRE = 5;
    public static final int FIXED_BLU = 6;

    public static final int DEFAULT_BORDER_SIZE = 3;
    public int borderSize = DEFAULT_BORDER_SIZE;

    public static final int DEFAULT_SELECTED_SIZE = 1;
    public int selectedSize = DEFAULT_SELECTED_SIZE;

    public static final Point DEFAULT_PICK_IMG_POS = new Point(0, 0);
    public Point pickImgPos = DEFAULT_PICK_IMG_POS;
    public static final Dimension DEFAULT_PICK_IMG_SIZE = new Dimension(15, 15);
    public Dimension pickImgSize = DEFAULT_PICK_IMG_SIZE;

    public static final Point DEFAULT_SLIDER_IMG_POS = new Point(15, 0);
    public Point sliderImgPos = DEFAULT_SLIDER_IMG_POS;
    public static final Dimension DEFAULT_SLIDER_IMG_SIZE = new Dimension(32, 9);
    public Dimension sliderImgSize = DEFAULT_SLIDER_IMG_SIZE;
    public static final int DEFAULT_SLIDER_ACTIVE_X = 15;
    public int sliderActiveX = DEFAULT_SLIDER_ACTIVE_X;
    public static final int DEFAULT_SLIDER_ACTIVE_W = 12;
    public int sliderActiveW = DEFAULT_SLIDER_ACTIVE_W;

    public static final Point DEFAULT_FIXED_IMG_POS = new Point(15, 20);
    public Point fixedImgPos = DEFAULT_FIXED_IMG_POS;
    public static final Dimension DEFAULT_FIXED_IMG_SIZE = new Dimension(19, 22);
    public Dimension fixedImgSize = DEFAULT_FIXED_IMG_SIZE;

    public static final int DEFAULT_FIXED_INNER_WIDTH = 15;
    public int fixedInnerWidth = DEFAULT_FIXED_INNER_WIDTH;

    public static final Color DEFAULT_BACKGROUND_COLOR = Color.lightGray;
    public static final Color DEFAULT_LIGHT_COLOR = Color.white;
    public static final Color DEFAULT_DARK_COLOR = new Color(136, 136, 136);
    public static final Color DEFAULT_DARKER_COLOR = new Color(90, 90, 90);
    public static final Color DEFAULT_DARKEST_COLOR = Color.black;
    public static final Color DEFAULT_SELECT_COLOR = DEFAULT_DARKEST_COLOR;

    public Color backgroundColor = DEFAULT_BACKGROUND_COLOR;
    public Color lightColor = DEFAULT_LIGHT_COLOR;
    public Color darkColor = DEFAULT_DARK_COLOR;
    public Color darkestColor = DEFAULT_DARKEST_COLOR;
    public Color darkerColor = DEFAULT_DARKER_COLOR;
    public Color selectColor = DEFAULT_SELECT_COLOR;

    public static final int IMAX_COLOR_PART = 255;
    public static final float FMAX_COLOR_PART = 255.0f;

    protected Border darkBorder;
    protected Border selectedBorder;

    protected int fixedType;

    protected boolean firstPaint = true;

    protected Color color;

    protected ColorSlider cs;
    protected ColorPalette cp;
    protected ColorBoxes cb;
    protected ColorPreview preview;
    protected JPanel pnLine;

    protected ArrayList colorChangeListeners;

    JToggleButton btHue;
    JToggleButton btSat;
    JToggleButton btBri;
    JToggleButton btRed;
    JToggleButton btGre;
    JToggleButton btBlu;
    JButton btNotUsed;
    JTextField txHue;
    JTextField txSat;
    JTextField txBri;
    JTextField txRed;
    JTextField txGre;
    JTextField txBlu;

    public static final String CMD_NOTUSED = "NUS";

    protected int lastAction = JOptionPane.CANCEL_OPTION;

    public ZColorChooser() {
        super();
        colorChangeListeners = new ArrayList();
        init();
    }

    public static final int COLORBOXES_COLS = 16;
    public static final int COLORBOXES_COUNT = 29;
    public static final Dimension COLORBOXES_SIZE = new Dimension(22, 22);
    public static final Dimension ICON_NOTUSED_SIZE = new Dimension(24, 12);
    public static final Dimension TEXTFIELD_SIZE = new Dimension(30, 10);

    public static final int IV_3 = 3;
    public static final int IV_4 = 4;
    public static final int IV_5 = 5;
    public static final int IV_6 = 6;
    public static final int IV_7 = 7;
    public static final int IV_8 = 8;
    public static final int IV_11 = 11;
    public static final int IV_12 = 12;
    public static final int IV_16 = 16;
    public static final float FV_4 = 4.0f;
    public static final float FV_6 = 6.0f;
    public static final float FV_10 = 10.0f;
    public static final float FV_360 = 360.0f;
    public static final float FV_100 = 100.0f;

    protected void init() {
        setLayout(new GridBagLayout());

        darkBorder = BorderFactory.createLineBorder(darkColor);
        selectedBorder = BorderFactory.createLineBorder(selectColor);

        cp = new ColorPalette(this);
        cs = new ColorSlider(this);
        cb = new ColorBoxes(COLORBOXES_COLS, COLORBOXES_COUNT, COLORBOXES_SIZE);
        ColorElement[] ces = (ColorElement[]) ZUIManager.getProperty(
                ZUIManager.KEY_DEFAULT_COLORS);
        cb.setColorElements(ces);
        cb.setColorNames(ces);

        int ws = borderSize + selectedSize;

        JPanel ccp = createColorPalettePanel(ws, cp);

        add(ccp, new GridBagConstraints(0, 0, 1, IV_8, 0.0, FV_6,
                GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
        add(cs, new GridBagConstraints(1, 0, 1, IV_8, 0.0, FV_6,
                GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, IV_11, 0, 0), 0, 0));

        pnLine = new JPanel() {
            public void paint(Graphics g) {
                super.paint(g);
                drawLineFixed(g);
            }
        };

        add(pnLine, new GridBagConstraints(2, 0, 1, IV_8, FV_10, FV_6,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));

        createValueItems();

        preview = new ColorPreview(this);
        add(preview, new GridBagConstraints(IV_3, IV_6, 2, 1, 1.0, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(IV_6, 0, 0, 0), 0, 0));

        btNotUsed = new JButton(new ColorIcon(ICON_NOTUSED_SIZE, null));
        btNotUsed.setToolTipText(ResourceManager.getString("undefinedColor.tooltip"));
        btNotUsed.setActionCommand(CMD_NOTUSED);
        btNotUsed.addActionListener(this);

        add(btNotUsed, new GridBagConstraints(IV_3, IV_7, 2, 1, 1.0, FV_4,
                GridBagConstraints.SOUTHEAST, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));

        add(cb, new GridBagConstraints(0, IV_8, IV_5, 1, FV_10, 1.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(IV_6, 0, IV_6, 0), 0, 0));

        btHue.setSelected(true);
        setFixedType(FIXED_HUE);
        setFixedButton(btHue);
        adjustPanels();

        cb.addColorSelectedListener(this);
    }

    private JPanel createColorPalettePanel(int ws, JComponent inner) {
        JPanel rslt = new JPanel() {
            public void paint(Graphics g) {
                super.paint(g);
                drawBorder(g, new Rectangle(
                        selectedSize, selectedSize,
                        ColorPalette.SIZE.width + borderSize * 2,
                        ColorPalette.SIZE.height + borderSize * 2));
            }
        };
        rslt.setBorder(BorderFactory.createEmptyBorder(ws, ws, ws, ws));
        rslt.setLayout(new BorderLayout());
        rslt.add(inner, BorderLayout.CENTER);
        return rslt;
    }

    protected JPanel createValueItems() {
        String names[] = {"H", "S", "B", "R", "G", "B"};
        String meas[] = {"\u00B0", "%", "%", "", "", ""};
        int inset[] = {IV_6, IV_6, IV_12, IV_6, IV_6, IV_6};

        JPanel pn = this;
//        pn.setLayout(new GridBagLayout());
        ButtonGroup group = new ButtonGroup();

        Object[] items;
        int i = 0;

        items = createCurrentValueItem(pn, group, names[i], meas[i], inset[i], i++);
        btHue = (JToggleButton) items[0];
        txHue = (JTextField) items[1];

        items = createCurrentValueItem(pn, group, names[i], meas[i], inset[i], i++);
        btSat = (JToggleButton) items[0];
        txSat = (JTextField) items[1];

        items = createCurrentValueItem(pn, group, names[i], meas[i], inset[i], i++);
        btBri = (JToggleButton) items[0];
        txBri = (JTextField) items[1];

        items = createCurrentValueItem(pn, group, names[i], meas[i], inset[i], i++);
        btRed = (JToggleButton) items[0];
        txRed = (JTextField) items[1];

        items = createCurrentValueItem(pn, group, names[i], meas[i], inset[i], i++);
        btGre = (JToggleButton) items[0];
        txGre = (JTextField) items[1];

        items = createCurrentValueItem(pn, group, names[i], meas[i], inset[i], i++);
        btBlu = (JToggleButton) items[0];
        txBlu = (JTextField) items[1];

        return pn;
    }

    // returns:
    // 0 - JToggleButton, 1 - JTextField
    protected Object[] createCurrentValueItem(JPanel pn, ButtonGroup group,
            String name, String meas, int inset, int row) {
        JToggleButton bt = new JToggleButton(name) {
            protected void paintComponent(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
                super.paintComponent(g);
            }
        };

        bt.setActionCommand("CH");
        bt.addActionListener(this);
        bt.setBorder(BorderFactory.createLineBorder(darkerColor));
        bt.setFocusable(false);
        group.add(bt);
        pn.add(bt, new GridBagConstraints(
                IV_3, row, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, inset, 0), 0, 0));

        JTextField tx = new JTextField();
        tx.setColumns(IV_4);
        tx.setMinimumSize(TEXTFIELD_SIZE);
        pn.add(tx, new GridBagConstraints(
                IV_4, row, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, IV_6, inset, 0), 0, 0));

        JLabel ms = new JLabel(meas) {
            protected void paintComponent(Graphics g) {
                Graphics2D g2 = (Graphics2D) g;
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
                super.paintComponent(g);
            }
        };
        pn.add(ms,  new GridBagConstraints(
                IV_5, row, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 2, inset, 0), 0, 0));

        return new Object[]{bt, tx};
    }

    public void repaintLineFixed() {
        pnLine.repaint();
    }

    protected void drawLineFixed(Graphics g) {
        JToggleButton bt =
                fixedType == FIXED_HUE ? btHue :
                fixedType == FIXED_SAT ? btSat :
                fixedType == FIXED_BRI ? btBri :
                fixedType == FIXED_RED ? btRed :
                fixedType == FIXED_GRE ? btGre :
                fixedType == FIXED_BLU ? btBlu :
                null;
        if (bt != null) {
            int x0 = cs.getBounds().x + cs.getBounds().width - pnLine.getBounds().x;
            int y0 = (IMAX_COLOR_PART - cs.getFixedValue()) + cs.getBounds().y +
            borderSize + selectedSize - pnLine.getBounds().y;
            int xs = (cs.getBounds().x + cs.COLOR_WIDTH +
                    (selectedSize + borderSize) * 2 + bt.getBounds().x - 1) / 2 -
                    pnLine.getBounds().x;
            int x1 = bt.getBounds().x - IV_6 - pnLine.getBounds().x;
            int y1 = bt.getBounds().y + bt.getBounds().width / 2 - pnLine.getBounds().y;
            g.setColor(darkerColor);
            g.drawLine(
                    bt.getBounds().x - 2 - pnLine.getBounds().x,
                    bt.getBounds().y - pnLine.getBounds().y,
                    bt.getBounds().x - 2 - pnLine.getBounds().x,
                    bt.getBounds().y + bt.getBounds().width - 1 - pnLine.getBounds().y);
            g.setColor(darkColor);
            g.drawLine(
                    bt.getBounds().x - IV_4 - pnLine.getBounds().x,
                    bt.getBounds().y - pnLine.getBounds().y,
                    bt.getBounds().x - IV_4 - pnLine.getBounds().x,
                    bt.getBounds().y + bt.getBounds().width - 1 - pnLine.getBounds().y);
            g.drawLine(x0, y0, xs, y0);
            g.drawLine(xs, y0, xs, y1);
            g.drawLine(xs, y1, x1, y1);
        }
    }

    protected void doFirstPaint() {
        ImageIcon im = ResourceManager.getImageIcon("colorchooser.images");
        Image im2 = createImage(ColorSlider.SLIDER_WIDTH, ColorSlider.SLIDER_HEIGHT);
        Graphics g = im2.getGraphics();
        // make initial image transparent
        g.setColor(Color.magenta);
        g.fillRect(0, 0, ColorSlider.SLIDER_WIDTH, ColorSlider.SLIDER_HEIGHT);
        im2 = ZwingUtils.makeColorTransparent(im2, Color.magenta);

        g.drawImage(im.getImage(), 0, 0,
                ColorSlider.SLIDER_WIDTH,
                ColorSlider.SLIDER_HEIGHT,
                DEFAULT_SLIDER_IMG_POS.x,
                DEFAULT_SLIDER_IMG_POS.y,
                DEFAULT_SLIDER_IMG_POS.x + ColorSlider.SLIDER_WIDTH,
                DEFAULT_SLIDER_IMG_POS.y + ColorSlider.SLIDER_HEIGHT,
                this);
        cs.setImgSlider(im2);

        int ww = txHue.getSize().height;
        btHue.setMinimumSize(new Dimension(ww, ww));
        btHue.setMaximumSize(new Dimension(ww, ww));
        btHue.setPreferredSize(new Dimension(ww, ww));

        adjustPanels();
        calcColor();

        invalidate();
        validate();
    }

    public void notifyListeners(Color c) {
        Iterator itr = colorChangeListeners.iterator();
        while (itr.hasNext()) {
            ((ColorChangeListener) itr.next()).colorChanged(c);
        }
    }

    public void paint(Graphics g) {
        if (firstPaint) {
            doFirstPaint();
            super.paint(g);
            firstPaint = false;
        } else {
            super.paint(g);
        }
    }

    public void drawBorder(Graphics g, Rectangle rect) {
        g.setColor(darkColor);
        g.drawRect(rect.x, rect.y, rect.width - 1, rect.height - 1);
        g.setColor(lightColor);
        g.drawRect(rect.x + 1, rect.y + 1, rect.width - IV_3, rect.height - IV_3);
        g.setColor(darkestColor);
        g.drawRect(rect.x + 2, rect.y + 2, rect.width - IV_5, rect.height - IV_5);
    }

    public void drawHSplitter(Graphics g, Rectangle rect) {
        int y2 = (rect.y + rect.height / 2);
        g.setColor(darkestColor);
        g.fillRect(rect.x, rect.y, rect.width, y2 - rect.y);
        g.setColor(darkerColor);
        g.fillRect(rect.x, y2, rect.width, rect.y + rect.width - y2);
    }

    public void setFixedButton(JToggleButton bt) {
        btHue.setBorder(darkBorder);
        btSat.setBorder(darkBorder);
        btBri.setBorder(darkBorder);
        btRed.setBorder(darkBorder);
        btGre.setBorder(darkBorder);
        btBlu.setBorder(darkBorder);
        bt.setBorder(selectedBorder);

        Color c = UIManager.getColor("Button.background");
        btHue.setBackground(c);
        btSat.setBackground(c);
        btBri.setBackground(c);
        btRed.setBackground(c);
        btGre.setBackground(c);
        btBlu.setBackground(c);
        bt.setBackground(darkColor);

        pnLine.repaint();
    }

    public int getFixedType() {
        return fixedType;
    }

    public void setFixedType(int t) {
        fixedType = t;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public ColorSlider getColorSlider() {
        return cs;
    }

    public ColorPalette getColorPalette() {
        return cp;
    }

    public ColorBoxes getColorBoxes() {
        return cb;
    }

    public void adjustPanels() {
        Color c = color != null ? color : Color.BLACK;
        int red = c.getRed();
        int gre = c.getGreen();
        int blu = c.getBlue();
        Point p = new Point();
        int f = 0;
        float[] hsb = new float[3];
        switch (fixedType) {
            case FIXED_HUE:
                Color.RGBtoHSB(red, gre, blu, hsb);
                p.x = (int) (hsb[1] * FMAX_COLOR_PART);
                p.y = IMAX_COLOR_PART - (int) (hsb[2] * FMAX_COLOR_PART);
                f = (int) (hsb[0] * FMAX_COLOR_PART);
                break;
            case FIXED_SAT:
                Color.RGBtoHSB(red, gre, blu, hsb);
                p.x = (int) (hsb[0] * FMAX_COLOR_PART);
                p.y = IMAX_COLOR_PART - (int) (hsb[2] * FMAX_COLOR_PART);
                f = (int) (hsb[1] * FMAX_COLOR_PART);
                break;
            case FIXED_BRI:
                Color.RGBtoHSB(red, gre, blu, hsb);
                p.x = (int) (hsb[0] * FMAX_COLOR_PART);
                p.y = IMAX_COLOR_PART - (int) (hsb[1] * FMAX_COLOR_PART);
                f = (int) (hsb[2] * FMAX_COLOR_PART);
                break;
            case FIXED_RED:
                p.x = blu;
                p.y = IMAX_COLOR_PART - gre;
                f = red;
                break;
            case FIXED_GRE:
                p.x = blu;
                p.y = IMAX_COLOR_PART - red;
                f = gre;
                break;
            case FIXED_BLU:
                p.x = red;
                p.y = IMAX_COLOR_PART - gre;
                f = blu;
                break;
            default:
        }
        cp.setValue(new Point(p));
        cs.setFixedValue(f);
        cp.reqRepaint();
        cp.updateColorSlider();
        repaintLineFixed();
    }

    public static final int MASK_FF = 0xFF;

    public void calcColor() {
        float[] thsb = new float[] {0.0f, 0.0f, 0.0f};
        int[] trgb = new int[] {0, 0, 0};
        int x = cp.getValue().x;
        int y = IMAX_COLOR_PART - cp.getValue().y;
        int f = cs.getFixedValue();
        boolean isHsb = fixedType == FIXED_HUE ||
                fixedType == FIXED_SAT || fixedType == FIXED_BRI;
        if (isHsb) {
            thsb = calcColorHSB(x, y, f);
        } else {
            trgb = calcColorRGB(x, y, f);
        }

        if (isHsb) {
            int c = Color.HSBtoRGB(thsb[0], thsb[1], thsb[2]);
            trgb[0] = (c >> IV_16) & MASK_FF;
            trgb[1] = (c >> IV_8) & MASK_FF;
            trgb[2] = c & MASK_FF;
        } else {
            Color.RGBtoHSB(trgb[0], trgb[1], trgb[2], thsb);
        }
        color = new Color(trgb[0], trgb[1], trgb[2]);
        preview.colorChanged(color);

        adjustTextValues(trgb[0], trgb[1], trgb[2], ((int) (thsb[0] * FV_360 + 0.5f)),
                ((int) (thsb[1] * FV_100 + 0.5f)), ((int) (thsb[2] * FV_100 + 0.5f)));
    }

    private int[] calcColorRGB(int x, int y, int f) {
        int[] rslt = new int[] {0, 0, 0};
        switch (fixedType) {
            case FIXED_RED:
                rslt[0] = f;
                rslt[1] = y;
                rslt[2] = x;
                break;
            case FIXED_GRE:
                rslt[0] = y;
                rslt[1] = f;
                rslt[2] = x;
                break;
            case FIXED_BLU:
                rslt[0] = x;
                rslt[1] = y;
                rslt[2] = f;
                break;
            default:
        }
        return rslt;
    }

    private float[] calcColorHSB(int x, int y, int f) {
        float[] rslt = new float[] {0.0f, 0.0f, 0.0f};
        switch (fixedType) {
            case FIXED_HUE:
                rslt[0] = ((float) f) / FMAX_COLOR_PART;
                rslt[1] = ((float) x) / FMAX_COLOR_PART;
                rslt[2] = ((float) y) / FMAX_COLOR_PART;
                break;
            case FIXED_SAT:
                rslt[0] = ((float) x) / FMAX_COLOR_PART;
                rslt[1] = ((float) f) / FMAX_COLOR_PART;
                rslt[2] = ((float) y) / FMAX_COLOR_PART;
                break;
            case FIXED_BRI:
                rslt[0] = ((float) x) / FMAX_COLOR_PART;
                rslt[1] = ((float) y) / FMAX_COLOR_PART;
                rslt[2] = ((float) f) / FMAX_COLOR_PART;
                break;
            default:
        }
        return rslt;
    }

    public void adjustTextValues(int red, int gre, int blu, int hue, int sat, int bri) {
        if (color != null) {
            txRed.setText(Integer.toString(red));
            txGre.setText(Integer.toString(gre));
            txBlu.setText(Integer.toString(blu));

            txHue.setText(Integer.toString(hue));
            txSat.setText(Integer.toString(sat));
            txBri.setText(Integer.toString(bri));
        } else {
            txRed.setText("");
            txGre.setText("");
            txBlu.setText("");

            txHue.setText("");
            txSat.setText("");
            txBri.setText("");
        }
    }

    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        String cmd = e.getActionCommand();
        if (o instanceof JToggleButton) {
            JToggleButton bt = (JToggleButton) o;
            if ("CH".equals(cmd)) {
                setFixedButton(bt);
                if (bt == btHue) {
                    setFixedType(FIXED_HUE);
                    adjustPanels();
                } else if (bt == btSat) {
                    setFixedType(FIXED_SAT);
                    adjustPanels();
                } else if (bt == btBri) {
                    setFixedType(FIXED_BRI);
                    adjustPanels();
                } else if (bt == btRed) {
                    setFixedType(FIXED_RED);
                    adjustPanels();
                } else if (bt == btGre) {
                    setFixedType(FIXED_GRE);
                    adjustPanels();
                } else if (bt == btBlu) {
                    setFixedType(FIXED_BLU);
                    adjustPanels();
                }
            }
        } else if (CMD_NOTUSED.equals(cmd)) {
            color = null;
            adjustPanels();
            adjustTextValues(0, 0, 0, 0, 0, 0);
            preview.colorChanged(color);
        }
    }

    public Color colorSelected(Color newColor, Object source) {
        setColor(newColor);
        adjustPanels();
        calcColor();
        return null;
    }
}
