package com.zwing;

import javax.swing.Action;

/**
 * Created: 09.08.2006 16:26:23
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface DropDownModel {
    static final int PRESSED_NO     = 0;
    static final int PRESSED_ARROW  = 1;
    static final int PRESSED_ACTION = 2;

    Action getCurrentAction();
    boolean isPressedArrow();
    boolean isPressedAction();
    void setPressed(int pressedType);
}
