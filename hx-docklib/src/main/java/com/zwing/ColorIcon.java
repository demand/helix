package com.zwing;

import javax.swing.Icon;
import java.awt.*;

/**
 * @author Andrey Demin
 * @since 08.01.2004
 */
public class ColorIcon implements Icon {
    protected Color color;
    protected Dimension size;
    protected ColorRectangle rc;


    public ColorIcon(Dimension size, Color color) {
        rc = new ColorRectangle(size, color);
    }

    public ColorIcon(Dimension size) {
        this(size, null);
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        rc.setPos(new Point(x, y));
        rc.paint(g);
    }

    public int getIconWidth() {
        return rc.getSize().width;
    }

    public int getIconHeight() {
        return rc.getSize().height;
    }

    public void setColor(Color color) {
        rc.setColor(color);
    }

    public Color getColor() {
        return rc.getColor();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ColorIcon colorIcon = (ColorIcon) o;

        if (color != null ? !color.equals(colorIcon.color) : colorIcon.color != null) {
            return false;
        }
        if (rc != null ? !rc.equals(colorIcon.rc) : colorIcon.rc != null) {
            return false;
        }
        if (size != null ? !size.equals(colorIcon.size) : colorIcon.size != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        return super.hashCode();
    }
}
