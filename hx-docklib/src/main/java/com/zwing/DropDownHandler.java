package com.zwing;

import javax.swing.SwingUtilities;
import javax.swing.AbstractButton;
import javax.swing.plaf.basic.BasicButtonListener;
import java.awt.event.MouseEvent;

import com.zwing.ui.basic.DropDownButtonBasicUI;
import com.zwing.ui.jgoodies.DropDownButtonUI;

/**
 * Created: 11.08.2006 12:06:59
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DropDownHandler extends BasicButtonListener {

    private BasicButtonListener basicListener;

    public DropDownHandler(AbstractButton b, BasicButtonListener basicListener) {
        super(b);
        this.basicListener = basicListener;
    }

    public void mousePressed(MouseEvent e) {
        basicListener.mousePressed(e);
        if (SwingUtilities.isLeftMouseButton(e) ) {
            ZDropDownButton b = (ZDropDownButton) e.getSource();

            if(b.contains(e.getX(), e.getY())) {
                DropDownModel dropDownModel = b.getDropDownModel();
                DropDownButtonBasicUI ui = (DropDownButtonBasicUI) b.getUI();
                int x = b.getSize().width - ui.getArrowRightShift();
                dropDownModel.setPressed(e.getX() < x ?
                        DropDownModel.PRESSED_ACTION : DropDownModel.PRESSED_ARROW);
            }
        }
    }

    public void mouseReleased(MouseEvent e) {
        basicListener.mouseReleased(e);
        if (SwingUtilities.isLeftMouseButton(e) ) {
            ZDropDownButton b = (ZDropDownButton) e.getSource();
            DropDownModel dropDownModel = b.getDropDownModel();
            dropDownModel.setPressed(DropDownModel.PRESSED_NO);

            executeMouseClicked(e);
        }
    }

    public void mouseClicked(MouseEvent e) {
        // do nothing
    }

    public void executeMouseClicked(MouseEvent e) {
        ZDropDownButton b = (ZDropDownButton) e.getSource();
        DropDownButtonUI ui = (DropDownButtonUI) b.getUI();
        int x = b.getSize().width - ui.getArrowRightShift();
        if (e.getX() < x) {
            // action
            b.executeDefaultAction();
        } else {
            // popup
            b.showPopup();
        }
    }


}
