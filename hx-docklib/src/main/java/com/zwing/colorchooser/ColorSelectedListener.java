package com.zwing.colorchooser;

import java.awt.Color;
import java.util.EventListener;

/**
 * Created: 19.07.2006 17:06:48
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ColorSelectedListener extends EventListener {
    /**
     * @param newColor selected color
     * @param source object in which color has been selected
     * @return previous color or <b>null</b> if can not return previous color
     */
    public Color colorSelected(Color newColor, Object source);
}
