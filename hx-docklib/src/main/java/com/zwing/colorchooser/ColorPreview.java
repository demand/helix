package com.zwing.colorchooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import com.zwing.ZColorChooser;
import com.zwing.ColorRectangle;

/**
 * @author Andrey Demin
 * @since 10.02.2004
 */
public class ColorPreview extends JComponent
    implements MouseMotionListener, MouseListener {

  protected static final Dimension DEFAULT_MINIMUM_SIZE = new Dimension(40,40);

  protected ColorRectangle rc;
  protected ZColorChooser colorChooser;
  protected boolean firstPaint;
  protected ColorWindow cw;
  protected Point clickPos;
  protected static final Dimension DRAG_SIZE = new Dimension(4, 4);

  public ColorPreview(ZColorChooser colorChooser) {
    this.colorChooser = colorChooser;
    rc = new PreviewRectColor();
    cw = new ColorWindow(colorChooser);
    clickPos = new Point();
    firstPaint = true;
    setMinimumSize(DEFAULT_MINIMUM_SIZE);
    setPreferredSize(DEFAULT_MINIMUM_SIZE);
//    setMinimumSize(defaultMinimumSize);

    addMouseListener(this);
    addMouseMotionListener(this);
  }

  public void colorChanged(Color c) {
    rc.setColor(c);
    repaint();
  }

  public void paint(Graphics g) {
    super.paint(g);
    if (firstPaint) {
      firstPaint = false;
      rc.setPos(new Point(0, 0));
      rc.setSize(this.getSize());
    }
    rc.paint(g);
  }

  public void mouseDragged(MouseEvent e) {
    Point pc = e.getPoint();
    if (!cw.isVisible() &&
        (Math.abs(pc.x - clickPos.x) > DRAG_SIZE.width ||
        Math.abs(pc.y - clickPos.y) > DRAG_SIZE.height)) {
      cw.setVisible(true);
    }
    if (cw.isVisible()) {
      Dimension d = cw.getSize();
      Point p = getLocationOnScreen();
      cw.setBounds(
          p.x + e.getX() - cw.getSize().width/2,
          p.y + e.getY() - cw.getSize().height/2, d.width, d.height);
    }
  }

  public void mouseMoved(MouseEvent e) {
  }

  public void mouseClicked(MouseEvent e) {
  }

  public void mousePressed(MouseEvent e) {
    cw.setColor(rc.getColor());
    clickPos = e.getPoint();
  }

  public void mouseReleased(MouseEvent e) {
    int x;
    int y;
    cw.setVisible(false);
    Point p = getLocationOnScreen();
    Point p2 = colorChooser.getColorBoxes().getLocationOnScreen();
    p.x += e.getX() - p2.x;
    p.y += e.getY() - p2.y;
    colorChooser.getColorBoxes().setColorAtPoint(p, rc.getColor());
  }

  public void mouseEntered(MouseEvent e) {
  }

  public void mouseExited(MouseEvent e) {
  }
}
