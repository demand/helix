package com.zwing.colorchooser;

import com.zwing.utils.ZUIManager;
import com.zwing.color.ColorElement;
import com.zwing.ColorRectangle;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Andrey Demin
 * @since 10.02.2004
 */
public class ColorBoxes extends JComponent
        implements MouseMotionListener, MouseListener {
    private int rows;
    private int cols;
    private int colsInLastRow;
    private Dimension boxSize;
    private int vSpace;
    private int hSpace;
    private boolean allowSelectNull;

    public static final int DEFAULT_HSPACE = 3;
    public static final int DEFAULT_VSPACE = 3;

    private ArrayList<NamedColorRectangle> boxes;
    private HashMap<Color, String> mapColor2Name;

    private int selectedBox = -1;
    private int hoverBox    = -1;
    private int pressedBox  = -1;

    private Border usualBorder;
    private Border pressedBorder;
    private Border hoverSelectedBorder;
    private Border selectedBorder;
    private Border hoverBorder;

    public ColorBoxes(int cols, int initialBoxesCount, Dimension boxSize) {
        if (cols < 0 || initialBoxesCount < 0) {
            throw new IllegalArgumentException();
        }
        allowSelectNull = true;
        hSpace = DEFAULT_HSPACE;
        vSpace = DEFAULT_VSPACE;
        this.cols = cols;
        this.boxSize = boxSize;
        boxes = new ArrayList<NamedColorRectangle>(initialBoxesCount);
        mapColor2Name = new HashMap<Color, String>(initialBoxesCount);
        createBorders();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    private void createBorders() {
        usualBorder = (Border) ZUIManager.getProperty(
                ZUIManager.KEY_BOX_BORDER_USUAL);
        pressedBorder = (Border) ZUIManager.getProperty(
                ZUIManager.KEY_BOX_BORDER_PRESSED);
        hoverSelectedBorder = (Border) ZUIManager.getProperty(
                ZUIManager.KEY_BOX_BORDER_HOVERSELECTED);
        selectedBorder = (Border) ZUIManager.getProperty(
                ZUIManager.KEY_BOX_BORDER_SELECTED);
        hoverBorder = (Border) ZUIManager.getProperty(
                ZUIManager.KEY_BOX_BORDER_HOVER);
    }

    public void setColorElements(ColorElement[] ces) {
        boxes.clear();
        int oldRows = rows;
        if (this.cols > 0 && ces.length > 0) {
            this.rows = (ces.length + cols - 1) / cols;
            this.colsInLastRow = ces.length - cols * (rows - 1);
        } else {
            this.rows = 0;
            this.colsInLastRow = 0;
        }
        Insets insets = getInsets();
        int y = insets.top;
        int k = 0;
        for (int i = 0; i < this.rows; i++) {
            int x = insets.left;
            for (int j = 0; j < this.cols; j++) {
                if (k >= ces.length) {
                    return;
                }
                ColorElement ce = ces[k];
                NamedColorRectangle r = new NamedColorRectangle(boxSize, ce.getColor(), ce.getName());
                r.setPos(new Point(x, y));
                boxes.add(r);
                k++;
                x += (boxSize.width + hSpace);
            }
            y += (boxSize.height + vSpace);
        }
        if (oldRows != rows) {
            revalidate();
            repaint();
        }
    }

    public void setColorNames(ColorElement[] ces) {
        mapColor2Name.clear();
        for(ColorElement ce : ces) {
            if (ce.getName() != null) {
                mapColor2Name.put(ce.getColor(), ce.getName());
            } else {
                mapColor2Name.remove(ce.getColor());
            }
        }
    }

    public Dimension getPreferredSize() {
        Insets insets = getInsets();
        Dimension sz = new Dimension(insets.left + insets.right,
                insets.top + insets.bottom);
        if (cols > 0) {
            sz.width += this.cols * (boxSize.width + hSpace) - hSpace;
        }
        if (rows > 0) {
            sz.height += this.rows * (boxSize.height + vSpace) - vSpace;
        }
        return sz;
    }

    public void setSelectedBox(int selected) {
        selectedBox = selected >= 0 && selected < boxes.size() ? selected : -1;
    }

    public void paint(Graphics g) {
        super.paint(g);
        Rectangle r = g.getClipBounds();
        int[] clipBoxes = getClipBoxes(r);
        if (clipBoxes != null) {
            for (int col = clipBoxes[0]; col < clipBoxes[2]; col++) {
                for (int row = clipBoxes[1]; row < clipBoxes[3]; row++) {
                    int i = getBoxIndex(row, col);
                    if (i >= 0) {
                        paintBox(i, g);
                    }
                }
            }
        }
    }

    private void paintBox(int index, Graphics g) {
        Border b = usualBorder;
        if (index == pressedBox) {
            b = pressedBorder;
        } else if (index == hoverBox) {
            if (index == selectedBox) {
                b = hoverSelectedBorder;
            } else {
                b = hoverBorder;
            }
        } else if (index == selectedBox) {
            b = selectedBorder;
        }
        ColorRectangle cr = boxes.get(index);
        cr.setBorder(b);
        cr.paint(g);
    }

    /**
     * @return int[4] - left col (incluseve), top row (inclusive),
     * right col (exclusive),  bottom row (exclusive)
     * or <code>null</code> if no boxes in rectangle
     */
    private int[] getClipBoxes(Rectangle clipRect) {
        int[] rslt = null;
        if (clipRect.width > 0 && clipRect.height > 0) {
            Insets insets = getInsets();
            int rightPixel = clipRect.x + clipRect.width - 1;
            int bottomPixel = clipRect.y + clipRect.height - 1;

            int topRow = getBoxPos(clipRect.y,  insets.top, boxSize.height, vSpace, rows);
            int botomRow = getBoxPos(bottomPixel,  insets.top, boxSize.height, vSpace, rows);
            if (topRow != botomRow || topRow%2 == 0) {
                topRow = (topRow + Math.abs(topRow%2)) / 2;
                botomRow = (botomRow - Math.abs(botomRow %2)) / 2;
                int leftCol = getBoxPos(clipRect.x,  insets.left, boxSize.width, hSpace, cols);
                int rightCol = getBoxPos(rightPixel,  insets.left, boxSize.width, hSpace, cols);
                if (leftCol != rightCol || leftCol%2 == 0) {
                    leftCol = (leftCol + Math.abs(leftCol%2)) / 2;
                    rightCol = (rightCol - Math.abs(rightCol%2)) / 2;
                    rslt = new int[] {leftCol, topRow, rightCol+1, botomRow+1};
                }

            }
        }
        return rslt;
    }

    /**
     * <table border=1>
     * <tr align="center"><td align="left"><b>pixel</b></td><td>less than<br>min bound</td>
     * <td>inset</td><td>box<sub>0</sub></td><td>space</td><td>...</td>
     * <td>box<sub>i</sub></td><td>space</td><td>...</td><td>box<sub>n</sub></td>
     * <td>higher than<br>last box<br>max bound</td></tr>
     * <tr align="center"><td align="left"><b>pos</b></td><td>-1</td><td>-1</td><td>0</td>
     * <td>1</td><td>...</td><td>i*2</td><td>i*2 + 1</td><td>...</td><td>N*2</td>
     * <td>N*2 + 1</td></tr>
     * </table>
     * @param pixel as named
     * @param inset as named
     * @param boxSize as named
     * @param spaceSize as named
     * @return box position - look at picture above
     */
    private static int getBoxPos(int pixel, int inset, int boxSize, int spaceSize, int boxCount) {
        int p2 = (pixel - inset);
        int pos = p2 < 0 ? -1 : (p2 / (boxSize + spaceSize));
        if(pos >= 0) {
            int maxPixel = inset + (pos + 1) * (boxSize + spaceSize) - spaceSize;
            pos *= 2;
            if (pixel >= maxPixel) {
                pos++;
            }
        }
        return Math.min(boxCount * 2 - 1, pos);
    }

    public int where(int xp, int yp) {
        Insets insets = getInsets();
        int rslt = -1;
        int x = xp - insets.left;
        int y = yp - insets.top;
        if (x < 0 || y < 0) {
            return rslt;        
        }
        int iRow = y / (boxSize.height + vSpace);
        if (iRow < rows && iRow >= 0) {
            int yMaxPixel = (iRow + 1) * (boxSize.height + vSpace) - vSpace;
            if (y < yMaxPixel) {
                int iCol = x / (boxSize.width + hSpace);
                int colsInRow = (iRow < rows-1) ? cols : colsInLastRow;
                if (iCol < colsInRow && iCol >= 0) {
                    int xMaxPixel = (iCol + 1) * (boxSize.width + hSpace) - hSpace;
                    if (x < xMaxPixel) {
                        rslt = iRow * cols + iCol;
                    }
                }
            }
        }
        return rslt;
    }

    public void setColorAtIndex(int i, Color c) {
        String name = mapColor2Name.get(c);
        boxes.get(i).setColor(c);
        boxes.get(i).setName(name);
        Point p = getMousePosition();
        if (p != null) {
            updateHover(p);
        }
    }

    public boolean setColorAtPoint(Point p, Color c) {
        boolean rslt = false;
        int i = where(p.x, p.y);
        if (i >= 0) {
            setColorAtIndex(i, c);
            repaintBox(i);
        }
        return rslt;
    }

    public Rectangle getBoxBounds(int boxIndex) {
        Rectangle rslt = null;
        if (boxIndex < boxes.size() && boxIndex >= 0) {
            ColorRectangle cr = boxes.get(boxIndex);
            rslt = new Rectangle(cr.getPos(), cr.getSize());
        }
        return rslt;
    }

    public Rectangle getBoxBounds(int row, int col) {
        return getBoxBounds(getBoxIndex(row, col));
    }

    public Rectangle calcBoxBounds(int row, int col) {
        return calcBoxBounds(row, col, new Rectangle());
    }

    public Rectangle calcBoxBounds(int row, int col, Rectangle rect) {
        Insets insets = getInsets();
        rect.x = insets.left + (col > 0 ? col * (boxSize.width + hSpace) - hSpace : 0);
        rect.y = insets.top + (row > 0 ? row * (boxSize.height + vSpace) - vSpace : 0);
        rect.setSize(boxSize);
        return rect;
    }

    public void repaintBox(int boxIndex) {
        repaint(getBoxBounds(boxIndex));
    }

    public int getBoxIndex(int row, int col) {
        int rslt = row * cols + col;
        return rslt < boxes.size() ? rslt : -1;
    }

    public void addColorSelectedListener(ColorSelectedListener listener) {
        listenerList.add(ColorSelectedListener.class, listener);
    }

    public void removeColorSelectedListener(ColorSelectedListener listener) {
        listenerList.remove(ColorSelectedListener.class, listener);
    }

    public void fireColorSelected(Color color) {
        Object[] listeners = listenerList.getListenerList();
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == ColorSelectedListener.class) {
                ((ColorSelectedListener)listeners[i+1]).colorSelected(color, this);
            }
        }
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
        updateHover(new Point(e.getX(), e.getY()));
    }

    private void updateHover(Point p) {
        int i = where(p.x, p.y);
        if (i != hoverBox) {
            int oldHoverBox = hoverBox;
            if (oldHoverBox >= 0) {
                repaintBox(oldHoverBox);
            }
            hoverBox = i;
            String tooltip = null;
            if (hoverBox >= 0) {
                repaintBox(hoverBox);
                tooltip = boxes.get(hoverBox).getName();
            }
            setToolTipText(tooltip);
        }
    }

    public void mouseClicked(MouseEvent e) {
        int i = where(e.getX(), e.getY());
        if (i >= 0) {
            int oldSelectedBox = selectedBox;
            if (oldSelectedBox >= 0) {
                repaint(oldSelectedBox);
            }
            selectedBox = i;
            if (oldSelectedBox != selectedBox) {
                repaint(selectedBox);
            }
            if (boxes.get(selectedBox).getColor() != null || allowSelectNull) {
                fireColorSelected(boxes.get(selectedBox).getColor());
            }
        }
    }

    public void mousePressed(MouseEvent e) {
        int i = where(e.getX(), e.getY());
        if (i >= 0) {
            pressedBox = i;
            repaintBox(i);
        } else {
            pressedBox = -1;
        }
    }

    public void mouseReleased(MouseEvent e) {
        int oldPressedBox = pressedBox;
        pressedBox = -1;
        if (oldPressedBox >= 0) {
            repaintBox(oldPressedBox);
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
        int oldHoverBox = hoverBox;
        hoverBox = -1;
        if (oldHoverBox >= 0) {
            repaintBox(oldHoverBox);
        }
    }

    public void updateUI() {
        super.updateUI();
        createBorders();
    }

    private static class NamedColorRectangle extends ColorRectangle {
        private String name;

        NamedColorRectangle(Dimension sz, Color c, String name) {
            super(sz, c);
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
