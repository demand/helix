package com.zwing.colorchooser;

import com.zwing.ZColorChooser;
import com.zwing.ColorRectangle;

import javax.swing.*;
import java.awt.*;

/**
 * @author Andrey Demin
 * @since 03.03.2004
 */
public class ColorWindow extends JWindow {

    protected ColorRectangle rc;
    public static final int SIZE = 18;

    public ColorWindow(ZColorChooser colorChooser) {
        this.setSize(SIZE, SIZE);
        rc = new PreviewRectColor();
        rc.setPos(new Point(0, 0));
        rc.setSize(new Dimension(this.getSize()));
    }

    public void setColor(Color c) {
        rc.setColor(c);
    }

    public void paint(Graphics g) {
        super.paint(g);
        rc.paint(g);
    }
}
