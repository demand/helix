package com.zwing.colorchooser;

import java.awt.*;

import com.zwing.ColorRectangle;

/**
 * @author Andrey Demin
 * @since 03.03.2004
 */
public class PreviewRectColor extends ColorRectangle {
  public PreviewRectColor() {
    super(new Dimension());
  }
}
