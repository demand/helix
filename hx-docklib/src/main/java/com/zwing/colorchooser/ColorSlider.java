package com.zwing.colorchooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.MemoryImageSource;

import com.zwing.ZColorChooser;

/**
 * @author Andrey Demin
 * @since 04.02.2004
 */
public class ColorSlider extends JComponent
        implements MouseMotionListener, MouseListener,
        MouseWheelListener {

    public static final int COLOR_WIDTH = 15;
    public static final int COLOR_HEIGHT = 256;
    public static final int SLIDER_WIDTH = 32;
    public static final int SLIDER_HEIGHT = 9;

    public Dimension size;

    protected int fixedValue;

    protected Image img;
    protected MemoryImageSource mem;
    protected int[] pix;

    protected Image imgSlider;

    protected ZColorChooser colorChooser;

    public ColorSlider(ZColorChooser cc) {
        super();
        this.colorChooser = cc;
        int w2 = colorChooser.borderSize + colorChooser.selectedSize;
        size = new Dimension(
                w2 + SLIDER_WIDTH,
                COLOR_HEIGHT + w2*2);
        init();
    }

    protected void init() {
        pix = new int[COLOR_WIDTH * COLOR_HEIGHT];
        mem = new MemoryImageSource(
                COLOR_WIDTH, COLOR_HEIGHT, pix, 0, COLOR_WIDTH);
        mem.setAnimated(true);
        img = createImage(mem);

        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);

        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);
    }

    protected void fillPix() {
        Point value = colorChooser.getColorPalette().getValue();
        switch (colorChooser.getFixedType()) {
            case ZColorChooser.FIXED_HUE:
                fillFixedHue(value);
                break;
            case ZColorChooser.FIXED_SAT:
                fillFixedSat(value);
                break;
            case ZColorChooser.FIXED_BRI:
                fillFixedBri(value);
                break;
            case ZColorChooser.FIXED_RED:
                fillFixedRed(value);
                break;
            case ZColorChooser.FIXED_GRE:
                fillFixedGre(value);
                break;
            case ZColorChooser.FIXED_BLU:
                fillFixedBlu(value);
                break;
            default:
        }
        mem.newPixels();
    }

    public static final int IV_4 = 4;
    public static final int IV_24 = 24;
    public static final int IV_16 = 16;
    public static final int IV_8 = 8;
    public static final int IV_90 = 90;
    public static final int IV_255 = 255;
    public static final float FV_255 = 255.0f;

    private void fillFixedHue(Point value) {
        int k = 0;
        float bri = 1.0f;
        float sat = 1.0f;
        for (int i = COLOR_HEIGHT - 1; i >= 0; i--) {
          float hue = (float) i / FV_255;
          for (int j = 0; j < COLOR_WIDTH; j++) {
            pix[k++] = Color.HSBtoRGB(hue, sat, bri);
          }
        }
    }

    private void fillFixedSat(Point value) {
        int k = 0;
        float hue = ((float) value.x) / FV_255;
        float bri = ((float) Math.max(IV_255 - value.y, IV_90)) / FV_255;
        for (int i = COLOR_HEIGHT - 1; i >= 0; i--) {
            float sat = (float) i / FV_255;
            for (int j = 0; j < COLOR_WIDTH; j++) {
                pix[k++] = Color.HSBtoRGB(hue, sat, bri);
            }
        }
    }

    private void fillFixedBri(Point value) {
        int k = 0;
        float hue = ((float) value.x) / FV_255;
        float sat = ((float) (IV_255 - value.y)) / FV_255;
        for (int i = COLOR_HEIGHT - 1; i >= 0; i--) {
            float bri = (float) i / FV_255;
            for (int j = 0; j < COLOR_WIDTH; j++) {
                pix[k++] = Color.HSBtoRGB(hue, sat, bri);
            }
        }
    }

    private void fillFixedRed(Point value) {
        int k = 0;
        int b = value.x;
        int g = IV_255-value.y;
        for (int r = COLOR_HEIGHT - 1; r >= 0; r--) {
            for (int j = 0; j < COLOR_WIDTH; j++) {
                pix[k++] = (IV_255 << IV_24) | (r << IV_16) | (g << IV_8) | b;
            }
        }
    }

    private void fillFixedGre(Point value) {
        int k = 0;
        int b = value.x;
        int r = IV_255 - value.y;
        for (int i = COLOR_HEIGHT - 1; i >= 0; i--) {
            int g = i;
            for (int j = 0; j < COLOR_WIDTH; j++) {
                pix[k++] = (IV_255 << IV_24) | (r << IV_16) | (g << IV_8) | b;
            }
        }
    }

    private void fillFixedBlu(Point value) {
        int k = 0;
        int r = value.x;
        int g = IV_255-value.y;
        for (int b = COLOR_HEIGHT - 1; b >= 0; b--) {
          for (int j = 0; j < COLOR_WIDTH; j++) {
            pix[k++] = (IV_255 << IV_24) | (r << IV_16) | (g << IV_8) | b;
          }
        }
    }

    public int getFixedValue() {
        return fixedValue;
    }

    public Image getImgSlider() {
        return imgSlider;
    }

    public void setImgSlider(Image imgSlider) {
        this.imgSlider = imgSlider;
    }

    public void setFixedValue(int v) {
        fixedValue = Math.max(0, Math.min(v, IV_255));
    }

    public void paint(Graphics g) {
        super.paint(g);
        colorChooser.drawBorder(g, new Rectangle(
                colorChooser.selectedSize,
                colorChooser.selectedSize,
                COLOR_WIDTH + colorChooser.borderSize*2,
                COLOR_HEIGHT + colorChooser.borderSize*2));
        int w2 = colorChooser.borderSize + colorChooser.selectedSize;
        g.drawImage(img, w2, w2, this);
        g.drawImage(imgSlider, w2, (IV_255 - fixedValue) - IV_4 + w2, this);
    }

    protected void newFixedPos(int v) {
        setFixedValue(v);
        repaint();
        colorChooser.getColorPalette().reqRepaint();
        colorChooser.repaintLineFixed();
        colorChooser.calcColor();
    }

    public void mouseDragged(MouseEvent e) {
        int w2 = colorChooser.borderSize + colorChooser.selectedSize;
        newFixedPos(IV_255 - (e.getY() - w2));
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        int w2 = colorChooser.borderSize + colorChooser.selectedSize;
        newFixedPos(IV_255 - (e.getY() - w2));
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
            int i = fixedValue - e.getUnitsToScroll();
            newFixedPos(i);
        }
    }
}
