package com.zwing.colorchooser;

import java.awt.*;

/**
 * @author Andrey Demin
 * @since 17.02.2004
 */
public interface ColorChangeListener {
  public void colorChanged(Color c);
}
