package com.zwing.colorchooser;

import com.zwing.ZColorChooser;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.MemoryImageSource;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Andrey Demin
 * @since 04.02.2004
 */
public class ColorPalette extends JComponent implements MouseMotionListener, MouseListener {

    public static final Dimension SIZE = new Dimension(256, 256);

    protected Point value;

    protected Image img;
    protected MemoryImageSource mem;
    protected int[] pix;

    protected Image imgPos;
    protected Image imgPosH;
    protected Image imgPosV;

    PainterThread paintManager;

    protected ZColorChooser colorChooser;

    public ColorPalette(ZColorChooser cc) {
        super();
        this.colorChooser = cc;
        init();
    }

    protected void init() {
        value = new Point();

        pix = new int[SIZE.width * SIZE.height];
        mem = new MemoryImageSource(SIZE.width, SIZE.height, pix, 0, SIZE.width);
        mem.setAnimated(true);
        img = createImage(mem);

        createImagePos();

        setPreferredSize(SIZE);
        setMinimumSize(SIZE);
        setMaximumSize(SIZE);

        paintManager = new PainterThread();
        paintManager.start();

        addMouseMotionListener(this);
        addMouseListener(this);
    }

    public static final int IV_3 = 3;
    public static final int IV_4 = 4;
    public static final int IV_5 = 5;
    public static final int IV_7 = 7;
    public static final int IV_8 = 8;
    public static final int IV_9 = 9;
    public static final int IV_10 = 10;
    public static final int IV_11 = 11;
    public static final int IV_15 = 15;
    public static final int IV_16 = 16;
    public static final int IV_24 = 24;
    public static final int IV_255 = 255;
    public static final float FV_255 = 255.0f;
    public static final int IBLACK = 0xFF000000;
    public static final int IWHITE = 0xFFFFFFFF;
    public static final int POS_WIDTH = 15;

    protected void createImagePos() {
        createCenterPos();
        createHorzPos();
        createVertPos();
    }

    protected void createHorzPos() {
        int[] p = new int[SIZE.width + 1];
        MemoryImageSource memPos = new MemoryImageSource(SIZE.width + 1, 1, p, 0, 1);
        for (int i = 0; i < p.length; i++) {
            p[i] = ((i % 2) == 0) ? IBLACK : IWHITE;
        }
        imgPosH = createImage(memPos);
    }

    protected void createVertPos() {
        int[] p = new int[SIZE.height + 1];
        MemoryImageSource memPos = new MemoryImageSource(1, SIZE.height + 1, p, 0, 1);
        for (int i = 0; i < p.length; i++) {
            p[i] = ((i % 2) == 0) ? IWHITE : IBLACK;
        }
        imgPosV = createImage(memPos);
    }

    protected void createCenterPos() {
        int iTransparent = 0x0;
        int[] p = new int[IV_15 * IV_15];
        MemoryImageSource memPos = new MemoryImageSource(POS_WIDTH, POS_WIDTH, p, 0, POS_WIDTH);

        for (int i = 0; i < POS_WIDTH * POS_WIDTH; i++) {
            p[i] = iTransparent;
        }
        fillCenterBorderBlack(p);
        fillCenterBorderWhite(p);
        setBW(p);

        imgPos = createImage(memPos);
    }

    private void fillCenterBorderBlack(int[] p) {
        for (int i = 0; i < POS_WIDTH; i++) {
            for (int j = 0; j < POS_WIDTH; j++) {
                int b = i * POS_WIDTH + j;
                if (isBlackBorderPos(i, j)) {
                    b = i * POS_WIDTH + j;
                    p[b] = IBLACK;
                }
            }
        }
    }
    
    private boolean isBlackBorderPos(int i, int j) {
        return
                i == 0 ||
                i == (POS_WIDTH - 1) ||
                j == 0 ||
                j == (POS_WIDTH - 1) ||
                i == 2 ||
                i == (POS_WIDTH - 3) ||
                j == 2 ||
                j == (POS_WIDTH - 3);
    }

    private void fillCenterBorderWhite(int[] p) {
        for (int i = 1; i < POS_WIDTH - 1; i++) {
            for (int j = 1; j < POS_WIDTH - 1; j++) {
                int b = i * POS_WIDTH + j;
                if (i == 1 || i == (POS_WIDTH - 2) || j == 1 || j == (POS_WIDTH - 2)) {
                    b = i * POS_WIDTH + j;
                    p[b] = IWHITE;
                }
            }
        }
    }

    private void setBW(int[] p) {
        p[IV_3  * POS_WIDTH + IV_7]  = IWHITE;
        p[IV_5  * POS_WIDTH + IV_7]  = IWHITE;
        p[IV_9  * POS_WIDTH + IV_7]  = IWHITE;
        p[IV_11 * POS_WIDTH + IV_7]  = IWHITE;
        p[IV_7  * POS_WIDTH + IV_3]  = IWHITE;
        p[IV_7  * POS_WIDTH + IV_5]  = IWHITE;
        p[IV_7  * POS_WIDTH + IV_9]  = IWHITE;
        p[IV_7  * POS_WIDTH + IV_11] = IWHITE;

        p[IV_4  * POS_WIDTH + IV_7] = IBLACK;
        p[IV_7  * POS_WIDTH + IV_4] = IBLACK;
        p[IV_10 * POS_WIDTH + IV_7] = IBLACK;
        p[IV_7 * POS_WIDTH + IV_10] = IBLACK;
    }

    protected void fillPix() {
        int fixedValue = colorChooser.getColorSlider().getFixedValue();
        switch (colorChooser.getFixedType()) {
            case ZColorChooser.FIXED_HUE:
                fillFixedHue(fixedValue);
                break;
            case ZColorChooser.FIXED_SAT:
                fillFixedSat(fixedValue);
                break;
            case ZColorChooser.FIXED_BRI:
                fillFixedBri(fixedValue);
                break;
            case ZColorChooser.FIXED_RED:
                fillFixedRed(fixedValue);
                break;
            case ZColorChooser.FIXED_GRE:
                fillFixedGre(fixedValue);
                break;
            case ZColorChooser.FIXED_BLU:
                fillFixedBlu(fixedValue);
                break;
            default:
        }
        mem.newPixels();
    }

    private void fillFixedHue(int fixedValue) {
        float hue = fixedValue / FV_255;
        int k = 0;
        for (int i = SIZE.width - 1; i >= 0; i--) {
            float bri = (float) i / FV_255;
            for (int j = 0; j < SIZE.height; j++) {
                float sat = (float) j / FV_255;
                pix[k++] = Color.HSBtoRGB(hue, sat, bri);
            }
        }
    }

    private void fillFixedSat(int fixedValue) {
        float sat = fixedValue / FV_255;
        int k = 0;
        for (int i = SIZE.width - 1; i >= 0; i--) {
            float bri = (float) i / FV_255;
            for (int j = 0; j < SIZE.height; j++) {
                float hue = (float) j / FV_255;
                pix[k++] = Color.HSBtoRGB(hue, sat, bri);
            }
        }
    }

    private void fillFixedBri(int fixedValue) {
        float bri = fixedValue / FV_255;
        int k = 0;
        for (int i = SIZE.width - 1; i >= 0; i--) {
            float sat = (float) i / FV_255;
            for (int j = 0; j < SIZE.height; j++) {
                float hue = (float) j / FV_255;
                pix[k++] = Color.HSBtoRGB(hue, sat, bri);
            }
        }
    }

    private void fillFixedRed(int r) {
        int k = 0;
        for (int g = SIZE.width - 1; g >= 0; g--) {
            for (int b = 0; b < SIZE.height; b++) {
                pix[k++] = (IV_255 << IV_24) | (r << IV_16) | (g << IV_8) | b;
            }
        }
    }

    private void fillFixedGre(int g) {
        int k = 0;
        for (int r = SIZE.width - 1; r >= 0; r--) {
            for (int b = 0; b < SIZE.height; b++) {
                pix[k++] = (IV_255 << IV_24) | (r << IV_16) | (g << IV_8) | b;
            }
        }
    }

    private void fillFixedBlu(int b) {
        int k = 0;
        for (int g = SIZE.width - 1; g >= 0; g--) {
            for (int r = 0; r < SIZE.height; r++) {
                pix[k++] = (IV_255 << IV_24) | (r << IV_16) | (g << IV_8) | b;
            }
        }
    }

    public void updateColorSlider() {
        ColorSlider csl = colorChooser.getColorSlider();
        if (csl != null) {
            csl.fillPix();
            csl.repaint();
        }
    }

    public Point getValue() {
        return value;
    }

    public void setValue(Point v) {
        value.x = Math.max(0, Math.min(v.x, SIZE.width - 1));
        value.y = Math.max(0, Math.min(v.y, SIZE.height - 1));
    }

    public void reqRepaint() {
        paintManager.setNeedRepaint();
    }

    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, this);
        g.drawImage(imgPosH, -((value.x + 1) % 2), value.y, this);
        g.drawImage(imgPosV, value.x, -(value.y % 2), this);
        g.drawImage(img, value.x - IV_7,
                value.y - IV_7, value.x + IV_7,
                value.y + IV_7, value.x - IV_7,
                value.y - IV_7, value.x + IV_7,
                value.y + IV_7, this);
        g.drawImage(imgPos, value.x - IV_7, value.y - IV_7, this);
    }

    protected void calculateAndChangeColor(Point pt) {
        setValue(pt);
        repaint();
        updateColorSlider();
        colorChooser.calcColor();
    }

    public void mouseDragged(MouseEvent e) {
        calculateAndChangeColor(e.getPoint());
    }

    public void mouseReleased(MouseEvent e) {
        calculateAndChangeColor(e.getPoint());
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    protected class PainterThread extends Thread {
        private final Lock lock = new ReentrantLock();
        private final Condition condition = lock.newCondition();

        public void run() {
            while (!isInterrupted()) {
                lock.lock();
                try {
                    condition.await();
                    fillPix();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            repaint();
                        }
                    });
                } catch (InterruptedException e) {
                    ;
                } finally {
                    lock.unlock();
                }
            }
        }

        public void setNeedRepaint() {
            lock.lock();
            try {
                condition.signal();
            } finally {
                lock.unlock();
            }
        }
    }
}
