package com.zwing.colorchooser;

import java.awt.*;

/**
 * @author Andrey Demin
 * @since 10.02.2004
 */
public interface ColorPrevewListener {
  public void colorChanged(Color c);
}
