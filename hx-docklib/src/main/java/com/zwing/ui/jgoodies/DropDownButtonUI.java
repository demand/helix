package com.zwing.ui.jgoodies;

import javax.swing.plaf.ComponentUI;
import javax.swing.JComponent;
import javax.swing.AbstractButton;

import com.zwing.ui.basic.DropDownButtonBasicUI;
import com.zwing.DefaultDropDownModel;
import com.zwing.ZDropDownButton;

import java.awt.Graphics;
import java.awt.Dimension;

/**
 * Created: 08.08.2006 17:23:52
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DropDownButtonUI extends DropDownButtonBasicUI {

    private static final DropDownButtonUI INSTANCE = new DropDownButtonUI();

    public static ComponentUI createUI(JComponent b) {
        return INSTANCE;
    }

    private int getRealBorderWidth() {
        return 2;
    }

    protected void paintButtonPressed(Graphics g, AbstractButton b) {
        if ( b.isContentAreaFilled() ) {
            Dimension size = b.getSize();
            g.setColor(getSelectColor());
            DropDownButtonBasicUI ui = (DropDownButtonBasicUI) b.getUI();
            DefaultDropDownModel model =
                    (DefaultDropDownModel) ((ZDropDownButton) b).getDropDownModel();
            int x = b.getSize().width - ui.getArrowRightShift();
            if (model.isPressedAction()) {
                g.fillRect(0, 0, x+1, size.height);
            } else {
                g.fillRect(x, 0, size.width-x, size.height);
            }
        }
    }
}
