package com.zwing.ui.jgoodies;

import com.jgoodies.looks.plastic.PlasticComboBoxUI;

import javax.swing.plaf.basic.ComboPopup;
import javax.swing.plaf.ComponentUI;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JComboBox;

import com.zwing.colorcombo.ColorComboPopup;
import com.zwing.ZColorComboBox;

/**
 * Created: 27.10.2006 15:36:59
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ColorComboBoxUI extends PlasticComboBoxUI {

    private static final ColorComboBoxUI INSTANCE = new ColorComboBoxUI();

    public ColorComboBoxUI() {
        listBox = new JList();
    }

    public static ComponentUI createUI(JComponent c) {
        return INSTANCE;
    }

    protected ComboPopup createPopup() {
        return new ColorComboPopup((ZColorComboBox) comboBox);

    }

    public boolean isFocusTraversable( JComboBox c ) {
        return true;
    }
}
