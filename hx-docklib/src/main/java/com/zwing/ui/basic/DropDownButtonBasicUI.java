package com.zwing.ui.basic;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.ButtonModel;
import javax.swing.AbstractButton;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import java.awt.*;

import com.jgoodies.looks.plastic.PlasticButtonUI;
import com.zwing.ZDropDownButton;
import com.zwing.DropDownHandler;
import com.zwing.utils.ResourceManager;

/**
 * Created: 09.08.2006 18:43:52
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DropDownButtonBasicUI extends PlasticButtonUI {

    private static final DropDownButtonBasicUI INSTANCE = new DropDownButtonBasicUI();

    private static final int DIVD_WIDTH = 1;
    private static final int ARROW_TOP_INSET    = 0;
    private static final int ARROW_LEFT_INSET   = 2;
    private static final int ARROW_RIGHT_INSET  = 0;
    private static final int ARROW_BOTTOM_INSET = 0;

    private static final int DIVD_TOP_INSET    = 0;
    private static final int DIVD_LEFT_INSET   = 1;
    private static final int DIVD_RIGHT_INSET  = 0;
    private static final int DIVD_BOTTOM_INSET = 0;

    private Icon arrowNormalIcon;
    private Icon arrowPressedIcon;
    private Icon arrowHoverIcon;
    private Icon arrowDisabledIcon;

    protected Insets arrowInsets;
    private Insets dividerInsets;
    private int extraWidth;

    public DropDownButtonBasicUI() {
        arrowNormalIcon = ResourceManager.getIcon("button.dropdown.arrow.icon.normal");

        arrowInsets = new Insets(ARROW_TOP_INSET, ARROW_LEFT_INSET,
                ARROW_BOTTOM_INSET, ARROW_RIGHT_INSET);
        dividerInsets = new Insets(DIVD_TOP_INSET, DIVD_LEFT_INSET,
                DIVD_BOTTOM_INSET, DIVD_RIGHT_INSET);
        extraWidth = arrowInsets.left + arrowInsets.right + arrowNormalIcon.getIconWidth() +
                dividerInsets.left + dividerInsets.right + DIVD_WIDTH;
    }

    public static ComponentUI createUI(JComponent b) {
        return INSTANCE;
    }

    // настройки компонента
    public void installUI(JComponent c) {
        super.installUI(c);
    }

    public void uninstallUI(JComponent c) {
        super.uninstallUI(c);
    }

    protected BasicButtonListener createButtonListener(AbstractButton b) {
        BasicButtonListener bl = super.createButtonListener(b);
        return new DropDownHandler(b, bl);
    }

    public int getExtraWidth() {
        return extraWidth;
    }

    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);
        ZDropDownButton b = (ZDropDownButton) c;
        ButtonModel model = b.getModel();
        Dimension sz = b.getSize();
        int x = sz.width - getArrowRightShift() + arrowInsets.left;
        int y = (sz.height - arrowNormalIcon.getIconHeight()) / 2;
        arrowNormalIcon.paintIcon(c, g, x, y);

        x -= arrowInsets.left;
        Insets ins = b.getInsets();
        if (!b.isRolloverEnabled() ||
                (model.isRollover() && !model.isPressed() && model.isEnabled())) {
            g.setColor(getSelectColor());
            int dividerHeight = sz.height - ins.top - ins.bottom -
                    dividerInsets.top - dividerInsets.bottom;
            g.fillRect(x, ins.top + dividerInsets.top, DIVD_WIDTH, dividerHeight);
        }

    }

//    public Dimension getPreferredSize(JComponent c) {
//        Dimension sz = super.getPreferredSize(c);
//        int w = sz.width + getExtraWidth();
//        return new Dimension(w, sz.height);
//    }
//
//    public Dimension getMinimumSize(JComponent c) {
//        Dimension sz = super.getMinimumSize(c);
//        int w = sz.width +  + getExtraWidth();
//        return new Dimension(w, sz.height);
//    }
//
//    public Dimension getMaximumSize(JComponent c) {
//        Dimension sz = super.getMaximumSize(c);
//        int extra  = getExtraWidth();
//        return new Dimension((Integer.MAX_VALUE - extra) >= sz.width ?
//                sz.width + extra : Integer.MAX_VALUE, sz.height);
//    }
//
    public int getArrowRightShift() {
        return extraWidth + getRealBorderWidth();
    }

    private int getRealBorderWidth() {
        return 2;
    }
}
