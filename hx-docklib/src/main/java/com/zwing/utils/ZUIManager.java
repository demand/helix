package com.zwing.utils;

import com.zwing.color.ColorElement;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.BevelBorder;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.*;
import java.util.*;

/**
 * Created: 02.08.2006 11:16:46
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZUIManager implements PropertyChangeListener {

    private static ZUIManager instance = null;
    private static ResourceManager resourceManager = null;

    private static final String DEFAULT_LNF_PROP_PART = "default";

    private static final String PROP_NAMENE_LNF = "lookAndFeel";

    private static final String MSG_CANT_INITIALIZE =
            "Error during Zwing UI Manager initialization.";
    private static final String MSG_NOT_INITIALIZED =
            "Attemption to get Zwing UI Manager before initialization.";
    private static final String MSG_UI_NOT_FOUND =
            "UI for ZWing compoonent is not defined";

    private static final String[] ADDUIS  = {
        "DropDownButtonUI",
        "ColorComboBoxUI"
    };

    /**
     * KEY_* - ключи в таблице properties
     * PROP_* - ключи в файлах ресурсов
     */
    public static final String KEY_DEFAULT_COLORS  = "defaultColorBoxes";
    public static final String PROP_DEFAULT_COLORS = "defaultColorBoxes";

    public static final String KEY_BOX_BORDER_USUAL            = "box.border.usual";
    public static final String KEY_BOX_BORDER_HOVER            = "box.border.hover";
    public static final String KEY_BOX_BORDER_SELECTED         = "box.border.selected";
    public static final String KEY_BOX_BORDER_HOVERSELECTED    = "box.border.hoverSelected";
    public static final String KEY_BOX_BORDER_PRESSED          = "box.border.pressed";
    public static final String KEY_BOX_BORDER_DISABLED         = "box.border.disabled";
    public static final String KEY_BOX_BORDER_DISABLEDSELECTED = "box.border.disabledSelected";

    private ArrayList<Component> rootComponents;

    private ZUIManager(ResourceManager resourceManager) {
        ZUIManager.resourceManager = resourceManager;
        rootComponents = new ArrayList<Component>(0);
        UIManager.addPropertyChangeListener(this);
        initializeDefaults();
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (PROP_NAMENE_LNF.equals(evt.getPropertyName())) {
            initializeDefaults();
        }
        for (Component c : rootComponents) {
            SwingUtilities.updateComponentTreeUI(c);
        }
    }

    public void addRootComponent(Component component) {
        rootComponents.add(component);
    }

    public void removeRootComponent(Component component) {
        rootComponents.remove(component);
    }

    protected void initializeDefaults() {
        LookAndFeel lnf = UIManager.getLookAndFeel();
        String lnfClassName = DEFAULT_LNF_PROP_PART;
        if (lnf != null) {
            lnfClassName = lnf.getClass().getName();
        }
        initializeUIs(lnfClassName);
        initGraphicsProperties();
        loadDefaultColors();
        createVisualElements();
    }

    private void initializeUIs(String lnfClassName) {
        StringBuffer sb = new StringBuffer(0);
        for (String uiName : ADDUIS) {
            sb.setLength(0);
            sb.append(uiName).append(ResourceManager.DOT).append(lnfClassName);
            String uiClassName = resourceManager.getStringProp(sb.toString());
            if (uiClassName != null) {
                UIManager.put(uiName, uiClassName);
            } else {
                throw new NullPointerException(MSG_UI_NOT_FOUND);
            }
        }
    }

    protected void loadDefaultColors() {
        ColorElement[] ces = ResourceManager.getColorElementList(PROP_DEFAULT_COLORS);
        if (ces.length > 0) {
            putProperty(KEY_DEFAULT_COLORS, ces);
        } else {
            removeProperty(KEY_DEFAULT_COLORS);
        }
    }

    protected void createVisualElements() {
        createBorders();
    }

    protected void createBorders() {
        Color dark = (Color) getProperty("Button.foreground");
        Color light = (Color) getProperty("Button.light");
        Color medium = (Color) getProperty("Button.shadow");
        Color focus = (Color) getProperty("Button.focus");

        Border inner = BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(light),
                BorderFactory.createLineBorder(dark));

        Border disabledInner = BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(light),
                BorderFactory.createLineBorder(medium));

        Border shadowInner = BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(medium), inner);

        Border darkShadowInner = BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(dark), inner);

        putProperty(KEY_BOX_BORDER_USUAL, BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(1,1,1,1), shadowInner));

        putProperty(KEY_BOX_BORDER_HOVER, BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(1,1,1,1), darkShadowInner));

        putProperty(KEY_BOX_BORDER_SELECTED, BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(focus), shadowInner));

        putProperty(KEY_BOX_BORDER_HOVERSELECTED, BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(focus), darkShadowInner));

        putProperty(KEY_BOX_BORDER_PRESSED, BorderFactory.createCompoundBorder(
                BorderFactory.createBevelBorder(BevelBorder.LOWERED),
                inner));

        putProperty(KEY_BOX_BORDER_DISABLED, BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(1,1,1,1),
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(medium), disabledInner)));

        putProperty(KEY_BOX_BORDER_DISABLEDSELECTED, BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(focus),
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(medium), disabledInner)));
    }

    public static final Dimension SERV_IMAGE_SIZE = new Dimension(10, 10);
    public static final Color COLOR_BG_BLINK = new Color(91, 135, 206);
    public static final int IV_3 = 3;
    public static final int IV_4 = 4;
    public static final int IV_5 = 5;
    public static final int IV_6 = 6;

    // from, to
    private static final String[] MAP_COLORS = new String[] {
            "InternalFrame.activeTitleBackground", "boatTitle.bgActive",
            "InternalFrame.activeTitleForeground", "boatTitle.frActive",
            "InternalFrame.inactiveTitleBackground", "boatTitle.bgInactive",
            "InternalFrame.inactiveTitleForeground", "boatTitle.frInactive",
            "InternalFrame.borderDarkShadow", "boat.borderColor"
    };

    private static final String[] MAP_IMAGES = new String[] {
            "docking.action.dock.icon", "boatTitle.imageDock",
            "docking.action.slide.icon", "boatTitle.imageSlide",
            "docking.action.float.icon", "boatTitle.imageFloat",
            "docking.action.minimz.icon", "boatTitle.imageMinimz",
            "docking.action.pin.icon", "boatTitle.imagePin",
            "docking.action.unpin.icon", "boatTitle.imageUnpin"
    };

    public static final int SIDEBAR_INSETS = 2;
    public static final int SIDEBUTTON_BORDER_SIZE = 1;
    public static final Insets SIDEBUTTON_INSETS = new Insets(2, 2, 2, 2);
    public static final Insets TITLE_INSETS = new Insets(2, 4, 2, 2);

    private void initGraphicsProperties() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

        Image im = ge.getDefaultScreenDevice().getDefaultConfiguration().
                createCompatibleImage(SERV_IMAGE_SIZE.width, SERV_IMAGE_SIZE.height);
        Graphics g = im.getGraphics();
        putProperty("service.image", im);

        UIDefaults ud = UIManager.getLookAndFeel().getDefaults();

        Font sideButtonFont = ud.getFont("Menu.font");
        if( sideButtonFont == null) {
            sideButtonFont = g.getFont();
        }

        putProperty("sideButton.font", sideButtonFont);
        // отступы от края кнопки до края панели
        putProperty("sideBar.insets", Integer.valueOf(SIDEBAR_INSETS));

        // отступ от границ текста до рамки кнопки
        putProperty("sideButton.insets", SIDEBUTTON_INSETS);

        // отступ от иконки до левой границы текста
        putProperty("sideButton.iconSpace", Integer.valueOf(2));

        // расстояние между кнопками
        putProperty("sideButton.space", Integer.valueOf(IV_6));

        // ширина рамки кнопки
        putProperty("sideButton.borderSize", Integer.valueOf(SIDEBUTTON_BORDER_SIZE));

        // цвет фона мигающей кнопки
        putProperty("sideButton.bgBlink", COLOR_BG_BLINK);

        // цвет текста мигающей кнопки
        putProperty("sideButton.frBlink", Color.WHITE);

        FontMetrics fmx = g.getFontMetrics(sideButtonFont);

        // для боковых панелей и их кнопок понятие height
        // зависит от ориентации панели. Для горизонтально
        // ориентированных это вертикальный размер, а для
        // верикально ориентированных - горизонтальный.
        int sideButtonHeight = fmx.getHeight() +
                SIDEBUTTON_INSETS.top + SIDEBUTTON_INSETS.bottom +
                SIDEBUTTON_BORDER_SIZE * 2;
        putProperty("sideButton.height", Integer.valueOf(sideButtonHeight));

        putProperty("sideBar.height", Integer.valueOf(sideButtonHeight + SIDEBAR_INSETS * 2));

        // ширина рамки окна
        putProperty("boat.borderSize", Integer.valueOf(IV_3));

        // ширина сплиттера
        putProperty("boat.splitWidth", Integer.valueOf(IV_5));

        Font titleFont = ud.getFont("Menu.font");
        if( titleFont == null) {
            titleFont = g.getFont();
        }
        if (!titleFont.isBold()) {
            Font f = new Font(titleFont.getFontName(), Font.BOLD, titleFont.getSize());
            if (f != null) {
                titleFont = f;
            }
        }
        putProperty("boatTitle.font", titleFont);

        // отступ от границ заголовка до текста
        putProperty("boatTitle.insets", TITLE_INSETS);

        // высота заголовка окна
        putProperty("boatTitle.height",
                Integer.valueOf(g.getFontMetrics(titleFont).getHeight() +
                TITLE_INSETS.top + TITLE_INSETS.bottom));

        for (int i = 0; i < MAP_COLORS.length; i+= 2) {
            putProperty(MAP_COLORS[i+1], ud.getColor(MAP_COLORS[i]));
        }

        for (int i = 0; i < MAP_IMAGES.length; i+= 2) {
            putProperty(MAP_IMAGES[i+1], ResourceManager.getImageIcon(MAP_IMAGES[i]));
        }
    }

    public ResourceManager getRM() {
        return resourceManager;
    }

    public static ZUIManager initializeInstance(String mainBundleName) throws RuntimeException {
        instance = new ZUIManager(ResourceManager.initializeInstance());
        return getInstance();
    }

    public static ZUIManager initializeInstance() {
        instance = new ZUIManager(ResourceManager.initializeInstance());
        return getInstance();
    }

    public static ZUIManager initializeInstance(ResourceManager resourceManager) {
        instance = new ZUIManager(resourceManager);
        return getInstance();
    }

    public static ZUIManager getInstance() throws NullPointerException {
        if (instance == null) {
            throw new NullPointerException(MSG_NOT_INITIALIZED);
        }
        return instance;
    }

    public static Object getProperty(Object key) {
        return UIManager.getDefaults().get(key);
    }

    public static Object putProperty(Object key, Object val) {
        return UIManager.getDefaults().put(key, val);
    }

    public static Object removeProperty(Object key) {
        return UIManager.getDefaults().remove(key);
    }
}
