package com.zwing.utils;

import com.zwing.color.ColorElement;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.util.*;
import java.awt.Color;
import java.net.URL;

/**
 * Created: 14.07.2006 10:04:50
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ResourceManager {

    private static ResourceManager instance = null;
    private static final String ZWING_BUNDLE_NAME = "zwing";
    private static final String DEFAULT_MAIN_BUNDLE_NAME = "main";

    private static final String MSG_WRONG_MAIN_BUNDLE_NAME =
            "Attemption to reget Resource Manager with different name.";
    private static final String MSG_NOT_INITIALIZED =
            "Attemption to get Resource Manager before initialization.";

    public static final String DOT = ".";
    public static final char DOT_CHAR = '.';

    private static final String RES_POSTFIX_COLOR = ResourceManager.DOT + "color";
    private static final String RES_POSTFIX_NAME = ResourceManager.DOT + "name";

    private String mainBundleName;
    private final ResourceBundle zwingBundle;
    private final ResourceBundle mainBundle;

    private ResourceManager(String mainBundleName) {
        this.mainBundleName = mainBundleName != null ? mainBundleName : DEFAULT_MAIN_BUNDLE_NAME;
        zwingBundle = ResourceBundle.getBundle(ZWING_BUNDLE_NAME);
        mainBundle = ResourceBundle.getBundle(this.mainBundleName);
    }

    public static ResourceManager initializeInstance(String mainBundleName) {
        instance = new ResourceManager(mainBundleName);
        return instance;
    }

    public static ResourceManager initializeInstance() {
        return initializeInstance(null);
    }

    public static ResourceManager getInstance() {
        if (instance == null) {
            throw new RuntimeException(MSG_NOT_INITIALIZED);
        }
        return instance;
    }

    protected Object getObject(String key) {
        Object rslt = null;
        BundleKey bk = splitFullKey(key);
        if (bk.getBundle() != null) {
            try {
                ResourceBundle b = ResourceBundle.getBundle(bk.getBundle());
                rslt = b.getObject(bk.getKey());
            } catch (MissingResourceException ex) {
                ; // do nothing
            }
        }
        if (rslt == null && mainBundle != null) {
            try {
                rslt = mainBundle.getString(key);
            } catch (MissingResourceException ex) {
                ; // do nothing
            }
        }
        if (rslt == null) {
            try {
                rslt = zwingBundle.getString(key);
            } catch (MissingResourceException ex) {
                ; // do nothing
            }
        }
        return rslt;
    }

    String getStringProp(String key) {
        return getStringProp(key, null);
    }

    String getStringProp(String key, String defVal) {
        Object o = getObject(key);
        return o != null ? o.toString() : defVal;
    }

    Color getColorProp(String key) {
        return getColorProp(key, null);
    }

    public static final String COLOR_ITEMS_PREFIX = "[";
    public static final String COLOR_ITEMS_POSTFIX = "]";

    Color getColorProp(String key, Color defVal) {
        String strColor = getString(key).trim();
        Color rslt = null;
        if (strColor != null) {
            rslt = getColorByResourceString(strColor);
        }
        return rslt != null ? rslt : defVal;
    }

    public static Color getColorByResourceString(String strColor) {
        Color rslt = null;
        String trimmed = strColor.trim();
        if (trimmed.startsWith(COLOR_ITEMS_PREFIX)) {
            int end = trimmed.indexOf(COLOR_ITEMS_POSTFIX, 1);
            if (end > 1) {
                rslt = ZwingUtils.getColorByValue(trimmed.substring(1, end));
            }
        } else {
            rslt = ZwingUtils.getColorByName(trimmed);
        }
        return rslt;
    }

    ColorElement getColorElementProp(String key) {
        return getColorElementProp(key, null);
    }

    ColorElement getColorElementProp(String key, ColorElement defVal) {
        ColorElement rslt = defVal;
        String cs = getString(key + RES_POSTFIX_COLOR);
        if (cs != null) {
            Color c = getColorByResourceString(cs);
            String n = getStringProp(key + RES_POSTFIX_NAME);
            rslt = new ColorElement(c, n);
        }
        return rslt;
    }

    Icon getIconProp(String key) {
        return getImageIconProp(key);
    }

    ImageIcon getImageIconProp(String key) {
        String iconPath = getStringProp(key);
        ImageIcon rslt = null;
        if (iconPath != null) {
            URL resource = ResourceManager.class.getResource(iconPath);
            if (resource != null) {
                rslt = new ImageIcon(resource);
            }
        }
        return rslt;
    }

    Color[] getColorListProp(String prefix) {
        ArrayList<Color> acols = new ArrayList<Color>(0);
        StringBuffer sb = new StringBuffer(prefix).append(DOT_CHAR);
        int baseLength = sb.length();
        int i = 0;
        Color c = null;
        while (true) {
            sb.setLength(baseLength);
            sb.append(i++);
            c = getColorProp(sb.toString());
            if (c == null) {
                break;
            } else {
                acols.add(c);
            }
        }
        return acols.toArray(new Color[acols.size()]);
    }

    ColorElement[] getColorElementListProp(String prefix) {
        ArrayList<ColorElement> acols = new ArrayList<ColorElement>(0);
        StringBuffer sb = new StringBuffer(prefix).append(DOT_CHAR);
        int baseLength = sb.length();
        int i = 0;
        ColorElement ce = null;
        while (true) {
            sb.setLength(baseLength);
            sb.append(i++);
            String key = sb.toString();
            ce = getColorElementProp(key);
            if (ce == null) {
                break;
            } else {
                acols.add(ce);
            }
        }
        return acols.toArray(new ColorElement[acols.size()]);
    }

    public static String getString(String key) {
        return getInstance().getStringProp(key);
    }

    public static String getString(String key, String defVal) {
        return getInstance().getStringProp(key, defVal);
    }

    public static Color getColor(String key) {
        return getInstance().getColorProp(key);
    }

    public static Color getColor(String key, Color defVal) {
        return getInstance().getColorProp(key, defVal);
    }

    public static ColorElement getColorElement(String key) {
        return getInstance().getColorElementProp(key);
    }

    public static ColorElement getColorElement(String key, ColorElement defVal) {
        return getInstance().getColorElementProp(key, defVal);
    }

    public static Icon getIcon(String key) {
        return getInstance().getIconProp(key);
    }

    public static ImageIcon getImageIcon(String key) {
        return getInstance().getImageIconProp(key);
    }

    public static Color[] getColorList(String prefix) {
        return getInstance().getColorListProp(prefix);
    }

    public static ColorElement[] getColorElementList(String prefix) {
        return getInstance().getColorElementListProp(prefix);
    }

    public static final char KEY_DELIMETER = '.';

    public static BundleKey splitFullKey(String fullKey) {
        String bundle = null;
        String key;
        int delimPos = fullKey.indexOf(KEY_DELIMETER);
        if (delimPos >= 0) {
            bundle = fullKey.substring(0, delimPos);
            key = fullKey.substring(delimPos + 1);
        } else {
            key = fullKey;
        }
        return new BundleKey(bundle, key);
    }

    private static class BundleKey {
        private final String bundle;
        private final String key;

        BundleKey(String bundle, String key) {
            this.bundle = bundle;
            this.key = key;
        }

        public String getBundle() {
            return bundle;
        }

        public String getKey() {
            return key;
        }
    }
}
