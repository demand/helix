package com.zwing.utils;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.Action;

/**
 * Created: 17.07.2006 17:15:48
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public abstract class ResourceAction extends AbstractAction {
    public static final String ACTIONS_PREFIX = "actions";
    public static final String ICON_POSTFIX = "icon";
    public static final String LABEL_POSTFIX = "label";
    public static final String TOOLTIP_POSTFIX = "tooltip";
    public static final String DOT = ".";

    public ResourceAction(String resName) {
        StringBuffer sb = new StringBuffer(ACTIONS_PREFIX);
        sb.append(DOT).append(resName).append(DOT);
        int len = sb.length();
        String name = ResourceManager.getString(sb.append(LABEL_POSTFIX).toString());
        if(name != null) {
            putValue(Action.NAME, name);
        }
        sb.setLength(len);
        Icon icon = ResourceManager.getIcon(sb.append(ICON_POSTFIX).toString());
        if (icon != null) {
            sb.setLength(len);
            putValue(Action.SMALL_ICON, icon);
        }
        sb.setLength(len);
        String tooltip = ResourceManager.getString(sb.append(TOOLTIP_POSTFIX).toString());
        if(name != null) {
            putValue(Action.SHORT_DESCRIPTION, tooltip);
        }
    }
}
