package com.zwing.utils;

/**
 * Created: 03.08.2006 13:38:14
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Strs {

    private Strs() {
        // Utilities class
    }

    public static final String NEGATIVE = "-";
    public static final String HEX_PREFIX = "0x";
    public static final int HEX_PREFIX_LEN = HEX_PREFIX.length();

    public static boolean startsWithPositiveHexPrefix(String str) {
        return  str.length() >= HEX_PREFIX_LEN &&
            HEX_PREFIX.equalsIgnoreCase(str.substring(0, HEX_PREFIX_LEN));
    }
}
