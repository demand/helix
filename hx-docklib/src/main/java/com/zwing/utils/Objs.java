package com.zwing.utils;

/**
 * Created: 02.08.2006 18:49:26
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Objs {
    private Objs() {
        // Utilities class
    }

    public static boolean safeEquals(Object o1, Object o2) {
        return o1 == null ? o2 == null : o1.equals(o2);
    }
}
