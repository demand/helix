package com.zwing.utils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageFilter;
import java.awt.image.RGBImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.FilteredImageSource;
import java.util.StringTokenizer;
import java.util.Arrays;

/**
 * @author Andrey Demin
 * @since 27.08.2003
 */
public class ZwingUtils {

  public static void setAllSizes(JComponent c, Dimension d) {
    c.setMinimumSize(d);
    c.setMaximumSize(d);
    c.setPreferredSize(d);
    c.setSize(d);
  }

  public static void setAllSizes(JComponent c, int w, int h) {
    setAllSizes(c, new Dimension(w, h));
  }

    public static Icon getIconForType(int iconType) {
        Icon rslt = null;
        switch (iconType) {
            case JOptionPane.ERROR_MESSAGE:
                rslt = UIManager.getIcon("OptionPane.errorIcon");
                break;
            case JOptionPane.INFORMATION_MESSAGE:
                rslt = UIManager.getIcon("OptionPane.informationIcon");
                break;
            case JOptionPane.WARNING_MESSAGE:
                rslt = UIManager.getIcon("OptionPane.warningIcon");
                break;
            case JOptionPane.QUESTION_MESSAGE:
                rslt = UIManager.getIcon("OptionPane.questionIcon");
                break;
            default:
        }
        return rslt;
    }


    public static Color getColorByName(String strColor) {
        Color rslt = null;
        try {
            rslt = (Color) Color.class.getField(strColor.toLowerCase()).get(null);
        } catch (IllegalAccessException e) {
            ; // do nothing
        } catch (NoSuchFieldException e) {
            ; // do nothing
        }
        return rslt;
    }

    private static final int HEX_RGB_LENGTH = 6; // RRGGBB
    private static final int HEX_RADIX = 16;
    private static final int DEC_RADIX = 10;

    public static Color getColorByValue(String colorString) {
        return  colorString.indexOf(COLOR_ITEMS_DELIM) < 0 ?
            getColorByHexValue(colorString) :
            getColorByParts(colorString);
    }

    public static final String COLOR_ITEMS_DELIM = ",";
    private static final int PART_RED   = 0;
    private static final int PART_GREEN = 1;
    private static final int PART_BLUE  = 2;
    private static final int PART_ALPHA = 3;
    private static final int MAX_PARTS  = 4;
    private static final int NO_PART    = -1;

    public static Color getColorByParts(String colorString) {
        Color rslt = null;
        StringTokenizer st = new StringTokenizer(colorString, COLOR_ITEMS_DELIM);
        try {
            int[] parts = new int[MAX_PARTS];
            Arrays.fill(parts, NO_PART);
            int partsCount = 0;
            while (st.hasMoreTokens() && partsCount < parts.length) {
                String tok = st.nextToken().trim();
                int radix = Strs.startsWithPositiveHexPrefix(tok) ? HEX_RADIX : DEC_RADIX;
                int p = Integer.parseInt(tok, radix);
                if (!isTrueColorPart(p)) {
                    partsCount = 0;
                    break;
                }
                parts[partsCount++] = p;
            }
            if (partsCount >= MAX_PARTS-1) {
                rslt = new Color(
                        parts[PART_RED], parts[PART_GREEN], parts[PART_BLUE],
                        partsCount > (MAX_PARTS-1) ? parts[PART_ALPHA] : MAX_COLOR_VALUE);
            }
        } catch (NumberFormatException e) {
            ; // do nothing
        }
        return rslt;
    }

    public static final int ALPHA_MASK = 0xFF000000;

    public static Color getColorByHexValue(String colorString) {
        Color rslt = null;
        if (Strs.startsWithPositiveHexPrefix(colorString)) {
            String hexString = colorString.substring(Strs.HEX_PREFIX_LEN);
            try {
                int argb = Integer.parseInt(hexString, HEX_RADIX);
                if ((hexString.length() <= HEX_RGB_LENGTH)) {
                    argb |= ALPHA_MASK;
                }
                rslt = new Color(argb, true);
            } catch (NumberFormatException e) {
                ; // do nothing
            }
        }
        return rslt;
    }


    public static final int MAX_COLOR_VALUE = 255;

    private static boolean isTrueColorPart(int part) {
        return part >= 0 && part <= MAX_COLOR_VALUE;
    }

    public static String colorToString(Color color)
            throws NullPointerException {
        StringBuffer rslt = new StringBuffer();
        if (color != null) {
            rslt.append(color.getRed()).append(COLOR_ITEMS_DELIM).
                    append(color.getGreen()).append(COLOR_ITEMS_DELIM).
                    append(color.getBlue());
        } else {
            throw new NullPointerException();
        }
        return rslt.toString();
    }

    public static String colorToStringSafe(Color color) {
        return color != null ? colorToString(color) : "";
    }

    public static final int NOALPHA_MASK = 0x00FFFFFF;

    public static Image makeColorTransparent(Image im, final Color color) {
        ImageFilter filter = new RGBImageFilter() {
            // Alpha bits are set to opaque, regardless of what they
            // might have been already.
            public int markerRGB = color.getRGB() | ALPHA_MASK;

            public final int filterRGB(int x, int y, int rgb) {
                if ((rgb | ALPHA_MASK) == markerRGB) {
                    // Mark the alpha bits as zero - transparent, but
                    // preserve the other information about the color
                    // of the pixel.
                    return NOALPHA_MASK & rgb;
                } else {
                    // leave the pixel untouched
                    return rgb;
                }
            }
        }; // end of inner class

        // Setup to use transparency filter
        ImageProducer ip = new FilteredImageSource(im.getSource(), filter);

        // Pull the old image thru this filter and create a new one
        return Toolkit.getDefaultToolkit().createImage(ip);
    }

    public static String makeStringShort(String s, FontMetrics fm, int maxWidth) {
      String rslt = s;
      if (rslt == null) {
        return "";
      }
      String mt = "...";
      int wMt = fm.stringWidth(mt);
      int maxShortWidth = maxWidth - wMt;
      if (maxShortWidth > 0) {
        if (fm.stringWidth(s) > maxWidth) {
          StringBuffer sb = new StringBuffer(0);
          int i = 0;
          while (i < s.length()) {
            char c = s.charAt(i);
            sb.append(c);
            int w1 = fm.stringWidth(sb.toString());
            if (w1 > maxShortWidth) {
              sb.setLength(Math.max(0, sb.length() - 1));
              break;
            }
            i++;
          }
          sb.append(mt);
          rslt = sb.toString();
        }
      } else {
        rslt = "";
      }
      return rslt;
    }

    public static Frame findFrame(Component component, Frame applicationFrame) {
        Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
        if (window instanceof Frame) {
            return (Frame) window;
        }
        Component c = component;
        while (c != null && !(c instanceof Frame)) {
            c = c.getParent();
        }
        if (c == null) {
            c = applicationFrame;
        }
        return (Frame) c;
    }

}
