package com.zwing;

import com.zwing.utils.ZUIManager;
import com.zwing.docking.DockingConst;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Компонент содержит боковые панели (ZSideBar) и рабочую область (#DockingPane)
 *
 * @author А.Дёмин
 * @since 30.11.2004
 */
public class ZSideBarPanel extends JPanel {

    public static final int LEFT   = 0;
    public static final int RIGHT  = 1;
    public static final int TOP    = 2;
    public static final int BOTTOM = 3;
    public static final int SIDEBARS_COUNT = 4;
    protected ZSideBar sidebars[];

    private static final String[] INDEX_TO_CONSTRAINTS = new String[] {
            BorderLayout.WEST, BorderLayout.EAST,
            BorderLayout.NORTH, BorderLayout.SOUTH
    };

    private DockingPane dockingPane;

    public static final String STR_ILLEGALLOCATION = "Illegal docking location";

    public ZSideBarPanel() {
        init();
    }

    public void init() {
        setLayout(new BorderLayout());

        int sideBarHeight = (Integer) ZUIManager.getProperty("sideBar.height");
        int sideButtonSpace = (Integer) ZUIManager.getProperty("sideButton.space");
        int sideBarInsets = (Integer) ZUIManager.getProperty("sideBar.insets");

        Dimension minSz = new Dimension(sideBarHeight, sideBarHeight);
        ZSideBar sb;
        sidebars = new ZSideBar[SIDEBARS_COUNT];
        sb = new ZSideBar(DockingConst.DK_WEST, sideButtonSpace);
        sb.setInsets(new Insets(sideBarInsets, sideBarInsets, sideBarInsets, sideBarInsets));
        sb.setMinimumSize(minSz);
        sidebars[LEFT] = sb;
        sb = new ZSideBar(DockingConst.DK_EAST, sideButtonSpace);
        sb.setInsets(new Insets(sideBarInsets, sideBarInsets, sideBarInsets, sideBarInsets));
        sb.setMinimumSize(minSz);
        sidebars[RIGHT] = sb;
        sb = new ZSideBar(DockingConst.DK_NORTH, sideButtonSpace);
        sb.setInsets(new Insets(sideBarInsets, sideBarHeight, sideBarInsets, sideBarInsets));
        sb.setMinimumSize(minSz);
        sidebars[TOP] = sb;
        sb = new ZSideBar(DockingConst.DK_SOUTH, sideButtonSpace);
        sb.setInsets(new Insets(sideBarInsets, sideBarHeight, sideBarInsets, sideBarInsets));
        sb.setMinimumSize(minSz);
        sidebars[BOTTOM] = sb;

        add(sidebars[LEFT], BorderLayout.WEST);
        add(sidebars[RIGHT], BorderLayout.EAST);
        add(sidebars[TOP], BorderLayout.NORTH);
        add(sidebars[BOTTOM], BorderLayout.SOUTH);

        dockingPane = new DockingPane();
        add(dockingPane, BorderLayout.CENTER);
    }

    public void addButton(ZSideButton btn, int location) {
        int idx = sidebarIndexByLocation(location);
        if (idx < 0) {
            throw new IllegalArgumentException(STR_ILLEGALLOCATION);
        }
        ZSideBar sb = sidebars[idx];
        sb.add(btn);
        sb.validate();
        sb.repaint();
    }

    public void removeButton(ZSideButton btn) {
        removeButton(btn, btn.getDockLocation());    
    }

    public void removeButton(ZSideButton btn, int location) {
        int idx = sidebarIndexByLocation(location);
        if (idx < 0) {
            throw new IllegalArgumentException(STR_ILLEGALLOCATION);
        }
        ZSideBar sb = sidebars[idx];
        sb.remove(btn);
        sb.validate();
        sb.repaint();
    }

    public void captureActionListeners(ActionListener a) {
        for (int i = 0; i < SIDEBARS_COUNT; i++) {
            sidebars[i].captureActionListener(a);
        }
    }

    private int sidebarIndexByLocation(int location) {
        return
                location == DockingConst.DK_WEST  ? LEFT :
                location == DockingConst.DK_EAST  ? RIGHT :
                location == DockingConst.DK_NORTH ? TOP :
                location == DockingConst.DK_SOUTH ? BOTTOM :
                -1;
    }

    public void setSidebarsVisible(boolean vis) {
        if (vis) {
            actualizeSidebar(LEFT);
            actualizeSidebar(RIGHT);
            actualizeSidebar(TOP);
            actualizeSidebar(BOTTOM);
//            add(sidebars[LEFT], BorderLayout.WEST);
//            add(sidebars[RIGHT], BorderLayout.EAST);
//            add(sidebars[TOP], BorderLayout.NORTH);
//            add(sidebars[BOTTOM], BorderLayout.SOUTH);
        } else {
            remove(sidebars[LEFT]);
            remove(sidebars[RIGHT]);
            remove(sidebars[TOP]);
            remove(sidebars[BOTTOM]);
        }
        validate();
    }

    public DockingPane getDockingPane() {
        return dockingPane;
    }

    public void actualizeSidebars() {
        int sideBarHeight = (Integer) ZUIManager.getProperty("sideBar.height");
        int sideBarInsets = (Integer) ZUIManager.getProperty("sideBar.insets");

        boolean hasLeft = actualizeSidebar(LEFT);
        actualizeSidebar(RIGHT);
        actualizeSidebar(TOP);
        actualizeSidebar(BOTTOM);

        Insets insets = new Insets(sideBarInsets,
                hasLeft ? sideBarHeight : sideBarInsets,
                sideBarInsets, sideBarInsets);

        sidebars[TOP].setInsets(insets);
        sidebars[BOTTOM].setInsets(insets);

        validate();
    }

    private boolean actualizeSidebar(int sbIndex) {
        boolean rslt = true;
        ZSideBar sb = sidebars[sbIndex];
        if (sb.getComponentCount() > 0) {
            add(sb, INDEX_TO_CONSTRAINTS[sbIndex]);
        } else {
            remove(sb);
            rslt = false;
        }
        return rslt;
    }
}
