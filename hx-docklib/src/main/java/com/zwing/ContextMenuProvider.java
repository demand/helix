package com.zwing;

import javax.swing.JPopupMenu;

/**
 * Created: 31.08.2006 16:17:51
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ContextMenuProvider {
    JPopupMenu getContextMenu(Object context);
}
