package com.zwing;

import java.beans.PropertyChangeEvent;

/**
 * Created: 04.09.2006 10:42:20
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface CargoListener {
    void propertyChanged(PropertyChangeEvent event);
}
