package com.zwing;

import com.zwing.colorcombo.ColorPopup;
import com.zwing.colorchooser.ColorSelectedListener;

import java.awt.*;
import java.awt.event.ItemEvent;

/**
 * Created: 09.02.2007 15:28:22
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZColorButton extends ZDropDownButton implements ColorSelectedListener {

    protected ColorIcon colorIcon;
    public static final Dimension DEFAULT_ICON_SIZE = new Dimension(16, 10);
    ColorPopup colorPopup;

    public ZColorButton() {
        super((String) null);
        colorIcon = new ColorIcon(DEFAULT_ICON_SIZE);
        setIcon(colorIcon);
        colorPopup = new ColorPopup(this, null);
        setPopupComponent(colorPopup);
    }

    public void setColor(Color color) {
//        colorIcon.setColor(color);
        setIcon(new ColorIcon(DEFAULT_ICON_SIZE, color));
    }

    public void setSelectedColor(Color color) {
        setColor(color);
    }

    public Color getColor() {
        return colorIcon.getColor();
    }

    public Color getSelectedColor() {
        return ((ColorIcon) getIcon()).getColor();
//        return getColor();
    }

    public void showPopup() {
        colorPopup.setInvoker(this);
        colorPopup.setColor(getColor());
        super.showPopup();
    }

    public void executeDefaultAction() {
        showPopup();
    }

    public Color colorSelected(Color newColor, Object source) {
        setIcon(new ColorIcon(DEFAULT_ICON_SIZE, newColor));
        stopPopup();
        colorPopup.setColor(newColor);
        fireItemStateChanged(new ItemEvent(
                this, ItemEvent.ITEM_STATE_CHANGED, this, ItemEvent.SELECTED));
        return null;
    }
}
