package com.zwing;

import javax.swing.*;

/**
 * Created: 18.07.2006 18:41:26
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ZPopupMenu extends JPopupMenu {
    private ActionSelectedListener asListener;

    public ZPopupMenu() {
        asListener = null;
    }

    public ZPopupMenu(ActionSelectedListener asListener) {
        super();
        this.asListener = asListener;
    }

    public JMenuItem add(Action action) {
        return super.add(new ActionHolder(action, asListener));
    }
}
