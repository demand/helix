package com.zwing;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.border.Border;
import java.awt.*;

/**
 * @author Andrey Demin
 * @since 10.02.2004
 */
public class ColorRectangle {
    public static final Border DEFAULT_BORDER = BorderFactory.createLineBorder(Color.black);

    public static final Color DEFAULT_NULL_BACKGROUND = Color.lightGray;
    protected Color nullBackground;

    public static final Color DEFAULT_NULL_LINE = Color.black;
    protected Color nullLine;

    public static final int DEFAULT_NULL_STEP = 4;
    protected int nullStep;

    public static final Point DEFAULT_POS = new Point(); // 0,0
    protected Point pos;

    public static final Color DEFAULT_COLOR = null;
    protected Color color;
    protected Dimension size;

    private Border border;
    private static final Component COMPONENT = new JLabel();

    public ColorRectangle(Dimension sz) {
        this(sz, DEFAULT_COLOR);
    }

    public ColorRectangle(Dimension sz, Color c) {
        border = DEFAULT_BORDER;
        nullBackground = DEFAULT_NULL_BACKGROUND;
        nullLine = DEFAULT_NULL_LINE;
        nullStep = DEFAULT_NULL_STEP;
        pos = DEFAULT_POS;
        color = c;
        this.size = sz != null ? sz : new Dimension(0, 0);
    }

    public int getLineStep() {
        return nullStep;
    }

    public void setLineStep(int lineStep) {
        this.nullStep = lineStep;
    }

    public Color getNullBackground() {
        return nullBackground;
    }

    public void setNullBackground(Color nullBackground) {
        this.nullBackground = nullBackground;
    }

    public Color getNullLine() {
        return nullLine;
    }

    public void setNullLine(Color nullLine) {
        this.nullLine = nullLine;
    }

    public Point getPos() {
        return pos;
    }

    public void setPos(Point pos) {
        this.pos = pos;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Dimension getSize() {
        return size;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }

    public void drawInner(Graphics g) {
        Insets insets = getInsets();
        int w = size.width - insets.left - insets.right;
        int h = size.height - insets.top - insets.bottom;
        int x = pos.x + insets.left;
        int y = pos.y + insets.top;
        if (color != null) {
            g.setColor(color);
            g.fillRect(x, y, w, h);
        } else {
//            ((Graphics2D) g).setRenderingHint(
//                    RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setColor(nullBackground);
            g.fillRect(x, y, w, h);
            g.setColor(nullLine);

            int p2 = w + h;
            int h1 = h - 1;
            int w1 = w - 1;
            for (int xs = 0; xs < p2; xs += nullStep) {
                int x1 = xs < h ? 0 : (xs - h1);
                int y1 = xs - x1;
                int x2 = xs < w ? xs : w1;
                int y2 = xs - x2;
                g.drawLine(x + x1, y + y1, x + x2, y + y2);
            }
        }
    }

    private Insets getInsets() {
        return border != null ? border.getBorderInsets(COMPONENT) : new Insets(0, 0, 0, 0);
    }

    public void drawBorder(Graphics g) {
        if (border != null) {
            border.paintBorder(COMPONENT, g, pos.x, pos.y, size.width, size.height);
        }
    }

    public void paint(Graphics g) {
        drawInner(g);
        drawBorder(g);
    }

    public Border getBorder() {
        return border;
    }

    public void setBorder(Border border) {
        this.border = border;
    }
}
