package com.zwing.docking;

/**
 * Определяет размещение окон - размеры, видимость.
 * <p/>
 * Created: 04.09.2006 11:29:18
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockingPlacement {
    /**
     * Имя "размещения"
     */
    private String name;

    /**
     * Ширина рабочей области, которой соответствует это размещение.
     */
    private int paneWidth;

    /**
     * Высота рабочей области, которой соответствует это размещение.
     */
    private int paneHeight;

    /**
     * Способ взаимного расположения DOCK окон
     */
    private String layoutUniqueName;

    /**
     * Активное окно
     */
    private BoatInfo activeBoat;

    /**
     * Центральное окно
     */
    private BoatInfo centerBoat;

    /**
     * Видимые DOCK окна
     */
    private BoatInfo[] visibleBoats;

    /**
     * Все окна
     */
    private BoatInfo[] boats;

    public DockingPlacement(DockingPlacement src) {
        if (src != null) {
            this.name = src.name;
            this.paneWidth = src.paneWidth;
            this.paneHeight = src.paneHeight;
            this.layoutUniqueName = src.layoutUniqueName;
            this.activeBoat = src.activeBoat;
            this.centerBoat = src.centerBoat;
            this.visibleBoats = BoatInfo.getSafeArrayCopy(src.visibleBoats);
            this.boats = BoatInfo.getSafeArrayCopy(src.boats);
        }
    }

    public DockingPlacement() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPaneWidth() {
        return paneWidth;
    }

    public void setPaneWidth(int paneWidth) {
        this.paneWidth = paneWidth;
    }

    public int getPaneHeight() {
        return paneHeight;
    }

    public void setPaneHeight(int paneHeight) {
        this.paneHeight = paneHeight;
    }

    public String getLayoutUniqueName() {
        return layoutUniqueName;
    }

    public void setLayoutUniqueName(String layoutUniqueName) {
        this.layoutUniqueName = layoutUniqueName;
    }

    public BoatInfo getActiveBoat() {
        return activeBoat;
    }

    public void setActiveBoat(BoatInfo activeBoat) {
        this.activeBoat = activeBoat;
    }

    public BoatInfo getCenterBoat() {
        return centerBoat;
    }

    public void setCenterBoat(BoatInfo centerBoat) {
        this.centerBoat = centerBoat;
    }

    public BoatInfo[] getVisibleBoats() {
        return visibleBoats;
    }

    public BoatInfo[] getBoats() {
        return boats;
    }

    public void setBoats(BoatInfo[] boats) {
        this.boats = boats;

        visibleBoats = new BoatInfo[DockingConst.DOCKS_COUNT];
        for (int i = 0; i < boats.length; i++) {
            BoatInfo boat = boats[i];
            if (boat.isVisible() && ((boat.getBehaviour() & BoatEmpty.BST_DOCK) != 0)) {
                visibleBoats[boat.getDockLocation()] = boat;
            }
        }
    }
}
