package com.zwing.docking;

import java.util.HashMap;
import java.util.Collection;

import com.zwing.DockingCargo;

/**
 * Created: 01.09.2006 18:23:12
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class CargoManager {
    private HashMap<String, DockingCargo> cgs;

    public CargoManager() {
        cgs = new HashMap<String,DockingCargo>(0);
    }

    public void registerCargo(DockingCargo cargo) {
        cgs.put(cargo.getUniqueName(), cargo);
    }

    public void unregisterCargo(DockingCargo cargo) {
        cgs.remove(cargo.getUniqueName());
    }

    public void unregisterCargo(String name) {
        cgs.remove(name);
    }

    public void clear() {
        cgs.clear();
    }

    public boolean contains(DockingCargo cargo) {
        return cgs.containsKey(cargo.getUniqueName());
    }

    public boolean contains(String name) {
        return cgs.containsKey(name);
    }

    public Collection<DockingCargo> getAllCargo() {
        return cgs.values();
    }

    public DockingCargo get(String uniqueName) {
        return cgs.get(uniqueName);
    }

    public DockingCargo remove(String uniqueName) {
        return cgs.remove(uniqueName);
    }
}
