package com.zwing.docking;

/**
 * Created: 14.08.2006 15:18:41
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Boatable {
    public void setBounds(int left, int top, int width, int height);
}
