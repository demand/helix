package com.zwing.docking;

import com.zwing.utils.ResourceManager;

import javax.swing.*;
import java.awt.*;

/**
 * Информация об отдельном окне
 *
 * @author А.Дёмин
 */
public class BoatInfo {
  /**
   * Строка, определяющая объект находящийся внутри окна
   */
  private String cargoUniquieName;

  /**
   * Заголовок окна
   */
  private String title;

  /**
   * Иконка окна
   */
  private ImageIcon icon;

  /**
   * Видимо ли окно
   */
  private boolean visible;

  /**
   * Сторона, к которой прикреплено окно
   */
  private int dockLocation;

  /**
   * Ширина окна в состоянии DOCK и SLIDE
   */
  private int dockWidth;

  /**
   * Высота окна в состоянии DOCK и SLIDE
   */
  private int dockHeight;

  /**
   * Размеры окна в состоянии FLOAT
   */
  private Rectangle floatBounds;

  /**
   * Поведение окна (DOCK/SLIDE/FLOAT) и признак Pin
   */
  private int behaviour;

  public BoatInfo(BoatInfo src) {
    if (src != null) {
      this.cargoUniquieName = src.cargoUniquieName;
      this.title = src.title;
      this.icon = src.icon;
      this.visible = src.visible;
      this.dockLocation = src.dockLocation;
      this.dockWidth = src.dockWidth;
      this.dockHeight = src.dockHeight;
      this.floatBounds = src.floatBounds != null ? new Rectangle(src.floatBounds) : null;
      this.behaviour = src.behaviour;
    } else {
      floatBounds = new Rectangle();
    }
  }

  public static BoatInfo getSafeCopy(BoatInfo src) {
    return src != null ? new BoatInfo(src) : null;
  }

  public static BoatInfo[] getSafeArrayCopy(BoatInfo[] src) {
    BoatInfo[] rslt = null;
    if (src != null) {
      rslt = new BoatInfo[src.length];
      int i = 0;
      for (BoatInfo bi : src) {
        rslt[i++] = BoatInfo.getSafeCopy(bi);
      }
    }
    return rslt;
  }

  public BoatInfo() {
    floatBounds = new Rectangle();
  }

  public void setBoat(BoatEmpty boat) {
    cargoUniquieName = boat.getCargo().getUniqueName();
    title = boat.getTitle();
    icon = boat.getIcon();
    visible = boat.isVisible();
    dockLocation = boat.getDockLocation();
    dockWidth = boat.getDockSizes().width;
    dockHeight = boat.getDockSizes().height;
    floatBounds = new Rectangle(boat.getFloatBounds());
    behaviour = boat.getBehaviour();
    if (boat.isPin()) {
      behaviour |= BoatEmpty.BST_PIN;
    }
  }

  /**
   * Для сохранения состояния при работе приложения
   *
   * @param boat
   */
  public BoatInfo(BoatEmpty boat) {
    setBoat(boat);
  }

  public String getCargoUniquieName() {
    return cargoUniquieName;
  }

  public void setCargoUniquieName(String cargoUniquieName) {
    this.cargoUniquieName = cargoUniquieName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = ResourceManager.getString(title, title);
  }

  public ImageIcon getIcon() {
    return icon;
  }

  public void setIcon(ImageIcon icon) {
    this.icon = icon;
  }

  public void setIconResource(String iconKey) {
    icon = ResourceManager.getImageIcon(iconKey);
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public int getDockLocation() {
    return dockLocation;
  }

  public void setDockLocation(int dockLocation) {
    this.dockLocation = dockLocation;
  }

  public int getDockWidth() {
    return dockWidth;
  }

  public void setDockWidth(int dockWidth) {
    this.dockWidth = dockWidth;
  }

  public int getDockHeight() {
    return dockHeight;
  }

  public void setDockHeight(int dockHeight) {
    this.dockHeight = dockHeight;
  }

  public int getFloatBoundsX() {
    return floatBounds.x;
  }

  public void setFloatBoundsX(int x) {
    this.floatBounds.x = x;
  }

  public int getFloatBoundsY() {
    return floatBounds.y;
  }

  public void setFloatBoundsY(int y) {
    this.floatBounds.y = y;
  }

  public int getFloatBoundsWidth() {
    return floatBounds.width;
  }

  public void setFloatBoundsWidth(int w) {
    this.floatBounds.width = w;
  }

  public int getFloatBoundsHeight() {
    return floatBounds.height;
  }

  public void setFloatBoundsHeight(int h) {
    this.floatBounds.height = h;
  }

  public int getBehaviour() {
    return behaviour;
  }

  public void setBehaviour(int behaviour) {
    this.behaviour = behaviour;
  }
}
