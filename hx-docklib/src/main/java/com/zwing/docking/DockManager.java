package com.zwing.docking;

import com.zwing.*;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Created: 14.08.2006 15:00:03
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockManager implements BoatListener, Disposer, DragPainter,
    ActionListener, ContextMenuProvider {

  private static final Logger log = Logger.getLogger(DockManager.class);

  private ZSideBarPanel sbPanel;

  private int curResizeType;
  private int curResizeDocks;

  private BoatEmpty centerBoat;
  private BoatEmpty activeBoat;

  private BoatEmpty[] visibleDockBoats = new BoatEmpty[DockingConst.DOCKS_COUNT];

  /**
   * ? ????? ??????, ????? ???????????? ????, ?? ?????????
   * ? ??????? ??????? ???????. ????????, ??????????? ????
   * ?????????. ????? ??? ????? ???? ???? c ???????????
   * ?????????? ?????????? FLOAT. ???? ? ?????????? DOCK
   * ? SLIDE ?????? ???? ??????? ? ??????? ??????? ??????.
   */
  private ArrayList<ZSideButton> btns;
  private ArrayList<BoatEmpty> boats;

  private HashMap<ZSideButton, BoatEmpty> btnToBoat;
  private HashMap<BoatEmpty, ZSideButton> boatToBtn;

  private Rectangle prevRect;
  private Rectangle lastRect;

  private CargoManager cargoManager;

  private DockingLayout layout;

  enum PainterMode {
    NOTHING, BOAT_BOUNDS, DOCK_SPLITTERS
  }
  private PainterMode painterMode;

  public static final int SPLITTER_SIZE = 5;
  public static final int BORDER_SIZE   = 4;

  public DockManager() {
    prevRect = null;
    lastRect = null;
    activeBoat = null;
    disposeInProcess = false;

    cargoManager = new CargoManager();

    painterMode = PainterMode.NOTHING;

    btns = new ArrayList<ZSideButton>(0);
    boats = new ArrayList<BoatEmpty>(0);
    btnToBoat = new HashMap<ZSideButton, BoatEmpty>(0);
    boatToBtn = new HashMap<BoatEmpty, ZSideButton>(0);

    sbPanel = new ZSideBarPanel();
    getDragCanvas().setDragPainter(this);
    getDragCanvas().setDisposer(this);

    getSplittersCanvas().setDisposer(this);
    getSplittersCanvas().setDockManager(this);

    sbPanel.captureActionListeners(this);
    sbPanel.getDockingPane().setDockManager(this);
    FocusTracker ft = new FocusTracker(this);
    ft.start();
  }

  public JComponent getVisualComponent(){
    return sbPanel;
  }

  private DockingPane getDockingPane() {
    return sbPanel.getDockingPane();
  }

  private DragCanvas getDragCanvas() {
    return getDockingPane().getDragCanvas();
  }

  private SplittersCanvas getSplittersCanvas() {
    return getDockingPane().getSplittersCanvas();
  }

  public CargoManager getCargoManager() {
    return cargoManager;
  }

  private DockingIngredients ingredients;

  public void setIngredients(DockingIngredients ingredients) {
    this.ingredients = ingredients;
    applyIngridients();
  }

  private void applyIngridients() {
    cargoManager.clear();
    if (ingredients != null) {
      DockingCargo[] cargos = ingredients.getCargos();
      if (cargos != null) {
        for (DockingCargo cargo : cargos) {
          cargoManager.registerCargo(cargo);
        }
      }
    }
  }

  public void loadPlacement(DockingPlacement placement) {
    Map<BoatInfo, BoatEmpty> bi2boat =
        new HashMap<BoatInfo, BoatEmpty>(0);

    detachAll();

    BoatInfo[] bis = placement.getBoats();
    for (BoatInfo bi : bis) {
      BoatEmpty boat = new BoatEmpty(
          BoatEmpty.BST_CENTER | BoatEmpty.BST_DOCK |
          BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
          bi.getBehaviour(), bi.getTitle());
      boat.setIcon(bi.getIcon());
      String cn = bi.getCargoUniquieName();
      boat.setCargo(getCargoManager().get(bi.getCargoUniquieName()));
      boat.setDockLocation(bi.getDockLocation());
      boat.setDockSizes(new Dimension(bi.getDockWidth(), bi.getDockHeight()));
      boat.setFloatBounds(new Rectangle(
          bi.getFloatBoundsX(), bi.getFloatBoundsY(),
          bi.getFloatBoundsWidth(), bi.getFloatBoundsHeight()));
      bi2boat.put(bi, boat);

      attach(boat, boat.getDockLocation());
    }

    BoatInfo[] vbis = placement.getVisibleBoats();
    for(int i = 0; i < vbis.length; i++) {
      BoatInfo bi = vbis[i];
      if (bi != null) {
        visibleDockBoats[i] = bi2boat.get(bi);
      }
    }

    if (placement.getCenterBoat() != null) {
      centerBoat = bi2boat.get(placement.getCenterBoat());
    }

    sbPanel.actualizeSidebars();

    adjustDocksBounds();

    for (BoatInfo bi : bis) {
      BoatEmpty boat = bi2boat.get(bi);
      setBoatVisible(boat, bi.isVisible());
      if (boat.isSlide()) {
        Rectangle bounds = DmUtils.getAdjustedSlideBounds(boat, getDockingPane().getSize());
        setBoatBoundsSafe(boat, bounds);
      }
      if (boat.isFloat()) {
        Rectangle bounds = DmUtils.getAdjustedFloatBounds(boat, getDockingPane().getSize());
        setBoatBoundsSafe(boat, bounds);
      }
    }

    if (placement.getActiveBoat() != null) {
      setBoatActive(bi2boat.get(placement.getActiveBoat()));
    }
  }

  public DockingPlacement createPlacement(String name) {
    DockingPlacement rslt = new DockingPlacement();
    rslt.setName(name);
    return savePlacement(rslt);
  }

  public DockingPlacement savePlacement(DockingPlacement placement) {
    Map<BoatEmpty, BoatInfo> boat2bi =
        new HashMap<BoatEmpty, BoatInfo>(0);

    ArrayList<BoatInfo> bisa = new ArrayList<BoatInfo>(boats.size());
    for (BoatEmpty boat : boats) {
      if (boat != null) {
        if (boat.isFloat()) {
          Rectangle bounds = boat.getBounds();
          boat.setFloatBounds(bounds);
        }
        BoatInfo bi = new BoatInfo(boat);
        bisa.add(bi);
        boat2bi.put(boat, bi);
      }
    }
    placement.setBoats(bisa.toArray(new BoatInfo[bisa.size()]));

    Dimension sz = getDockingPane().getSize();
    placement.setPaneWidth(sz.width);
    placement.setPaneWidth(sz.height);
    placement.setLayoutUniqueName(layout != null ? layout.getUniqueName() : null);
    placement.setActiveBoat(activeBoat != null ? boat2bi.get(activeBoat) : null);
    placement.setCenterBoat(centerBoat != null ? boat2bi.get(centerBoat) : null);

    BoatInfo[] vbis = new BoatInfo[visibleDockBoats.length];
    for(int i = 0; i < visibleDockBoats.length; i++) {
      BoatEmpty boat = visibleDockBoats[i];
      if (boat != null) {
        vbis[i] = boat2bi.get(boat);
      }
    }
    return placement;
  }

  private void removeSideButton(ZSideButton btn) {
    if (btn != null) {
      sbPanel.removeButton(btn);
      btns.remove(btn);
      btnToBoat.remove(btn);
    }
  }

  public void attach(BoatEmpty boat, int location) {
    int oldLocation = boat.getDockLocation();
    removeSideButton(boatToBtn.get(boat));

    ZSideButton newBtn = null;
    if (location != DockingConst.DK_CENTER) {
      newBtn = new ZSideButton(location, boat.getTitle(), boat.getIcon());
      sbPanel.addButton(newBtn, location);
    } else {
      centerBoat = boat;
    }

    boat.setDockLocation(location);
    if (!boats.contains(boat)) {
      boats.add(boat);

      boat.setDisposer(this);
      boat.setBoatListener(this);
      boat.setVisible(false);

      getDockingPane().add(boat, DockingConst.ZO_DEFAULT);
      Dimension szPref = boat.getPreferredSize();
      boat.setBounds(0, 0, szPref.width, szPref.height);
    } else {
      if (boat.isDock() && boat.isVisible()) {
        visibleDockBoats[oldLocation] = null;
        if (visibleDockBoats[location] == null) {
          visibleDockBoats[location] = boat;
        } else {
          if (!visibleDockBoats[location].isActive()) {
            setBoatVisible(visibleDockBoats[location], false);
            visibleDockBoats[location] = boat;
          } else {
            boat.setVisible(false);
          }
        }
      }
    }
    if (newBtn != null) {
      newBtn.setSelected(boat.isVisible());
      boatToBtn.put(boat, newBtn);
      btnToBoat.put(newBtn, boat);
      btns.add(newBtn);
      newBtn.addActionListener(this);
      newBtn.setContextMenuProvider(this);
      newBtn.setSelected(boat.isVisible());
    } else {
      boatToBtn.remove(boat);
    }
    DmUtils.adjustBoatBordersAndMovable(getDockingPane(), boat);
  }

  public void detach(BoatEmpty boat) {
    int dockLocation = boat.getDockLocation();
    ZSideButton btn = boatToBtn.get(boat);
    if (btn != null) {
      sbPanel.removeButton(btn, dockLocation);
      btns.remove(btn);
      btnToBoat.remove(btn);
    }
    boatToBtn.remove(boat);
    boats.remove(boat);
    if (visibleDockBoats[dockLocation] == boat) {
      visibleDockBoats[dockLocation] = null;
    }
    if (activeBoat == boat) {
      activeBoat = null;
    }
    boat.setDisposer(null);
    boat.setBoatListener(null);
    getDockingPane().remove(boat);
  }

  public void detachAll() {
    for (ZSideButton btn : btns) {
        sbPanel.removeButton(btn);
    }
    btns.clear();
    btnToBoat.clear();

    for (BoatEmpty boat: boats) {
        boat.setDisposer(null);
        boat.setBoatListener(null);
        getDockingPane().remove(boat);
    }
    boatToBtn.clear();
    boats.clear();

    Arrays.fill(visibleDockBoats, null);
    activeBoat = null;
  }

  public void requestAttach(BoatEmpty boat, int location) {
    attach(boat, location);
    if (boat.isVisible() && boat.isDock()) {
      adjustDocksBounds();
    }
    if (boat.isSlide()) {
      Rectangle bounds = DmUtils.getAdjustedSlideBounds(boat, getDockingPane().getSize());
      setBoatBoundsSafe(boat, bounds);
    }
    sbPanel.actualizeSidebars();
  }

  public void requestDetach(BoatEmpty boat) {
    boolean visible = boat.isVisible();
    boolean dock = boat.isDock();
    boolean active = (activeBoat == boat && activeBoat != null);
    detach(boat);
    if (dock && visible) {
      adjustDocksBounds();
    }
    if (active && centerBoat != null) {
      setBoatActive(centerBoat);
    }
    sbPanel.actualizeSidebars();
  }

  public void doPaint(Graphics g, Dimension sz) {
    if (painterMode == PainterMode.BOAT_BOUNDS) {
      doPaintBoatBounds(g, sz);
    } else if (painterMode == PainterMode.DOCK_SPLITTERS) {
      doPaintSplitters(g, sz);
    }
  }

  public static final int COLOR_CLEAR       = 0x00000000;
  public static final int COLOR_OUTER_BOUND = 0x80000000;
  public static final int COLOR_INNER_BOUND = 0x80FFFFFF;
  public static final int COLOR_INNER_AREA  = 0x40808080;
  public static final int COLOR_SPLITTER    = 0xC0404040;

  private void doPaintBoatBounds(Graphics g, Dimension sz) {
    if (lastRect != null) {
      g.setColor(new Color(COLOR_OUTER_BOUND, true));
      g.drawRect(lastRect.x + 1, lastRect.y + 1, lastRect.width - 2, lastRect.height - 2);

      g.setColor(new Color(COLOR_INNER_BOUND, true));
      g.drawRect(lastRect.x+2, lastRect.y+2, lastRect.width-5, lastRect.height-5);

      g.setColor(new Color(COLOR_INNER_AREA, true));
      g.fillRect(lastRect.x+3, lastRect.y+3, lastRect.width-2-2, lastRect.height-2-2);
    }
  }

  private Rectangle vspRect;
  private Rectangle hspRect;

  private void doPaintSplitters(Graphics g, Dimension sz) {
    g.setColor(new Color(COLOR_SPLITTER, true));
    if (vspRect != null) {
      g.fillRect(vspRect.x, vspRect.y, vspRect.width, vspRect.height);
    }
    if (hspRect != null) {
      g.fillRect(hspRect.x, hspRect.y, hspRect.width, hspRect.height);
    }
  }

  protected Rectangle adjustBoatBounds(BoatEmpty boat, Rectangle r, int drag) {
    return DmUtils.adjustBoatBounds(boat, r, drag, getDockingPane().getSize());
  }

  public void drawRect(BoatEmpty boat, Rectangle r, int drag) {
    painterMode = PainterMode.BOAT_BOUNDS;
    if (r == null) {
      throw new NullPointerException();
    }
    prevRect = lastRect;
    lastRect = r;
    RectBounds rb = DmUtils.calcRectBounds(lastRect, prevRect);
    getDragCanvas().repaint(rb.x0, rb.y0, rb.x1 - rb.x0, rb.y1 - rb.y0);
  }

  private RectBounds calcCenterBounds() {
    Dimension szPane = getDockingPane().getSize();
    int west  = getDockSizeSafe(DockingConst.DK_WEST);
    int east  = getDockSizeSafe(DockingConst.DK_EAST);
    int north = getDockSizeSafe(DockingConst.DK_NORTH);
    int south = getDockSizeSafe(DockingConst.DK_SOUTH);

    int x0 = 0;
    int y0 = 0;
    int x1 = szPane.width;
    int y1 = szPane.height;
    x0 += west != DockingConst.INT_UNUSED ? west + SPLITTER_SIZE : 0;
    y0 += north != DockingConst.INT_UNUSED ? north + SPLITTER_SIZE : 0;
    x1 -= east != DockingConst.INT_UNUSED ? east + SPLITTER_SIZE : 0;
    y1 -= south != DockingConst.INT_UNUSED ? south + SPLITTER_SIZE : 0;

    return new RectBounds(x0, y0, x1, y1);
  }

  private void adjustDocksBounds() {
    RectBounds rb = calcCenterBounds();

    Dimension szPane = getDockingPane().getSize();

    setBoatBoundsSafe(centerBoat,
        new Rectangle(rb.x0, rb.y0, rb.x1 - rb.x0, rb.y1 - rb.y0));
    setBoatBoundsSafe(visibleDockBoats[DockingConst.DK_WEST],
        new Rectangle(0, rb.y0, rb.x0 - SPLITTER_SIZE, rb.y1 - rb.y0));
    setBoatBoundsSafe(visibleDockBoats[DockingConst.DK_EAST],
        new Rectangle(rb.x1 + SPLITTER_SIZE, rb.y0,
        szPane.width - rb.x1 - SPLITTER_SIZE, rb.y1 - rb.y0));
    setBoatBoundsSafe(visibleDockBoats[DockingConst.DK_NORTH],
        new Rectangle(0, 0, szPane.width, rb.y0 - SPLITTER_SIZE));
    setBoatBoundsSafe(visibleDockBoats[DockingConst.DK_SOUTH],
        new Rectangle(0, rb.y1 + SPLITTER_SIZE,
        szPane.width, szPane.height - rb.y1 - SPLITTER_SIZE));
  }

  private void setBoatBoundsSafe(BoatEmpty boat, Rectangle bounds) {
    if (boat != null && !boat.getBounds().equals(bounds) && bounds != null) {
      boat.setBounds(bounds);
      boat.validate();
    }
  }

  private int getDockSizeSafe(int dock) {
    return visibleDockBoats[dock] != null ?
        visibleDockBoats[dock].getDockSize() : DockingConst.INT_UNUSED;
  }

  private void servBoatVisible(BoatEmpty boat, boolean visible) {
    logDebug("servBoatVisible(" + boat.getTitle() + ", " + visible + ")");
    if (boat != null) {
      boat.setVisible(visible);
      ZSideButton btn = boatToBtn.get(boat);
      if (btn != null) {
        btn.setSelected(visible);
      }
      if (boat.isDock() && !visible) {
        int location = boat.getDockLocation();
        if (visibleDockBoats[location] == boat) {
          visibleDockBoats[location] = null;
        }
      }
    }
  }

  private void setBoatVisible(BoatEmpty boat, boolean visible) {
    logDebug("setBoatVisible(" + boat.getTitle() + ", " + visible + ")");
    if (boat.isVisible() == visible) {
      return;
    }
    if(boat.isDock()) {
      int location = boat.getDockLocation();
      if (visible) {
          BoatEmpty old = visibleDockBoats[location];
          if (old != null) {
              servBoatVisible(old, false);
          }
          visibleDockBoats[location] = boat;
      }
    }
    servBoatVisible(boat, visible);
  }

  public Rectangle[] getSplittersRect() {
    Rectangle[] rcs = new Rectangle[DockingConst.DOCKS_COUNT];

    Dimension szPane = getDockingPane().getSize();
    int west  = getDockSizeSafe(DockingConst.DK_WEST);
    int east  = getDockSizeSafe(DockingConst.DK_EAST);
    int north = getDockSizeSafe(DockingConst.DK_NORTH);
    int south = getDockSizeSafe(DockingConst.DK_SOUTH);

    RectBounds cb = calcCenterBounds();

    int y0 = 0;
    int y1 = szPane.height;

    if (north != DockingConst.INT_UNUSED) {
      rcs[DockingConst.DK_NORTH] = new Rectangle(
          0, cb.y0 - SPLITTER_SIZE, szPane.width, SPLITTER_SIZE);
      y0 = cb.y0 - SPLITTER_SIZE;
    }
    if (south != DockingConst.INT_UNUSED) {
      rcs[DockingConst.DK_SOUTH] = new Rectangle(
          0, cb.y1, szPane.width, SPLITTER_SIZE);
      y1 = cb.y1 + SPLITTER_SIZE;
    }
    if (west != DockingConst.INT_UNUSED) {
      rcs[DockingConst.DK_WEST] = new Rectangle(
          cb.x0 - SPLITTER_SIZE, y0, SPLITTER_SIZE, y1 - y0);
    }
    if (east != DockingConst.INT_UNUSED) {
      rcs[DockingConst.DK_EAST] = new Rectangle(
          cb.x1, y0, SPLITTER_SIZE, y1 - y0);
    }
    return rcs;
  }

  private void setBoatActive(BoatEmpty boat) {
    logDebug("setBoatActive(" + boat.getTitle() + ")");
    if (activeBoat == boat) {
      return;
    }
    if (activeBoat != null) {
      activeBoat.setActive(false);
    }
    activeBoat = boat;
    activeBoat.setActive(true);
    getDockingPane().moveToFront(activeBoat);
    Component c = activeBoat.getLastFocusedComponent();
    if (c != null) {
      c.requestFocusInWindow();
    } else {
      activeBoat.requestFocusInWindow();
    }
  }

  private void hideUnpinnedBoats() {
    logDebug("hideUnpinnedBoats");
    for (BoatEmpty boat : boats) {
      if (boat.isVisible() && (boat.isSlide() || !boat.isPin()) &&
          !boat.isActive() && !boat.isCenter()) {
        servBoatVisible(boat, false);
      }
    }
  }

  public void adjustAfterResize() {
    adjustDocksBounds();
    if (activeBoat != null && activeBoat.isSlide()) {
      Rectangle bounds = DmUtils.getAdjustedSlideBounds(
          activeBoat, getDockingPane().getSize());
      setBoatBoundsSafe(activeBoat, bounds);
    }
  }

  private Point adjustDocksSplitters(Point p) {
    Point rslt = new Point(p.x, p.y);
    Dimension paneSize = getDockingPane().getSize();
    if ((curResizeDocks & SplittersCanvas.SPL_NORTH) == SplittersCanvas.SPL_NORTH) {
      int y = paneSize.height;
      if (rects[DockingConst.DK_SOUTH] != null) {
        y = rects[DockingConst.DK_SOUTH].y;
      }
      rslt.y = Math.min(Math.max(rslt.y, 0), y - SPLITTER_SIZE);
    }
    if ((curResizeDocks & SplittersCanvas.SPL_SOUTH) == SplittersCanvas.SPL_SOUTH) {
      int y = 0;
      if (rects[DockingConst.DK_NORTH] != null) {
        y = rects[DockingConst.DK_NORTH].y + SPLITTER_SIZE;
      }
      rslt.y = Math.min(Math.max(rslt.y, y), paneSize.height - SPLITTER_SIZE);
    }
    if ((curResizeDocks & SplittersCanvas.SPL_WEST) == SplittersCanvas.SPL_WEST) {
      int x = paneSize.width;
      if (rects[DockingConst.DK_EAST] != null) {
        x = rects[DockingConst.DK_EAST].x;
      }
      rslt.x = Math.min(Math.max(rslt.x, 0), x - SPLITTER_SIZE);
    }
    if ((curResizeDocks & SplittersCanvas.SPL_EAST) == SplittersCanvas.SPL_EAST) {
      int x = 0;
      if (rects[DockingConst.DK_WEST] != null) {
        x = rects[DockingConst.DK_WEST].x + SPLITTER_SIZE;
      }
      rslt.x = Math.min(Math.max(rslt.x, x), paneSize.width - SPLITTER_SIZE);
    }

    return rslt;
  }

  /**
   * ?????????? ??? ????????? ?????? ?????, ????????
   * ??? ??????? ?? ????????? ???? ??? ??? ?????? ?????? ?? ??????? ??????
   */
  public void requestActive(BoatEmpty boat) {
    logDebug("requestActive(" + boat.getTitle() + ")");
    if (!boat.isActive()) {
      logDebug("    ----> do active");
      if (boat.isSlide()) {
        Rectangle bounds = DmUtils.getAdjustedSlideBounds(boat, getDockingPane().getSize());
        setBoatBoundsSafe(boat, bounds);
      }
      setBoatVisible(boat, true);
      setBoatActive(boat);
      hideUnpinnedBoats();
      adjustDocksBounds();
    }
  }

  public void requestVisible(BoatEmpty boat) {
    logDebug("requestVisible(" + boat.getTitle() + ")");
    if (boat.isSlide()) {
      Rectangle bounds = DmUtils.getAdjustedSlideBounds(boat, getDockingPane().getSize());
      setBoatBoundsSafe(boat, bounds);
    }
    setBoatVisible(boat, true);
    adjustDocksBounds();
  }

  public void requestInvisible(BoatEmpty boat) {
    logDebug("requestInvisible(" + boat.getTitle() + ")");
    setBoatVisible(boat, false);
    if ((activeBoat == null || activeBoat == boat) && centerBoat != null) {
      setBoatActive(centerBoat);
    }
    adjustDocksBounds();
  }

  /**
   * ?????????? ??? ????????? ????????? ???? (????????
   * ? ????????? ????).
   */
  public void requestBehaviour(BoatEmpty boat, int bhv) {
    int curBhv = boat.getBehaviour();
    if (curBhv == bhv) {
      return;
    }
    saveRequestBhv(boat);
    setRequestBhv(boat, bhv);
    if (curBhv == BoatEmpty.BST_DOCK || bhv == BoatEmpty.BST_DOCK) {
      adjustDocksBounds();
    }
  }

  private void saveRequestBhv(BoatEmpty boat) {
    int curBhv = boat.getBehaviour();
    int location = boat.getDockLocation();
    Rectangle bounds = boat.getBounds();
    if (curBhv == BoatEmpty.BST_DOCK) {
      if (visibleDockBoats[location] == boat) {
        visibleDockBoats[location] = null;
      }
      boat.setDockSize(
          location == DockingConst.DK_WEST || location == DockingConst.DK_EAST ?
          bounds.width : bounds.height);
    } else if (curBhv == BoatEmpty.BST_FLOAT) {
      boat.setFloatBounds(bounds);
    } else if (curBhv == BoatEmpty.BST_SLIDE) {
      Insets dki = DmUtils.getInsetsForBehaviour(boat, BoatEmpty.BST_DOCK);
      Insets sli = DmUtils.getInsetsForBehaviour(boat, BoatEmpty.BST_SLIDE);
      int ds = location == DockingConst.DK_WEST || location == DockingConst.DK_EAST ?
          bounds.width - sli.left - sli.right + dki.left + dki.right :
          bounds.height - sli.top - sli.bottom + dki.top + dki.bottom;
      boat.setDockSize(ds);
    }
  }

  private void setRequestBhv(BoatEmpty boat, int bhv) {
    int location = boat.getDockLocation();
    Rectangle bounds = null;
    if (bhv == BoatEmpty.BST_DOCK) {
      if (boat.isVisible()) {
        if (visibleDockBoats[location] != null) {
          servBoatVisible(visibleDockBoats[location], false);
        }
        visibleDockBoats[location] = boat;
      }
    } else if (bhv == BoatEmpty.BST_FLOAT) {
      bounds = boat.getFloatBounds();
    } else if (bhv == BoatEmpty.BST_SLIDE) {
      bounds = DmUtils.getAdjustedSlideBounds(boat, getDockingPane().getSize());
    }
    setBoatBoundsSafe(boat, bounds);
    boat.setBehaviour(bhv);
    DmUtils.adjustBoatBordersAndMovable(getDockingPane(), boat);
    if (bhv == BoatEmpty.BST_FLOAT && activeBoat == boat) {
      getDockingPane().moveToFront(activeBoat);
    }
    boat.updateButtons();
  }

  public void requestPin(BoatEmpty boat, boolean pin) {
    boat.setPin(pin);
    boat.updatePinIcon();
  }

  public void actionPerformed(ActionEvent e) {
    Object o = e.getSource();
    if (o instanceof ZSideButton) {
      ZSideButton btn = (ZSideButton) o;
      BoatEmpty boat = btnToBoat.get(btn);
      if (boat != null) {
        if (btn.isSelected()) {
          requestActive(boat);
        } else {
          requestInvisible(boat);
        }
      }
    }
  }

  private boolean disposeInProcess = false;

  public void startMoveBoat(BoatEmpty boat) {
    disposeInProcess = true;
    getDragCanvas().setVisible(true);
    getDragCanvas().setCursor(DmUtils.getCursorForDragType(DockingConst.DRAG_MV));
  }

  public void executeMoveBoat(BoatEmpty boat, Rectangle r) {
    Rectangle rt = adjustBoatBounds(boat, r, DockingConst.DRAG_MV);
    drawRect(boat, rt, DockingConst.DRAG_MV);
  }

  public void applyMoveBoat(BoatEmpty boat, Rectangle r) {
    if (disposeInProcess) {
      Rectangle rt = adjustBoatBounds(boat, r, DockingConst.DRAG_MV);
      boat.setBounds(rt);
      cancelWhatever();
    }
  }

  public void cancelMoveBoat(BoatEmpty boat) {
    cancelWhatever();
  }

  public void startResizeBoat(BoatEmpty boat, int resizeType) {
    disposeInProcess = true;
    curResizeType = resizeType;
    getDragCanvas().setVisible(true);
    getDragCanvas().setCursor(DmUtils.getCursorForDragType(resizeType));
  }

  public void executeResizeBoat(BoatEmpty boat, Rectangle r) {
    Rectangle rt = adjustBoatBounds(boat, r, curResizeType);
    drawRect(boat, rt, 0);
  }

  public void applyResizeBoat(BoatEmpty boat, Rectangle r) {
    if (disposeInProcess) {
      Rectangle rt = adjustBoatBounds(boat, r, curResizeType);
      setBoatBoundsSafe(boat, rt);
      if (boat.isSlide()) {
        int location = boat.getDockLocation();

        Insets dki = DmUtils.getInsetsForBehaviour(boat, BoatEmpty.BST_DOCK);
        Insets sli = DmUtils.getInsetsForBehaviour(boat, BoatEmpty.BST_SLIDE);
        int ds = location == DockingConst.DK_WEST || location == DockingConst.DK_EAST ?
            rt.width - sli.left - sli.right + dki.left + dki.right :
            rt.height - sli.top - sli.bottom + dki.top + dki.bottom;
        boat.setDockSize(ds);
      }
      cancelWhatever();
    }
  }

  public void cancelResizeBoat(BoatEmpty boat) {
    cancelWhatever();
  }

  Rectangle[] rects;

  public void startResizeDock(int resizeType, int resizeDocks) {
    disposeInProcess = true;
    curResizeType = resizeType;
    curResizeDocks = resizeDocks;
    rects = getSplittersRect();
    getDragCanvas().setVisible(true);
    getDragCanvas().setCursor(DmUtils.getCursorForDragType(resizeType));
  }

  public void executeResizeDock(int dockMask, Point p) {
    Point np = adjustDocksSplitters(p);
    Rectangle vr = null;
    Rectangle hr = null;
    if ((dockMask & SplittersCanvas.SPL_WEST) != 0 && rects[DockingConst.DK_WEST] != null) {
      vr = new Rectangle(rects[DockingConst.DK_WEST]);
      vr.x = np.x;
    }
    if ((dockMask & SplittersCanvas.SPL_NORTH) != 0 && rects[DockingConst.DK_NORTH] != null) {
      hr = new Rectangle(rects[DockingConst.DK_NORTH]);
      hr.y = np.y;
    }
    if ((dockMask & SplittersCanvas.SPL_SOUTH) != 0 && rects[DockingConst.DK_SOUTH] != null) {
      hr = new Rectangle(rects[DockingConst.DK_SOUTH]);
      hr.y = np.y;
    }
    if ((dockMask & SplittersCanvas.SPL_EAST) != 0 && rects[DockingConst.DK_EAST] != null) {
      vr = new Rectangle(rects[DockingConst.DK_EAST]);
      vr.x = np.x;
    }
    drawSplitters(vr, hr);
  }

  private void drawSplitters(Rectangle vr, Rectangle hr) {
    painterMode = PainterMode.DOCK_SPLITTERS;
    vspRect = vr;
    hspRect = hr;
    getDragCanvas().repaint();
  }

  public void printBoatsInfo() {
    if (log.isDebugEnabled()) {
      log.debug("--==## boats ##==--");
      for (BoatEmpty boat : boats) {
        log.debug("" + boat.getTitle() + " : " + boat.getDockSizes());
      }
    }
  }

  public void applyResizeDock(int dockMask, Point p) {
    if (disposeInProcess) {
      Point np = adjustDocksSplitters(p);
      Dimension paneSize = getDockingPane().getSize();
      if ((dockMask & SplittersCanvas.SPL_WEST) != 0 && rects[DockingConst.DK_WEST] != null) {
        visibleDockBoats[DockingConst.DK_WEST].setDockSize(np.x);
      }
      if ((dockMask & SplittersCanvas.SPL_EAST) != 0 && rects[DockingConst.DK_EAST] != null) {
        visibleDockBoats[DockingConst.DK_EAST].setDockSize(
            paneSize.width - np.x - SPLITTER_SIZE);
      }
      if ((dockMask & SplittersCanvas.SPL_NORTH) != 0 && rects[DockingConst.DK_NORTH] != null) {
        visibleDockBoats[DockingConst.DK_NORTH].setDockSize(np.y);
      }
      if ((dockMask & SplittersCanvas.SPL_SOUTH) != 0 && rects[DockingConst.DK_SOUTH] != null) {
        visibleDockBoats[DockingConst.DK_SOUTH].setDockSize(
            paneSize.height - np.y - SPLITTER_SIZE);
      }
      cancelWhatever();
    }
  }

  public void cancelResizeDock(int dockMask) {
    cancelWhatever();
  }

  public void cancelWhatever() {
    if (disposeInProcess) {
      curResizeType = 0;
      curResizeDocks = 0;
      vspRect = null;
      hspRect = null;
      prevRect = null;
      lastRect = null;
      getDragCanvas().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
      getDragCanvas().setVisible(false);
      disposeInProcess = false;
    }
  }

  public JPopupMenu getContextMenu(Object context) {
    BoatEmpty boat = null;
    if (context instanceof ZSideButton) {
      boat = btnToBoat.get(context);
    } else if(context instanceof BoatTitle) {
      boat = ((BoatTitle) context).getBoat();
    }
    return boat != null ? createContextMenuForBoat(boat) : null;
  }

  private JPopupMenu createContextMenuForBoat(BoatEmpty boat) {
    return DockPopupMenu.createPopupMenu(this, boat);
  }

  private static void logDebug(String s) {
    if (log.isDebugEnabled()) {
      log.debug(s);
    }
  }
}
