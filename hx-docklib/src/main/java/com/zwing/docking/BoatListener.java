package com.zwing.docking;

/**
 * Реакция на действия пользователя
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface BoatListener {
    /**
     * изменение поведения окна: BST_CENTER, BST_DOCK, BST_SLIDE, BST_FLOAT
     */
    void requestBehaviour(BoatEmpty b, int bhv);
    /**
     * Разрешает/запрещает автоматическое скрытие окна при потере фокуса.
     */
    void requestPin(BoatEmpty b, boolean pin);
    /**
     * Делает окно активным. Если окно невидимо, то ещё и делает окно видимым.
     */
    void requestActive(BoatEmpty b);
    /**
     * Делает окно видимым, активность окна не меняется.
     */
    void requestVisible(BoatEmpty b);
    /**
     * Делает окно невидимым. Если окно было активным, то активность
     * переключается на другое окно (центральное).
     */
    void requestInvisible(BoatEmpty b);
}
