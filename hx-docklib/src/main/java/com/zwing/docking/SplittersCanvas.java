package com.zwing.docking;

import com.zwing.utils.ZUIManager;

import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

/**
 * Created: 13.11.2006 10:48:40
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class SplittersCanvas extends JComponent implements
    MouseListener, MouseMotionListener {

  private DockManager dm;
  private Disposer disposer;

  public static final int SPL_NORTH = 0x1;
  public static final int SPL_WEST  = 0x2;
  public static final int SPL_SOUTH = 0x4;
  public static final int SPL_EAST  = 0x8;

  private static final int SPLITTER_INSET = 2;
  private static final int STROKE_STEP = 3;

  private static final int INT_UNDEF = -28532;
  private static final Point POINT_UNDEF = new Point(INT_UNDEF, INT_UNDEF);

  private BufferedImage hTexture;
  private BufferedImage vTexture;

  private static final int DRAG_SENSITIVITY = 4;

  private Point clickPoint;
  private int resizeType;
  private int resizeDocks;
  private boolean pressed;
  private boolean dragMode;

  public SplittersCanvas() {
    clickPoint = POINT_UNDEF;
    resizeType = 0;
    resizeDocks = 0;
    pressed = false;
    dragMode = false;
    init();
  }

  protected void init() {
    setLayout(null);

    UIDefaults ud = UIManager.getLookAndFeel().getDefaults();
    Color cBg = ud.getColor("Button.shadow");
    setBackground(cBg);

    createTexture(cBg);

    addMouseListener(this);
    addMouseMotionListener(this);

    this.setFocusable(false);
    this.setEnabled(true);
  }

  private static final int TEXTURE_SIZE = 3000;

  private void createTexture(Color c) {
    int h = DockManager.SPLITTER_SIZE - SPLITTER_INSET - SPLITTER_INSET;

    int ic = c.getRGB();
    hTexture = new BufferedImage(TEXTURE_SIZE, h, BufferedImage.TYPE_INT_ARGB);
    Graphics g = hTexture.getGraphics();
    g.setColor(c);
    for (int y = 0; y < h; y++) {
      for (int x = STROKE_STEP - 1 - y % STROKE_STEP; x < TEXTURE_SIZE; x += STROKE_STEP) {
        hTexture.setRGB(x, y, ic);
      }
    }

    vTexture = new BufferedImage(h, TEXTURE_SIZE, BufferedImage.TYPE_INT_ARGB);
    g = vTexture.getGraphics();
    g.setColor(c);
    for (int x = 0; x < h; x++) {
      for (int y = STROKE_STEP - 1 - x % STROKE_STEP; y < TEXTURE_SIZE; y += STROKE_STEP) {
        vTexture.setRGB(x, y, ic);
      }
    }
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    paintSplitters(g);
  }

  protected void paintSplitters(Graphics g) {
    Rectangle[] rcs = dm.getSplittersRect();
    Color highlight = (Color) ZUIManager.getProperty("SplitPane.highlight");
    Color background = (Color) ZUIManager.getProperty("SplitPane.background");
    Color shadow = (Color) ZUIManager.getProperty("SplitPane.shadow");

    Rectangle rn = rcs[DockingConst.DK_NORTH];
    Rectangle rs = rcs[DockingConst.DK_SOUTH];
    Rectangle rw = rcs[DockingConst.DK_WEST];
    Rectangle re = rcs[DockingConst.DK_EAST];

    drawHorizontal(g, rn, background, highlight, shadow);
    drawHorizontal(g, rs, background, highlight, shadow);
    drawVertical(g, rw, rn, rs, background, highlight, shadow);
    drawVertical(g, re, rn, rs, background, highlight, shadow);
  }

  private void drawHorizontal(Graphics g, Rectangle r, Color background, Color highlight, Color shadow) {
    if (r != null) {
      g.setColor(background);
      g.fillRect(r.x, r.y, r.width, r.height);
      g.drawImage(hTexture,
          r.x, r.y + SPLITTER_INSET, r.x + r.width,
          r.y + SPLITTER_INSET + hTexture.getHeight(),
          0, 0, r.width, hTexture.getHeight(),
          this);
      g.setColor(highlight);
      g.fillRect(r.x, r.y, r.width, 1);
      g.setColor(shadow);
      g.fillRect(r.x, r.y + r.height - 1, r.width, 1);
    }
  }

  private void drawVertical(Graphics g, Rectangle r, Rectangle rn, Rectangle rs,
      Color background, Color highlight, Color shadow) {
    if (r != null) {
      int y00 = r.y;
      int y01 = y00;
      int y10 = r.y + r.height;
      int y11 = y10;
      if (rn != null) {
        y00 = rn.y + rn.height - 1;
        y01 = rn.y + rn.height;
      }
      if (rs != null) {
        y10 = rs.y + 1;
        y11 = rs.y;
      }
      g.setColor(background);
      g.fillRect(r.x + 1, y00, r.width - 2, y10 - y00);
      g.drawImage(vTexture,
          r.x + SPLITTER_INSET, y00, r.x + SPLITTER_INSET + vTexture.getWidth(), y10,
          0, 0, vTexture.getWidth(), y10 - y00, this);
      g.setColor(highlight);
      g.fillRect(r.x, y00, 1, y10 - y00);
      g.setColor(shadow);
      g.fillRect(r.x + r.width - 1, y01, r.width, y11 - y01);
    }
  }

  private void updateCursor() {
    updateCursor(getParent().getMousePosition());
  }

  private void updateCursor(final Point p) {
    Cursor c = DmUtils.getCursorForDragType(calcResizeType(p));
    setCursor(c);
  }

  public static final int JUST1 = 1;
  public static final int JUST2 = 2;
  public static final int JUST4 = 4;
  public static final int JUST8 = 8;

  private static final int[] RESIZE_TYPES= new int[] {
    DockingConst.DRAG_NO,  DockingConst.DRAG_SV,  //  0  1
    DockingConst.DRAG_SV,  DockingConst.DRAG_NO,  //  2  3
    DockingConst.DRAG_SH,  DockingConst.DRAG_SHV, //  4  5
    DockingConst.DRAG_SHV, DockingConst.DRAG_NO,  //  6  7
    DockingConst.DRAG_SH,  DockingConst.DRAG_SHV, //  8  9
    DockingConst.DRAG_SHV, DockingConst.DRAG_NO,  // 10 11
    DockingConst.DRAG_NO,  DockingConst.DRAG_NO,  // 12 13
    DockingConst.DRAG_NO,  DockingConst.DRAG_NO   // 14 15
  };

  private int calcResizeType(Point p) {
    int c = 0;
    if (p != null) {
      Rectangle[] rcs = dm.getSplittersRect();
      if (safeRectContains(rcs[DockingConst.DK_NORTH], p)) {
        c |= JUST4;
      }
      if (safeRectContains(rcs[DockingConst.DK_SOUTH], p)) {
        c |= JUST8;
      }
      if (c == 0 && safeRectContains(rcs[DockingConst.DK_WEST], p)) {
        c |= JUST1;
      }
      if (c == 0 && safeRectContains(rcs[DockingConst.DK_EAST], p)) {
        c |= JUST2;
      }
    }
    return RESIZE_TYPES[c];
  }

  private boolean safeRectContains(Rectangle rect, Point p) {
    return rect != null && p != null && rect.contains(p);
  }

  public void mouseClicked(MouseEvent e) {
  }

  public void mouseEntered(MouseEvent e) {
    updateCursor(e.getPoint());
  }

  public void mouseExited(MouseEvent e) {
    setCursor(null);
  }

  private Rectangle[] rects = null;
  private Point dockPoint = new Point();

  public void mousePressed(MouseEvent e) {
    clickPoint = e.getPoint();
    resizeType = calcResizeType(clickPoint);
    resizeDocks = 0;
    rects = dm.getSplittersRect();
    if (safeRectContains(rects[DockingConst.DK_NORTH], clickPoint)) {
      resizeDocks |= SPL_NORTH;
      dockPoint.y = rects[DockingConst.DK_NORTH].y;
    }
    if (safeRectContains(rects[DockingConst.DK_SOUTH], clickPoint)) {
      resizeDocks |= SPL_SOUTH;
      dockPoint.y = rects[DockingConst.DK_SOUTH].y;
    }
    if (resizeDocks == 0 && safeRectContains(rects[DockingConst.DK_WEST], clickPoint)) {
      resizeDocks |= SPL_WEST;
      dockPoint.x = rects[DockingConst.DK_WEST].x;
    }
    if (resizeDocks == 0 && safeRectContains(rects[DockingConst.DK_EAST], clickPoint)) {
      resizeDocks |= SPL_EAST;
      dockPoint.x = rects[DockingConst.DK_EAST].x;
    }
    pressed = resizeType != 0;
    dragMode = false;
  }

  public void mouseReleased(MouseEvent e) {
    int x = e.getPoint().x;
    int y = e.getPoint().y;
    int dx = x - clickPoint.x;
    int dy = y - clickPoint.y;
    Point rszPoint = new Point(dockPoint.x + dx, dockPoint.y + dy);
    disposer.applyResizeDock(resizeDocks, rszPoint);
    updateCursor();
    clickPoint = POINT_UNDEF;
    resizeType = 0;
    resizeDocks = 0;
  }

  public void mouseDragged(MouseEvent e) {
    int x = e.getPoint().x;
    int y = e.getPoint().y;
    if (pressed && !dragMode) {
      if (Math.abs(clickPoint.x - x) >= DRAG_SENSITIVITY ||
          Math.abs(clickPoint.y - y) >= DRAG_SENSITIVITY) {
        dragMode = true;
        disposer.startResizeDock(resizeType, resizeDocks);
      }
    }
    if (dragMode) {
      int dx = x - clickPoint.x;
      int dy = y - clickPoint.y;
      Point rszPoint = new Point(dockPoint.x + dx, dockPoint.y + dy);
      disposer.executeResizeDock(resizeDocks, rszPoint);
    }
  }

  public void mouseMoved(MouseEvent e) {
    updateCursor(e.getPoint());
  }

  public void setDockManager(DockManager dm) {
    this.dm = dm;
  }

  public void setDisposer(Disposer disposer) {
    this.disposer = disposer;
  }
}
