package com.zwing.docking;

import org.apache.log4j.Logger;

import java.awt.KeyboardFocusManager;
import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created: 28.08.2006 18:46:55
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class FocusTracker implements PropertyChangeListener {

  private static final Logger log = Logger.getLogger(FocusTracker.class);

  private DockManager dm;

  private static final String PERMANENT_FOCUS_OWNER = "permanentFocusOwner";

  private KeyboardFocusManager focusManager;

  public FocusTracker(DockManager dm) {
    this.dm = dm;
    focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
    focusManager.addPropertyChangeListener(PERMANENT_FOCUS_OWNER, this);
  }

  public void adjustFocus() {
    dbg("adjustFocus");
    BoatEmpty boat = null;
    Component c = focusManager.getPermanentFocusOwner();
    dbg("    ----> " + c);
    if (c instanceof BoatEmpty) {
      boat = (BoatEmpty) c;
    }
    if (c != null) {
      for (Container p = c.getParent(); p != null && boat == null; p = p.getParent()) {
        if (p instanceof BoatEmpty) {
          boat = (BoatEmpty) p;
        }
      }
    }
    if (boat != null) {
      dbg("    ----> " + boat.getTitle());
      if (boat.getLastFocusedComponent() != c) {
        boat.setLastFocusedComponent(c);
      }
      dm.requestActive(boat);
    }
  }

  private void dbg(String s) {
    if (log.isDebugEnabled()) {
      log.debug(s);
    }
  }

  public void start(){
    focusManager.addPropertyChangeListener(PERMANENT_FOCUS_OWNER, this);
    adjustFocus();
  }

  public void stop(){
    focusManager.removePropertyChangeListener(PERMANENT_FOCUS_OWNER, this);
  }

  public void propertyChange(PropertyChangeEvent evt) {
    adjustFocus();
  }
}
