package com.zwing.docking;

import java.awt.Rectangle;
import java.awt.Point;

/**
 * Created: 18.08.2006 19:09:04
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Disposer {
    /**
     * ?????? ??????????? float ????
     * @param boat float ????
     */
    void startMoveBoat(BoatEmpty boat);

    /**
     * ??????? ??????????? float ????
     * @param boat float ????
     * @param r ???????? ?????????? ????
     */
    void executeMoveBoat(BoatEmpty boat, Rectangle r);

    /**
     * ??????????? ??????????? float ????
     * @param boat float ????
     * @param r ???????? ?????????? ????
     */
    void applyMoveBoat(BoatEmpty boat, Rectangle r);

    /**
     * ???????? ??????????? float ????
     * @param boat float ????
     */
    void cancelMoveBoat(BoatEmpty boat);

    /**
     * ?????? ????????? ???????? float ??? slide ????
     * @param boat float ??? slide ????
     * @param resizeType DockingConst.DRAG_XXX
     */
    void startResizeBoat(BoatEmpty boat, int resizeType);

    /**
     * ??????? ????????? ???????? float ??? slide ????
     * @param boat float ??? slide ????
     * @param r ???????? ?????????? ????
     */
    void executeResizeBoat(BoatEmpty boat, Rectangle r);

    /**
     * ??????????? ????????? ???????? float ??? slide ????
     * @param boat float ??? slide ????
     * @param r ???????? ?????????? ????
     */
    void applyResizeBoat(BoatEmpty boat, Rectangle r);

    /**
     * ???????? ????????? ???????? float ??? slide ????
     * @param boat float ??? slide ????
     */
    void cancelResizeBoat(BoatEmpty boat);

    /**
     * ?????? ????????? ???????? dock ????
     * @param resizeType <code>DRAG_SV || DRAG_SH || DRAG_SHV</code>
     * @param resizeDocks <code>SPL_XXX (| SPL_XXX)+</code>
     */
    void startResizeDock(int resizeType, int resizeDocks);

    /**
     * ??????? ????????? ???????? dock ????
     * @param resizeDocks <code>SPL_XXX (| SPL_XXX)+</code>
     * @param p ??????? ???????? ???????????
     */
    void executeResizeDock(int resizeDocks, Point p);

    /**
     * ??????????? ????????? ???????? dock ????
     * @param resizeDocks <code>SPL_XXX (| SPL_XXX)+</code>
     * @param p ??????? ???????? ???????????
     */
    void applyResizeDock(int resizeDocks, Point p);

    /**
     * ???????? ????????? ???????? dock ????
     * @param resizeDocks <code>SPL_XXX (| SPL_XXX)+</code>
     */
    void cancelResizeDock(int resizeDocks);

    /**
     * ???????? ????? ??????? ???????? ??? ??????
     */
    void cancelWhatever();

}
