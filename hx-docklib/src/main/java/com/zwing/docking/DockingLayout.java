package com.zwing.docking;

import javax.swing.Icon;
import java.awt.Image;

/**
 * Экземпляры этого класса определяют способы расположения окон.<br>
 * <br>
 * Created: 11.09.2006 19:12:49
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface DockingLayout {
    //
    // методы для идентификации экземпляра DockingLayout в приложении
    //

    /**
     * Имя способа расположения
     * @return
     */
    String getUniqueName(); // имя должно быть уникальным

    /**
     * Схематическое изображение способа расположения
     * @return картинка
     */
    Image getOutline();

    /**
     * Иконка, например, для пункта меню
     * @return иконка
     */
    Icon getIcon();

    // методы определяющие функциональность DockingLayout
}
