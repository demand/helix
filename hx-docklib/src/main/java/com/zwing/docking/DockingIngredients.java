package com.zwing.docking;

import com.zwing.DockingCargo;

/**
 * Created: 18.09.2006 12:42:18
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockingIngredients {
    private DockingCargo[] cargos;
    private DockingPlacement[] placements;
    private int currentPlacement;

    public DockingIngredients() {
    }

    public DockingCargo[] getCargos() {
        return cargos;
    }

    public void setCargos(DockingCargo[] cargos) {
        this.cargos = cargos;
    }

    public DockingPlacement[] getPlacements() {
        return placements;
    }

    public void setPlacements(DockingPlacement[] placements) {
        this.placements = placements;
    }

    public int getCurrentPlacement() {
        return currentPlacement;
    }

    public void setCurrentPlacement(int currentPlacement) {
        this.currentPlacement = currentPlacement;
    }


}
