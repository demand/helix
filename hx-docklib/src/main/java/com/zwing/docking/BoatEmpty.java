package com.zwing.docking;

import com.zwing.DockingCargo;
import com.zwing.utils.ZUIManager;

import javax.accessibility.Accessible;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * BoatEmpty
 *
 * @author А.Дёмин
 * @since 06.12.2004
 */
public class BoatEmpty extends JComponent
    implements Boatable, MouseListener,
    MouseMotionListener, Accessible {

  /**
   * Координаты окна для режима FLOAT.
   */
  private Rectangle floatBounds;

  /**
   * Точный размер окна в пикселах для режимов DOCK или SLIDE
   * При размещении окна сверху или снизу, используется поле height.
   * При размещении окна слева или справа, используется поле width.
   */
  private Dimension dockSizes;

  private static final Dimension DEFAULT_DOCK_SIZES = new Dimension(100, 100);

  /**
   * Использовать проценты для определения границ окна
   */
  private boolean usePercents;

  private Point clickPoint = null;

  private Disposer disposer;

  private boolean pressed;
  private boolean dragMode;
  private static final int DRAG_SENSITIVITY = 4;
  private static final int CORNER_SNAP_SIZE = 12;
  private int resizeType;

  private static final int BORDER_SIZE = 4;

  private boolean active;

  /**
   * @see DockingConst#DK_WEST
   * @see DockingConst#DK_EAST
   * @see DockingConst#DK_SOUTH
   * @see DockingConst#DK_CENTER
   * @see DockingConst#DK_NORTH
   */
  private int dock;

  /**
   * Возможное расположение бокового окна (boat):<br>
   * <b>center</b> - центральное окно, чаще всего это окно
   *            текстового редактора. Вокруг него
   *            располагаются dock-boat, а поверх -
   *            slide-boat и float-boat.<br>
   * <b>dock</b> - Окно занимает своё зарезервированное место
   *            сбоку от центрального окна. В любой момент
   *            времени с каждой стороны может быть открыто
   *            не более одного dock-boat.<br>
   * <b>slide</b> - Окно выезжает поверх центрального окна и
   *            dock-boat. Это окно может быть открыто
   *            только пока фокус не получит другое center,
   *            dock или slide окно. То есть пока фокус на
   *            нём или на другом float-boat.<br>
   * <b>float</b> - Окно представляет собой обычное окно с
   *            заголовком и располагается поверх center,
   *            dock и slide окон. Пользователь может
   *            произвольно изменять размеры и положение
   *            окна. В любой момент может быть открыто
   *            произвольное количество float-boat.<br>
   * Опция pin актуальна для dock и float окон. Если она
   * выключена для данного окна, то окно автоматически
   * пропадает (закрывается) при потере фокуса. Если опция
   * опция pin включена, то float-boat остаётся открытым
   * пока пользователь сам не закроет его. Если опция
   * включена у dock-boat, окно остаётся видимым пока
   * пользователь его не закроет сам или пока не будет
   * открыто другое dock-boat с этой же стороны.
   */
  private int avBehaviours;

  /**
   *
   */
  public static final int BST_MASK_BEHAVIOUR = 0x000F;
  /**
   * Поведение окна - center-boat (0)
   */
  public static final int BST_CENTER = 0x0001;
  /**
   * Поведение окна - dock-boat (1)
   */
  public static final int BST_DOCK = 0x0002;
  /**
   * Поведение окна - slide-boat (2)
   */
  public static final int BST_SLIDE = 0x0004;
  /**
   * Поведение окна - float-boat (0)
   */
  public static final int BST_FLOAT = 0x0008;
  /**
   * Скрывается окно автоматически при потере активности.
   */
  public static final int BST_PIN = 0x0010;
  /**
   * Может ли пользователь перемещать окна "схватив" мышью за заголовок
   */
  public static final int BST_MOVABLE = 0x0020;
  /**
   * Какие границы окна можно использовать для изменения его размеров
   */
  public static final int BST_RESIZABLE_NORTH = 0x0100;
  public static final int BST_RESIZABLE_SOUTH = 0x0200;
  public static final int BST_RESIZABLE_WEST  = 0x0400;
  public static final int BST_RESIZABLE_EAST  = 0x0800;
  public static final int BST_RESIZABLE_ALL   = 0x0F00;
  public static final int BST_RESIZABLE_NONE  = 0x0000;

  public static final int IV_100 = 100;
  public static final int IV_110 = 110;
  public static final int IV_200 = 200;

  private int behaviour;

  private String title;

  private BoatListener boatListener;

  protected Insets insets;
  private BoatTitle cTitle;
  private JComponent panel;
  private DockingCargo cargo;
  private ImageIcon icon;

  private Component lastFocusedComponent;

  public BoatEmpty(int avBehaviours, int behaviour, String title) {
    super();
    this.behaviour = BST_RESIZABLE_ALL | BST_MOVABLE | BST_PIN;
    setAvailableBehaviours(avBehaviours);
    setBehaviour(behaviour);

    pressed = false;
    dragMode = false;
    resizeType = 0;
    usePercents = false;

    dockSizes = new Dimension(DEFAULT_DOCK_SIZES);

    insets = new Insets(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE);

    Container c = this;
    c.setLayout(new BorderLayout());
    setTitle(title);
    panel = new JPanel();
    add(panel, BorderLayout.CENTER);

    setOpaque(false);

    addMouseListener(this);
    addMouseMotionListener(this);
    setRequestFocusEnabled(true);
    setFocusable(true);
    setFocusCycleRoot(true);
    setEnabled(true);

    int titleHeight = (Integer) ZUIManager.getProperty("boatTitle.height");
    Insets ist = getInsets();
    setMinimumSize(new Dimension(IV_110, titleHeight + ist.top + ist.bottom));
    //    setMaximumSize(new Dimension(2000,300));
    setPreferredSize(new Dimension(IV_200, IV_200));

    Dimension sz = getPreferredSize();
    floatBounds = new Rectangle(0, 0, sz.width, sz.height);
    lastFocusedComponent = null;
  }

  public void setWorkAreaComponent(JComponent c) {
    if (panel != null) {
      remove(panel);
    }
    panel = c;
    add(panel, BorderLayout.CENTER);
    panel.setFocusCycleRoot(true);
    validate();
  }

  public JComponent getWorkAreaComponent(JComponent c) {
    return panel;
  }

  public void setCargo(DockingCargo cargo) {
    if (panel != null) {
      remove(panel);
    }
    panel = null;
    if (cargo != null) {
      panel = cargo.getVisualComponent();
      add(panel, BorderLayout.CENTER);
      panel.setFocusCycleRoot(true);
    }
    this.cargo = cargo;
    validate();
  }

  public DockingCargo getCargo() {
    return cargo;
  }

  public int getPreferredDockSize() {
    return dock == DockingConst.DK_WEST || dock == DockingConst.DK_EAST ?
        DEFAULT_DOCK_SIZES.width : DEFAULT_DOCK_SIZES.height;
  }

  public boolean mustHideOnInactive() {
    return isSlide() || !(isPin() || isCenter());
  }

  private int testBehaviour(int b) throws IllegalArgumentException {
    // b должно быть одним из: BST_CENTER, BST_DOCK,
    // BST_SLIDE, BST_FLOAT. То есть ровно один из
    // младших 4-х бит должен быть взведён
    int rslt = b & BST_MASK_BEHAVIOUR;
    if (rslt == 0 || (rslt & (rslt-1)) != 0) {
      throw new IllegalArgumentException();
    }
    return rslt;
  }

  public void setAvailableBehaviours(int av) {
    avBehaviours = av & BST_MASK_BEHAVIOUR;
  }

  public int getAvailableBehaviours() {
    return avBehaviours & BST_MASK_BEHAVIOUR;
  }

  /**
   * @param b маска запрашиваемых способов поведения окна
   * @return true, если все запрашиваемые способы разрешены
   */
  public boolean isAvailableBehaviours(int b) {
    int tb = b & BST_MASK_BEHAVIOUR;
    return (avBehaviours & tb) == tb;
  }

  public void setBehaviour(int b) {
    int tb = testBehaviour(b);
    if (isAvailableBehaviours(tb)) {
      behaviour = (behaviour & ~BST_MASK_BEHAVIOUR) | tb;
    } else {
      throw new IllegalArgumentException();
    }
  }

  public int getBehaviour() {
    return behaviour & BST_MASK_BEHAVIOUR;
  }

  public boolean isCenter() {
    return getBehaviour() == BST_CENTER;
  }

  public boolean isDock() {
    return getBehaviour() == BST_DOCK;
  }

  public boolean isSlide() {
    return getBehaviour() == BST_SLIDE;
  }

  public boolean isFloat() {
    return getBehaviour() == BST_FLOAT;
  }

  public boolean isPin() {
    return (behaviour & BST_PIN) == BST_PIN;
  }

  public void setPin(boolean pin) {
    if (pin) {
      behaviour |= BST_PIN;
    } else {
      behaviour &= ~BST_PIN;
    }
  }

  public boolean hasTitle() {
    return title != null;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
    if (title == null) {
      if (cTitle != null) {
        remove(cTitle);
      }
      cTitle = null;
    } else {
      if (cTitle == null) {
        cTitle = new BoatTitle(this, title);
        cTitle.setVisible(true);
        add(cTitle, BorderLayout.NORTH);
      } else {
        cTitle.repaint();
      }
    }
  }

  public int getResizable() {
    return (behaviour & BST_RESIZABLE_ALL);
  }

  public void setResizable(int resizable) {
    behaviour =
        (behaviour & ~BST_RESIZABLE_ALL) |
            (resizable & BST_RESIZABLE_ALL);
  }

  public boolean isMovable() {
    return (behaviour & BST_MOVABLE) == BST_MOVABLE;
  }

  public void setMovable(boolean movable) {
    if (movable) {
      behaviour |= BST_MOVABLE;
    } else {
      behaviour &= ~BST_MOVABLE;
    }
  }

//    public DockManager getManager() {
//        return manager;
//    }
//
//    public void setManager(DockManager m) {
//        if(manager != null) {
//            manager.remove(this);
//        }
//        manager = m;
//        if (manager != null) {
//            manager.add(this, location);
//        }
//    }
//

  public void setDockLocation(int loc) {
    dock = loc;
  }

  public void adjustByPercent(Rectangle rect) {
    //    rect.
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    Dimension sz = getSize();

    Color bg = (Color) ZUIManager.getProperty("Panel.background");
//        bg = new Color((bg.getRGB() & 0xFFFFFF) | 0xA0000000, true);
    g.setColor(bg);
    g.fillRect(0, 0, sz.width, sz.height);

    Color borderColor = (Color) ZUIManager.getProperty("boat.borderColor");
//        borderColor = new Color((borderColor.getRGB() & 0xFFFFFF) | 0x60000000, true);

    Insets ins = getInsets();
    g2.setColor(borderColor);

    drawLeftBorder(g2, ins, sz);
    drawTopBorder(g2, ins, sz);
    drawBottomBorder(g2, ins, sz);
    drawRightBorder(g2, ins, sz);

    g2.drawRect(ins.left, ins.top,
        sz.width - ins.left - ins.right - 1,
        sz.height - ins.top - ins.bottom - 1);
  }

  protected void paintChildren(Graphics g) {
    super.paintChildren(g);
    Color borderColor = (Color) ZUIManager.getProperty("boat.borderColor");
    g.setColor(borderColor);
    Dimension sz = getSize();
    g.drawRect(0, 0, sz.width - 1, sz.height - 1);
  }

  private void drawLeftBorder(Graphics2D g2, Insets ins, Dimension sz) {
    if (ins.left > 0) {
      // левая внешняя граница
      g2.fillRect(0, 0, 1, sz.height);
      if (ins.left > 1) {
        // левая внутренняя граница
        g2.fillRect(ins.left, ins.top, 1, sz.height - ins.top - ins.bottom);
        if ((behaviour & BST_RESIZABLE_WEST) == BST_RESIZABLE_WEST) {
          // гориз. лев. верхн.
          if ((behaviour & BST_RESIZABLE_NORTH) == BST_RESIZABLE_NORTH) {
            g2.fillRect(1, CORNER_SNAP_SIZE, insets.left-1, 1);
          }
          // гориз. лев. нижн.
          if ((behaviour & BST_RESIZABLE_SOUTH) == BST_RESIZABLE_SOUTH) {
            g2.fillRect(1, sz.height-CORNER_SNAP_SIZE -1, insets.left-1, 1);
          }
        }
      }
    }
  }

  private void drawTopBorder(Graphics2D g2, Insets ins, Dimension sz) {
    if (ins.top > 0) {
      // верхняя внешняя граница
      g2.fillRect(0, 0, sz.width, 1);
      if (ins.top > 1) {
        // верхняя внутреняя граница
        g2.fillRect(ins.left, ins.top, sz.width - ins.left - ins.right, 1);
        if ((behaviour & BST_RESIZABLE_NORTH) == BST_RESIZABLE_NORTH) {
          // верт. лев. верхн.
          if ((behaviour & BST_RESIZABLE_WEST) == BST_RESIZABLE_WEST) {
            g2.fillRect(CORNER_SNAP_SIZE, 1, 1, insets.top-1);
          }
          // верт. прав. верхн.
          if ((behaviour & BST_RESIZABLE_EAST) == BST_RESIZABLE_EAST) {
            g2.fillRect(sz.width-CORNER_SNAP_SIZE-1, 1, 1, insets.top-1);
          }
        }
      }
    }
  }

  private void drawBottomBorder(Graphics2D g2, Insets ins, Dimension sz) {
    if (ins.bottom > 0) {
      // нижняя внешняя граница
      g2.fillRect(0, sz.height-1, sz.width, 1);
      if (ins.bottom > 1) {
        // нижняя внутренняя граница
        g2.fillRect(ins.left, sz.height - ins.bottom - 1, sz.width - ins.left - ins.right, 1);
        if ((behaviour & BST_RESIZABLE_SOUTH) == BST_RESIZABLE_SOUTH) {
          // верт. лев. нижн.
          if ((behaviour & BST_RESIZABLE_WEST) == BST_RESIZABLE_WEST) {
            g2.fillRect(CORNER_SNAP_SIZE, sz.height-insets.bottom, 1, insets.bottom-1);
          }
          // верт. прав. нижн.
          if ((behaviour & BST_RESIZABLE_EAST) == BST_RESIZABLE_EAST) {
            g2.fillRect(sz.width-CORNER_SNAP_SIZE -1, sz.height-insets.bottom, 1, insets.bottom-1);
          }
        }
      }
    }
  }

  private void drawRightBorder(Graphics2D g2, Insets ins, Dimension sz) {
    if (ins.right > 0) {
      // правая внешняя граница
      g2.fillRect(sz.width-1, 0, 1, sz.height);
      if (ins.right > 1) {
        // правая внутренняя граница
        g2.fillRect(sz.width - ins.right - 1, ins.top, 1, sz.height - ins.top - ins.bottom);
        if ((behaviour & BST_RESIZABLE_EAST) == BST_RESIZABLE_EAST) {
          // гориз. прав. верхн.
          if ((behaviour & BST_RESIZABLE_NORTH) == BST_RESIZABLE_NORTH) {
            g2.fillRect(sz.width-insets.right, CORNER_SNAP_SIZE, insets.right-1, 1);
          }
          // гориз. прав. нижн.
          if ((behaviour & BST_RESIZABLE_SOUTH) == BST_RESIZABLE_SOUTH) {
            g2.fillRect(sz.width-insets.right, sz.height-CORNER_SNAP_SIZE-1, insets.right-1, 1);
          }
        }
      }
    }
  }

  public void paintBorder(Graphics g) {
  }

//    public void update(Graphics g) {
//        super.update(g);
//    }

  public void paint(Graphics g) {
    super.paint(g);
  }

  public Insets getInsets() {
    return insets;
  }

  public void mouseClicked(MouseEvent e) {
    if (boatListener != null) {
      boatListener.requestActive(this);
    }
  }

  public void mouseEntered(MouseEvent e) {
    updateCursor(e.getPoint());
  }

  public void mouseExited(MouseEvent e) {
    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
  }

  public void mousePressed(MouseEvent e) {
    clickPoint = e.getPoint();
    resizeType = calcResizeType(clickPoint);
    pressed = resizeType != 0;
    dragMode = false;
  }

  public void mouseReleased(MouseEvent e) {
    pressed = false;
    if (dragMode) {
      // переместить окно
      Rectangle r = getBounds();
      Rectangle r2 = new Rectangle(r);
      adgustResizingRect(r, r2, e.getPoint());
      disposer.applyResizeBoat(this, r2);
    }
    dragMode = false;
    resizeType = 0;
    updateCursor();
  }

  public void mouseDragged(MouseEvent e) {
    int x = e.getPoint().x;
    int y = e.getPoint().y;
    if (pressed && !dragMode) {
      if (Math.abs(clickPoint.x - x) >= DRAG_SENSITIVITY ||
          Math.abs(clickPoint.y - y) >= DRAG_SENSITIVITY) {
        dragMode = true;
        disposer.startResizeBoat(this, resizeType);
      }
    }
    if (dragMode) {
      Rectangle r = getBounds();
      Rectangle r2 = new Rectangle(r);
      adgustResizingRect(r, r2, e.getPoint());
      disposer.executeResizeBoat(this, r2);
    }
  }

  public void mouseMoved(MouseEvent e) {
    updateCursor(e.getPoint());
  }

  private void updateCursor() {
    Point p = getMousePosition();
    if (p != null) {
      updateCursor(p);
    } else {
      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }

  private void updateCursor(Point p) {
    int ic = calcResizeType(p);
    setCursor(DmUtils.getCursorForDragType(ic));
  }

  public static final int MASK_WEST  = 0x1;
  public static final int MASK_EAST  = 0x2;
  public static final int MASK_NORTH = 0x4;
  public static final int MASK_SOUTH = 0x8;

  private static final int[] RESIZE_TYPES = new int[] {
      DockingConst.DRAG_NO, DockingConst.DRAG_W,  //  0  1
      DockingConst.DRAG_E,  DockingConst.DRAG_NO, //  2  3
      DockingConst.DRAG_N,  DockingConst.DRAG_NW, //  4  5
      DockingConst.DRAG_NE, DockingConst.DRAG_NO, //  6  7
      DockingConst.DRAG_S,  DockingConst.DRAG_SW, //  8  9
      DockingConst.DRAG_SE, DockingConst.DRAG_NO, // 10 11
      DockingConst.DRAG_NO, DockingConst.DRAG_NO, // 12 13
      DockingConst.DRAG_NO, DockingConst.DRAG_NO  // 14 15
  };

  private boolean isNotInActiveBorder(Dimension sz, Insets ins, int behaviour, Point p) {
    Rectangle r = new Rectangle(
        ins.left, ins.top, sz.width - ins.right - ins.left,
        sz.height - ins.bottom - ins.top);
    if (p.x < 0 || p.y < 0 || p.x >= sz.width || p.y >= sz.height || r.contains(p)) {
      return true;
    }
    return false;
  }


  /**
   * @param p точка внутри окна
   * @return DockingConst.DRAG_XXX
   */
  private int calcResizeType(Point p) {
    int x0 = p.x;
    int y0 = p.y;
    Dimension sz = getSize();
    Insets ins = getInsets();

    if (isNotInActiveBorder(sz, ins, behaviour, p)) {
      return DockingConst.DRAG_NO;
    }

    int c = 0;
    boolean northActive = (behaviour & BoatEmpty.BST_RESIZABLE_NORTH) != 0;
    boolean westActive = (behaviour & BoatEmpty.BST_RESIZABLE_WEST) != 0;
    boolean southActive = (behaviour & BoatEmpty.BST_RESIZABLE_SOUTH) != 0;
    boolean eastActive = (behaviour & BoatEmpty.BST_RESIZABLE_EAST) != 0;

    if (northActive && y0 < ins.top + CORNER_SNAP_SIZE) {
      c |= MASK_NORTH;
    } else if (southActive && y0 > sz.height - ins.bottom - CORNER_SNAP_SIZE) {
      c |= MASK_SOUTH;
    }

    if (westActive && x0 < ins.left + CORNER_SNAP_SIZE) {
      c |= MASK_WEST;
    } else if (eastActive && x0 > sz.width - ins.right - CORNER_SNAP_SIZE) {
      c |= MASK_EAST;
    }

    return RESIZE_TYPES[c];
  }

  public void updateButtons() {
    cTitle.updateButtonsSuite();
  }

  public void updatePinIcon() {
    if (cTitle != null) {
      cTitle.updatePinIcon();
    }
  }

  /**
   * @return одно из значений DK_WEST, DK_EAST и т.д.
   */
  public int getDockLocation() {
    return dock;
  }

  public void setDisposer(Disposer disposer) {
    this.disposer = disposer;
    if (cTitle != null) {
      cTitle.setDisposer(disposer);
    }
  }

  public void setBoatListener(BoatListener bl) {
    boatListener = bl;
    if (cTitle != null) {
      cTitle.setBoatListener(bl);
    }
  }

  public void setDockSize(int sz) {
    if (dock == DockingConst.DK_WEST || dock == DockingConst.DK_EAST) {
      dockSizes.width = sz;
    } else {
      dockSizes.height = sz;
    }
  }

  public void setDockSizes(Dimension sz) {
    dockSizes.width = sz.width;
    dockSizes.height = sz.height;
  }

  public int getDockSize() {
    return dock == DockingConst.DK_WEST || dock == DockingConst.DK_EAST ?
        dockSizes.width : dockSizes.height;
  }

  public Dimension getDockSizes() {
    return dockSizes;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    if (this.active != active) {
      this.active = active;
      if (cTitle != null) {
        cTitle.setActive(active);
      }
    }
  }

  public void setInsets(Insets insets) {
    this.insets = insets;
  }

  public void setFloatBounds(Rectangle rt) {
    floatBounds = new Rectangle(rt);
  }

  public Rectangle getFloatBounds() {
    return floatBounds;
  }

  public Component getLastFocusedComponent() {
    return lastFocusedComponent;
  }

  public void setLastFocusedComponent(Component lastFocusedComponent) {
    this.lastFocusedComponent = lastFocusedComponent;
  }

  public ImageIcon getIcon() {
    return icon;
  }

  public void setIcon(ImageIcon icon) {
    this.icon = icon;
  }

  public boolean isUsePercents() {
    return usePercents;
  }

  public void setUsePercents(boolean usePercents) {
    this.usePercents = usePercents;
  }

  private void adgustResizingRect(Rectangle source, Rectangle result, Point p) {
    if (isResizingWest()) {
      result.x = source.x + p.x - clickPoint.x;
      result.width -= p.x - clickPoint.x;
    }

    if (isResizingNorth()) {
      result.y = source.y + p.y - clickPoint.y;
      result.height -= p.y - clickPoint.y;
    }

    if (isResizingEast()) {
      result.width += p.x - clickPoint.x;
    }

    if (isResizingSouth()) {
      result.height += p.y - clickPoint.y;
    }
  }

  private boolean isResizingWest() {
    return resizeType == DockingConst.DRAG_W ||
        resizeType == DockingConst.DRAG_NW ||
        resizeType == DockingConst.DRAG_SW;
  }

  private boolean isResizingNorth() {
    return resizeType == DockingConst.DRAG_N ||
        resizeType == DockingConst.DRAG_NW ||
        resizeType == DockingConst.DRAG_NE;
  }

  private boolean isResizingEast() {
    return resizeType == DockingConst.DRAG_E ||
        resizeType == DockingConst.DRAG_NE ||
        resizeType == DockingConst.DRAG_SE;
  }

  private boolean isResizingSouth() {
    return resizeType == DockingConst.DRAG_S ||
        resizeType == DockingConst.DRAG_SW ||
        resizeType == DockingConst.DRAG_SE;
  }
}
