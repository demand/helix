package com.zwing.docking;

/**
 * DockingConst
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockingConst {

    /**
     *
     */
    public static final int INT_UNUSED = -28542;

    /**
     * элемент расположен слева
     */
    public static final int DK_WEST = 1;
    /**
     * элемент расположен сверху
     */
    public static final int DK_NORTH = 2;
    /**
     * элемент расположен справа
     */
    public static final int DK_EAST = 3;
    /**
     * элемент расположен снизу
     */
    public static final int DK_SOUTH = 4;
    /**
     * элемент расположен в центре
     */
    public static final int DK_CENTER = 5;
    /**
     * Элемент не связан с боковыми панелями. Это самостоятельное окно.
     */
    public static final int DK_DRIFT = 6;

    public static final int DOCKS_COUNT = 7;

    /**
     * Слой разделителей (splitter)
     */
    public static final Integer ZO_SPLIT = 10;
    /**
     * Слой окна редактора и dockable-панелей
     */
    public static final Integer ZO_CENTER = 20;
    /**
     * Слой dock-панелей
     */
    public static final Integer ZO_DOCK = 30;
    /**
     * Слой side-панелей
     */
    public static final Integer ZO_SLIDE = 40;
    /**
     * Слой плавающих панелей
     */
    public static final Integer ZO_FLOAT = 50;
    /**
     * Слой переносимых элементов (рамок в момент перемещения)
     */
    public static final Integer ZO_DRAG = 100;
    /**
     * Слой всплывающих элементов - подсказок и т.п.
     */
    public static final Integer ZO_POPUP = 110;
    /**
     * Слой в которое помещается окно при добавлении. Позже
     * окно переводится в слой, соответствующий его статусу.
     */
    public static final Integer ZO_DEFAULT = ZO_CENTER;

    /**
     * Границы окна не меняются
     */
    public static final int DRAG_NO = 0;
    /**
     * Движется только левая граница (WEST)
     */
    public static final int DRAG_W = 1;
    /**
     * Движется только правая граница (EAST)
     */
    public static final int DRAG_E = 2;
    /**
     * Движется только верхняя граница (NORTH)
     */
    public static final int DRAG_N = 3;
    /**
     * Движется только нижняя граница (SOUTH)
     */
    public static final int DRAG_S = 4;
    /**
     * Движется левый верхний угол
     */
    public static final int DRAG_NW = 5;
    /**
     * Движется левая и нижняя
     */
    public static final int DRAG_SW = 6;
    /**
     * Движется правый и верхний угол
     */
    public static final int DRAG_NE = 7;
    /**
     * Движется правый нижний угол
     */
    public static final int DRAG_SE = 8;
    /**
     * Движется окно целиком
     */
    public static final int DRAG_MV = 9;
    /**
     * Движется вертикальный разделитель (splitter)
     */
    public static final int DRAG_SV = 10;
    /**
     * Движется горизонтальный разделитель (splitter)
     */
    public static final int DRAG_SH = 11;
    /**
     * Движутся горизонтальный и вертикальный разделители
     */
    public static final int DRAG_SHV = 12;
}
