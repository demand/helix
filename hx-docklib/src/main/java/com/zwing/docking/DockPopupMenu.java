package com.zwing.docking;

import com.zwing.utils.ResourceManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created: 26.09.2006 15:29:15
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockPopupMenu extends JPopupMenu {
    private DockManager dm;
    private BoatEmpty boat;

    private DockPopupMenu() {
    }

    public BoatEmpty getBoat() {
        return boat;
    }

    public void setBoat(BoatEmpty boat) {
        this.boat = boat;
    }

    public DockManager getDockManager() {
        return dm;
    }

    public void setDockManager(DockManager dm) {
        this.dm = dm;
    }

    private void addPin() {
        JCheckBoxMenuItem pinItem = new JCheckBoxMenuItem(
                new DockAction(ResourceManager.getString("docking.action.pin.label"), this) {
            public void actionPerformed(ActionEvent e) {
                dm.requestPin(boat, !boat.isPin());
            }
        });
        pinItem.setSelected(boat.isPin());
        add(pinItem);
    }

    private void addSideMenu() {
        add(createSideMenu());
    }

    private JMenu createSideMenu() {
        JMenu menuMoveTo = new JMenu(ResourceManager.getString("docking.sidebar.moveto"));

        JRadioButtonMenuItem northItem = createNorth();
        JRadioButtonMenuItem southItem = createSouth();
        JRadioButtonMenuItem westItem = createWest();
        JRadioButtonMenuItem eastItem = createEast();

        menuMoveTo.add(northItem);
        menuMoveTo.add(southItem);
        menuMoveTo.add(westItem);
        menuMoveTo.add(eastItem);

        ButtonGroup groupDock = new ButtonGroup();
        groupDock.add(northItem);
        groupDock.add(southItem);
        groupDock.add(westItem);
        groupDock.add(eastItem);

        JRadioButtonMenuItem selectedDock =
                boat.getDockLocation() == DockingConst.DK_NORTH ? northItem :
                boat.getDockLocation() == DockingConst.DK_SOUTH ? southItem :
                boat.getDockLocation() == DockingConst.DK_WEST ? westItem :
                boat.getDockLocation() == DockingConst.DK_EAST ? eastItem :
                null;
        if (selectedDock != null) {
            selectedDock.setSelected(true);
        }
        return menuMoveTo;
    }

    private JRadioButtonMenuItem createNorth() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.sidebar.north"), this) {
            public void actionPerformed(ActionEvent e) {
                attachSafe(boat, DockingConst.DK_NORTH);
            }
        });
    }

    private JRadioButtonMenuItem createSouth() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.sidebar.south"), this) {
            public void actionPerformed(ActionEvent e) {
                attachSafe(boat, DockingConst.DK_SOUTH);
            }
        });
    }

    private JRadioButtonMenuItem createWest() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.sidebar.west"), this) {
            public void actionPerformed(ActionEvent e) {
                attachSafe(boat, DockingConst.DK_WEST);
            }
        });
    }

    private JRadioButtonMenuItem createEast() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.sidebar.east"), this) {
            public void actionPerformed(ActionEvent e) {
                attachSafe(boat, DockingConst.DK_EAST);
            }
        });
    }

    private JRadioButtonMenuItem createDock() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.action.dock.label"), this) {
            public void actionPerformed(ActionEvent e) {
                if (!boat.isDock()) {
                    dm.requestBehaviour(boat, BoatEmpty.BST_DOCK);
                }
            }
        });
    }

    private JRadioButtonMenuItem createFloat() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.action.float.label"), this) {
            public void actionPerformed(ActionEvent e) {
                if (!boat.isFloat()) {
                    dm.requestBehaviour(boat, BoatEmpty.BST_FLOAT);
                }
            }
        });
    }

    private JRadioButtonMenuItem createSlide() {
        return new JRadioButtonMenuItem(
                new DockAction(ResourceManager.getString("docking.action.slide.label"), this) {
            public void actionPerformed(ActionEvent e) {
                if (!boat.isSlide()) {
                    dm.requestBehaviour(boat, BoatEmpty.BST_SLIDE);
                }
            }
        });
    }

    private void addBehaviour() {
        JRadioButtonMenuItem dockItem = createDock();
        JRadioButtonMenuItem slideItem = createSlide();
        JRadioButtonMenuItem floatItem = createFloat();

        ButtonGroup groupDock = new ButtonGroup();
        groupDock.add(dockItem);
        groupDock.add(slideItem);
        groupDock.add(floatItem);

        JRadioButtonMenuItem selectedBehaviour =
                boat.isDock() ? dockItem :
                boat.isSlide() ? slideItem :
                boat.isFloat() ? floatItem :
                null;
        if (selectedBehaviour != null) {
            selectedBehaviour.setSelected(true);
        }
        add(dockItem);
        add(slideItem);
        add(floatItem);
    }

    private void addMinimize() {
        Action showHideAction = boat.isVisible() ?
                new DockAction(
                        ResourceManager.getString("docking.action.minimz.label"), this) {
                    public void actionPerformed(ActionEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                dm.requestInvisible(boat);
                            }
                        });
                    }
                } :
                new DockAction(
                        ResourceManager.getString("docking.action.show.label"), this) {
                    public void actionPerformed(ActionEvent e) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                dm.requestActive(boat);
                            }
                        });
                    }
                };
        add(showHideAction);
    }

    private void attachSafe(BoatEmpty boat, int dockLocation) {
        if (boat.getDockLocation() != dockLocation) {
            dm.requestAttach(boat, dockLocation);
        }
    }

    public static JPopupMenu createPopupMenu(DockManager dm, BoatEmpty boat) {
        DockPopupMenu menu = new DockPopupMenu();
        menu.setDockManager(dm);
        menu.setBoat(boat);

        menu.addPin();
        menu.addSeparator();
        menu.addBehaviour();
        menu.addSideMenu();
        menu.addSeparator();
        menu.addMinimize();

        return menu;
    }

    abstract static class DockAction extends AbstractAction {
        DockPopupMenu popup;
        DockAction(String name, DockPopupMenu popup) {
            super(name);
            this.popup = popup;
        }

        DockAction(String name, Icon icon, DockPopupMenu popup) {
            super(name, icon);
            this.popup = popup;
        }
    }
}
