package com.zwing.docking;

import javax.swing.JComponent;
import javax.swing.KeyStroke;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Created: 14.08.2006 15:21:10
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DragCanvas extends JComponent {

  private DragPainter dragPainter;
  private Disposer disposer;

  public DragCanvas() {
    setOpaque(false);
    setFocusable(true);
    registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        disposer.cancelWhatever();
      }
    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
    JComponent.WHEN_IN_FOCUSED_WINDOW);
    init();
  }

  protected void init() {
  }

  public DragPainter getDragPainter() {
    return dragPainter;
  }

  public void setDragPainter(DragPainter dragPainter) {
    this.dragPainter = dragPainter;
  }

  public void paint(Graphics g) {
    Dimension sz = getSize();

    if (dragPainter != null) {
      dragPainter.doPaint(g, sz);
    }
  }

  public void setDisposer(Disposer disposer) {
      this.disposer = disposer;
  }
}
