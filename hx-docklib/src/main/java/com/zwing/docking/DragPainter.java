package com.zwing.docking;

import java.awt.Graphics;
import java.awt.Dimension;

/**
 * Created: 14.08.2006 15:21:33
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface DragPainter {
  void doPaint(Graphics g, Dimension sz);
}
