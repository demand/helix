package com.zwing.docking;

import com.zwing.DockingPane;

import javax.swing.JLayeredPane;
import java.awt.Insets;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.Dimension;

/**
 * Created: 27.09.2006 17:32:29
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DmUtils {

  static int[] iCursorByDragType = new int[] {
      Cursor.DEFAULT_CURSOR,   //  0 - границы окна не меняются
      Cursor.W_RESIZE_CURSOR,  //  1 - левая граница
      Cursor.W_RESIZE_CURSOR,  //  2 - правая граница
      Cursor.N_RESIZE_CURSOR,  //  3 - верхняя граница
      Cursor.N_RESIZE_CURSOR,  //  4 - нижняя граница
      Cursor.NW_RESIZE_CURSOR, //  5 - левая и верхняя граница
      Cursor.NE_RESIZE_CURSOR, //  6 - левая и нижняя граница
      Cursor.NE_RESIZE_CURSOR, //  7 - правая и верхняя граница
      Cursor.NW_RESIZE_CURSOR, //  8 - правая и нижняя граница
      Cursor.HAND_CURSOR,      //  9 - окно целиком
      Cursor.W_RESIZE_CURSOR,  // 10 - горизонтальный разделитель
      Cursor.N_RESIZE_CURSOR,  // 11 - вертикальный разделитель
      Cursor.MOVE_CURSOR       // 12 - горизонтальный и вертикальный разделители
  };

  public static Cursor getCursorForDragType(int dragType) {
    return Cursor.getPredefinedCursor(iCursorByDragType[dragType]);
  }

  static Insets getInsetsForBehaviour(BoatEmpty boat, int behaviour) {
    Insets rslt = new Insets(0, 0, 0, 0);
    if ((behaviour & BoatEmpty.BST_CENTER) == BoatEmpty.BST_CENTER) {
      ; // do nothing - all insets is zero size
    } else if ((behaviour & BoatEmpty.BST_FLOAT) == BoatEmpty.BST_FLOAT) {
      rslt.left = DockManager.BORDER_SIZE;
      rslt.right = DockManager.BORDER_SIZE;
      rslt.top = DockManager.BORDER_SIZE;
      rslt.bottom = DockManager.BORDER_SIZE;
    } else if ((behaviour & BoatEmpty.BST_SLIDE) == BoatEmpty.BST_SLIDE) {
      int location = boat.getDockLocation();
      if (location == DockingConst.DK_WEST) {
        rslt.left = 0;
        rslt.top = 0;
        rslt.bottom = 0;
        rslt.right = DockManager.BORDER_SIZE;
      } else if (location == DockingConst.DK_EAST) {
        rslt.left = DockManager.BORDER_SIZE;
        rslt.top = 0;
        rslt.right = 0;
        rslt.bottom = 0;
      } else if (location == DockingConst.DK_NORTH) {
        rslt.left = 0;
        rslt.top = 0;
        rslt.right = 0;
        rslt.bottom = DockManager.BORDER_SIZE;
      } else if (location == DockingConst.DK_SOUTH) {
        rslt.left = 0;
        rslt.top = DockManager.BORDER_SIZE;
        rslt.right = 0;
        rslt.bottom = 0;
      }
    } else if ((behaviour & BoatEmpty.BST_DOCK) == BoatEmpty.BST_DOCK) {
      ; // do nothing - all insets is zero size
    }
    return rslt;
  }

  static boolean isDragWest(int drag) {
    return drag == DockingConst.DRAG_W  ||
        drag == DockingConst.DRAG_NW ||
        drag == DockingConst.DRAG_SW;
  }

  static boolean isDragNorth(int drag) {
    return drag == DockingConst.DRAG_N  ||
        drag == DockingConst.DRAG_NW ||
        drag == DockingConst.DRAG_NE;
  }

  static boolean isDragEast(int drag) {
    return drag == DockingConst.DRAG_E  ||
        drag == DockingConst.DRAG_NE ||
        drag == DockingConst.DRAG_SE;
  }

  static boolean isDragSouth(int drag) {
    return drag == DockingConst.DRAG_S  ||
        drag == DockingConst.DRAG_SW ||
        drag == DockingConst.DRAG_SE;
  }

  static RectBounds calcRectBounds(Rectangle rect1, Rectangle rect2) {
    int x0 = rect1.x;
    int y0 = rect1.y;
    int x1 = x0 + rect1.width;
    int y1 = y0 + rect1.height;

    if (rect2 != null) {
      x0 = Math.min(x0, rect2.x);
      y0 = Math.min(y0, rect2.y);
      x1 = Math.max(x1, rect2.x+rect2.width);
      y1 = Math.max(y1, rect2.y+rect2.height);
    }
    return new RectBounds(x0, y0, x1, y1);
  }

  static void adjustBoatBordersAndMovable(DockingPane pane, BoatEmpty boat) {
    Insets insets = new Insets(0, 0, 0, 0);
    Integer zo = DockingConst.ZO_DOCK;
    if (boat.isDock()) {
      boat.setMovable(false);
      boat.setResizable(BoatEmpty.BST_RESIZABLE_NONE);
    } else if (boat.isSlide()) {
      boat.setMovable(false);
      int location = boat.getDockLocation();
      if (location == DockingConst.DK_WEST) {
        insets.right = DockManager.BORDER_SIZE;
        boat.setResizable(BoatEmpty.BST_RESIZABLE_EAST);
      } else if (location == DockingConst.DK_EAST) {
        insets.left = DockManager.BORDER_SIZE;
        boat.setResizable(BoatEmpty.BST_RESIZABLE_WEST);
      } else if (location == DockingConst.DK_NORTH) {
        insets.bottom = DockManager.BORDER_SIZE;
        boat.setResizable(BoatEmpty.BST_RESIZABLE_SOUTH);
      } else if (location == DockingConst.DK_SOUTH) {
        insets.top = DockManager.BORDER_SIZE;
        boat.setResizable(BoatEmpty.BST_RESIZABLE_NORTH);
      }
      zo = DockingConst.ZO_SLIDE;
    } else if (boat.isFloat()) {
      insets.left = DockManager.BORDER_SIZE;
      insets.top = DockManager.BORDER_SIZE;
      insets.right = DockManager.BORDER_SIZE;
      insets.bottom = DockManager.BORDER_SIZE;
      boat.setMovable(true);
      boat.setResizable(BoatEmpty.BST_RESIZABLE_ALL);
      zo = DockingConst.ZO_FLOAT;
    } else if (boat.isCenter()) {
      boat.setMovable(false);
      boat.setResizable(BoatEmpty.BST_RESIZABLE_NONE);
      zo = DockingConst.ZO_CENTER;
    }
    adjustZo(boat, pane, zo.intValue());
    boat.setInsets(insets);
  }

  private static void adjustZo(BoatEmpty boat, DockingPane pane, int zo) {
    int oldZo = JLayeredPane.getLayer(boat);
    if (oldZo != zo) {
      pane.setLayer(boat, zo);
    }
  }

  static Rectangle getAdjustedSlideBounds(BoatEmpty boat, Dimension paneSize) {
    Rectangle bounds = new Rectangle();
    int location = boat.getDockLocation();
    Insets dki = DmUtils.getInsetsForBehaviour(boat, BoatEmpty.BST_DOCK);
    Insets sli = DmUtils.getInsetsForBehaviour(boat, BoatEmpty.BST_SLIDE);
    if (location == DockingConst.DK_WEST) {
      bounds.x = 0;
      bounds.y = 0;
      bounds.width = boat.getDockSize() - dki.left - dki.right +
          sli.left + sli.right;
      bounds.height = paneSize.height;
    } else if (location == DockingConst.DK_EAST) {
      bounds.y = 0;
      bounds.width = boat.getDockSize() - dki.left - dki.right + sli.left + sli.right;
      bounds.height = paneSize.height;
      bounds.x = paneSize.width - bounds.width;
    } else if (location == DockingConst.DK_NORTH) {
      bounds.x = 0;
      bounds.y = 0;
      bounds.width = paneSize.width;
      bounds.height = boat.getDockSize() - dki.top - dki.bottom + sli.top + sli.bottom;
    } else if (location == DockingConst.DK_SOUTH) {
      bounds.x = 0;
      bounds.width = paneSize.width;
      bounds.height = boat.getDockSize() - dki.top - dki.bottom + sli.top + sli.bottom;
      bounds.y = paneSize.height - bounds.height;
    }
    return bounds;
  }

  static Rectangle getAdjustedFloatBounds(BoatEmpty boat, Dimension paneSize) {
    Rectangle bounds = new Rectangle(boat.getFloatBounds());
    bounds.x = Math.max(0, bounds.x);
    bounds.y = Math.max(0, bounds.y);
    bounds.width = Math.min(bounds.width, paneSize.width);
    bounds.height = Math.min(bounds.height, paneSize.height);
    if (bounds.x + bounds.width > paneSize.width) {
      bounds.x = paneSize.width - bounds.width;
    }
    if (bounds.y + bounds.height > paneSize.height) {
      bounds.y = paneSize.height - bounds.height;
    }
    return bounds;
  }

  static RectBounds adjustMoveRect(BoatEmpty boat, RectBounds rb, Dimension szPane) {
    RectBounds nb = new RectBounds(rb.x0, rb.y0, rb.x1, rb.y1);
    Dimension sz = boat.getSize();

    if (nb.x0 < 0) {
      nb.x0 = 0;
      nb.x1 = nb.x0 + sz.width - 1;
    }
    if (nb.x1 >= szPane.width) {
      nb.x1 = szPane.width - 1;
      nb.x0 = nb.x1 - sz.width + 1;
    }
    if (nb.y0 < 0) {
      nb.y0 = 0;
      nb.y1 = nb.y0 + sz.height - 1;
    }
    if (nb.y1 >= szPane.height) {
      nb.y1 = szPane.height - 1;
      nb.y0 = nb.y1 - sz.height + 1;
    }
    return nb;
  }

  private static RectBounds adjustResizeWestRect(BoatEmpty boat, RectBounds rb, RectBounds nb) {
    Dimension szMin = boat.getMinimumSize();
    Dimension szMax = boat.getMaximumSize();

    nb.x0 = Math.max(rb.x0, 0);
    if (nb.x1 - nb.x0 < szMin.width) {
      nb.x0 = nb.x1 - szMin.width + 1;
    }
    if (nb.x1 - nb.x0 > szMax.width) {
      nb.x0 = nb.x1 - szMax.width + 1;
    }
    return nb;
  }

  private static RectBounds adjustResizeNorthRect(BoatEmpty boat, RectBounds rb, RectBounds nb) {
    Dimension szMin = boat.getMinimumSize();
    Dimension szMax = boat.getMaximumSize();

    nb.y0 = Math.max(rb.y0, 0);
    if (nb.y1 - nb.y0 < szMin.height) {
      nb.y0 = nb.y1 - szMin.height + 1;
    }
    if (nb.y1 - nb.y0 > szMax.height) {
      nb.y0 = nb.y1 - szMax.height + 1;
    }
    return nb;
  }

  private static RectBounds adjustResizeEastRect(BoatEmpty boat, RectBounds rb,
                                                 RectBounds nb, Dimension szPane) {
    Dimension szMin = boat.getMinimumSize();
    Dimension szMax = boat.getMaximumSize();

    nb.x1 = Math.min(rb.x1, szPane.width-1);
    if (nb.x1 - nb.x0 < szMin.width) {
      nb.x1 = nb.x0 + szMin.width - 1;
    }
    if (nb.x1 - nb.x0 > szMax.width) {
      nb.x1 = nb.x0 + szMax.width - 1;
    }
    return nb;
  }

  static RectBounds adjustResizeSouthRect(BoatEmpty boat, RectBounds rb,
                                          RectBounds nb, Dimension szPane) {
    Dimension szMin = boat.getMinimumSize();
    Dimension szMax = boat.getMaximumSize();

    nb.y1 = Math.min(rb.y1, szPane.height-1);
    if (nb.y1 - nb.y0 < szMin.height) {
      nb.y1 = nb.y0 + szMin.height - 1;
    }
    if (nb.y1 - nb.y0 > szMax.height) {
      nb.y1 = nb.y0 + szMax.height - 1;
    }
    return nb;
  }

  protected static Rectangle adjustBoatBounds(BoatEmpty boat, Rectangle r,
                                              int drag, Dimension szPane) {
    RectBounds rb = new RectBounds(r.x, r.y, r.x + r.width - 1, r.y + r.height - 1);
    RectBounds nb = new RectBounds(rb.x0, rb.y0, rb.x1, rb.y1);

    if (drag == DockingConst.DRAG_MV) {
      nb = adjustMoveRect(boat, rb, szPane);
    } else {
      if (DmUtils.isDragWest(drag)) {
        adjustResizeWestRect(boat, rb, nb);
      }
      if (DmUtils.isDragNorth(drag)) {
        adjustResizeNorthRect(boat, rb, nb);
      }
      if (DmUtils.isDragEast(drag)) {
        adjustResizeEastRect(boat, rb, nb, szPane);
      }
      if (DmUtils.isDragSouth(drag)) {
        adjustResizeSouthRect(boat, rb, nb, szPane);
      }
    }
    return new Rectangle(nb.x0, nb.y0, nb.x1 - nb.x0 + 1, nb.y1 - nb.y0 + 1);
  }
}
