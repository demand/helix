package com.zwing.docking;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

import com.jgoodies.looks.plastic.PlasticUtils;
import com.zwing.utils.ZUIManager;
import com.zwing.utils.ZwingUtils;

/**
 * BoatTitle
 *
 * @author А.Дёмин
 * @since 06.12.2004
 */
public class BoatTitle extends JToolBar implements  MouseListener,
    MouseMotionListener, ComponentListener, ActionListener {

  private static final int  DRAG_SENSITIVITY = 4;

  private Disposer disposer;

  private String title;
  private boolean active;
  public static final int INT_UNDEF = -67364;
  int xp = 0;
  int yp = 0;

  int mouseX;
  int mouseY;

  private boolean pressed;
  private boolean dragMode;
  private BoatEmpty boat;
  private BoatListener boatListener;


  private static final String CMD_DOCK  = "DOCK";
  private static final String CMD_SLIDE = "SLIDE";
  private static final String CMD_FLOAT = "FLOAT";
  private static final String CMD_PIN   = "PIN";
  private static final String CMD_MIN   = "MIN";

//  JButton btnDock;
//  JButton btnSlide;
//  JButton btnFloat;
//  JButton btnPin;
//  JButton btnMinimz;

  private JButton btns[];
  private static final int BTN_DOCK = 0;
  private static final int BTN_SLIDE = 1;
  private static final int BTN_FLOAT = 2;
  private static final int BTN_PIN = 3;
  private static final int BTN_MINIMZ = 4;
  private static final int BTNS_COUNT = 5;
  private int curBtnsCount = 0;

  private ImageIcon imPin;
  private ImageIcon imUnpin;
  private Dimension btnSize;

  public static final int MIN_WIDTH  = 20;
  public static final int PREF_WIDTH = 50;
  public static final int MAX_WIDTH  = 4885;

  public BoatTitle(BoatEmpty boat, String title) {
    super();
    this.boat = boat;
    setTitle(title);

    btns = new JButton[BTNS_COUNT];
    curBtnsCount = 0;

    int height = (Integer) ZUIManager.getProperty("boatTitle.height");

    setMinimumSize(new Dimension(MIN_WIDTH, height));
    setPreferredSize(new Dimension(PREF_WIDTH, height));
    setMaximumSize(new Dimension(MAX_WIDTH, height));
    setFloatable(false);
    xp = INT_UNDEF;
    yp = INT_UNDEF;

    pressed = false;
    dragMode = false;
    addComponentListener(this);
    addMouseListener(this);
    addMouseMotionListener(this);
    setRequestFocusEnabled(true);
    setFocusable(false);
    setEnabled(true);

    setLayout(null);

    ImageIcon im;

    imPin = (ImageIcon) ZUIManager.getProperty("boatTitle.imagePin");
    imUnpin = (ImageIcon) ZUIManager.getProperty("boatTitle.imageUnpin");

    int btnWidth = imPin.getIconWidth() + IV_4;
    int btnHeight = (Integer) ZUIManager.getProperty("boatTitle.height") - 2;
    btnSize = new Dimension(btnWidth, btnHeight);

    im = (ImageIcon) ZUIManager.getProperty("boatTitle.imageDock");
    btns[BTN_DOCK] = createButton(im, CMD_DOCK);
    im = (ImageIcon) ZUIManager.getProperty("boatTitle.imageSlide");
    btns[BTN_SLIDE] = createButton(im, CMD_SLIDE);
    im = (ImageIcon) ZUIManager.getProperty("boatTitle.imageFloat");
    btns[BTN_FLOAT] = createButton(im, CMD_FLOAT);
    btns[BTN_PIN] = createButton(boat.isPin() ? imPin : imUnpin, CMD_PIN);
    im = (ImageIcon) ZUIManager.getProperty("boatTitle.imageMinimz");
    btns[BTN_MINIMZ] = createButton(im, CMD_MIN);
//    setRollover(true);
  }

  private JButton createButton(ImageIcon im, String ac) {
    JButton btn = new JButton(im);
    btn.setMinimumSize(btnSize);
    btn.setMaximumSize(btnSize);
    btn.setPreferredSize(btnSize);
    btn.setActionCommand(ac);
    btn.addActionListener(this);
    btn.setVisible(true);
    btn.setFocusable(false);
    btn.setRolloverEnabled(true);
    btn.setOpaque(true);
    add(btn);
    return btn;
  }

  public void paint(Graphics g) {
    super.paint(g);

    Dimension sz = getSize();
    int w = sz.width - btnSize.width * curBtnsCount;

    Graphics2D g2 = (Graphics2D) g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

    int height = (Integer) ZUIManager.getProperty("boatTitle.height");
    Insets insets = (Insets) ZUIManager.getProperty("boatTitle.insets");

    Color bg = getBackground();
    Color fr = getForeground();

//        Color bg = (Color) ZUIManager.getProperty(isActive() ?
//                "boatTitle.bgActive" : "boatTitle.bgInactive");
//        Color fr = (Color) ZUIManager.getProperty(isActive() ?
//                "boatTitle.frActive" : "boatTitle.frInactive");

    Color borderColor = (Color) ZUIManager.getProperty("boat.borderColor");

    Font f = (Font) ZUIManager.getProperty("boatTitle.font");
    FontMetrics fm = g.getFontMetrics(f);
    int descent = fm.getDescent();

    g2.setFont(f);
    g2.setColor(bg);
    g2.fillRect(0, 0, w, sz.height);
    PlasticUtils.addLight3DEffekt(g, new Rectangle(0, 0, sz.width, sz.height));

    g2.setColor(borderColor);
    g2.drawRect(0, 0, sz.width-1, sz.height-1);

    if (title != null) {
      g2.setColor(fr);
      String st = ZwingUtils.makeStringShort(title, g2.getFontMetrics(f), w - insets.left);
      g2.drawString(st, insets.left, height - descent - insets.bottom);
    }
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
    new MotionPainter2(active).start();
    repaint();
  }

  public void mouseClicked(MouseEvent e) {
  }

  public void mouseEntered(MouseEvent e) {
  }

  public void mouseExited(MouseEvent e) {
  }

  public void mousePressed(MouseEvent e) {
    if (boatListener != null) {
      boatListener.requestActive(boat);
    }
    xp = e.getPoint().x;
    yp = e.getPoint().y;
    if (boat.isMovable()) {
      pressed = true;
      dragMode = false;
    }
  }

  public void mouseReleased(MouseEvent e) {
    mouseX = e.getPoint().x;
    mouseY = e.getPoint().y;
    pressed = false;
    if (dragMode) {
      // переместить окно
      Rectangle r = boat.getBounds();
      Rectangle r2 = new Rectangle(r);
      r2.x += mouseX-xp;
      r2.y += mouseY - yp;
      disposer.applyMoveBoat(boat, r2);
    }
  }

  public void mouseDragged(MouseEvent e) {
    mouseX = e.getPoint().x;
    mouseY = e.getPoint().y;
    if (pressed && !dragMode) {
      if (Math.abs(xp - mouseX) >= DRAG_SENSITIVITY ||
          Math.abs(yp - mouseY) >= DRAG_SENSITIVITY) {
        dragMode = true;
        disposer.startMoveBoat(boat);
      }
    }
    if (dragMode) {
      Rectangle r = boat.getBounds();
      Rectangle r2 = new Rectangle(r);
      r2.x += mouseX-xp;
      r2.y += mouseY - yp;
      disposer.executeMoveBoat(boat, r2);
    }
  }

  public void mouseMoved(MouseEvent e) {
  }

  public void componentHidden(ComponentEvent e) {
  }

  public void componentMoved(ComponentEvent e) {
  }

  public static final int IV_3 = 3;
  public static final int IV_4 = 4;

  void updateButtonsSuite() {
    Dimension sz = getSize();
    int behaviour = boat.getBehaviour();
    curBtnsCount = IV_4;

    if (behaviour == BoatEmpty.BST_DOCK) {
      // curBtnCount остаётся равным 4
      btns[BTN_DOCK].setVisible(false);
      btns[BTN_SLIDE].setVisible(true);
      btns[BTN_FLOAT].setVisible(true);
      btns[BTN_PIN].setVisible(true);
      btns[BTN_MINIMZ].setVisible(true);
    } else if (behaviour == BoatEmpty.BST_SLIDE) {
      curBtnsCount = IV_3;
      btns[BTN_DOCK].setVisible(true);
      btns[BTN_SLIDE].setVisible(false);
      btns[BTN_FLOAT].setVisible(true);
      btns[BTN_PIN].setVisible(false);
      btns[BTN_MINIMZ].setVisible(true);
    } else if (behaviour == BoatEmpty.BST_FLOAT) {
      // curBtnCount остаётся равным 4
      btns[BTN_DOCK].setVisible(true);
      btns[BTN_SLIDE].setVisible(true);
      btns[BTN_FLOAT].setVisible(false);
      btns[BTN_PIN].setVisible(true);
      btns[BTN_MINIMZ].setVisible(true);
    } else if (behaviour == BoatEmpty.BST_CENTER) {
      curBtnsCount = 0;
      btns[BTN_DOCK].setVisible(false);
      btns[BTN_SLIDE].setVisible(false);
      btns[BTN_FLOAT].setVisible(false);
      btns[BTN_PIN].setVisible(false);
      btns[BTN_MINIMZ].setVisible(false);
    }

    int x = sz.width - btnSize.width * curBtnsCount;
    for (int i = 0; i < BTNS_COUNT; i++) {
      if (btns[i].isVisible()) {
        btns[i].setBounds(x, 1, btnSize.width, btnSize.height);
        x += btnSize.width;
      }
    }
    repaint();
  }

  public void componentResized(ComponentEvent e) {
    updateButtonsSuite();
  }

  public void componentShown(ComponentEvent e) {
  }

  public void updatePinIcon() {
    btns[BTN_PIN].setIcon(boat.isPin() ? imPin : imUnpin);
  }

  public void actionPerformed(ActionEvent e) {
    String cmd = e.getActionCommand();
    if (cmd == CMD_FLOAT) {
      boatListener.requestActive(boat);
      boatListener.requestBehaviour(boat, BoatEmpty.BST_FLOAT);
    } else if (cmd == CMD_SLIDE) {
      boatListener.requestActive(boat);
      boatListener.requestBehaviour(boat, BoatEmpty.BST_SLIDE);
    } else if (cmd == CMD_DOCK) {
      boatListener.requestActive(boat);
      boatListener.requestBehaviour(boat, BoatEmpty.BST_DOCK);
    } else if (cmd == CMD_PIN) {
      boatListener.requestActive(boat);
      boatListener.requestPin(boat, !boat.isPin());
    } else if (cmd == CMD_MIN) {
      boatListener.requestInvisible(boat);
    }
  }

  public void setDisposer(Disposer disposer) {
    this.disposer = disposer;
  }

  public void setBoatListener(BoatListener bl) {
    boatListener = bl;
  }

  public BoatEmpty getBoat() {
    return boat;
  }

  private class MotionPainter2 implements ActionListener {

    private static final int SLEEP_TIME = 10;
    private static final int MOTION_TIME = 100;

    private Color colorFrom;
    private Color colorTo;
    private Color colorFont;
    private int steps;
    private int step;
    private Timer timer;

    MotionPainter2(boolean activate) {
      if (activate) {
        colorFrom = (Color) ZUIManager.getProperty("boatTitle.bgInactive");
        colorTo = (Color) ZUIManager.getProperty("boatTitle.bgActive");
        colorFont = (Color) ZUIManager.getProperty("boatTitle.frActive");
      } else {
        colorFrom = (Color) ZUIManager.getProperty("boatTitle.bgActive");
        colorTo = (Color) ZUIManager.getProperty("boatTitle.bgInactive");
        colorFont = (Color) ZUIManager.getProperty("boatTitle.frInactive");
      }
      steps = MOTION_TIME / SLEEP_TIME;
      step = 0;
      timer = new Timer(SLEEP_TIME, this);
    }

    public void start() {
      timer.start();
    }

    public void actionPerformed(ActionEvent evt) {
      if (step < steps) {
        Color c1 = nextColor(colorFrom, colorTo, steps, step);
        BoatTitle.this.setBackground(c1);
        step++;
      } else {
        timer.stop();
        BoatTitle.this.setBackground(colorTo);
        BoatTitle.this.setForeground(colorFont);
      }
    }

    private Color nextColor(Color c1, Color c2, int total, int step) {
      return new Color(
          conv(c1.getRed(), c2.getRed(), total, step),
          conv(c1.getGreen(), c2.getGreen(), total, step),
          conv(c1.getBlue(), c2.getBlue(), total, step));
    }

    private int conv(int a1, int a2, int h, int i) {
      return a1 + (i * (a2 - a1)) / (h - 1);
    }
  }
}
