package com.zwing.color;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created: 02.08.2006 17:58:43
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DefaultColors {

    private ArrayList<ColorElement> colors;

    public DefaultColors() {
        colors = new ArrayList<ColorElement>(0);
    }

    public DefaultColors(ColorElement[] colors) {
        this.colors = new ArrayList<ColorElement>(0);
        this.colors.addAll(Arrays.asList(colors));
    }

    public void add(ColorElement ce) {
        colors.add(ce);
    }

}
