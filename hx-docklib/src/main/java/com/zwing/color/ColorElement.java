package com.zwing.color;

import com.zwing.utils.ResourceManager;
import com.zwing.utils.Objs;

import java.awt.Color;

/**
 * Created: 02.08.2006 18:02:41
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ColorElement {
    private final Color color;
    private final String name;

    private static final String RES_UNDEFINED_COLOR = "colorElement.undefined";
    private static final ColorElement UNDEFINED_COLOR = createUndefinedColorElement();

    private static final String RES_POSTFIX_COLOR = ResourceManager.DOT + "color";
    private static final String RES_POSTFIX_NAME = ResourceManager.DOT + "name";

    public ColorElement(Color color, String name) {
        this.color = color;
        this.name = name;
    }

    public ColorElement(String name) {
        this(null, name);
    }

    public ColorElement(Color color) {
        this(color, null);
    }

    public boolean isUndefined() {
        return color == null;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        boolean rslt = false;
        if (o instanceof ColorElement) {
            final ColorElement other = (ColorElement) o;
            rslt = Objs.safeEquals(this.color, other.color) &&
                    Objs.safeEquals(this.name, other.name);
        }
        return rslt;
    }

    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }

    private static ColorElement createUndefinedColorElement() {
        String n = ResourceManager.getString(
                RES_UNDEFINED_COLOR + RES_POSTFIX_NAME, "undefined");
        return new ColorElement(null, n);
    }

    public static ColorElement getUndefinedColorElement() {
        return UNDEFINED_COLOR;
    }

    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }
}
