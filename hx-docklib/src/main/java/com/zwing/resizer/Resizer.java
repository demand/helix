package com.zwing.resizer;


import java.awt.*;

/**
 * Created: 20.09.2007 10:50:09
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Resizer {

    /**
     * Какие границы объекта можно использовать для изменения его размеров
     */
    public static final int RESIZABLE_NORTH = 0x0100;
    public static final int RESIZABLE_SOUTH = 0x0200;
    public static final int RESIZABLE_WEST  = 0x0400;
    public static final int RESIZABLE_EAST  = 0x0800;
    public static final int RESIZABLE_ALL   = 0x0F00;
    public static final int RESIZABLE_NONE  = 0x0000;

    private static final ActivityMode[] TRANSLATE = new ActivityMode[] {
            ActivityMode.NONE, ActivityMode.RESIZE_W,       //  0  1
            ActivityMode.RESIZE_E,  ActivityMode.NONE,      //  2  3
            ActivityMode.RESIZE_N,  ActivityMode.RESIZE_NW, //  4  5
            ActivityMode.RESIZE_NE, ActivityMode.NONE,      //  6  7
            ActivityMode.RESIZE_S,  ActivityMode.RESIZE_SW, //  8  9
            ActivityMode.RESIZE_SE, ActivityMode.NONE,      // 10 11
            ActivityMode.NONE, ActivityMode.NONE,           // 12 13
            ActivityMode.NONE, ActivityMode.NONE            // 14 15
    };

    private static final int MASK_WEST  = 0x1;
    private static final int MASK_EAST  = 0x2;
    private static final int MASK_NORTH = 0x4;
    private static final int MASK_SOUTH = 0x8;

    /**
     *
     */
    public enum ActivityMode {
        NONE,                             // 0
        MOVE_FREE, MOVE_H, MOVE_V,        // 1, 2, 3
        MVPART_FREE, MVPART_H, MVPART_V,  // 4, 5, 6
        RESIZE_N, RESIZE_NW, RESIZE_NE,   // 7, 8, 9
        RESIZE_S, RESIZE_SW, RESIZE_SE,   // 10, 11, 12
        RESIZE_W, RESIZE_E,               // 13, 14
        SPLIT_H, SPLIT_V, SPLIT_HV        // 15, 16, 17
    }
    private static final int[] CURSOR_BY_DRAG_TYPE = new int[] {
            Cursor.DEFAULT_CURSOR,   //  0 - границы окна не меняются
            Cursor.HAND_CURSOR,      //  1 - объект целиком
            Cursor.W_RESIZE_CURSOR,  //  2 - объект только горизонтально
            Cursor.N_RESIZE_CURSOR,  //  3 - объект только вертикально
            Cursor.MOVE_CURSOR,      //  4 - часть объекта в любом направлении
            Cursor.W_RESIZE_CURSOR,  //  5 - часть объекта только горизонтально
            Cursor.N_RESIZE_CURSOR,  //  6 - часть объекта только вертикально
            Cursor.N_RESIZE_CURSOR,  //  7 - верхняя граница
            Cursor.NW_RESIZE_CURSOR, //  8 - верхняя и левая граница
            Cursor.NE_RESIZE_CURSOR, //  9 - верхняя и правая граница
            Cursor.N_RESIZE_CURSOR,  // 10 - нижняя граница
            Cursor.NE_RESIZE_CURSOR, // 11 - нижняя и левая граница
            Cursor.NW_RESIZE_CURSOR, // 12 - нижняя и правая граница
            Cursor.W_RESIZE_CURSOR,  // 13 - правая граница
            Cursor.W_RESIZE_CURSOR,  // 14 - леввая граница
            Cursor.W_RESIZE_CURSOR,  // 15 - разделитель, ориентированный вертикально
            Cursor.N_RESIZE_CURSOR,  // 16 - разделитель, ориентированный горизонтально
            Cursor.MOVE_CURSOR       // 17 - оба разделителя - горизонтальный и вертикальный
    };

    public static Cursor getCursorForDragType(ActivityMode activityMode) {
        return Cursor.getPredefinedCursor(CURSOR_BY_DRAG_TYPE[activityMode.ordinal()]);
    }

    private static boolean isNotInActiveBorder(Dimension sz, Insets ins, int behaviour, Point p) {
        Rectangle r = new Rectangle(
                ins.left, ins.top, sz.width - ins.right - ins.left,
                sz.height - ins.bottom - ins.top);
        if (p.x < 0 || p.y < 0 || p.x >= sz.width || p.y >= sz.height || r.contains(p)) {
            return true;
        }
        return false;
    }

    /**
     * @param sz размер объекта
     * @param rc объект
     * @param p точка внутри объекта
     * @return DockingConst.DRAG_XXX
     */
    public static ActivityMode calcResizeType(Dimension sz, ResizeObject rc, Point p) {
        int x0 = p.x;
        int y0 = p.y;
        Insets ins = rc.getBorderSize();

        if (isNotInActiveBorder(sz, ins, rc.getBehaviour(), p)) {
            return ActivityMode.NONE;
        }

        int c = 0;
        boolean northActive = (rc.getBehaviour() & RESIZABLE_NORTH) != 0;
        boolean westActive = (rc.getBehaviour() & RESIZABLE_WEST) != 0;
        boolean southActive = (rc.getBehaviour() & RESIZABLE_SOUTH) != 0;
        boolean eastActive = (rc.getBehaviour() & RESIZABLE_EAST) != 0;

        if (northActive && y0 < ins.top + rc.getCornerSnapSize()) {
            c |= MASK_NORTH;
        } else if (southActive && y0 > sz.height - ins.bottom - rc.getCornerSnapSize()) {
            c |= MASK_SOUTH;
        }

        if (westActive && x0 < ins.left + rc.getCornerSnapSize()) {
            c |= MASK_WEST;
        } else if (eastActive && x0 > sz.width - ins.right - rc.getCornerSnapSize()) {
            c |= MASK_EAST;
        }

        return TRANSLATE[c];
    }

    public static boolean innerPoint(Dimension sz, ResizeObject rc, Point p) {
        boolean rslt = false;
        Insets insets = rc.getBorderSize();
        if (isValueInsideRange(p.x, insets.left, sz.width - insets.right) &&
                isValueInsideRange(p.y, insets.top, sz.height - insets.bottom - 1)){
            rslt = true;
        }
        return rslt;
    }

    public static boolean isValueInsideRange(int value, int from, int to) {
        return value >= from && value <= to;
    }

    public static Rectangle calcResizedRectangle(ResizeObject rc,
            ActivityMode activityMode, Point dragDistance) {
        Rectangle r = rc.getBounds();
        Rectangle r2 = new Rectangle(r);

        if (activityMode == Resizer.ActivityMode.MOVE_FREE) {
            r2.x = r.x + dragDistance.x;
            r2.y = r.y + dragDistance.y;
        } else {
            if (isResizeWest(activityMode)) {
                r2.x = r.x + dragDistance.x;
                r2.width -= dragDistance.x;
            }

            if (isResizeNorth(activityMode)) {
                r2.y = r.y + dragDistance.y;
                r2.height -= dragDistance.y;
            }

            if (isResizeEast(activityMode)) {
                r2.width += dragDistance.x;
            }

            if (isResizeSouth(activityMode)) {
                r2.height += dragDistance.y;
            }
        }

        return r2;
    }

    private static boolean isResizeNorth(Resizer.ActivityMode activityMode) {
        return activityMode == Resizer.ActivityMode.RESIZE_N  ||
                activityMode == Resizer.ActivityMode.RESIZE_NW ||
                activityMode == Resizer.ActivityMode.RESIZE_NE;
    }

    private static boolean isResizeWest(Resizer.ActivityMode activityMode) {
        return activityMode == Resizer.ActivityMode.RESIZE_W  ||
                activityMode == Resizer.ActivityMode.RESIZE_NW ||
                activityMode == Resizer.ActivityMode.RESIZE_SW;
    }

    private static boolean isResizeEast(Resizer.ActivityMode activityMode) {
        return activityMode == Resizer.ActivityMode.RESIZE_E  ||
                activityMode == Resizer.ActivityMode.RESIZE_NE ||
                activityMode == Resizer.ActivityMode.RESIZE_SE;
    }

    private static boolean isResizeSouth(Resizer.ActivityMode activityMode) {
        return activityMode == Resizer.ActivityMode.RESIZE_S  ||
                activityMode == Resizer.ActivityMode.RESIZE_SW ||
                activityMode == Resizer.ActivityMode.RESIZE_SE;
    }
}
