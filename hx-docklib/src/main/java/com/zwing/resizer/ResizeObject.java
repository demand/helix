package com.zwing.resizer;

import java.awt.*;

/**
 * Created: 21.09.2007 18:55:09
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ResizeObject {
    int getCornerSnapSize();
    Insets getBorderSize();
    int getBehaviour();

    Rectangle getBounds();
    Dimension getMinimumSize();
    Dimension getMaximumSize();
    Dimension getPrefferedSize();
}
