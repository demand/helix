package com.genego.st;

import com.Ostermiller.Syntax.ToHTML;
import com.Ostermiller.bte.CompileException;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;

/**
 * InfoSourcePanel
 * Created: 23.05.2011, 19:10:50
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class InfoSourcePanel extends JPanel {

    private JTextPane sourceTextPane;
    private DocumentHandler docHandler;

    private JTextPane outputTextPane;
//    private JEditorPane codeEditor;

    public InfoSourcePanel() {
        outputTextPane = new JTextPane();

        JScrollPane scrollPane = new JScrollPane(outputTextPane,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.CENTER);

        docHandler = new DocumentHandler();

        updateSourceText();
    }

    public void setSourceTextPane(JTextPane textPane) {
        if (this.sourceTextPane != null) {
            this.sourceTextPane.getDocument().removeDocumentListener(docHandler);
        }

        this.sourceTextPane = textPane;

        textPane.getDocument().addDocumentListener(docHandler);
        updateSourceText();
    }

    private void updateSourceText() {
        String rslt = "";

        if (sourceTextPane != null) {
            String htmlText = sourceTextPane.getText();

            try {
                StringWriter writer = new StringWriter();
                ToHTML toHTML = new ToHTML();
                toHTML.setInput(new StringReader(htmlText));
                toHTML.setOutput(writer);
                toHTML.setMimeType("text/html");
                toHTML.writeFullHTML();
                writer.close();
                rslt = writer.toString();

                rslt = rslt.replaceFirst("<!DOCTYPE[^>]*>", "");
                rslt = rslt.replaceAll("\r\n", "");
                rslt = rslt.replaceAll("<head>.*</head>", "");

            } catch (IOException e) {
            } catch (InvocationTargetException e) {
            } catch (CompileException e) {
            } catch (Exception e) {
            }
        }
        outputTextPane.setContentType("text/html");
        outputTextPane.setText(rslt);
    }


    private class DocumentHandler implements DocumentListener {
        public void insertUpdate(DocumentEvent e) {
            updateSourceText();
        }
        public void removeUpdate(DocumentEvent e) {
            updateSourceText();
        }
        public void changedUpdate(DocumentEvent e) {
            updateSourceText();
        }
    }

}
