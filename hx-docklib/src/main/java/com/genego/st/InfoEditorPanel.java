package com.genego.st;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * InfoEditorPanel
 * Created: 10.05.2011, 22:19:20
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class InfoEditorPanel extends JPanel {

    private InfoEditor editor;


    public InfoEditorPanel() {
        setLayout(new BorderLayout());

        JScrollPane scrollPane = new JScrollPane(
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        editor = new InfoEditor();
        scrollPane.setViewportView(editor);

        add(scrollPane, BorderLayout.CENTER);

        Component toolbar = createToolbar();
        if (toolbar != null) {
            add(toolbar, BorderLayout.NORTH);
        }
    }

    public InfoEditor getEditor() {
        return editor;
    }

    protected Component createToolbar() {
        JToolBar toolbar = new JToolBar(JToolBar.HORIZONTAL);
        toolbar.setFloatable(false);
        toolbar.setRollover(true);


        for (final Object attribKey : new Object[] {
                StyleConstants.Bold, StyleConstants.Italic, StyleConstants.Underline,
                StyleConstants.Subscript, StyleConstants.Superscript, StyleConstants.StrikeThrough
        }) {
            JToggleButton button = new JToggleButton(editor.getToggleAction(attribKey)) {
                public Insets getInsets() {
                    Border border = getBorder();
                    return border.getBorderInsets(this);
                }
            };
            button.setModel(editor.getToggleButtonModel(attribKey));
            button.setFocusable(false);
            toolbar.add(button);
        }

        toolbar.addSeparator();

        for (final Object attribKey : new Object[] {
                StyleConstants.FontFamily, StyleConstants.FontSize
        }) {
            JComboBox combo = new JComboBox() {
                public Dimension getMaximumSize() {
                    return getPreferredSize();
                }
            };
            combo.setModel(editor.getComboModel(attribKey));
            combo.addItemListener(editor.getComboListener(attribKey));
            combo.setFocusable(false);
            toolbar.add(combo);

            toolbar.addSeparator();
        }

        for (final Object attribKey : new Object[] {
                ToggleActionsContainer.AlignmentLeftKey,
                ToggleActionsContainer.AlignmentRightKey,
                ToggleActionsContainer.AlignmentCenterKey,
                ToggleActionsContainer.AlignmentJustifyKey
        }) {
            JToggleButton button = new JToggleButton(editor.getToggleAction(attribKey)) {
                public Insets getInsets() {
                    Border border = getBorder();
                    return border.getBorderInsets(this);
                }
            };
            button.setModel(editor.getToggleButtonModel(attribKey));
            button.setFocusable(false);
            toolbar.add(button);
        }

        toolbar.addSeparator();

        JComboBox cmbStyles = new JComboBox(editor.getStylesComboModel()) {
            public Dimension getMaximumSize() {
                return getPreferredSize();               
            }
        };
        cmbStyles.addItemListener(editor.getStylesComboListener());
        cmbStyles.setFocusable(false);
        toolbar.add(cmbStyles);

        toolbar.addSeparator();

        /*
        toolbar.add(new JButton(new AbstractAction("Add combo") {
            public void actionPerformed(ActionEvent e) {
                editor.addComboComponent();
            }
        }));
        */

        toolbar.add(new JButton(new AbstractAction("Add link") {
            public void actionPerformed(ActionEvent e) {
                editor.addHyperlink();
            }
        }));

        /*
        toolbar.add(new JButton(new AbstractAction("Add button") {
            public void actionPerformed(ActionEvent e) {
                editor.addButton();
            }
        }));
        */
        
        return toolbar;
    }
}
