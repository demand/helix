package com.genego.st;

import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyleContext;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * InfoDocument
 * Created: 26.05.2011, 17:40:16
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class InfoDocument extends DefaultStyledDocument {

    public String[] getStyles() {
        ArrayList<String> rslt = new ArrayList<String>();
        StyleContext styleContext = (StyleContext) getAttributeContext();
        Enumeration<?> styleNames = styleContext.getStyleNames();
        while (styleNames.hasMoreElements()) {
            Object style = styleNames.nextElement();
            rslt.add(style.toString());
        }
        return rslt.toArray(new String[rslt.size()]);
    }
}
