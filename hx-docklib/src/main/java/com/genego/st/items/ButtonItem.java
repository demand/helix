package com.genego.st.items;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

/**
 * ButtonItem
 * Created: 20.05.2011, 20:08:01
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class ButtonItem extends JButton {
    public ButtonItem() {
        setText("Button");
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                System.out.println("moved in button");
            }
        });
    }
}
