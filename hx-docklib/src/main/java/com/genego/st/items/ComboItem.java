package com.genego.st.items;

import javax.swing.*;

/**
 * ComboItem
 * Created: 19.05.2011, 14:49:29
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class ComboItem extends JComboBox {
    public ComboItem() {
        for (int i = 0; i < 10; i++) addItem("Item " + i);
        setOpaque(false);
    }
}
