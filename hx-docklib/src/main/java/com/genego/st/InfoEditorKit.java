package com.genego.st;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.*;
import java.awt.*;

/**
 * InfoEditorKit
 * Created: 24.05.2011, 17:24:45
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class InfoEditorKit extends StyledEditorKit
        implements ViewFactory, DocumentListener {

    public InfoEditorKit() {
    }

    public View create(Element elem) {
        return null;
    }

    public void insertUpdate(DocumentEvent e) {
    }

    public void removeUpdate(DocumentEvent e) {
    }

    public void changedUpdate(DocumentEvent e) {
    }


    static class HighlightView extends WrappedPlainView {

        public HighlightView(Element elem) {
            super(elem);
        }

        protected int drawUnselectedText (Graphics g, int x, int y, int p0, int p1)
                throws BadLocationException {
            Document doc = getDocument();
            Segment segment = getLineBuffer();
            int t = 0;//findIndexOfTokenContaining(p0);

            while (p0 < p1) {
                int p2 = 0;//getToken(t+1).position;
                if (p2 > p1) p2 = p1;
                doc.getText(p0, p2 - p0, segment);
//                g.setColor(colorOf(getToken(t).symbol.type));
                x = Utilities.drawTabbedText(segment, x, y, g, this, p0);
                p0 = p2;
                t++;
            }
            return x;
        }

        protected int drawSelectedText (Graphics g, int x, int y, int p0, int p1)
                throws BadLocationException {
            return super.drawSelectedText(g, x, y, p0, p1);
        }
    }
}
