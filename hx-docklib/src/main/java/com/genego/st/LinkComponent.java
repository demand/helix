package com.genego.st;

import javax.swing.*;
import java.awt.*;

/**
 * LinkComponent
 * Created: 12.07.2011, 18:45:34
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class LinkComponent extends JPanel {

    private JLabel label;

    public LinkComponent(String text) {
        setLayout(new BorderLayout());
        label = new JLabel(text);
        add(label, BorderLayout.CENTER);
        setOpaque(true);

        setBackground(Color.YELLOW);
        label.setBackground(Color.MAGENTA);
        label.setOpaque(true);
    }

    public Dimension getMaximumSize() {
        return label.getMaximumSize();
    }

    public Dimension getPreferredSize() {
        return label.getMinimumSize();
    }
}
