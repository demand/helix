package com.genego.st;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeModelListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * DocStructurePanel
 * Created: 23.05.2011, 15:08:29
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class DocStructurePanel extends JPanel {

    private JTree tree;
    private JTextPane textPane;
    private DocumentHandler docHandler;

    public DocStructurePanel() {
        tree = new JTree();
        JScrollPane scrollPane = new JScrollPane(tree,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.CENTER);

        docHandler = new DocumentHandler();

        tree.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                TreePath path = tree.getClosestPathForLocation(e.getX(), e.getY());
                DocTreeElement treeElement = (DocTreeElement) path.getLastPathComponent();
                tree.setToolTipText(treeElement != null ? treeElement.getHint() : null);
            }
        });

        updateStructure();
    }

    public void updateStructure() {
        if (textPane != null) {
            Document doc = textPane.getDocument();
            if (doc instanceof DefaultStyledDocument) {
                DefaultStyledDocument hdoc = (DefaultStyledDocument) doc;
                Element root = textPane.getDocument().getDefaultRootElement();
                tree.setModel(new DocTree(createTreeElement(root)));
            }
        }    
    }

    private DocTreeElement createTreeElement(Element element) {
        DocTreeElement rslt;
        if (element == null) {
            rslt = new DocTreeElement("[NULL]");
        } else {
            String name = element.getName();
            name = name == null ? "name is NULL" : name;
            int start = element.getStartOffset();
            int end = element.getEndOffset();
            int childrenCount = element.getElementCount();
            rslt = new DocTreeElement(name + " (" + start + ":" + end + ")",
                    createHintHtml(element.getAttributes()));
            for (int i = 0; i < childrenCount; i++) {
                rslt.addChild(createTreeElement(element.getElement(i)));
            }
        }
        return rslt;
    }

    private String createHintHtml(AttributeSet attributes) {
        if (attributes == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("<html><b>Attributes</b>");
        Enumeration<?> attributeNames = attributes.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            Object elm = attributeNames.nextElement();
            sb.append("<br>");
            sb.append(elm.toString());
            sb.append(" = ");
            sb.append(attributes.getAttribute(elm));
        }
        return sb.toString();
    }


    public void setTextPane(JTextPane textPane) {
        if (this.textPane != null) {
            this.textPane.getDocument().removeDocumentListener(docHandler);
        }

        this.textPane = textPane;

        textPane.getDocument().addDocumentListener(docHandler);
        updateStructure();
    }

    private class DocumentHandler implements DocumentListener {
        public void insertUpdate(DocumentEvent e) {
            updateStructure();
        }
        public void removeUpdate(DocumentEvent e) {
            updateStructure();
        }
        public void changedUpdate(DocumentEvent e) {
            updateStructure();
        }
    }

    private static class DocTreeElement {
        private String name;
        private String hint;

        private ArrayList<DocTreeElement> children;

        private DocTreeElement(String name) {
            this(name, null);
        }

        private DocTreeElement(String name, String hint) {
            this.name = name;
            this.hint = hint;
        }

        private String getName() {
            return name;
        }

        private String getHint() {
            return hint;
        }

        private DocTreeElement getChild(int index) {
            return isLeaf() ? null : children.get(index);
        }

        private int getChildCount() {
            return isLeaf() ? 0 : children.size();
        }

        private boolean isLeaf() {
            return children == null;
        }

        private int getIndexOfChild(Object child) {
            return isLeaf() ? null : children.indexOf((DocTreeElement) child);
        }

        public void addChild(DocTreeElement treeElement) {
            if (children == null) {
                children = new ArrayList<DocTreeElement>();
            }
            children.add(treeElement);
        }

        public String toString() {
            return name;
        }
    }

    private static class DocTree implements TreeModel {

        private Object root;

        public DocTree(Object root) {
            this.root = root;
        }

        public Object getRoot() {
            return root;
        }

        public Object getChild(Object parent, int index) {
            return ((DocTreeElement) parent).getChild(index);
        }

        public int getChildCount(Object parent) {
            return ((DocTreeElement) parent).getChildCount();
        }

        public boolean isLeaf(Object node) {
            return ((DocTreeElement) node).isLeaf();
        }

        public void valueForPathChanged(TreePath path, Object newValue) {
        }

        public int getIndexOfChild(Object parent, Object child) {
            return ((DocTreeElement) parent).getIndexOfChild(child);
        }

        public void addTreeModelListener(TreeModelListener l) {
        }

        public void removeTreeModelListener(TreeModelListener l) {
        }
    }

}
