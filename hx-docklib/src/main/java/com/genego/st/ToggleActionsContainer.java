package com.genego.st;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static javax.swing.text.StyleConstants.*;

/**
 * ToggleActionsContainer
 * Created: 27.05.2011, 18:14:10
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class ToggleActionsContainer {

    private JTextPane editor;

    private Map<Object, AttibController> ctrlMap;

    public static final String ICON_PATH = "/com/genego/st/";

    public static final Object AlignmentLeftKey = "Alignment.Left";
    public static final Object AlignmentRightKey = "Alignment.Right";
    public static final Object AlignmentCenterKey = "Alignment.Center";
    public static final Object AlignmentJustifyKey = "Alignment.Justify";

    public ToggleActionsContainer(JTextPane editor) {
        this.editor = editor;

        createDefaultActions();
    }

    private void createDefaultActions() {

        String[] availableFontFamilyNames =
                GraphicsEnvironment.getLocalGraphicsEnvironment().
                getAvailableFontFamilyNames();

        ctrlMap = new HashMap<Object, AttibController>();
        for (AttibController ctrl : new AttibController[] {
                new BooleanAttibController(Bold, "Bold", "bold24.png"),
                new BooleanAttibController(Italic, "Italic", "italic24.png"),
                new BooleanAttibController(Underline, "Underline", "uline24.png"),
                new BooleanAttibController(Subscript, "Subscript", "sub24.png"),
                new BooleanAttibController(Superscript, "Superscript", "super24.png"),
                new BooleanAttibController(StrikeThrough, "StrikeThrough", "strike24.png"),

                new ComboAttribController(FontFamily, availableFontFamilyNames),
//                new ComboAttribController(StyleConstants.FontFamily, new String[] {
//                        "Arial", "Serif", "Times New Roman"}),
                new ComboAttribController(FontSize, new Integer[] {
                        8, 9, 10, 11, 12, 13, 14, 15, 16,
                        18, 20, 22, 24, 26, 32, 38, 44, 60}),

                new GroupAttibController(Alignment),
                new GroupAttibItem(AlignmentLeftKey, Alignment, "Align left", "left24.png", ALIGN_LEFT, ALIGN_JUSTIFIED),
                new GroupAttibItem(AlignmentRightKey, Alignment, "Align right", "right24.png", ALIGN_RIGHT, ALIGN_LEFT),
                new GroupAttibItem(AlignmentCenterKey, Alignment, "Align center", "center24.png", ALIGN_CENTER, ALIGN_LEFT),
                new GroupAttibItem(AlignmentJustifyKey, Alignment, "Align justify", "just24.png", ALIGN_JUSTIFIED, ALIGN_LEFT),

        }) registerController(ctrl);
    }

    private void registerController(AttibController attibController) {
        if (attibController instanceof GroupAttibItem) {
            GroupAttibItem item = (GroupAttibItem) attibController;
            GroupAttibController groupController = (GroupAttibController) ctrlMap.get(item.getStyleKey());
            groupController.registerItem(item);
            ctrlMap.put(item.getCtrlKey(), attibController);
        } else {
            ctrlMap.put(attibController.getStyleKey(), attibController);
        }
    }

    public void updateButtonModels(AttributeSet attributes) {
        for (Map.Entry<Object, AttibController> entry : ctrlMap.entrySet()) {
            Object key = entry.getKey();
            AttibController ctrl = entry.getValue();
            if (ctrl instanceof ToggleAttibController) {
                ToggleAttibController c = (ToggleAttibController) ctrl;
                c.getButtonModel().setSelected(c.allowSelected(attributes.getAttribute(ctrl.getStyleKey())));
            } else if (ctrl instanceof ComboAttribController) {
                ComboAttribController c = (ComboAttribController) ctrl;
                c.setListenerEnabled(false);
                c.getComboModel().setSelectedItem(attributes.getAttribute(key));
                c.setListenerEnabled(true);
            }
        }
    }


    private void registerAttributeAction(Object attributeKey) {
        if (!ctrlMap.containsKey(attributeKey)) {
                                                
        }
    }

    public Action getToggleAction(Object attribKey) {
        Action rslt = null;
        AttibController attibController = ctrlMap.get(attribKey);
        rslt = ((ToggleAttibController) attibController).getAction();
        return rslt;
    }

    public ButtonModel getToggleButtonModel(Object attribKey) {
        return ((ToggleAttibController) ctrlMap.get(attribKey)).getButtonModel();
    }

    public ComboBoxModel getComboModel(Object attribKey) {
        return ((ComboAttribController) ctrlMap.get(attribKey)).getComboModel();
    }

    public ItemListener getComboListener(Object attribKey) {
        return ((ComboAttribController) ctrlMap.get(attribKey)).getComboListener();
    }

    private static class ToggleAttributes {
        private final SimpleAttributeSet on;
        private final SimpleAttributeSet off;

        private ToggleAttributes(Object attributeKey, Object valueOn, Object valueOff) {
            on = new SimpleAttributeSet();
            off = new SimpleAttributeSet();
            on.addAttribute(attributeKey, valueOn);
            off.addAttribute(attributeKey, valueOff);
        }

        public SimpleAttributeSet on() {
            return on;
        }

        public SimpleAttributeSet off() {
            return off;
        }
    }

    private class ComboAttribController extends AttibController {

        private Object[] values;
        private SimpleAttributeSet[] attributes;
        private ComboBoxModel model;
        private boolean listenerEnabled;

        private ItemListener itemListener;

        private ComboAttribController(Object styleKey, Object[] values) {
            super(styleKey);
            model = new DefaultComboBoxModel();
            setValues(values);
            itemListener = new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    if (listenerEnabled) {
                        int start = editor.getSelectionStart();
                        int end = editor.getSelectionEnd();
                        if (end > start) {
                            int index = getSelectedIndex(model.getSelectedItem());
                            if (index >= 0) {
                                editor.getStyledDocument().setCharacterAttributes(
                                        start, end - start,
                                        attributes[index],
                                        false);
                            }
                        }
                    }
                }
            };
        }

        public boolean isListenerEnabled() {
            return listenerEnabled;
        }

        public void setListenerEnabled(boolean listenerEnabled) {
            this.listenerEnabled = listenerEnabled;
        }

        private int getSelectedIndex(Object item) {
            int rslt = -1;
            if (item != null) {
                int i = 0;
                for (Object v : this.values) {
                    if (item.equals(v)) {
                        rslt = i;
                        break;
                    }
                    i++;
                }
            }
            return rslt;
        }

        public void setValues(Object[] values) {
            this.values = values;
            attributes = new SimpleAttributeSet[values != null ? values.length : 0];
            DefaultComboBoxModel dmodel = (DefaultComboBoxModel) model;
            dmodel.removeAllElements();
            int i = 0;
            for(Object v : this.values) {
                dmodel.addElement(v);
                SimpleAttributeSet a = new SimpleAttributeSet();
                a.addAttribute(getStyleKey(), v);
                attributes[i++] = a;
            }
            model.setSelectedItem(this.values.length > 0 ? this.values[0] : null);
        }

        public ComboBoxModel getComboModel() {
            return model;
        }

        public ItemListener getComboListener() {
            return itemListener;
        }
    }

    private class BooleanAttibController extends ToggleAttibController {
        private BooleanAttibController(Object styleKey, String name, String iconName) {
            super(styleKey, name, iconName, Boolean.TRUE, Boolean.FALSE);
        }
    }

    /**
     * Класс, связаный c toggle кнопкой и отвечающий как за
     * изменение документа при выполнении команды, так и за
     * изменение модели кнопки при движении по документу.
     */
    private class ToggleAttibController extends AttibController {

        /**
         * Значение, атрибута, которое будет установлено
         * при нажатии на кнопку. Это значение используется
         * для определения состояния модели кнопки при
         * движении по документу.
         */
        private Object valueToBeSelected;

        private ButtonModel buttonModel;

        private Action action;

        /**
         * Значение, атрибута, которое будет установлено
         * когда кнопка "отжимается".
         */
        private Object valueToBeDeselected;

        private ToggleAttributes attributes;

        /**
         * @param styleKey Ключ атрибута. Используются ключи из StyleConstants.
         * @param name Имя кнопки. (не используется)
         * @param iconName Имя файла с иконкой. (без пути, с расширением)
         * @param valueToBeSelected Значение, атрибута, которое будет установлено при нажатии на кнопку.
         * @param valueToBeDeselected Значение, атрибута, которое будет установлено когда кнопка "отжимается".
         */
        private ToggleAttibController(Object styleKey,
                String name, String iconName,
                Object valueToBeSelected,
                Object valueToBeDeselected) {
            super(styleKey);
            URL resource = getClass().getResource(ICON_PATH + iconName);
            Icon icon = resource != null ? new ImageIcon(resource) : null;
            init(name, icon, valueToBeSelected, valueToBeDeselected);
        }

        private void init(String name, Icon icon,
                Object valueToBeSelected,
                Object valueToBeDeselected) {
            this.action = new AbstractAction(null, icon) {
                public void actionPerformed(ActionEvent e) {
                    ToggleAttibController.this.actionPerformed(e);
                }
            };
            this.valueToBeSelected = valueToBeSelected;
            this.valueToBeDeselected = valueToBeDeselected;
            this.buttonModel = new DefaultButtonModel();
            attributes = new ToggleAttributes(getStyleKey(),
                    valueToBeSelected, valueToBeDeselected);
        }

        public Action getAction() {
            return action;
        }

        public ButtonModel getButtonModel() {
            return buttonModel;
        }

        public void actionPerformed(ActionEvent e) {
            applyAttribites(buttonModel != null && buttonModel.isSelected() ?
                            attributes.off() : attributes.on());
            if (buttonModel != null) {
                buttonModel.setSelected(!buttonModel.isSelected());
            }
        }

        public boolean allowSelected(Object attributeValue) {
            return valueToBeSelected != null &&
                    valueToBeSelected.equals(attributeValue);
        }
    }

    private class GroupAttibController extends AttibController {

        private Map<Object, GroupAttibItem> map;

        public GroupAttibController(Object styleKey) {
            super(styleKey);
            map = new HashMap<Object, GroupAttibItem>(0);
        }

        public ButtonModel getButtonModel(Object ctrlKey) {
            return map.get(ctrlKey).getButtonModel();
        }

        public Action getAction(Object ctrlKey) {
            return map.get(ctrlKey).getAction();
        }

        public void updateItems() {
            StyledDocument doc = editor.getStyledDocument();
            javax.swing.text.Element element = doc.getCharacterElement(editor.getCaretPosition());
            updateButtonModels(element.getAttributes());
        }

        public void registerItem(GroupAttibItem item) {
            map.put(item.getCtrlKey(), item);
            item.setGroup(GroupAttibController.this);
        }
    }

    private class GroupAttibItem extends ToggleAttibController {
        private Object ctrlKey;
        private GroupAttibController group;

        public GroupAttibItem(Object ctrlKey,
                Object styleKey, String name, String iconName,
                Object valueToBeSelected,
                Object valueToBeDeselected) {
            super(styleKey, name, iconName, valueToBeSelected, valueToBeDeselected);
            this.ctrlKey = ctrlKey;
        }

        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            group.updateItems();
        }

        public Object getCtrlKey() {
            return ctrlKey;
        }

        public void setGroup(GroupAttibController group) {
            this.group = group;
        }
    }

    private class AttibController {
        /**
         * Ключ атрибута. Используются ключи из StyleConstants.
         */
        private Object styleKey;

        private AttibController(Object styleKey) {
            this.styleKey = styleKey;
        }

        public Object getStyleKey() {
            return styleKey;
        }

        public boolean isCharacterAttribute() {
            return styleKey instanceof AttributeSet.CharacterAttribute;
        }

        public boolean isParagraphAttribute() {
            return styleKey instanceof AttributeSet.ParagraphAttribute;
        }

        public void applyCharacterStyle(AttributeSet attributes) {
            int start = editor.getSelectionStart();
            int end = editor.getSelectionEnd();
            if (end > start) {
                editor.getStyledDocument().setCharacterAttributes(
                        start, end - start, attributes, false);
            }
        }

        public void applyParagraphStyle(AttributeSet attributes) {
            editor.setParagraphAttributes(attributes, false);
        }

        public void applyAttribites(AttributeSet attributes) {
            if (isCharacterAttribute()) {
                applyCharacterStyle(attributes);
            } else {
                applyParagraphStyle(attributes);
            }
        }
    }
}
