package com.genego.st;

import javax.swing.*;
import java.awt.*;

/**
 * HyperlinkDialog
 * Created: 15.07.2011, 14:33:16
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class HyperlinkDialog extends HyperlinkForm {

    private HyperlinkData data;

    public HyperlinkDialog(Frame owner) {
        super(owner);
        initialize();
    }

    public HyperlinkDialog(Dialog owner) {
        super(owner);
        initialize();
    }

    private void initialize() {
        isOk = false;
    }

    public void setData(HyperlinkData data) {
        this.data = data != null ? data : new HyperlinkData();
        jtxtName.setText(this.data.getName());
        jtxtUrl.setText(this.data.getUrl());
    }

    public HyperlinkData getData() {
        if (data == null) {
            data = new HyperlinkData();
        }
        data.setName(jtxtName.getText());
        data.setUrl(jtxtUrl.getText());
        return data;
    }

    private boolean isOk;

    @Override
    protected void doOk() {
        super.doOk();
        isOk = true;
    }

    @Override
    protected void doCancel() {
        super.doCancel();
    }

    public boolean isOk() {
        return isOk;
    }

    public static HyperlinkDialog showHyperlinkDialog(Component parent, HyperlinkData data) {

        Window window = SwingUtilities.getWindowAncestor(parent);

        HyperlinkDialog dlg;
        if (window instanceof Frame) {
            dlg = new HyperlinkDialog((Frame) window);
        } else if (window instanceof Dialog) {
            dlg = new HyperlinkDialog((Dialog) window);
        } else {
            throw new IllegalComponentStateException("Can't find proper dialog ancestor");
        }

        Rectangle r = window.getBounds();
        int x = r.x + (r.width - dlg.getSize().width)/2;
        int y = r.y + (r.height - dlg.getSize().height)/2;
        dlg.setLocation(x, y);


        dlg.setData(data);
        dlg.setModal(true);
        dlg.setVisible(true);
        return dlg;
    }

}
