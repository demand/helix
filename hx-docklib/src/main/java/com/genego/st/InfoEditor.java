package com.genego.st;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.*;
import javax.swing.text.html.HTML;
import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;

/**
 * InfoEditor
 * Created: 10.05.2011, 22:19:42
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class InfoEditor extends JTextPane {

    Style buttonStyle;
    Style normalStyle;
    Style header1Style;
    Style header2Style;

    private StylesComboListener stylesComboListener;
    private ComboBoxModel stylesComboModel;


    private ToggleActionsContainer toggleActionsContainer;

    public InfoEditor() {
        setDocument(new InfoDocument());

        EditorKit kit = getEditorKit();    // StyledEditorKit

        Document document = getDocument(); // DefaultStyledDocument

        setNavigationFilter(new InfoNavigationFilter());

        toggleActionsContainer = new ToggleActionsContainer(this);

//        createDefaultActions();

        System.out.println("k:" + kit + " document:" + document);

        StyledDocument doc = (StyledDocument) getDocument();

        Style defaultStyle = doc.getStyle(StyleContext.DEFAULT_STYLE);
        normalStyle = doc.addStyle("Normal", defaultStyle);
        header1Style = doc.addStyle("Header 1", defaultStyle);
        header2Style = doc.addStyle("Header 2", defaultStyle);

        normalStyle.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);

        header1Style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_CENTER);
        header1Style.addAttribute(StyleConstants.FontSize, 26);

        header2Style.addAttribute(StyleConstants.Alignment, StyleConstants.ALIGN_LEFT);
        header2Style.addAttribute(StyleConstants.FontSize, 20);

        stylesComboModel = new DefaultComboBoxModel(getStyles());
        stylesComboListener = new StylesComboListener();

        addCaretListener(new CaretHandler());

        try {
            doc.insertString(0, "<html><body><h1>Header</h1><a href=\"url\">link</a></body></html>", null);
        } catch (BadLocationException e) {
            ;
        }
        doc.setLogicalStyle(0, normalStyle);
        updateStylesCombo();

        setEditable(true);

        ActionMap am = getActionMap();
        am.put(DefaultEditorKit.deletePrevCharAction, new DeletePrevItemAction());
        am.put(DefaultEditorKit.deleteNextCharAction, new DeleteNextItemAction());

        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                Cursor defaultCursor = Cursor.getPredefinedCursor(
                        isEditable() ? Cursor.TEXT_CURSOR : Cursor.DEFAULT_CURSOR);
                Cursor handCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);

//                if (!isEditable()) {
                    Point pt = new Point(e.getX(), e.getY());
                    int pos = viewToModel(pt);
                    if (pos >= 0) {
                        Document doc = getDocument();
                        if (doc instanceof DefaultStyledDocument) {
                            DefaultStyledDocument hdoc = (DefaultStyledDocument) doc;
                            Element element = hdoc.getCharacterElement(pos);
                            AttributeSet a = element.getAttributes();

                            Enumeration<?> attributeNames = a.getAttributeNames();
                            while (attributeNames.hasMoreElements()) {
                                Object elm = attributeNames.nextElement();
                                System.out.println("     **     " + elm.toString() + " -> " + a.getAttribute(elm)); 
                            }


                            String href = (String) a.getAttribute(HTML.Attribute.HREF);
                            if (href != null) {
                                if (isShowToolTip()){
                                    setToolTipText(href);
                                }
                                if (isShowCursorFeedback()) {
                                    if (getCursor() != handCursor) {
                                        setCursor(handCursor);
                                    }
                                }
                            } else {
                                if (isShowToolTip()){
                                    setToolTipText(null);
                                }
                                if (isShowCursorFeedback()) {
                                    if (getCursor() != defaultCursor) {
                                        setCursor(defaultCursor);
                                    }
                                }
                            }
                        }
                    } else {
                        setToolTipText(null);
                    }
//                }
            }
        });
    }

    private boolean isShowToolTip() {
        return true;
    }

    private boolean isShowCursorFeedback() {
        return true;
    }

    public ComboBoxModel getStylesComboModel() {
        return stylesComboModel;
    }

    public ItemListener getStylesComboListener() {
        return stylesComboListener;
    }

    class StylesComboListener implements ItemListener {
        boolean listenerEnabled = true;

        public void itemStateChanged(ItemEvent e) {
            if (listenerEnabled) {
                int pos = getCaretPosition();
                String styleName = (String) stylesComboModel.getSelectedItem();
                if (styleName != null) {
                    Style style = getStyledDocument().getStyle(styleName);
                    getStyledDocument().setLogicalStyle(pos, style);
                }
            }
        }

        public boolean isListenerEnabled() {
            return listenerEnabled;
        }

        public void setListenerEnabled(boolean listenerEnabled) {
            this.listenerEnabled = listenerEnabled;
        }
    }

    int hypId = 0;
    public static final String ID_PROP = "HYPID";

    public void addHyperlink() {
        HyperlinkDialog dlg = HyperlinkDialog.showHyperlinkDialog(this, new HyperlinkData());
        if (dlg.isOk()) {
            HyperlinkData data = dlg.getData();
            String name = data.getName();
            if (name.length() > 0) {
                StyledDocument doc = (StyledDocument) getDocument();
                try {
                    SimpleAttributeSet attrs = new SimpleAttributeSet();
                    StyleConstants.setUnderline(attrs, true);
                    StyleConstants.setForeground(attrs, Color.BLUE);
                    attrs.addAttribute(ID_PROP, hypId++);
                    attrs.addAttribute(HTML.Attribute.HREF, data.getUrl());
                    doc.insertString(getCaret().getDot(), name, attrs);
                } catch (BadLocationException e) {
                    ;
                }
            }
        }
    }

    int linkCount = 0;

    private void createAttributes() {
    }

    public Action getToggleAction(Object attribKey) {
        return toggleActionsContainer.getToggleAction(attribKey);
    }

    public ButtonModel getToggleButtonModel(Object attribKey) {
        return toggleActionsContainer.getToggleButtonModel(attribKey);
    }

    public ComboBoxModel getComboModel(Object attribKey) {
        return toggleActionsContainer.getComboModel(attribKey);
    }

    public ItemListener getComboListener(Object attribKey) {
        return toggleActionsContainer.getComboListener(attribKey);
    }

    public String[] getStyles() {
        return ((InfoDocument) getDocument()).getStyles();
    }

    public String getCurrentStyle() {
        Style style = getLogicalStyle();
        return style != null ? style.getName() : null;
    }

    private class CaretHandler implements CaretListener {

        public void caretUpdate(CaretEvent e) {
            int dot = e.getDot();
            int mark = e.getMark();
            StyledDocument doc = getStyledDocument();
            Element element = doc.getCharacterElement(getCaretPosition());
            toggleActionsContainer.updateButtonModels(element.getAttributes());
            updateStylesCombo();

            System.out.println("dot:" + dot + "  mark:" + mark);
        }
    }

    private void updateStylesCombo() {
        StyledDocument doc = getStyledDocument();
        Style style = doc.getLogicalStyle(getCaretPosition());
        String styleName = style.getName();
        stylesComboListener.setListenerEnabled(false);
        stylesComboModel.setSelectedItem(styleName);
        stylesComboListener.setListenerEnabled(true);
    }

    private class InfoNavigationFilter extends NavigationFilter {

        public InfoNavigationFilter() {
            super();
        }

        public void setDot(FilterBypass fb, int dot, Position.Bias bias) {
            int step = -1;
            while (!isCaretAllowed(dot)) {
                dot += step;
            }
            fb.setDot(dot, bias);
        }

        public void moveDot(FilterBypass fb, int dot, Position.Bias bias) {
            int mark = getCaret().getMark();
            int step = mark > dot ? -1 : 1;
            while (!isCaretAllowed(dot)) {
                dot += step;
            }
            fb.moveDot(dot, bias);
        }

        public int getNextVisualPositionFrom(JTextComponent text,
                int pos, Position.Bias bias, int direction,
                Position.Bias[] biasRet) throws BadLocationException {

            int rslt = super.getNextVisualPositionFrom(text, pos, bias, direction, biasRet);
            int step = (direction == SwingConstants.WEST || direction == SwingConstants.NORTH) ? -1 : 1;

            while(!isCaretAllowed(rslt)) {
                rslt += step;
            }
            return rslt;
        }

        private boolean isCaretAllowed(int pos) {
            if (pos == 0) {
                return true;
            } else {
                StyledDocument doc = getStyledDocument();
                Integer idBefore = getHypId(doc, pos - 1);
                Integer idThis = getHypId(doc, pos);
                return pos == doc.getLength() || idBefore == null || !idBefore.equals(idThis);
            }
        }
    }

    private boolean isSpecialItem(int pos) {
        return getStyledDocument().getCharacterElement(pos).
                getAttributes().containsAttribute(
                StyleConstants.Underline, Boolean.TRUE);
    }

    private Integer getHypId(StyledDocument doc, int pos) {
        return (Integer) doc.getCharacterElement(pos).getAttributes().getAttribute(ID_PROP);
    }

    class DeleteNextItemAction extends TextAction {

        DeleteNextItemAction() {
            super(DefaultEditorKit.deleteNextCharAction);
        }

        public void actionPerformed(ActionEvent e) {
            JTextComponent target = getTextComponent(e);
            boolean beep = true;
            if ((target != null) && (target.isEditable())) {
                try {
                    Document doc = target.getDocument();
                    Caret caret = target.getCaret();
                    int dot = caret.getDot();
                    int mark = caret.getMark();
                    if (dot != mark) {
                        doc.remove(Math.min(dot, mark), Math.abs(dot - mark));
                        beep = false;
                    } else if (dot < doc.getLength()) {
                        int delChars = 1;
                        if (isSpecialItem(dot)) {
                            int i = dot;
                            while (isSpecialItem(i)) {
                                i++;
                            }
                            delChars = i - dot;
                        } else if (dot < doc.getLength() - 1) {
                            String dotChars = doc.getText(dot, 2);
                            char c0 = dotChars.charAt(0);
                            char c1 = dotChars.charAt(1);

                            if (c0 >= '\uD800' && c0 <= '\uDBFF' &&
                                c1 >= '\uDC00' && c1 <= '\uDFFF') {
                                delChars = 2;
                            }
                        }
                        doc.remove(dot, delChars);
                        beep = false;
                    }
                } catch (BadLocationException bl) {
                }
            }
            if (beep) {
		        UIManager.getLookAndFeel().provideErrorFeedback(target);
            }
        }
    }

    class DeletePrevItemAction extends TextAction {

        DeletePrevItemAction() {
            super(DefaultEditorKit.deletePrevCharAction);
        }

        public void actionPerformed(ActionEvent e) {
            JTextComponent target = getTextComponent(e);
            boolean beep = true;
            if ((target != null) && (target.isEditable())) {
                try {
                    Document doc = target.getDocument();
                    Caret caret = target.getCaret();
                    int dot = caret.getDot();
                    int mark = caret.getMark();
                    if (dot != mark) {
                        doc.remove(Math.min(dot, mark), Math.abs(dot - mark));
                        beep = false;
                    } else if (dot > 0) {
                        int delChars = 1;
                        if (isSpecialItem(dot-1)) {
                            int i = dot - 1;
                            while (isSpecialItem(i) && i > 0) {
                                i--;
                            }
                            i = isSpecialItem(i) ? i : i + 1;
                            delChars = dot - i;
                        } else if (dot > 1) {
                            String dotChars = doc.getText(dot - 2, 2);
                            char c0 = dotChars.charAt(0);
                            char c1 = dotChars.charAt(1);

                            if (c0 >= '\uD800' && c0 <= '\uDBFF' &&
                                c1 >= '\uDC00' && c1 <= '\uDFFF') {
                                delChars = 2;
                            }
                        }
                        doc.remove(dot - delChars, delChars);
                        beep = false;
                    }
                } catch (BadLocationException bl) {
                }
            }
            if (beep) {
        		UIManager.getLookAndFeel().provideErrorFeedback(target);
            }
        }
    }

}
