package com.genego.st;

/**
 * HyperlinkData
 * Created: 15.07.2011, 14:35:07
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class HyperlinkData {

    private String name;
    private String url;
    private String tooltip;

    public HyperlinkData() {
    }

    public HyperlinkData(String name, String url, String tooltip) {
        this.name = name;
        this.url = url;
        this.tooltip = tooltip;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url == null ? "" : url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTooltip() {
        return tooltip == null ? "" : tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }
}
