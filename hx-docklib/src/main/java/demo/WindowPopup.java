package demo;

import javax.swing.JWindow;
import javax.swing.JComponent;
import java.awt.*;
import java.awt.event.*;

/**
 * Created: 20.07.2006 12:30:50
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class WindowPopup extends JWindow implements WindowStateListener, WindowListener,
        WindowFocusListener, ComponentListener, HierarchyListener {
    public WindowPopup() {
        init();
    }

    public WindowPopup(Frame owner) {
        super(owner);
        init();
    }

    public WindowPopup(Window owner) {
        super(owner);
        init();
    }

    private void init() {
        setFocusable(true);
        requestFocusInWindow();
        addListeners();
    }

    private void addListeners() {
        addWindowStateListener(this);
        addWindowListener(this);
        addWindowFocusListener(this);
        addComponentListener(this);
        addHierarchyListener(this);
    }

    public void update(Graphics g) {
        super.update(g);
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Dimension sz = getSize();
        g.setColor(Color.YELLOW);
        g.fillRect(0, 0, sz.width-5, sz.height-5);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, sz.width-6, sz.height-6);
        g.setColor(new Color(100, 100, 100, 100));
        g.fillRect(sz.width-6, 5, 5, sz.height-5);
        g.fillRect(5, sz.height-6, sz.width-5, 5);
    }
    public void windowStateChanged(WindowEvent e) {
        System.out.println("StateChanged: " + e.paramString());
    }

    public void windowOpened(WindowEvent e) {
        System.out.println("Opened");
    }

    public void windowClosing(WindowEvent e) {
        System.out.println("Closing");
    }

    public void windowClosed(WindowEvent e) {
        System.out.println("Closed");
    }

    public void windowIconified(WindowEvent e) {
        System.out.println("Iconified");
    }

    public void windowDeiconified(WindowEvent e) {
        System.out.println("Deiconified");
    }

    public void windowActivated(WindowEvent e) {
        System.out.println("Activated");
    }

    public void windowDeactivated(WindowEvent e) {
        System.out.println("Deactivated");
    }

    public void windowGainedFocus(WindowEvent e) {
        System.out.println("GainedFocus");
    }

    public void windowLostFocus(WindowEvent e) {
        System.out.println("LostFocus");
    }

    public void componentResized(ComponentEvent e) {
        System.out.println("Resized");
    }

    public void componentMoved(ComponentEvent e) {
        System.out.println("Moved");
    }

    public void componentShown(ComponentEvent e) {
        System.out.println("Shown");
    }

    public void componentHidden(ComponentEvent e) {
        System.out.println("Hidden");
    }

    public void hierarchyChanged(HierarchyEvent e) {
        System.out.println("hierarchyChanged");
    }
}
