package demo;

import com.zwing.utils.ZUIManager;

import demo.laf.LookAndFeelManager;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;

/**
 * Created: 23.06.2006 17:04:52
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Main {
    public static void main(String[] args) {
//        Locale.setDefault(new Locale("ru"));
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                LookAndFeelManager.initialize();
                ZUIManager.initializeInstance(); // initialize

                Enumeration<Object> keys = UIManager.getDefaults().keys();
                ArrayList<Object> sortedKeys = new ArrayList<Object>();
                while(keys.hasMoreElements()) {
                    sortedKeys.add(keys.nextElement());
                }
                Collections.sort(sortedKeys, new Comparator<Object>() {
                    public int compare(Object o1, Object o2) {
                        return o1.toString().compareTo(o2.toString());
                    }
                });
                for (Object k : sortedKeys) {
                    Object v = UIManager.getDefaults().get(k);
                    System.out.println(k.toString() + " : " + v);
                }


                MainFrame.showFrame();
            }
        });
    }
}
