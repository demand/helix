package demo.laf;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.PlasticLookAndFeel;
import com.jgoodies.looks.plastic.theme.*;

import javax.swing.*;

import java.awt.Toolkit;

/**
 * Created: 11.07.2006 10:34:06
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class LookAndFeelManager {
    public static void initialize() {
        try {
            // synthetica.jar
            //UIManager.setLookAndFeel(new SyntheticaStandardLookAndFeel());

            // kunststoff.jar
            // UIManager.setLookAndFeel(new com.incors.plaf.kunststoff.KunststoffLookAndFeel());

            //// metouia.jar
            //UIManager.setLookAndFeel(new net.sourceforge.mlf.metouia.MetouiaLookAndFeel());

            //// napkinlaf.jar
            //UIManager.setLookAndFeel(new napkin.NapkinLookAndFeel());

            // jgoodies
//            PlasticLookAndFeel.setCurrentTheme(new ExperienceBlue());
            UIManager.put("jgoodies.popupDropShadowEnabled", Boolean.FALSE);
            UIManager.setLookAndFeel(new com.jgoodies.looks.plastic.Plastic3DLookAndFeel());
            Plastic3DLookAndFeel.setCurrentTheme(new DesertBluer());

            // default
//             UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            // pgs
//            UIManager.setLookAndFeel("com.pagosoft.plaf.PgsLookAndFeel");


            // quaqua
//            UIManager.setLookAndFeel("ch.randelshofer.quaqua.QuaquaLookAndFeel");

            // alloy
//          installAlloy();

            Toolkit.getDefaultToolkit().setDynamicLayout(false);
            JPopupMenu.setDefaultLightWeightPopupEnabled(true);
            ToolTipManager.sharedInstance().setLightWeightPopupEnabled(true);

            JDialog.setDefaultLookAndFeelDecorated(true);
            JFrame.setDefaultLookAndFeelDecorated(true);

//            System.setProperty("sun.awt.noerasebackground", "true");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static final String LICENSE_CODE_PROPERTY = "alloy.licenseCode";
//    public static final String LAF_RESOURCE = "swing/plaf/laf.properties";
//
//    public static void installAlloy() {
//        try {
//            AlloyLookAndFeel.setProperty(LICENSE_CODE_PROPERTY, "6#Luxoft_Ltd#1rskky0#5n87c0");
//            Lm.verifyLicense("Luxoft", "VSP", "2oA6OcY0se:NQ8AiYTQo0wfSF0:kIeO1");
//
//            AlloyTheme theme = new GlassTheme();
//            javax.swing.UIManager.setLookAndFeel(new VSPLookAndFeel(theme));
//
//            Toolkit.getDefaultToolkit().setDynamicLayout(true);
//            JPopupMenu.setDefaultLightWeightPopupEnabled(false);
//            ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);
//
//        } catch (UnsupportedLookAndFeelException e) {
//        }
//    }

}
