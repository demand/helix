package demo;

import javax.swing.*;

import demo.tabs.*;

/**
 * Created: 19.07.2006 12:26:07
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class MainTabs extends JTabbedPane {
    public MainTabs() {
        localInit();
    }

    private void localInit() {
//        addTab("Tab 1", new ButtonsTab());
        addTab("Blocks", new SchemeTab());
        addTab("Docking", new DockTab());
        addTab("Toolbars", new ToolBarTab());
        addTab("Html Editor", new HtmlEditorTab());
        addTab("Actions", new ActionsTab());

        setSelectedIndex(getTabCount() - 1);
    }

}
