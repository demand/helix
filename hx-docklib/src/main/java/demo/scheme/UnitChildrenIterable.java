package demo.scheme;

import java.util.Iterator;

/**
 * Created: 27.06.2007 18:57:12
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class UnitChildrenIterable implements Iterable {
    private Unit unit;

    public UnitChildrenIterable(Unit unit) {
        this.unit = unit;
    }

    public Iterator<Unit> iterator() {
        return unit.getChildrenIterator();
    }
}
