package demo.scheme;

/**
 * Created: 15.05.2007 11:15:01
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface SchemeViewContext {
    public int size();
    public boolean isEmpty();
    public boolean containsKey(Object key);
    public Object get(Object key);
}
