package demo.scheme;

/**
 * Created: 25.07.2007 18:49:17
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ActiveModelEvent {

    /**
     * Модель, содержащая действия пользователя
     */
    private ActiveModel model;

    /**
     * Название изменения
     */
    private String actionName;

    /**
     * Старое значение изменившегося элемента
     */
    private Object oldValue;

    /**
     * Текущее значение изменившегося элемента
     */
    private Object newValue;

    /**
     * Объект, инициирующий изменение дерева
     */
    private Object source;

    public ActiveModelEvent(ActiveModel model, String actionName,
            Object oldValue, Object newValue, Object source) {
        this.model = model;
        this.actionName = actionName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.source = source;
    }

    public ActiveModel getModel() {
        return model;
    }

    public Object getSource() {
        return source;
    }

    public String getActionName() {
        return actionName;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }
}
