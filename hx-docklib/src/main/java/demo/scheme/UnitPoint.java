package demo.scheme;

import java.awt.*;

/**
 * Created: 23.08.2007 15:08:23
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class UnitPoint {
    private Unit unit;
    private Point point;

    public UnitPoint(Unit unit, Point point) {
        this.unit = unit;
        this.point = point;
    }

    public Unit getUnit() {
        return unit;
    }

    public Point getPoint() {
        return point;
    }
}
