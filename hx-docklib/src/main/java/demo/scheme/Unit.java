package demo.scheme;

import com.zwing.resizer.ResizeObject;
import com.zwing.resizer.Resizer;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created: 13.04.2007 11:11:47
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Unit implements TreeChangeListener, PropertyChangeListener, ResizeObject {

    public static final String PROP_NAME   = "NAME";
    public static final String PROP_ANCHOR = "ANCHOR";
    public static final String PROP_BOUNDS = "BOUNDS";
    public static final String PROP_STATE  = "STATE";
    public static final String PROP_LAYER  = "LAYER";
    public static final String PROP_VALUE  = "VALUE";

    private List<Unit> children;
    private List<ConnectorSpot> spots;
    private Unit parent;
    private TreeChangeListenersList treeChangeListeners;
    private PropertyChangeListenersList propertyChangeListeners;

    private Insets insetsExpanded  = new Insets(6, 6, 6, 6);
    private Insets insetsCollapsed = new Insets(16, 6, 6, 6);

    private static final int CORNER_SNAP_SIZE = 6;
    private static final int BORDER_SIZE = 3;
    private static Insets INSETS = new Insets(BORDER_SIZE, BORDER_SIZE, BORDER_SIZE, BORDER_SIZE);

    /**
     * Название
     */
    private String name;

    /**
     * Внешние границы компонента в координатах родительского компонента
     */
    private Rectangle bounds;

    /**
     * Активная координата - это смещение левого верхнего
     * угла при создании компонента.
     */
    private Point anchor;

    /**
     * Состояние компонента
     */
    private State state;

    /**
     * Слой отображения. Чем больше номер слоя, тем позже
     * рисуются элементы, которые в нём лежат.
     */
    private int layer;

    /**
     * Пользовательский объект.
     */
    private Object value;

    private Dimension minSize;
    private Dimension maxSize;

    public static final int MAX_WIDTH  = 10000;
    public static final int MAX_HEIGHT = 10000;

    public Unit() {
        children = new ArrayList<Unit>(0);
        spots = new ArrayList<ConnectorSpot>(0);
        createTreeChangeListeners();
        createPropertyChangeListeners();
        bounds = new Rectangle();
        anchor = new Point();
        state = State.EXPANDED;
        layer = 0;

        maxSize = new Dimension(MAX_WIDTH, MAX_HEIGHT);
    }

    public Rectangle getBounds() {
        return new Rectangle(bounds);
    }

    public boolean setBounds(Rectangle bounds) {
        boolean rslt = false;
        Rectangle oldBounds = this.bounds;
        if (!bounds.equals(oldBounds)) {
            this.bounds = bounds;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_BOUNDS, oldBounds, bounds));
            rslt = true;
        }
        return rslt;
    }

    public Point getAnchor() {
        return anchor;
    }

    public boolean setAnchor(Point anchor) {
        boolean rslt = false;
        Point oldAnchor = this.anchor;
        if (!anchor.equals(oldAnchor)) {
            this.anchor = anchor;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_ANCHOR, oldAnchor, anchor));
            rslt = true;
        }
        return rslt;
    }

    public State getState() {
        return state;
    }

    public boolean setState(State state) {
        boolean rslt = false;
        State oldState = this.state;
        if (state != oldState) {
            this.state = state;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_STATE, oldState, state));
            rslt = true;
        }
        return rslt;
    }

    public int getLayer() {
        return layer;
    }

    public boolean setLayer(int layer) {
        boolean rslt = false;
        int oldLayer = this.layer;
        if (layer != oldLayer) {
            this.layer = layer;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_LAYER, oldLayer, layer));
            rslt = true;
        }
        return rslt;
    }

    public String getName() {
        return name;
    }

    public boolean setName(String name) {
        boolean rslt = false;
        String oldName = this.name;
        if (!(name != null && name.equals(oldName) || name == oldName)) {
            this.name = name;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_NAME, oldName, name));
            rslt = true;
        }
        return rslt;
    }

    public Object getValue() {
        return value;
    }

    public boolean setValue(Object value) {
        boolean rslt = false;
        Object oldValue = this.value;
        if (!(value != null && value.equals(oldValue) || value == oldValue)) {
            this.value = value;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_VALUE, oldValue, value));
            rslt = true;
        }
        return rslt;
    }

    public Unit getChildAt(int index) {
        return children.get(index);
    }

    public int getChildrenCount() {
        return children.size();
    }

    /**
     * @return общее количество потомков.
     */
    public int getDescendantCount() {
        int rslt = 0;
        for (Unit c : children) {
            rslt += c.getDescendantCount();
        }
        return rslt + getChildrenCount();
    }

    /**
     * @return Минимальный прямоугольник, включающий габаритные
     * прямоугольники дочерних компонентов.
     */
    public Rectangle getChildrenBounds() {
        int rx0 = 0;
        int ry0 = 0;
        int rx1 = 0;
        int ry1 = 0;
        if (getChildrenCount() > 0) {
            Unit c = getChildAt(0);
            Rectangle r = c.getBounds();
            rx0 = r.x;
            ry0 = r.y;
            rx1 = r.x + r.width;
            ry1 = r.y + r.height;
            int n = getChildrenCount();
            for (int i = 1; i < n; i++) {
                r = getChildAt(i).getBounds();
                rx0 = Math.min(rx0, r.x);
                ry0 = Math.min(ry0, r.y);
                rx1 = Math.max(rx1, r.x + r.width);
                ry1 = Math.max(ry1, r.y + r.height);
            }
        }
        return new Rectangle(rx0, ry0, rx1 - rx0, ry1 - ry0);
    }

    public Unit[] getChildren() {
        return children.toArray(new Unit[children.size()]);
    }

    public Unit getParent() {
        return parent;
    }

    public Unit setParent(Unit parent) {
        final Unit oldParent = this.parent;
        if (oldParent != parent) {
            if (oldParent != null) {
                oldParent.removeChild(this);
            }
            if (parent != null) {
                parent.addChild(this);
            }
        }
        return oldParent;
    }

    public boolean addChild(Unit child) {
        boolean rslt = false;
        if (child.getParent() != this) {
            if (child.getParent() != null) {
                child.getParent().removeChild(child);
            }
            children.add(child);
            child.parent = this;
            child.addTreeChangeListener(this);
            child.addPropertyChangeListener(this);
            child.treeChangeListeners.notifyListeners(new TreeChangeEvent(
                    null, this, child, TreeChangeEvent.Type.CHILD_ADDED));
            rslt = true;
        }
        return rslt;
    }

    public boolean removeChild(Unit child) {
        return removeChild(children.indexOf(child));
    }

    public boolean removeChild(int index) {
        boolean rslt = false;
        if (index >= 0) {
            Unit child = children.remove(index);
            child.parent = null;
            child.removeTreeChangeListener(this);
            child.removePropertyChangeListener(this);
            treeChangeListeners.notifyListeners(new TreeChangeEvent(
                    null, this, child, TreeChangeEvent.Type.CHILD_REMOVED));
            child.treeChangeListeners.notifyListeners(new TreeChangeEvent(
                    null, this, child, TreeChangeEvent.Type.CHILD_REMOVED));
            rslt = true;
        }
        return rslt;
    }

    public void adjustBoundsByChildren() {
        Rectangle b = getBounds();
        Rectangle cb = getChildrenBounds();

        int cx0 = cb.x;
        int cy0 = cb.y;
        int cx1 = cb.x + cb.width;
        int cy1 = cb.y + cb.height;

        int newWidth = Math.max(Math.max(b.width, cx1), cx1-cx0);
        int newHeight = Math.max(Math.max(b.height, cy1), cy1-cy0);

        if (newWidth != b.width || newHeight != b.height) {
            setBounds(new Rectangle(b.x, b.y,  newWidth, newHeight));
        }
        int dx = 0;
        int dy = 0;
        if (cx0 < 0) {
            dx = -cx0;
        }
        if (cy0 < 0) {
            dy = -cy0;
        }
        if (dx != 0 || dy != 0) {
            shiftChildrenBy(dx, dy);
        }
    }

    private void shiftChildrenBy(int deltaX, int deltaY) {
        if (deltaX != 0 || deltaY != 0) {
            for(Unit child : getChildrenIterable()) {
                Rectangle r = child.getBounds();
                r.setLocation(r.x + deltaX, r.y + deltaY);
                child.setBounds(r);
            }
        }
    }

    public Dimension getMinSize() {
        if (minSize == null) {
            Rectangle r = getChildrenBounds();
            minSize = r.getSize();
        }
        return minSize;
    }

    public Dimension getMaxSize() {
        return maxSize;
    }

    public Iterator<Unit> getChildrenIterator() {
        return new UnitChildrenIterator(this);
    }

    public Iterable<Unit> getChildrenIterable() {
        return new UnitChildrenIterable(this);
    }

    public void addTreeChangeListener(TreeChangeListener listener) {
        treeChangeListeners.addListener(listener);
    }

    public void removeTreeChangeListener(TreeChangeListener listener) {
        treeChangeListeners.removeListener(listener);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeListeners.addListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeListeners.removeListener(listener);
    }

    private void createTreeChangeListeners() {
        treeChangeListeners = new TreeChangeListenersList();
    }

    public void setTreeChangeListeners(TreeChangeListenersList treeChangeListeners) {
        if (treeChangeListeners == null) {
            createTreeChangeListeners();
        } else {
            this.treeChangeListeners = treeChangeListeners;
        }
    }

    public TreeChangeListenersList getTreeChangeListeners() {
        return treeChangeListeners;
    }

    private void createPropertyChangeListeners() {
        propertyChangeListeners = new PropertyChangeListenersList();
    }

    public void setPropertyChangeListeners(PropertyChangeListenersList propertyChangeListeners) {
        if (propertyChangeListeners == null) {
            createPropertyChangeListeners();
        } else {
            this.propertyChangeListeners = propertyChangeListeners;
        }
    }

    public PropertyChangeListenersList getPropertyChangeListeners() {
        return propertyChangeListeners;
    }

    public void treeChanged(TreeChangeEvent evt) {
        if (evt.getType() == TreeChangeEvent.Type.CHILD_ADDED &&
                    evt.getParent() == this) {
                adjustBoundsByChildren();
        }
        treeChangeListeners.notifyListeners(evt);
    }

    public void propertyChanged(PropertyChangeEvent evt) {
        String pName = evt.getPropertyName();
        if (PROP_BOUNDS.equals(pName)) {
            Unit unit = (Unit) evt.getPropertyHolder();
            if (unit.getParent() == this) {
                adjustBoundsByChildren();
            }
        }
        propertyChangeListeners.notifyListeners(evt);
    }

    public int getCornerSnapSize() {
        return CORNER_SNAP_SIZE;
    }

    public Insets getBorderSize() {
        return INSETS;
    }

    public int getBehaviour() {
        return state == State.EXPANDED ?
                Resizer.RESIZABLE_ALL :
                Resizer.RESIZABLE_NONE;
    }

    public Dimension getMinimumSize() {
        return null;
    }

    public Dimension getMaximumSize() {
        return null;
    }

    public Dimension getPrefferedSize() {
        return null;
    }

    public enum State {
        EXPANDED,
        COLLAPSED
    }

    public class ChildrenIterable implements Iterable {
        public Iterator<Unit> iterator() {
            return Unit.this.getChildrenIterator();
        }
    }

    private class UnitChildrenIterator implements Iterator<Unit> {
        private int currentIndex;

        public UnitChildrenIterator(Unit parent) {
            if (parent == null) {
                throw new NullPointerException();
            }
            currentIndex = 0;
        }

        public boolean hasNext() {
            return currentIndex < parent.getChildrenCount();
        }

        public Unit next() {
            return getChildAt(currentIndex++);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
