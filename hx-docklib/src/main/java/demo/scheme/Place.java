package demo.scheme;

import java.awt.*;

/**
 * Created: 28.03.2007 17:43:06
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Place {
    Point getLocation();
    int getLayer();
    boolean isVisible();
}
