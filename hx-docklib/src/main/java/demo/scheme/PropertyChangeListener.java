package demo.scheme;

/**
 * Created: 27.04.2007 18:34:39
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface PropertyChangeListener {
    void propertyChanged(PropertyChangeEvent evt);
}
