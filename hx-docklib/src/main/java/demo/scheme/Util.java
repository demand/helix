package demo.scheme;

/**
 * Created: 17.05.2007 17:52:29
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Util {

    public static boolean safeEquals(Object one, Object two) {
        return one != null && one.equals(two) || one == two;
    }
}
