package demo.scheme;

import java.awt.*;

/**
 * Created: 18.05.2007 13:05:44
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class PointImmutable {
    private int x;
    private int y;

    public PointImmutable() {
        this(0, 0);
    }

    public PointImmutable(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public PointImmutable(Point p) {
        this(p.x,  p.y);
    }

    public PointImmutable(PointImmutable p) {
        this(p.x,  p.y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


}
