package demo.scheme;

import java.util.List;
import java.util.ArrayList;

/**
 * Created: 27.04.2007 18:36:21
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class PropertyChangeListenersList {
    private List<PropertyChangeListener> listeners;

    public PropertyChangeListenersList() {
        listeners = new ArrayList<PropertyChangeListener>(0);
    }

    public void addListener(PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        if (!listeners.contains(listener)) {
            System.out.println("listener added: " + this.hashCode());
            listeners.add(listener);
        }
    }

    public void removeListener(PropertyChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        System.out.println("listener removed: " + this.hashCode());
        listeners.remove(listener);
    }

    public void clear() {
        System.out.println("listeners cleared: " + this.hashCode());
        listeners.clear();
    }

    public void notifyListeners(PropertyChangeEvent event) {
        for(PropertyChangeListener listener : listeners) {
            listener.propertyChanged(event);
        }
    }
}
