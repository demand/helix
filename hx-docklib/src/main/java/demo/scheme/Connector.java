package demo.scheme;

/**
 * Created: 21.03.2007 12:12:06
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class Connector {

    public static final String PROP_FROM   = "FROM";
    public static final String PROP_TO     = "TO";
    public static final String PROP_NAME   = "NAME";
    public static final String PROP_LAYER  = "LAYER";
    public static final String PROP_POINTS = "POINTS";

    private PropertyChangeListenersList propertyChangeListeners;

    private ConnectorSpot from;
    private ConnectorSpot to;
    private String name;
    private PointImmutable[] points;

    private int layer;

    /**
     * Пользовательский объект.
     */
    private Object value;

    public Connector() {
        propertyChangeListeners = new PropertyChangeListenersList();
    }

    public ConnectorSpot getFrom() {
        return from;
    }

    public void setFrom(ConnectorSpot from) {
        ConnectorSpot oldFrom = this.from;
        propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                null, this, PROP_FROM, oldFrom, from));
        this.from = from;
    }

    public ConnectorSpot getTo() {
        return to;
    }

    public boolean setTo(ConnectorSpot to) {
        boolean rslt = false;
        if (to != this.to) {
            ConnectorSpot oldTo = this.to;
            this.to = to;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_TO, oldTo, to));
        }
        return rslt;
    }

    public String getName() {
        return name;
    }

    public boolean setName(String name) {
        boolean rslt = false;
        String oldName = this.name;
        if (!(name != null && name.equals(oldName) || name == oldName)) {
            this.name = name;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_NAME, oldName, name));
            rslt = true;
        }
        return rslt;
    }

    public int getLayer() {
        return layer;
    }

    public boolean setLayer(int layer) {
        boolean rslt = false;
        int oldLayer = this.layer;
        if (oldLayer != layer)
        {
            this.layer = layer;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_LAYER, oldLayer, layer));
            rslt = true;
        }
        return rslt;
    }

    public PointImmutable[] getPoints() {
        PointImmutable[] rslt = null;
        if (points != null) {
            rslt = new PointImmutable[points.length];
            if (points.length > 0) {
                System.arraycopy(points, 0, rslt, 0, points.length);
            }
        } else {
            rslt = new PointImmutable[0];
        }
        return rslt;
    }

    public boolean setPoints(PointImmutable[] points) {
        boolean rslt = false;
        PointImmutable[] oldPoints = this.points;
        if (points != this.points) {
            this.points = points;
            propertyChangeListeners.notifyListeners(new PropertyChangeEvent(
                    null, this, PROP_POINTS, oldPoints, layer));
            rslt = true;
        }
        return rslt;
    }

    public int getPointsCount() {
        return points != null ? points.length : 0;
    }

    public PointImmutable getPointAt(int index) {
        return points[index];
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    private boolean compareArrays(Object[] one, Object[] two) {
        return false;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeListeners.addListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeListeners.removeListener(listener);
    }

    private void createPropertyChangeListeners() {
        propertyChangeListeners = new PropertyChangeListenersList();
    }

    public void setPropertyChangeListeners(PropertyChangeListenersList propertyChangeListeners) {
        if (propertyChangeListeners == null) {
            createPropertyChangeListeners();
        } else {
            this.propertyChangeListeners = propertyChangeListeners;
        }
    }

    public PropertyChangeListenersList getPropertyChangeListeners() {
        return propertyChangeListeners;
    }
}
