package demo.scheme;

import java.util.HashMap;

/**
 * Created: 15.05.2007 18:18:31
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class SchemeViewContextBase implements SchemeViewContext {

    private HashMap<Object, Object> properties;

    SchemeViewContextBase() {
        properties = new HashMap<Object, Object>(0);
    }

    public int size() {
        return properties.size();
    }

    public boolean isEmpty() {
        return properties.isEmpty();
    }

    public boolean containsKey(Object key) {
        return properties.containsKey(key);
    }

    public Object get(Object key) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void put(Object key, Object value) {
        properties.put(key, value);
    }

    public Object remove(Object key) {
        return properties.remove(key);
    }

    public void clear() {
        properties.clear();
    }
}
