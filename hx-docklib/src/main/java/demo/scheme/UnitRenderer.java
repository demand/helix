package demo.scheme;

import java.awt.*;

/**
 * Created: 25.04.2007 17:42:55
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface UnitRenderer {
    void renderComponent(Unit c, Graphics g, Point p,
            DrawMode dmode, SchemeViewContext context);

    Dimension getCollapsedSize();
}
