package demo.scheme;

/**
 * Created: 24.04.2007 11:22:01
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class TreeChangeEvent {
    /**
     * Объект, инициирующий изменение дерева
     */
    private Object source;

    /**
     * Компонент, у которого изменился состав дочерних компонентов.
     */
    private Unit parent;

    /**
     * Дочерний компонент, который был удалён или добавлен к parent.
     */
    private Unit child;

    /**
     * Тип действия.
     */
    private Type type;

    public TreeChangeEvent(Object source, Unit parent, Unit child, Type type) {
        this.source = source;
        this.parent = parent;
        this.child = child;
        this.type = type;
    }

    public Object getSource() {
        return source;
    }

    public Unit getParent() {
        return parent;
    }

    public Unit getChild() {
        return child;
    }

    public Type getType() {
        return type;
    }

    static enum Type {
        /**
         * К текущему (parent) компоненту добавлен дочерний компонент (child).
         */
        CHILD_ADDED,
        /**
         * У текущего (parent) компонента удален дочерний компонент (child).
         */
        CHILD_REMOVED
    }
}
