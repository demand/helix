package demo.scheme;

import java.util.*;
import java.util.List;

/**
 * Created: 21.03.2007 12:36:19
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class SchemeModel implements TreeChangeListener, PropertyChangeListener {

    private Unit root;
    private SortedMap<Integer, List<Unit>> layer2Components;
    private int componentsCount;

    private TreeChangeListenersList treeListeners;
    private PropertyChangeListenersList componentPropertyListeners;
    private PropertyChangeListenersList connectorPropertyListeners;

    public SchemeModel(Unit component) {
        layer2Components = new TreeMap<Integer, List<Unit>>();
        componentsCount = component.getDescendantCount() + 1;
        initializeListeners();
        setRoot(component);
    }

    public SchemeModel() {
        layer2Components = new TreeMap<Integer, List<Unit>>();
        componentsCount = 0;
        initializeListeners();
        setRoot(createRoot());
    }

    private Unit createRoot() {
        return new Unit();
    }

    public Unit getRoot() {
        return root;
    }

    private void setRoot(Unit root) {
        this.root = root;
        root.addTreeChangeListener(this);
        root.addPropertyChangeListener(this);
    }

    private void initializeListeners() {
        treeListeners = new TreeChangeListenersList();
        componentPropertyListeners = new PropertyChangeListenersList();
        connectorPropertyListeners = new PropertyChangeListenersList();
    }

    public int getComponentsCount() {
        return componentsCount;
    }

    public void addComponent(Unit child, Unit parent) {
        if (parent.addChild(child)) {
            parent.adjustBoundsByChildren();
            componentsCount += (child.getDescendantCount() + 1);
            addComponentToLayer(child);
        }
    }

    public void removeComponent(Unit component) {
        if (component.getParent().removeChild(component)) {
            componentsCount -= (component.getDescendantCount() + 1);
            removeComponentFromLayer(component);
        }
    }

    private void addComponentToLayer(Unit c) {
        List<Unit> cmps = layer2Components.get(c.getLayer());
        if (cmps == null) {
            cmps = new ArrayList<Unit>(1);
        }
        cmps.add(c);
    }

    private void removeComponentFromLayer(Unit c) {

//        int layer = c.getLayer();
//        List<Unit> cmps = layer2Components.get(layer);
//        cmps.remove(c);
//        if (cmps.size() == 0) {
//            layer2Components.remove(layer);
//        }
    }

    public SchemeModel createBranchModel(Unit node) {
        return new SchemeModel(node);
    }

    public void addTreeChangedListener(TreeChangeListener listener) {
        treeListeners.addListener(listener);
    }

    public void removeTreeChangedListener(TreeChangeListener listener) {
        treeListeners.removeListener(listener);
    }

    public void addComponentPropertyChangeListener(PropertyChangeListener listener) {
        componentPropertyListeners.addListener(listener);
    }

    public void removeComponentPropertyChangeListener(PropertyChangeListener listener) {
        componentPropertyListeners.removeListener(listener);
    }

    public void addConnectorPropertyChangeListener(PropertyChangeListener listener) {
        connectorPropertyListeners.addListener(listener);
    }

    public void removeConnectorPropertyChangeListener(PropertyChangeListener listener) {
        connectorPropertyListeners.removeListener(listener);
    }

    public void treeChanged(TreeChangeEvent evt) {
        treeListeners.notifyListeners(evt);
    }

    public void propertyChanged(PropertyChangeEvent evt) {
        Object propertyHolder = evt.getPropertyHolder();
        if (propertyHolder instanceof Unit) {
            ;
        }
    }
}
