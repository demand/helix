package demo.scheme;

/**
 * Created: 26.04.2007 17:48:13
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ConnectorChangeListener {
    void connectorChanged();
}
