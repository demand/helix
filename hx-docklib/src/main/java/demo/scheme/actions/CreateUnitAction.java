package demo.scheme.actions;

import demo.scheme.SchemeCanvas;
import demo.scheme.Unit;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * Created: 31.05.2007 14:03:25
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class CreateUnitAction extends AbstractAction {

    private SchemeCanvas canvas;
    private Unit unit;
    private Point point;

    public CreateUnitAction(SchemeCanvas canvas) {
        super("Create Unit");
        this.canvas = canvas;
    }

    public void actionPerformed(ActionEvent e) {
        canvas.doCreateComponentAt(unit, point);
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
