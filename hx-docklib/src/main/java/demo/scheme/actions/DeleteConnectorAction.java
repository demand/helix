package demo.scheme.actions;

import demo.scheme.SchemeCanvas;
import demo.scheme.Connector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created: 12.10.2007 20:59:58
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DeleteConnectorAction extends AbstractAction {

    private SchemeCanvas canvas;
    private Connector connector;
    private Point point;

    public DeleteConnectorAction(SchemeCanvas canvas) {
        super("Delete Connector");
        this.canvas = canvas;
    }

    public void actionPerformed(ActionEvent e) {
        canvas.doDeleteConnector(connector, point);
    }

    public void setConnector(Connector connector) {
        this.connector = connector;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
