package demo.scheme.actions;

import demo.scheme.SchemeCanvas;
import demo.scheme.Unit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created: 12.10.2007 20:53:41
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class CreateConnectorAction extends AbstractAction {
    private SchemeCanvas canvas;
    private Unit unit;
    private Point point;

    public CreateConnectorAction(SchemeCanvas canvas) {
        super("Connect");
        this.canvas = canvas;
    }

    public void actionPerformed(ActionEvent e) {
        canvas.doBeginConnectMode(unit, point);
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
