package demo.scheme.actions;

import demo.scheme.SchemeCanvas;
import demo.scheme.Unit;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created: 06.06.2007 16:16:18
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class PropertiesComponentAction extends AbstractAction {

    private SchemeCanvas canvas;
    private Unit component;

    public PropertiesComponentAction(SchemeCanvas canvas) {
        super("Properties...");
        this.canvas = canvas;
    }

    public void actionPerformed(ActionEvent e) {
        
    }

    public void setComponent(Unit component) {
        this.component = component;
    }
}
