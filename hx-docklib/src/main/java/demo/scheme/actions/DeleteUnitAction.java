package demo.scheme.actions;

import demo.scheme.SchemeCanvas;
import demo.scheme.Unit;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created: 04.06.2007 18:23:31
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DeleteUnitAction extends AbstractAction {

    private SchemeCanvas canvas;
    private Unit unit;

    public DeleteUnitAction(SchemeCanvas canvas) {
        super("Delete Unit");
        this.canvas = canvas;
    }

    public void actionPerformed(ActionEvent e) {
        canvas.doDeleteUnit(unit);
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
