package demo.scheme;

import java.awt.*;

/**
 * Created: 20.03.2007 17:59:20
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Block {
    Rectangle getBounds();
    int getLayer();
}
