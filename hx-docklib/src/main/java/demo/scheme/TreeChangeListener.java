package demo.scheme;

/**
 * Created: 24.04.2007 11:21:33
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface TreeChangeListener {
    void treeChanged(TreeChangeEvent evt);
}
