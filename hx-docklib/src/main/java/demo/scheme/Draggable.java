package demo.scheme;

import java.awt.*;

/**
 * Created: 20.03.2007 18:02:11
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Draggable {
    public void startDrag();
    public Point attemptDrag(Point original);
    public void finishDrag(Point original);

    
}
