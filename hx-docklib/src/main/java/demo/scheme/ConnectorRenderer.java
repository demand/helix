package demo.scheme;

import java.awt.*;

/**
 * Created: 28.04.2007 11:29:24
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ConnectorRenderer {
    void renderConnector(Connector c, Graphics g, Point p, DrawMode dmode, SchemeViewContext context);
}
