package demo.scheme;

/**
 * Created: 21.03.2007 12:11:03
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Node {
    Node getChildAt(int index);
    int getChildrenCount();
    Node[] getChildren();
    Node getParent();


    Node setParent(Node parent);
    void addChild(Node child);
    void removeChild(Node child);




    Node removeChild(int index);

    void addTreeChangedListener(TreeChangeListener listener);
    void removeTreeChangedListener(TreeChangeListener listener);
}
