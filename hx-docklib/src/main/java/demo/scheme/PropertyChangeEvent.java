package demo.scheme;

/**
 * Created: 27.04.2007 18:08:42
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class PropertyChangeEvent {
    private Object initiator;
    private Object propertyHolder;
    private String propertyName;
    private Object oldValue;
    private Object newValue;

    public PropertyChangeEvent(Object initiator, Object propertyHolder,
            String propertyName, Object oldValue, Object newValue) {
        this.initiator = initiator;
        this.propertyHolder = propertyHolder;
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public Object getInitiator() {
        return initiator;
    }

    public Object getPropertyHolder() {
        return propertyHolder;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }
}
