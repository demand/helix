package demo.scheme;

/**
 * Created: 27.04.2007 13:54:28
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class UnitChangedEvent {
    private Object source;
    private String propertyName;
    private Object oldValue;
    private Object newValue;

    public UnitChangedEvent(Object source, String propertyName,
            Object oldValue, Object newValue) {
        this.source = source;
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public Object getSource() {
        return source;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }
}
