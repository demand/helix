package demo.scheme;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 27.03.2007 19:02:46
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class SchemeCanvas extends JComponent implements Scrollable,
        TreeChangeListener, PropertyChangeListener, ActiveModelListener {

    private SchemeModel model;
    private ActiveModel activeModel;
    private UnitRenderer unitRenderer;
    private ActiveRenderer activeRenderer;
    private ConnectorRenderer connectorRenderer;

    public static final int INITIAL_WIDTH = 800;
    public static final int INITIAL_HEIGHT = 700;

    private SchemeCanvasMouseListener mouseListener;

    public SchemeCanvas() {
        SchemeModel m = new SchemeModel();
        activeModel = new ActiveModel();
        m.getRoot().setBounds(new Rectangle(
                0, 0, INITIAL_WIDTH, INITIAL_HEIGHT));
        setModel(m);
        unitRenderer = new UnitRendererImpl();
        activeRenderer = new ActiveRenderer();
        connectorRenderer = new ConnectorRendererImpl();

        mouseListener = new SchemeCanvasMouseListener(this);
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
        activeModel.addActiveModelListener(this);
    }

    public void setModel(final SchemeModel model) {
        this.model = model;
        model.addTreeChangedListener(this);
        repaint();
    }

    public SchemeModel getModel() {
        return model;
    }

    public void paint(Graphics g) {

        // позже надо обязательно использовать клиппинг
        // и рисовать только изменившуюся часть редактора
        // Rectangle rect = g.getClipBounds();
        Dimension sz = getSize();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, sz.width, sz.height);

//        g.setColor(Color.GRAY);
//        g.drawLine(0, 0, sz.width, sz.height);
//        g.drawLine(sz.width, 0, 0, sz.height);

        Point local = new Point(0, 0);
        int n = model.getRoot().getChildrenCount();
        for (int i = 0; i < n; i++) {
            Unit child = model.getRoot().getChildAt(i);
            Point pc = new Point(
                    child.getBounds().x,
                    child.getBounds().y);
            paintUnit(child, g, pc);
        }

        paintActive(g);
    }

    private void paintActive(Graphics g) {

        Rectangle r = activeModel.getActiveRectangle();
        if (r != null) {
            activeRenderer.drawActiveRectangle(g, r);
        }

//        Unit activeUnit = activeModel.getActiveUnit();
//        if (activeUnit != null) {
//            Point where = getAbsoluteUnitPosition(activeUnit);
//            where.x += activeModel.getDragShift().x;
//            where.y += activeModel.getDragShift().y;
//            paintUnit(activeModel.getActiveUnit(), g, where);
//        }
    }

    public Point getAbsolutePoint(Unit parent, Point point) {
        Point rslt = new Point(point);
        Unit current = parent;
        while (current != model.getRoot() && current != null) {
            Rectangle bounds = current.getBounds();
            rslt.x += bounds.x;
            rslt.y += bounds.y;
            current = current.getParent();
        }
        return rslt;
    }

    private Point getAbsoluteUnitPosition(Unit unit) {
        return getAbsolutePoint(unit.getParent(), unit.getBounds().getLocation());
    }

    private void paintUnit(Unit unit, Graphics g, Point p) {
        Point local = new Point(
                p.x + unit.getBounds().x,
                p.y + unit.getBounds().y);
        if (unit == activeModel.getActiveUnit()) {
            // ;

        }

        DrawMode dm =
                unit == activeModel.getSelectedUnit() ? DrawMode.SELECTED :
                unit == activeModel.getHoverUnit() ? DrawMode.HOVER :
                        DrawMode.NORMAL;

        unitRenderer.renderComponent(unit, g, p, dm, null);
        if (unit.getState() == Unit.State.EXPANDED) {
            int n = unit.getChildrenCount();
            for (int i = 0; i < n; i++) {
                Unit child = unit.getChildAt(i);
                Point pc = new Point(
                        p.x + child.getBounds().x,
                        p.y + child.getBounds().y);
                paintUnit(child, g, pc);
            }
        }
    }

    public Dimension getPreferredSize() {
        Dimension sz = model.getRoot().getBounds().getSize();
        return sz;
    }

    public Dimension getPreferredScrollableViewportSize() {
        return new Dimension(200, 200);
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 10;
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 10;
    }

    public boolean getScrollableTracksViewportWidth() {
        return false;
    }

    public boolean getScrollableTracksViewportHeight() {
        return false;
    }

    public void propertyChanged(PropertyChangeEvent evt) {
        Object propertyHolder = evt.getPropertyHolder();
        if (propertyHolder instanceof Unit) {
            Unit unit = (Unit) propertyHolder;
            if (Unit.PROP_BOUNDS.equals(evt.getPropertyName())) {

            }
        } else if (propertyHolder instanceof Connector) {
            ;
        } else if (propertyHolder instanceof ConnectorSpot) {
            ;
        }
    }

    public Unit whichUnitAt(Point p, Unit current) {
        Unit rslt = null;
        UnitPoint up = whichUnitPointAt(p, current);
        if (up != null) {
            rslt = up.getUnit();
        }
        return rslt;
    }

    public UnitPoint whichUnitPointAt(Point p, Unit current) {
        UnitPoint rslt = doWhichChildUnitAt(model.getRoot(), p, current);
        if (rslt != null && rslt.getUnit() == model.getRoot()) {
            rslt = null;
        }
        return rslt;
    }

    /**
     * @param current компонент внутри которого ищется
     * дочерний компонент, содержащий точку
     * @param point точка для которой проверяется попадание
     * в компонент (в координатах current)
     * @param old текущий выделенный компонент
     * @return компонент самого нижнего уровня, содержащий указанную точку
     */
    private UnitPoint doWhichChildUnitAt(Unit current,
            Point point, Unit old) {
        UnitPoint rslt = null;
        int n = current.getChildrenCount();
        for (int i = 0; i < n; i++) {
            Unit child = current.getChildAt(i);
            if (child.getBounds().contains(point)) {
                Point local = new Point(point.x - child.getBounds().x,
                            point.y - child.getBounds().y);
                rslt = new UnitPoint(child, local);
                if (child.getState() == Unit.State.EXPANDED) {
                    UnitPoint up =  doWhichChildUnitAt(child, local, old);
                    if (up != null) {
                        rslt = up;
                        break;
                    }
                }
            }
        }
        return rslt;
    }

    public void treeChanged(TreeChangeEvent evt) {
        TreeChangeEvent.Type type = evt.getType();
        if (type == TreeChangeEvent.Type.CHILD_ADDED) {
            repaint();
        } else if (type == TreeChangeEvent.Type.CHILD_REMOVED) {
            repaint();
        }
    }

    private static int count = 0;
    public void doCreateComponentAt(Unit current, Point point) {
        Unit unit = new Unit();
        unit.setBounds(new Rectangle(point, unitRenderer.getCollapsedSize()));
        count++;
        unit.setName(Integer.toString(count));
        model.addComponent(unit, current);
    }

    public void doDeleteUnit(Unit component) {
        count--;
        model.removeComponent(component);
    }

    public void moveUnitBy(Unit unit, int deltaX, int deltaY) {
        Rectangle bounds = unit.getBounds();
        unit.setBounds(new Rectangle(
                bounds.x + deltaX, bounds.y + deltaY,
                bounds.width, bounds.height));
        repaint();
    }

    public void modelChanged(ActiveModelEvent evt) {
        String propName = evt.getActionName();
        if (propName == ActiveModel.PROP_CURSOR) {
            setCursor((Cursor) evt.getNewValue());
        }
        repaint();
    }

    public ActiveModel getActiveModel() {
        return activeModel;
    }

    public void doDeleteConnector(Connector connector, Point point) {
        
    }

    public void doBeginConnectMode(Unit unit, Point point) {
    }
}
