package demo.scheme;

import java.awt.*;

/**
 * Created: 11.10.2007 18:34:44
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ActiveRenderer {
    public static final Color COLOR_BORDER = new Color(255, 100, 80, 150);
    public static final Color COLOR_INNER  = new Color(255, 220, 220, 150);

    public void drawActiveRectangle(Graphics g, Rectangle rect) {
        g.setColor(COLOR_BORDER);
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
        g.drawRect(rect.x+1, rect.y+1, rect.width-2, rect.height-2);
        g.setColor(COLOR_INNER);
        g.fillRect(rect.x+2, rect.y+2, rect.width-3, rect.height-3);
    }

    public void drawActiveConnector(Graphics g, Rectangle rect) {

    }
}
