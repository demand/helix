package demo.scheme;

/**
 * Created: 21.05.2007 19:47:25
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ConnectorSpot {
    public static final String PROP_NAME   = "NAME";

    private PropertyChangeListenersList propertyChangeListeners;

    private String name;

    /**
     * в координатах родительского компонента
     */
    private PointImmutable point;
    private Direction direction;

    /**
     * Родительский компонент
     */
    private Unit unit;

    private Connector connector;

    public ConnectorSpot() {
        this.propertyChangeListeners = new PropertyChangeListenersList();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PointImmutable getPoint() {
        return point;
    }

    public void setPoint(PointImmutable point) {
        this.point = point;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeListeners.addListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeListeners.removeListener(listener);
    }

    private void createPropertyChangeListeners() {
        propertyChangeListeners = new PropertyChangeListenersList();
    }

    public enum Direction {
        IN, OUT
    }
}
