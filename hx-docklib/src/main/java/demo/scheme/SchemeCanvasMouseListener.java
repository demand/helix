package demo.scheme;

import javax.swing.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.*;

import com.zwing.resizer.Resizer;
import demo.scheme.actions.*;

/**
 * Created: 29.05.2007 15:34:25
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 *
 */
public class SchemeCanvasMouseListener implements MouseListener, MouseMotionListener {

    private SchemeCanvas canvas;
    private ActiveModel activeModel;
    private ActiveController controller;
    CreateUnitAction createUnitAction;
    DeleteUnitAction deleteUnitAction;
    PropertiesComponentAction propertiesComponentAction;
    CreateConnectorAction createConnectorAction;
    DeleteConnectorAction deleteConnectorAction;

    private static final int DRAG_SENSITIVITY = 4;
    public boolean dragMode;
    public Unit activeUnit;
    public Resizer.ActivityMode activityMode;
    private Point dragStartPoint;

    public SchemeCanvasMouseListener(SchemeCanvas canvas) {
        this.canvas = canvas;
        createUnitAction = new CreateUnitAction(canvas);
        deleteUnitAction = new DeleteUnitAction(canvas);
        propertiesComponentAction = new PropertiesComponentAction(canvas);

        activeModel = canvas.getActiveModel();

        dragMode = false;
        activeUnit = null;
        activityMode = Resizer.ActivityMode.NONE;
        dragStartPoint = new Point();
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3) {
            Point p = e.getPoint();
            JPopupMenu menu = new JPopupMenu("menu");
            UnitPoint up = canvas.whichUnitPointAt(p, null);
            if (up != null) {
                createUnitAction.setUnit(up.getUnit());
                createUnitAction.setPoint(up.getPoint());
                menu.add(createUnitAction);
                deleteUnitAction.setUnit(up.getUnit());
                menu.add(deleteUnitAction);
            } else {
                createUnitAction.setUnit(canvas.getModel().getRoot());
                createUnitAction.setPoint(p);
                menu.add(createUnitAction);
            }
            menu.show(canvas, p.x, p.y);
        } else if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON1) {
            Unit u = canvas.whichUnitAt(e.getPoint(), null);
            activeModel.makeSelected(u);
        }
    }

    public void mousePressed(MouseEvent e) {
        Point p = e.getPoint();
        activeUnit = canvas.whichUnitAt(p, null);
        dragStartPoint = p;
    }

    public void mouseReleased(MouseEvent e) {
        if (dragMode) {
            Point p = e.getPoint();
            Point dragDistance = new Point(
                    p.x - dragStartPoint.x,
                    p.y - dragStartPoint.y);
            Rectangle newRect = Resizer.calcResizedRectangle(activeUnit, activityMode, dragDistance);

            activeUnit.setBounds(newRect);
        }
        activeModel.setActiveUnit(null, 0, 0);
        activeModel.setActiveRectangle(null);
        activeUnit = null;
        dragMode = false;
    }

    public void mouseEntered(MouseEvent e) {
        ; // do nothing
    }

    public void mouseExited(MouseEvent e) {
        ; // do nothing
    }

    public void mouseDragged(MouseEvent e) {
        Point p = e.getPoint();
        if (!dragMode && activeUnit != null) {
            dragMode =
                    Math.abs(p.x - dragStartPoint.x) > DRAG_SENSITIVITY ||
                    Math.abs(p.y - dragStartPoint.y) > DRAG_SENSITIVITY;
        }
        if (dragMode) {
            // передаётся компонент и его текущее смещение (дельта)
            Point dragDistance = new Point(
                    p.x - dragStartPoint.x,
                    p.y - dragStartPoint.y);

            Rectangle newRect = Resizer.calcResizedRectangle(activeUnit, activityMode, dragDistance);
            Point corner = canvas.getAbsolutePoint(activeUnit.getParent(), newRect.getLocation());
            newRect.x = corner.x;
            newRect.y = corner.y;
            activeModel.setActiveRectangle(newRect);

            activeModel.setActiveUnit(
                    activeUnit,
                    p.x - dragStartPoint.x, p.y - dragStartPoint.y);
            activeModel.setActivityMode(activityMode);
        }
    }

    public void mouseMoved(MouseEvent e) {
        Point p = e.getPoint();
        UnitPoint unp = canvas.whichUnitPointAt(p, null);

        activeModel.setHoverUnit(unp != null ? unp.getUnit() : null);
        if (unp != null) {
            performMouseMove(unp.getUnit(), unp.getPoint());
        } else {
            mouseMovedAtNothing();
        }
    }

    public void performMouseMove(Unit unit, Point point) {
        calculateDragType(unit, point);
        Cursor cursor = Resizer.getCursorForDragType(activityMode);
        activeModel.setCursor(cursor);
    }

    private void calculateDragType(Unit unit, Point point) {
        Dimension sz = unit.getBounds().getSize();
        activityMode = Resizer.ActivityMode.NONE;
        if (Resizer.innerPoint(sz, unit, point)) {
            activityMode = Resizer.ActivityMode.MOVE_FREE;
        } else {
            activityMode = Resizer.calcResizeType(sz, unit, point);
        }
    }

    public void mouseMovedAtNothing() {
        activeModel.setCursor(Cursor.getDefaultCursor());
    }
}
