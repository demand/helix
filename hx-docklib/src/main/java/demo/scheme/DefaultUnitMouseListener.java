package demo.scheme;

import java.awt.event.MouseEvent;

/**
 * Created: 06.07.2007 17:13:49
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DefaultUnitMouseListener implements UnitMouseListener {
    private SchemeCanvas canvas;

    public void mouseClicked(Unit unit, MouseEvent e) {
    }

    public void mousePressed(Unit unit, MouseEvent e) {
    }

    public void mouseReleased(Unit unit, MouseEvent e) {
    }

    public void mouseEntered(Unit unit, MouseEvent e) {
    }

    public void mouseExited(Unit unit, MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void setCanvas(SchemeCanvas canvas) {
        this.canvas = canvas;
    }
}
