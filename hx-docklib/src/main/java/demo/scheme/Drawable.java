package demo.scheme;

import java.awt.*;

/**
 * Created: 28.03.2007 17:45:05
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Drawable {
    /**
     * Возвращает границы компонента в координатах экрана
     */
    Rectangle getBounds();

    /**
     *  Рисует компонент.
     * @param g графический контекст
     * @param  where левый верхний угол в коррдинатах экрана
     */
    void draw(Graphics g, Point where);
}
