package demo.scheme;

import com.zwing.utils.ResourceManager;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 18.05.2007 19:47:45
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class UnitRendererImpl implements UnitRenderer {

    private static final Color NORMAL_BORDER_COLOR = Color.LIGHT_GRAY;
    private static final Color SELECTED_BORDER_COLOR = Color.DARK_GRAY;
    private static final Color HOVER_BORDER_COLOR = new Color(180, 160, 160);
    private static final Insets ICON_INSETS = new Insets(4, 4, 4, 4);
    private static final Insets EXPANDED_INSETS = new Insets(4, 4, 4, 4);

    private ImageIcon icon;
    private Dimension collapsedSize;

    public UnitRendererImpl() {
        icon = ResourceManager.getImageIcon("scheme.unit.icon");
        collapsedSize = new Dimension(
                icon.getIconWidth() + ICON_INSETS.left + ICON_INSETS.right,
                icon.getIconHeight() + ICON_INSETS.top + ICON_INSETS.bottom);
    }

    public Dimension getCollapsedSize() {
        return collapsedSize;
    }

    public void renderComponent(Unit unit, Graphics g, Point p,
            DrawMode dmode, SchemeViewContext context) {
        Rectangle bounds = unit.getBounds();
        Color borderColor =
                dmode == DrawMode.NORMAL ? NORMAL_BORDER_COLOR :
                dmode == DrawMode.SELECTED ? SELECTED_BORDER_COLOR :
                dmode == DrawMode.HOVER ? HOVER_BORDER_COLOR :
                NORMAL_BORDER_COLOR;

        g.setColor(borderColor);
        g.drawRect(p.x,  p.y, bounds.width, bounds.height);
        g.drawRect(p.x + 1,  p.y + 1, bounds.width - 2, bounds.height - 2);

        if (unit.getChildrenCount() == 0) {
            g.drawImage(icon.getImage(), p.x + ICON_INSETS.left, p.y + ICON_INSETS.top, null);
        }

        g.setColor(Color.BLACK);
        g.drawString(unit.getName(), p.x + 5, p.y + 14);
    }
}
