package demo.scheme;

/**
 * Created: 12.04.2007 12:22:12
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface Resizable {
    boolean canResize();
}
