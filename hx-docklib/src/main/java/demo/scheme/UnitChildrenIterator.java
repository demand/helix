package demo.scheme;

import java.util.Iterator;

/**
 * Created: 28.03.2007 13:10:28
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class UnitChildrenIterator implements Iterator<Unit> {

    private Unit parent;
    private int currentIndex;

    public UnitChildrenIterator(Unit parent) {
        if (parent == null) {
            throw new NullPointerException();
        }
        this.parent = parent;
        currentIndex = 0;
    }

    public boolean hasNext() {
        return currentIndex < parent.getChildrenCount();
    }

    public Unit next() {
        return parent.getChildAt(currentIndex++);
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
