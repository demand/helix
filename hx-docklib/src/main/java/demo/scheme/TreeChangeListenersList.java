package demo.scheme;

import java.util.List;
import java.util.ArrayList;

/**
 * Created: 03.05.2007 10:34:01
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class TreeChangeListenersList {
    private List<TreeChangeListener> listeners;

    public TreeChangeListenersList() {
        listeners = new ArrayList<TreeChangeListener>(0);
    }

    public void addListener(TreeChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeListener(TreeChangeListener listener) {
        if (listener == null) {
            throw new NullPointerException();
        }
        listeners.remove(listener);
    }

    public void clear() {
        listeners.clear();
    }

    public void notifyListeners(TreeChangeEvent event) {
        for(TreeChangeListener listener : listeners) {
            listener.treeChanged(event);
        }
    }
}
