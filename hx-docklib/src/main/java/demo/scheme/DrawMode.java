package demo.scheme;

/**
 * Created: 04.06.2007 15:16:19
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public enum DrawMode {
    NORMAL,
    HOVER,
    SELECTED,
    ACTIVE
}
