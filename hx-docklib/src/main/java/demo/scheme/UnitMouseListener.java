package demo.scheme;

import java.awt.event.MouseEvent;

/**
 * Created: 05.07.2007 15:28:59
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface UnitMouseListener {
    public void mouseClicked(Unit unit, MouseEvent e);
    public void mousePressed(Unit unit, MouseEvent e);
    public void mouseReleased(Unit unit, MouseEvent e);
    public void mouseEntered(Unit unit, MouseEvent e);
    public void mouseExited(Unit unit, MouseEvent e);

    public void mouseDragged(MouseEvent e);
    public void mouseMoved(MouseEvent e);
}
