package demo.scheme;

import com.zwing.resizer.Resizer;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created: 11.07.2007
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ActiveModel {

    public static final String PROP_SELECTEDUNIT = "AM_SELECTEDUNIT";
    public static final String PROP_HOVERUNIT    = "AM_HOVERUNIT";
    public static final String PROP_ACTIVEUNIT   = "AM_ACTIVEUNIT";
    public static final String PROP_ACTIVITYMODE = "AM_ACTIVITYMODE";
    public static final String PROP_BOUNDS       = "AM_BOUNDS";
    public static final String PROP_ACTIVERECT   = "AM_ACTIVERECT";
    public static final String PROP_CURSOR       = "AM_CURSOR";

    private List<ActiveModelListener> activeModelListeners;

    private Unit selectedUnit;
    private Unit hoverUnit;

    // компонент, подвергающийся изменениям
    private Unit activeUnit;
    // в координатах родительского компонента
    private Point dragStart;
    // измкенение размеров или положения компонента
    private Point dragShift;
    // тип изменения компонента (перемещение, размеры)
    private Resizer.ActivityMode activityMode;
    // в координатах родительского компонента
    private Rectangle bounds;
    // в координатах окна
    private Rectangle activeRectangle;

    private Cursor cursor;

    public ActiveModel() {
        activityMode = Resizer.ActivityMode.NONE;
        activeModelListeners = new ArrayList<ActiveModelListener>(0);
        dragStart = new Point();
        dragShift = new Point();
    }

    public Point getDragShift() {
        return dragShift;
    }

    public void setDragShift(Point dragShift) {
        this.dragShift = dragShift;
        notifyListeners(new ActiveModelEvent(this, PROP_BOUNDS, dragShift, this.dragShift, null));
    }

    public Unit getSelectedUnit() {
        return selectedUnit;
    }

    public void setSelectedUnit(Unit selectedUnit) {
        boolean rslt = false;
        Unit oldUnit = this.selectedUnit;
        if (selectedUnit != oldUnit) {
            this.selectedUnit = selectedUnit;
            notifyListeners(new ActiveModelEvent(this, PROP_HOVERUNIT, oldUnit, hoverUnit, null));
            rslt = true;
        }
    }

    public Unit getHoverUnit() {
        return hoverUnit;
    }

    public void setHoverUnit(Unit hoverUnit) {
        boolean rslt = false;
        Unit oldUnit = this.hoverUnit;
        if (hoverUnit != oldUnit) {
            this.hoverUnit = hoverUnit;
            notifyListeners(new ActiveModelEvent(this, PROP_HOVERUNIT, oldUnit, hoverUnit, null));
            rslt = true;
        }
    }

    public Unit getActiveUnit() {
        return activeUnit;
    }

    public Point getDragStart() {
        return dragStart;
    }

    public void setDragStart(Point dragStart) {
        this.dragStart = dragStart;
    }

    public Resizer.ActivityMode getActivityMode() {
        return activityMode;
    }

    public void setActivityMode(Resizer.ActivityMode activityMode) {
        Resizer.ActivityMode oldActivityMode = activityMode;
        this.activityMode = activityMode;
        ActiveModelEvent event = new ActiveModelEvent(this,
                PROP_ACTIVITYMODE, oldActivityMode, activityMode, null);
        notifyListeners(event);
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void setBounds(Rectangle bounds) {
        this.bounds = bounds;
    }

    public void addActiveModelListener(ActiveModelListener listener) {
        if (!activeModelListeners.contains(listener)) {
            activeModelListeners.add(listener);
        }
    }

    public void removeActiveModelListener(ActiveModelListener listener) {
        activeModelListeners.remove(listener);
    }

    public void notifyListeners(ActiveModelEvent event) {
        for (ActiveModelListener listener : activeModelListeners) {
            listener.modelChanged(event);
        }
    }

    public void setActiveUnit(Unit activeUnit, int x, int y) {
        Unit oldUnit = activeUnit;
        this.activeUnit = activeUnit;
        activityMode = Resizer.ActivityMode.NONE;
        dragShift.x = x;
        dragShift.y = y;
        ActiveModelEvent event = new ActiveModelEvent(this,
                PROP_ACTIVEUNIT, oldUnit, activeUnit, null);
        notifyListeners(event);
    }

    public void makeSelected(Unit u) {
    }

    public Rectangle getActiveRectangle() {
        return activeRectangle;
    }

    public void setActiveRectangle(Rectangle activeRectangle) {
        boolean rslt = false;
        Rectangle oldRect = this.activeRectangle;
        if (!Util.safeEquals(oldRect, activeRectangle)) {
            this.activeRectangle = activeRectangle;
            notifyListeners(new ActiveModelEvent(this,
                    PROP_ACTIVERECT, oldRect, activeRectangle, null));
            rslt = true;
        }
    }


    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        boolean rslt = false;
        Cursor oldCursor = this.cursor;
        if (cursor != oldCursor) {
            this.cursor = cursor;
            notifyListeners(new ActiveModelEvent(this,
                    PROP_CURSOR, oldCursor, cursor, null));
            rslt = true;
        }
    }
}
