package demo.scheme;

/**
 * Created: 26.04.2007 15:31:33
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface UnitChangeListener {
    void propertyChanged(UnitChangedEvent evt);
}
