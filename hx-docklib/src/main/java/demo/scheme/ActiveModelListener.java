package demo.scheme;

/**
 * Created: 11.07.2007 19:16:53
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public interface ActiveModelListener {
    void modelChanged(ActiveModelEvent evt);
}
