package demo;

import javax.swing.*;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

import com.zwing.utils.ResourceManager;
import com.zwing.utils.ResourceAction;
import com.zwing.utils.ZUIManager;
import com.zwing.*;
import com.zwing.colorcombo.ColorPopup;
import com.zwing.colorchooser.ColorSelectedAdapter;

/**
 * Created: 23.06.2006 17:05:05
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class MainFrame extends JFrame {

    Action newAction;
    Action openAction;
    Action saveAction;
    Action printAction;
    Action settingsAction;
    Action exitAction;

    Action cutAction;
    Action copyAction;
    Action pasteAction;

    Action backAction;
    Action forwardAction;

    Action editFileAction;

    ZDropDownButton ddb2;

    MainFrame() {
        ZUIManager.getInstance().addRootComponent(this); 

        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setTitle(ResourceManager.getString("app.title"));

        enableEvents(AWTEvent.WINDOW_EVENT_MASK);

        createComponents();
    }

    private void makeVisible() {
        setResizable(true);
        setPreferredSize(new Dimension(700, 500));
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        setMaximizedBounds(env.getMaximumWindowBounds());
        pack();
        setVisible(true);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    private void createComponents() {
        createActions();
        createMenu();
        JToolBar tbr = createToolbar();

        JPanel contentPane = (JPanel) this.getContentPane();
        JComponent main = new MainTabs();

        contentPane.setLayout(new BorderLayout());
        contentPane.add(main, BorderLayout.CENTER);
        contentPane.add(tbr, BorderLayout.NORTH);
//        contentPane.add(statusBar, BorderLayout.SOUTH);
    }

    private void createActions() {
        newAction      = new NewAction();
        openAction     = new OpenAction();
        saveAction     = new SaveAction();
        printAction    = new PrintAction();
        settingsAction = new SettingsAction();
        exitAction     = new ExitAction();

        cutAction   = new CutAction();
        copyAction  = new CopyAction();
        pasteAction = new PasteAction();

        backAction = new BackAction();
        forwardAction = new ForwardAction();
    }

    private void createMenu() {
        JMenuBar mb = new JMenuBar();

        JMenu fileMenu = new JMenu(ResourceManager.getString(
                ResourceAction.ACTIONS_PREFIX + ".menu.file.label"));
        JMenu editMenu = new JMenu(ResourceManager.getString(
                ResourceAction.ACTIONS_PREFIX + ".menu.edit.label"));
        JMenu settingsMenu = new JMenu(ResourceManager.getString(
                ResourceAction.ACTIONS_PREFIX + ".menu.settings.label"));

        fileMenu.add(new JMenuItem(newAction));
        fileMenu.add(new JMenuItem(openAction));
        fileMenu.add(new JMenuItem(saveAction));
        fileMenu.addSeparator();
        fileMenu.add(new JMenuItem(printAction));
        fileMenu.addSeparator();
        fileMenu.add(new JMenuItem(exitAction));

        editMenu.add(new JMenuItem(cutAction));
        editMenu.add(new JMenuItem(copyAction));
        editMenu.add(new JMenuItem(pasteAction));

        settingsMenu.add(new JMenuItem(settingsAction));

        mb.add(fileMenu);
        mb.add(editMenu);
        mb.add(settingsMenu);
        setJMenuBar(mb);
    }

    private void doNew() {
        ZColorChooserDialog.showColorChooserDialog(this, "Select Color", null);
    }

    private void doOpen() {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Open file");
        int returnVal = fc.showOpenDialog(this);
    }

    private void doSave() {
        JWindow jw = new JWindow(this) {
            public Dimension getPreferredSize() {
                return new Dimension(200, 300);
            }

            public void paint(Graphics g) {
                Dimension sz = getSize();
                g.setColor(Color.YELLOW);
                g.fillRect(0, 0, sz.width, sz.height);
                g.setColor(Color.BLACK);
                g.drawRect(0, 0, sz.width-1, sz.height-1);
            }
        };
        jw.setBounds(200, 300, 200, 300);
        jw.setVisible(true);
    }

    private void doPrint() {
    }

    private void doSettings() {
        final JDialog dialog = new JDialog(this, "Test 1", true);
        Container c = dialog.getContentPane();
        c.setLayout(new BorderLayout());
        JPanel pn = new JPanel();
        pn.setPreferredSize(new Dimension(300, 200));
        pn.add(new JButton(new AbstractAction("dialog...") {
            public void actionPerformed(ActionEvent e) {
                final JDialog dialog2 = new JDialog(dialog, "Test 2", true);
                Container c = dialog2.getContentPane();
                c.setLayout(new BorderLayout());
                JPanel pn = new JPanel();
                pn.setPreferredSize(new Dimension(200, 150));
                pn.add(new JButton(new AbstractAction("close") {
                    public void actionPerformed(ActionEvent e) {
                        dialog2.setVisible(false);
                    }
                }));
                c.add(pn, BorderLayout.CENTER);
                dialog2.pack();
                dialog2.setLocationRelativeTo(dialog2.getOwner());
                dialog2.setVisible(true);
            }
        }));
        pn.add(new JButton(new AbstractAction("close") {
            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        }));
        c.add(pn, BorderLayout.CENTER);
        dialog.pack();
        dialog.setLocationRelativeTo(dialog.getOwner());
        dialog.setVisible(true);
    }

    private void doCut() {
        System.out.println("cut");
    }

    private void doCopy() {
        System.out.println("copy");
    }

    private void doPaste() {
        System.out.println("paste");
    }

    private void doBack() {
        copyAction.setEnabled(false);
    }

    private void doForward() {
        copyAction.setEnabled(true);
    }

    private void doExit() {
        fin();
    }

    private JToolBar createToolbar() {
        JToolBar tbr = new JToolBar();
        tbr.setFloatable(false);
        tbr.setOrientation(JToolBar.HORIZONTAL);
        tbr.setRollover(true);
        tbr.setFocusable(false);

        tbr.add(newAction);
        tbr.add(openAction);
        tbr.add(saveAction);
        tbr.addSeparator();
        tbr.add(cutAction);
        tbr.add(copyAction);
        tbr.add(pasteAction);
        tbr.addSeparator();
        tbr.add(backAction);
        tbr.add(forwardAction);
        tbr.addSeparator();
        tbr.add(settingsAction);

        ZDropDownButton ddb = new ZDropDownButton("Btn1",
                ResourceManager.getIcon("actions.print.icon"));
        ZPopupMenu zp = new ZPopupMenu(ddb);
        zp.add(cutAction);
        zp.add(copyAction);
        zp.add(pasteAction);
        zp.add(new AbstractAction("No icon") {
            public void actionPerformed(ActionEvent e) {
            }
        });
        ddb.setPopupMenu(zp);
        tbr.add(ddb);

        ColorSelectedAdapter csa = new ColorSelectedAdapter() {
            public Color colorSelected(Color newColor, Object source) {
                ddb2.setIcon(new ColorIcon(new Dimension(14,14),newColor));
                ddb2.stopPopup();
                return null;
            }
        };
        ddb2 = new ZDropDownButton(null,
                new ColorIcon(new Dimension(12,12),null));
        ddb2.setPopupComponent(new ColorPopup(csa, null));
        tbr.add(ddb2);

        final ZDropDownButton ddb3 = new ZDropDownButton(null,
                new ColorIcon(new Dimension(12,12),null));
        ColorSelectedAdapter csa2 = new ColorSelectedAdapter() {
            public Color colorSelected(Color newColor, Object source) {
                ddb3.setIcon(new ColorIcon(new Dimension(12,12),newColor));
                return null;
            }
        };
        SlidersPopup sp = new SlidersPopup(csa2);
        ddb3.setIcon(new ColorIcon(new Dimension(12,12),sp.getColor()));
        ddb3.setPopupComponent(sp);
        tbr.add(ddb3);

//        JDateChooser dc = new JDateChooser();
//        dc.setPreferredSize(new Dimension(100, getPreferredSize().height));
//        dc.setMaximumSize(new Dimension(100, getPreferredSize().height));
//        tbr.add(dc);

        return tbr;
    }

    protected void fin() {
        System.exit(0);
    }

    protected void processWindowEvent(WindowEvent e) {
      super.processWindowEvent(e);
      if (e.getID() == WindowEvent.WINDOW_CLOSING) {
          fin();
      }
    }

    public static final void showFrame() {
        new MainFrame().makeVisible();
    }

    private class NewAction extends ResourceAction {
        private NewAction() {
            super("new");
        }
        public void actionPerformed(ActionEvent e) {
            doNew();
        }
    }

    private class OpenAction extends ResourceAction {
        private OpenAction() {
            super("open");
        }

        public void actionPerformed(ActionEvent e) {
            doOpen();
        }
    }

    private class SaveAction extends ResourceAction {
        private SaveAction() {
            super("save");
        }

        public void actionPerformed(ActionEvent e) {
            doSave();
        }
    }

    private class PrintAction extends ResourceAction {
        private PrintAction() {
            super("print");
        }

        public void actionPerformed(ActionEvent e) {
            doPrint();
        }
    }

    private class SettingsAction extends ResourceAction {
        private SettingsAction() {
            super("settings");
        }

        public void actionPerformed(ActionEvent e) {
            doSettings();
        }
    }

    private class CutAction extends ResourceAction {
        private CutAction() {
            super("cut");
        }

        public void actionPerformed(ActionEvent e) {
            doCut();
        }
    }

    private class CopyAction extends ResourceAction {
        private CopyAction() {
            super("copy");
        }

        public void actionPerformed(ActionEvent e) {
            doCopy();
        }
    }

    private class PasteAction extends ResourceAction {
        private PasteAction() {
            super("paste");
        }

        public void actionPerformed(ActionEvent e) {
            doPaste();
        }
    }

    private class BackAction extends ResourceAction {
        private BackAction() {
            super("back");
        }

        public void actionPerformed(ActionEvent e) {
            doBack();
        }
    }

    private class ForwardAction extends ResourceAction {
        private ForwardAction() {
            super("forward");
        }

        public void actionPerformed(ActionEvent e) {
            doForward();
        }
    }

    private class EditFileAction extends ResourceAction {
        private EditFileAction() {
            super("editfile");
        }

        public void actionPerformed(ActionEvent e) {
            // do nothing
        }
    }

    private class ExitAction extends ResourceAction {
        private ExitAction() {
            super("exit");
        }

        public void actionPerformed(ActionEvent e) {
            doExit();
        }
    }

}
