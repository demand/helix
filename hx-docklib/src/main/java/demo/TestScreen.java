package demo;

import java.awt.*;

/**
 * TestScreen
 * Created: 27.05.2009, 16:31:05
 *
 * @author A.Demin (demin@genego.com)
 */
public class TestScreen {

    public static void main(String[] args) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();

        Rectangle screenBounds;
        Insets screenInsets;
        GraphicsConfiguration gc = null;
        // Try to find GraphicsConfiguration, that includes mouse
        // pointer position
        GraphicsEnvironment ge =
            GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
        for(int i = 0; i < gd.length; i++) {
            if(gd[i].getType() == GraphicsDevice.TYPE_RASTER_SCREEN) {
                GraphicsConfiguration dgc = gd[i].getDefaultConfiguration();
                gc = dgc;
            }
        }

        if(gc != null) {
            // If we have GraphicsConfiguration use it to get
            // screen bounds and insets
            screenInsets = toolkit.getScreenInsets(gc);
            screenBounds = gc.getBounds();
        } else {
            // If we don't have GraphicsConfiguration use primary screen
            // and empty insets
            screenInsets = new Insets(0, 0, 0, 0);
            screenBounds = new Rectangle(toolkit.getScreenSize());
        }

        int scrWidth = screenBounds.width -
                    Math.abs(screenInsets.left+screenInsets.right);
        int scrHeight = screenBounds.height -
                    Math.abs(screenInsets.top+screenInsets.bottom);

    }
}
