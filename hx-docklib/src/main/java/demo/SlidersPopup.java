package demo;

import com.zwing.colorchooser.ColorSelectedListener;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Dimension;

/**
 * Created: 21.07.2006 13:06:03
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class SlidersPopup extends JComponent implements ChangeListener {
    JComponent parent;

    JSlider sliderRed = new JSlider(JSlider.HORIZONTAL, 0, 255, 128);
    JSlider sliderGreen = new JSlider(JSlider.HORIZONTAL, 0, 255, 128);
    JSlider sliderBlue = new JSlider(JSlider.HORIZONTAL, 0, 255, 128);

    ColorSelectedListener listener;

    public SlidersPopup(ColorSelectedListener listener) {
        this.listener = listener;
        localInit();
    }


    private void localInit() {
        setBorder(
                BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(Color.black),
                        BorderFactory.createLineBorder(Color.white)));
        setLayout(new GridLayout(3, 1, 0, 7));

        sliderRed.setPreferredSize(new Dimension(150, sliderRed.getPreferredSize().height));
        sliderGreen.setPreferredSize(new Dimension(150, sliderGreen.getPreferredSize().height));
        sliderBlue.setPreferredSize(new Dimension(150, sliderBlue.getPreferredSize().height));

        sliderRed.setBackground(Color.red);
        sliderGreen.setBackground(Color.green);
        sliderBlue.setBackground(Color.blue);

        sliderRed.addChangeListener(this);
        sliderGreen.addChangeListener(this);
        sliderBlue.addChangeListener(this);

        add(sliderRed);
        add(sliderGreen);
        add(sliderBlue);
    }


    public void stateChanged(ChangeEvent e) {
        listener.colorSelected(getColor(), this);
    }

    public Color getColor() {
        return new Color(
                sliderRed.getValue(),
                sliderGreen.getValue(),
                sliderBlue.getValue());
    }
}
