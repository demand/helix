package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatAttributes
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatAttributes extends BoatEmpty {
    public BoatAttributes() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_DOCK, ResourceManager.getString("boat.attribs.label"));
        setIcon(ResourceManager.getImageIcon("boat.attribs.icon"));
        setCargo(new CargoAttributes());
    }

    public static class CargoAttributes extends JScrollPane implements DockingCargo {
        public CargoAttributes() {
            super(new JTextArea("Attributes"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Attributes";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}
