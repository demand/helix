package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatFind
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatFind extends BoatEmpty {
    public BoatFind() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_SLIDE, ResourceManager.getString("boat.find.label"));
        setIcon(ResourceManager.getImageIcon("boat.find.icon"));
        setCargo(new CargoFind());
    }

    public static class CargoFind extends JScrollPane implements DockingCargo {
        public CargoFind() {
            super(new JTextArea("Find"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Find";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}

