package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatAnnotations
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatAnnotations extends BoatEmpty {
    public BoatAnnotations() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
            BoatEmpty.BST_DOCK, ResourceManager.getString("boat.anns.label"));
        setIcon(ResourceManager.getImageIcon("boat.anns.icon"));
        setCargo(new CargoAnnotations());
    }

    public static class CargoAnnotations extends JScrollPane implements DockingCargo {
        public CargoAnnotations() {
            super(new JTextArea("Annotations"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Annotations";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}
