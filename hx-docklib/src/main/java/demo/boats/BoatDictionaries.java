package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatDictionaries
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatDictionaries extends BoatEmpty {
    public BoatDictionaries() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_SLIDE, ResourceManager.getString("boat.dics.label"));
        setIcon(ResourceManager.getImageIcon("boat.dics.icon"));
        setCargo(new CargoDictionaries());
    }

    public static class CargoDictionaries extends JScrollPane implements DockingCargo {
        public CargoDictionaries() {
            super(new JTextArea("Dictionaries"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Dictionaries";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}