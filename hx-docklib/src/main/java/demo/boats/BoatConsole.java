package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatConsole
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatConsole extends BoatEmpty {
    public BoatConsole() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_FLOAT, ResourceManager.getString("boat.console.label"));
        setIcon(ResourceManager.getImageIcon("boat.console.icon"));
        setCargo(new CargoConsole());
    }

    public static class CargoConsole extends JScrollPane implements DockingCargo {
        public CargoConsole() {
            super(new JTextArea("Console"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Console";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}
