package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatMessages
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatMessages extends BoatEmpty {
    public BoatMessages() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_FLOAT, ResourceManager.getString("boat.mess.label"));
        setIcon(ResourceManager.getImageIcon("boat.mess.icon"));
        setCargo(new CargoMessages());
    }

    public static class CargoMessages extends JScrollPane implements DockingCargo {
        public CargoMessages() {
            super(new JTextArea("Messages"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Messages";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}

