package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JComponent;
import java.awt.Insets;

/**
 * BoatEditor
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatEditor extends BoatEmpty {
    public BoatEditor() {
        super(BoatEmpty.BST_CENTER, BoatEmpty.BST_CENTER, "Center");

        setResizable(BST_RESIZABLE_NONE);
        setMovable(false);
        insets = new Insets(0, 0, 0, 0);

        setCargo(new CargoEditor());
    }

    public static class CargoEditor extends JScrollPane implements DockingCargo {
        public CargoEditor() {
            super(new JEditorPane());
            JEditorPane editor = new JEditorPane();
            editor.setText("Editor");
            getViewport().add(editor, null);
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Editor";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}
