package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComponent;

/**
 * BoatFiles
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatFiles extends BoatEmpty {
    public BoatFiles() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_SLIDE, ResourceManager.getString("boat.files.label"));
        setIcon(ResourceManager.getImageIcon("boat.files.icon"));
        setCargo(new CargoFiles());
    }

    public static class CargoFiles extends JScrollPane implements DockingCargo {
        public CargoFiles() {
            super(new JTextArea("Files"));
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Files";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}
