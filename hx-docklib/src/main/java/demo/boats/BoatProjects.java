package demo.boats;

import com.zwing.docking.BoatEmpty;
import com.zwing.utils.ResourceManager;
import com.zwing.DockingCargo;
import com.zwing.CargoListener;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;

/**
 * BoatProjects
 *
 * @author А.Дёмин
 * @since 13.01.2005
 */
public class BoatProjects extends BoatEmpty {
    public BoatProjects() {
        super(BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_DOCK, ResourceManager.getString("boat.project.label"));
        setIcon(ResourceManager.getImageIcon("boat.project.icon"));
        setCargo(new CargoProjects());
    }

    public static class CargoProjects extends JScrollPane implements DockingCargo {
        public CargoProjects() {
            JList prj = new JList();
            getViewport().add(prj, null);
            String[] data = new String[100];
            for (int i = 0; i < data.length; i++) {
                data[i] = "Item " + i;
            }
            prj.setListData(data);
        }

        public JComponent getVisualComponent() {
            return this;
        }

        public String getUniqueName() {
            return "Projects";
        }

        public Object getProperty(String propName) {
            return null;
        }

        public void addCargoListener(CargoListener cargoListener) {
        }

        public void removeCargoListener(CargoListener cargoListener) {
        }
    }
}
