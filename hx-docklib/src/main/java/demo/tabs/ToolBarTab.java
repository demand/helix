package demo.tabs;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.*;

import com.zwing.*;
import com.zwing.utils.ResourceAction;

/**
 * ToolBarTab
 * Created: 22.05.2009, 14:31:24
 *
 * @author A.Demin (demin@genego.com)
 */
public class ToolBarTab extends JSplitPane {

    public ToolBarTab() {
        super(JSplitPane.HORIZONTAL_SPLIT);
        setLeftComponent(createLeftComponent());
        JPanel p = new JPanel();
        p.setBackground(Color.YELLOW);
        setRightComponent(p);
        setDividerLocation(300);
    }

    private Component createLeftComponent() {
        JSplitPane left = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        left.setTopComponent(createTopComponent());
        left.setBottomComponent(createBottomComponent());
        left.setDividerLocation(300);
        return left;
    }

    private Component createTopComponent() {
        JPanel rslt = new JPanel();
        rslt.setLayout(new BorderLayout());
        JPanel p = new JPanel();
        p.setBackground(Color.BLUE);
        rslt.add(p, BorderLayout.CENTER);
        rslt.add(createToolbar(JToolBar.HORIZONTAL), BorderLayout.NORTH);
        return rslt;
    }

    private Component createBottomComponent() {
        JPanel rslt = new JPanel();
        rslt.setLayout(new BorderLayout());
        JPanel p = new JPanel();
        p.setBackground(Color.CYAN);
        rslt.add(p, BorderLayout.CENTER);
        rslt.add(createToolbar(JToolBar.VERTICAL), BorderLayout.WEST);
        return rslt;
    }

    private Component createToolbar(int orientation) {
        ZToolBar tbr = new com.zwing.ZToolBar(orientation);
        tbr.setFloatable(false);
        tbr.setRollover(true);
        tbr.setFocusable(false);

        tbr.add(new VoidAction("new"));
        tbr.add(new VoidAction("open"));
        tbr.add(new VoidAction("save"));
        tbr.addSeparator();
        tbr.add(new VoidAction("cut"));
        tbr.add(new VoidAction("copy"));
        tbr.add(new VoidAction("paste"));
        tbr.addSeparator();
        tbr.add(new VoidAction("back"));
        tbr.add(new VoidAction("forward"));
        tbr.addSeparator();
        tbr.add(new VoidAction("settings"));

        com.zwing.ZDropDownButton ddb = new com.zwing.ZDropDownButton("Btn1",
                com.zwing.utils.ResourceManager.getIcon("actions.print.icon"));
        com.zwing.ZPopupMenu zp = new com.zwing.ZPopupMenu(ddb);
        zp.add(new VoidAction("cut"));
        zp.add(new VoidAction("copy"));
        zp.add(new VoidAction("paste"));
        zp.add(new AbstractAction("No icon") {
            public void actionPerformed(ActionEvent e) {
            }
        });
        ddb.setPopupMenu(zp);
        tbr.add(ddb);

        return tbr;
    }

    private static class VoidAction extends ResourceAction {
        private VoidAction(String resName) {
            super(resName);
        }
        public void actionPerformed(ActionEvent e) {
            ;
        }
    }
}
