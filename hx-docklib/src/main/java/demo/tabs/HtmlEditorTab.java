package demo.tabs;

import com.genego.st.DocStructurePanel;
import com.genego.st.InfoEditorPanel;
import com.genego.st.InfoSourcePanel;
import com.zwing.CargoListener;
import com.zwing.DockingCargo;
import com.zwing.docking.BoatEmpty;
import com.zwing.docking.DockManager;
import com.zwing.docking.DockingConst;

import javax.swing.*;
import java.awt.*;

/**
 * HtmlEditorTab
 * Created: 10.05.2011, 19:21:58
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class HtmlEditorTab extends JPanel {

    DockManager dm;

    public HtmlEditorTab() {
        setLayout(new BorderLayout());

        dm = new DockManager();
        add(dm.getVisualComponent(), BorderLayout.CENTER);
        registerBoats();
    }

    protected void registerBoats() {

        final DocStructurePanel structurePanel = new DocStructurePanel();
        final InfoEditorPanel infoEditorPanel = new InfoEditorPanel();
        final InfoSourcePanel infoSourcePanel = new InfoSourcePanel();
        structurePanel.setTextPane(infoEditorPanel.getEditor());
        infoSourcePanel.setSourceTextPane(infoEditorPanel.getEditor());

        BoatEmpty leftVoid = new BoatEmpty(
                BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_DOCK, "Stricture") {{
            setCargo(new DockingCargo() {
                public JComponent getVisualComponent() {
                    return structurePanel;
                }
                public String getUniqueName() {
                    return "leftVoid";
                }
                public Object getProperty(String propName) {
                    return null;
                }
                public void addCargoListener(CargoListener cargoListener) {
                }
                public void removeCargoListener(CargoListener cargoListener) {
                }
            });
        }};
        dm.getCargoManager().registerCargo(leftVoid.getCargo());
        leftVoid.setDockSizes(new Dimension(400, 300));
        dm.attach(leftVoid, DockingConst.DK_WEST);

        BoatEmpty bottomVoid = new BoatEmpty(
                BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
                BoatEmpty.BST_DOCK, "Source") {{
            setCargo(new DockingCargo() {
                public JComponent getVisualComponent() {
                    return infoSourcePanel;
                }
                public String getUniqueName() {
                    return "bottomVoid";
                }
                public Object getProperty(String propName) {
                    return null;
                }
                public void addCargoListener(CargoListener cargoListener) {
                }
                public void removeCargoListener(CargoListener cargoListener) {
                }
            });
        }};
        dm.getCargoManager().registerCargo(bottomVoid.getCargo());
        bottomVoid.setDockSizes(new Dimension(400, 300));
        dm.attach(bottomVoid, DockingConst.DK_SOUTH);

        BoatEmpty center = new BoatEmpty(BoatEmpty.BST_CENTER, BoatEmpty.BST_CENTER, "Center") {{
            setResizable(BoatEmpty.BST_RESIZABLE_NONE);
            setMovable(false);
            setInsets(new Insets(0, 0, 0, 0));
            setCargo(new DockingCargo() {
                public JComponent getVisualComponent() {
                    return infoEditorPanel;
                }
                public String getUniqueName() {
                    return "center";
                }
                public Object getProperty(String propName) {
                    return null;
                }
                public void addCargoListener(CargoListener cargoListener) {
                }
                public void removeCargoListener(CargoListener cargoListener) {
                }
            });
        }};
        dm.getCargoManager().registerCargo(center.getCargo());
        dm.attach(center, DockingConst.DK_CENTER);

        center.setVisible(false);

        dm.requestVisible(center);
        dm.requestVisible(leftVoid);
        dm.requestVisible(bottomVoid);
    }
}
