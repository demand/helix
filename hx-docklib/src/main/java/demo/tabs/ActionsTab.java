package demo.tabs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;

/**
 * Created by andrey on 27.05.14.
 */
public class ActionsTab extends JPanel implements KeyListener{

    private JLabel lblInfo;
    private JTextField txtData;

    public ActionsTab() {
        setLayout(new FlowLayout());

        lblInfo = new JLabel("Key:");
        txtData = new JTextField(50);

        add(lblInfo);
        add(txtData);

        txtData.addKeyListener(this);

        InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = getActionMap();

        KeyStroke shiftPressedKs = KeyStroke.getKeyStroke("shift pressed SHIFT");
        KeyStroke shiftReleasedKs = KeyStroke.getKeyStroke("released SHIFT");

        am.put("shiftp", new AbstractAction("shiftp") {
            public void actionPerformed(ActionEvent e) {
                System.out.println("shift p");
            }
        });
        im.put(shiftPressedKs, "shiftp");

        am.put("shiftr", new AbstractAction("shiftr") {
            public void actionPerformed(ActionEvent e) {
                System.out.println("shift r");
            }
        });
        im.put(shiftReleasedKs, "shiftr");
    }

    public void keyTyped(KeyEvent e) {
        System.out.println("typed:" + e.getKeyCode());
    }

    public void keyPressed(KeyEvent e) {
        System.out.println("pressed:" + e.getKeyCode());
    }

    public void keyReleased(KeyEvent e) {
        System.out.println("released:" + e.getKeyCode());
    }
}
