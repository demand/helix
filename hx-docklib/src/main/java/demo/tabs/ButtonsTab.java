package demo.tabs;

import com.zwing.utils.ResourceManager;
import com.zwing.icon.ZTextIcon;
import com.zwing.icon.ZJointIcon;
import com.zwing.ZDropDownButton;
import com.zwing.ColorIcon;
import com.zwing.ZColorComboBox;
import com.zwing.colorchooser.ColorSelectedAdapter;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import demo.SlidersPopup;
import demo.tabs.buttons.ScaleButton;
import demo.tabs.buttons.ValuePopup;
import demo.tabs.buttons.ResizingPopup;

/**
 * ButtonsTab
 * Created: 12.11.2009, 15:59:59
 *
 * @author A.Demin (demin@genego.com)
 */
public class ButtonsTab extends JPanel {
    public ButtonsTab() {
        init();
    }

    private void init() {

        JPanel pn = new JPanel();
        JToolBar tb = new JToolBar();
        tb.setRollover(true);

        JTextField jtxf = new JTextField("Abcd");
        jtxf.setPreferredSize(new Dimension(100, jtxf.getPreferredSize().height));
        pn.add(jtxf);

        JButton btn = new JButton(new AbstractAction(
                "a", ResourceManager.getIcon("actions.open.icon")) {
            public void actionPerformed(ActionEvent e) {
                JPopupMenu menu = new JPopupMenu("Menu");

                menu.setLayout(new BoxLayout(menu, BoxLayout.X_AXIS));
                menu.add(new JButton("Button"));

//                menu.add(new JMenuItem("Item"));

                JComponent sourceBtn = (JComponent) e.getSource();
                Dimension btnSz = sourceBtn.getSize();
                menu.show(sourceBtn, 0, btnSz.height);
            }
        });
        btn.setBorder(BorderFactory.createEmptyBorder());
        pn.add(btn);

        JButton btn2 = new JButton(new AbstractAction(
                "Btn2", ResourceManager.getIcon("actions.open.icon")) {
            public void actionPerformed(ActionEvent e) {
                final ResizingPopup popup = new ResizingPopup((Component) e.getSource());
                Dimension btnSz = ((JComponent) e.getSource()).getSize();
                popup.show(0, btnSz.height);
            }
        });
        pn.add(btn2);

        Icon ic1 = ResourceManager.getIcon("sort.az.icon");
        Icon ic2 = new ZTextIcon("42", -2, true);
        Icon ji = new ZJointIcon(ic1, ic2, ZJointIcon.JointType.EAST, 2);
        JButton btn3 = new JButton("Btn3", ji);
        pn.add(btn3);

        Icon ic3 = ResourceManager.getIcon("sort.za.icon");
        Icon ic4 = new ZTextIcon("24", -2);
        Icon ji2 = new ZJointIcon(ic3, ic4, ZJointIcon.JointType.EAST, 2);
        JButton btn4 = new JButton("Btn3", ji2);
        pn.add(btn4);

        final ZDropDownButton ddb3 = new ZDropDownButton(null,
                new ColorIcon(new Dimension(12,12),null));
        ColorSelectedAdapter csa = new ColorSelectedAdapter() {
            public Color colorSelected(Color newColor, Object source) {
                ddb3.setIcon(new ColorIcon(new Dimension(12,12),newColor));
                return null;
            }
        };
        SlidersPopup sp = new SlidersPopup(csa);
        ddb3.setIcon(new ColorIcon(new Dimension(12,12),sp.getColor()));
        ddb3.setPopupComponent(sp);
        pn.add(ddb3);

        final ZDropDownButton ddb4 = new ZDropDownButton("Text",
                new ColorIcon(new Dimension(12,12),null));
        ColorSelectedAdapter csa2 = new ColorSelectedAdapter() {
            public Color colorSelected(Color newColor, Object source) {
                ddb4.setIcon(new ColorIcon(new Dimension(12,12),newColor));
                return null;
            }
        };
        SlidersPopup sp2 = new SlidersPopup(csa2);
        ddb4.setIcon(new ColorIcon(new Dimension(12,12),sp.getColor()));
        ddb4.setPopupComponent(sp2);
        ddb4.setPreferredSize(new Dimension(120, 22));
        pn.add(ddb4);

        final ZDropDownButton ddb5 = new ZDropDownButton("jhgjh");
        ddb5.setContentAreaFilled(false);
        ddb5.setRolloverEnabled(true);
        ddb5.setPopupComponent(new ValuePopup());
        pn.add(ddb5);

        final ZColorComboBox ccb = new ZColorComboBox();
        pn.add(ccb);

        final JComboBox cb = new JComboBox(new String[] {
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 7", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6",
                "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"
        });
        cb.setEditable(true);
        pn.add(cb);


        final ScaleButton sb = new ScaleButton();
        tb.add(sb);

        final JTextField txf = new JTextField(10);
        sb.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                txf.setText("" + sb.getScale() + "%");
            }
        });

        tb.add(sb);
        tb.add(txf);

        JPanel rowsPanel = new JPanel();
        rowsPanel.setLayout(new BoxLayout(rowsPanel, BoxLayout.Y_AXIS));
        rowsPanel.add(pn);
        rowsPanel.add(tb);

        add(rowsPanel);
    }
}
