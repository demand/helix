package demo.tabs;

import demo.scheme.SchemeCanvas;
import demo.scheme.SchemeModel;

import javax.swing.*;
import java.awt.*;

/**
 * Created: 21.03.2007 12:36:19
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class SchemeTab extends JPanel {
    private JScrollPane sp;
    private SchemeCanvas bc;
    private SchemeModel model;

    public SchemeTab() {
        setLayout(new BorderLayout());
        bc = new SchemeCanvas();
        sp = new JScrollPane(bc);
        add(sp, BorderLayout.CENTER);
    }
}
