package demo.tabs;

import com.zwing.docking.BoatEmpty;
import com.zwing.docking.DockingConst;
import com.zwing.docking.DockManager;

import demo.boats.*;

import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * Created: 11.08.2006 19:33:17
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class DockTab extends JPanel {

    DockManager dm;

    public DockTab() {
        setLayout(new BorderLayout());

        dm = new DockManager();
        add(dm.getVisualComponent(), BorderLayout.CENTER);
        registerBoats();
    }

    protected void registerBoats() {
        BoatEmpty boat;

        BoatEmpty project = new BoatProjects();
        dm.getCargoManager().registerCargo(project.getCargo());
        dm.attach(project, DockingConst.DK_WEST);

        boat = new BoatFiles();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_WEST);

        BoatEmpty center = null;
        center = new BoatEditor();
        dm.getCargoManager().registerCargo(center.getCargo());
        dm.attach(center, DockingConst.DK_CENTER);

        boat = new BoatAnnotations();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_EAST);

        boat = new BoatDictionaries();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_EAST);

        boat = new BoatAttributes();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_NORTH);

        boat = new BoatMessages();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_SOUTH);

        boat = new BoatConsole();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_SOUTH);

        boat = new BoatFind();
        dm.getCargoManager().registerCargo(boat.getCargo());
        dm.attach(boat, DockingConst.DK_SOUTH);

        if (center != null) {
            center.setVisible(false);
            dm.requestVisible(center);
        }
        dm.requestVisible(project);
    }
}
