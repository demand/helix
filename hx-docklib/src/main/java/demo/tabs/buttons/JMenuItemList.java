package demo.tabs.buttons;

import javax.swing.*;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * Created: 31.10.2006 17:16:14
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class JMenuItemList extends JMenu {

    private JPopupMenu popupMenu;
    JList list;

    public JMenuItemList(String name) {
        super(name);
        createPopup();
    }

    private void createPopup() {
        popupMenu = new JPopupMenu();
        popupMenu.setLayout(new BorderLayout());

        ListModel bigData = new AbstractListModel() {
            public int getSize() { return 200; }
            public Object getElementAt(int index) { return "Index JHGJhgj hgjhg jhgj " + index; }
        };
        list = new JList(bigData);
        JScrollPane jscrProps = new JScrollPane(list) {
            public Dimension getPreferredSize() {
                return new Dimension(200, 80);
            }
        };
        list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                MenuSelectionManager msm = MenuSelectionManager.defaultManager();
                msm.clearSelectedPath();
            }
        });

        list.addMouseMotionListener(new MouseMotionListener() {
            public void mouseDragged(MouseEvent e) {
            }

            public void mouseMoved(MouseEvent e) {
                if (e.getSource() == list) {
                    Point location = e.getPoint();
                    Rectangle r = new Rectangle();
                    list.computeVisibleRect( r );
                    if ( r.contains( location ) ) {
                        updateListBoxSelectionForEvent(e, false);
                    }
                }
            }
        });

        popupMenu.add(jscrProps, BorderLayout.CENTER);
        popupMenu.setInvoker(this);
        popupListener = createWinListener(popupMenu);
    }

    /**
     * A utility method used by the event listeners.  Given a mouse event, it changes
     * the list selection to the list item below the mouse.
     */
    protected void updateListBoxSelectionForEvent(MouseEvent anEvent,boolean shouldScroll) {
    // XXX - only seems to be called from this class. shouldScroll flag is
    // never true
        Point location = anEvent.getPoint();
        if ( list == null )
            return;
        int index = list.locationToIndex(location);
        if ( index == -1 ) {
//            if ( location.y < 0 )
//                index = 0;
//            else
//                index = comboBox.getModel().getSize() - 1;
        }
        if ( list.getSelectedIndex() != index ) {
            list.setSelectedIndex(index);
            if ( shouldScroll )
                list.ensureIndexIsVisible(index);
        }
    }

    public void setPopupMenu(JPopupMenu popupMenu) {
        this.popupMenu = popupMenu;
    }

    public JPopupMenu getPopupMenu() {
        return popupMenu;
    }

    public void menuSelectionChanged(boolean isIncluded) {
        super.menuSelectionChanged(isIncluded);
        if (!isIncluded) {
            popupMenu.setVisible(false);
        }
    }
}
