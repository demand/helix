package demo.tabs.buttons;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * ResizingPopup
 * Created: 12.11.2009, 17:56:08
 *
 * @author A.Demin (demin@genego.com)
 */
public class ResizingPopup extends JPopupMenu {

    Component owner;
    Contents contents;

    public ResizingPopup(Component owner) {
        this.owner = owner;
        contents = new Contents();

        setLayout( new BoxLayout(this, BoxLayout.Y_AXIS) );
        setBorderPainted( true );
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        setOpaque( true );
        add(contents);
        setDoubleBuffered( true );
        setFocusable( true );
    }

    public void show(int x, int y) {
        show(owner, x, y);
    }

    public void addNotify() {
        super.addNotify();
        requestFocus();
    }

    public void hidePopup() {
        MenuSelectionManager manager = MenuSelectionManager.defaultManager();
        MenuElement [] selection = manager.getSelectedPath();
        for ( int i = 0 ; i < selection.length ; i++ ) {
            if ( selection[i] == this ) {
                manager.clearSelectedPath();
                break;
            }
        }
        setVisible(false);
    }

    class Contents extends JPanel {

        private JButton btn1;
        private JButton btn2;
        private JButton btn3;

        Contents() {
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

            add(btn1 = new JButton(new AbstractAction("1") {
                public void actionPerformed(ActionEvent e) {
//                    btn1.setText("8768768");
//                    contents.setPreferredSize(new Dimension(200, 300));
                    contents.revalidate();

//                    showComponents((Component) e.getSource());

                    JPopupMenu menu = (JPopupMenu) findComponent(
                            Contents.this, JPopupMenu.class);
                    Rectangle bounds = menu.getBounds();
                    System.out.println(".....bounds:" + bounds);
                    bounds.x += 2;
                    bounds.width += 2;
                    bounds.height += 2;

                    contents.setPreferredSize(new Dimension(100,60));

                    menu.setBounds(bounds);
                    bounds = menu.getBounds();
                    System.out.println(".....bounds 2:" + bounds);
                    menu.validate();
                    menu.repaint();
                    menu.pack();
                    menu.setLocation(1,1);
                }
            }));

            add(btn2 = new JButton(new AbstractAction("2") {
                public void actionPerformed(ActionEvent e) {
                    btn1.setText("1");
                    contents.setPreferredSize(null);
                    contents.revalidate();

                    Window window = (Window) findComponent(Contents.this, Window.class);
                    window.pack();

                }
            }));

            add(btn3 = new JButton(new AbstractAction("Hide") {
                public void actionPerformed(ActionEvent e) {
                    hidePopup();
                }
            }));
        }
    }

    static Component findComponent(Component component, Class clazz) {
        Component rslt = null;
        Component cur = component;
        while (cur != null) {
            if (clazz.isInstance(cur)) {
                rslt = cur;
                break;
            }
            cur = cur.getParent();
        }
        return rslt;
    }

    static void showComponents(Component component) {
        Component rslt = null;
        Component cur = component;
        while (cur != null) {
            System.out.println(">>" + cur);
            cur = cur.getParent();
        }
    }

}
