package demo.tabs.buttons;

import com.zwing.icon.ZJointIcon;
import com.zwing.icon.ZTextIcon;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.Hashtable;

/**
 * ScaleButton
 * Created: 11.11.2011, 16:42:29
 *
 * @author A.Demin (andrey.demin@thomsonreuters.com)
 */
public class ScaleButton extends JButton implements PopupMenuListener {

    private static final int DEFAULT_SCALE = 100;
    private static final int DEFAULT_MAX_SCALE = 500;
    private static final int DEFAULT_MIN_SCALE = 10;

    private ScaleSlider scaleSlider;
    private ScalePopup scalePopup;

    private Handler handler;

    private Icon basicIcon;
    private ZTextIcon textIcon;
    private Icon dropdownIcon;

    private int fontModifier;

    private static Object hidePopupComboValue;

    public ScaleButton() {
        fontModifier = -2;

        URL resource = ScaleButton.class.getResource("zoom16.png");
        basicIcon = resource != null ? new ImageIcon(resource) : null;

        resource = ScaleButton.class.getResource("ddarrow.gif");
        dropdownIcon = resource != null ? new ImageIcon(resource) : null;

        textIcon = new ZTextIcon("XXX%", fontModifier, true, SwingConstants.CENTER);
        // fix text icon size
        textIcon.setSize(new Dimension(
                textIcon.getIconWidth(), textIcon.getIconHeight()));

        handler = new Handler();

        putClientProperty(NOT_CANCEL_POPUP, getHidePopupComboValue());
        
        scaleSlider = new ScaleSlider();
        scalePopup = new ScalePopup(this, scaleSlider);
        scalePopup.addPopupMenuListener(this);

        scaleSlider.addChangeListener(handler);
        scaleSlider.addMouseListener(handler);
        scaleSlider.addMouseWheelListener(handler);


        addActionListener(handler);
        addMouseListener(handler);
        addMouseWheelListener(handler);

        setScale(DEFAULT_SCALE);
        changeIcon();
    }

    private void scaleChanged() {
        changeIcon();
        fireItemStateChanged(new ItemEvent(
                this, ItemEvent.ITEM_STATE_CHANGED,
                getScale(), ItemEvent.SELECTED));
    }

    public int getScale() {
        return scaleSlider.getPercentValue();
    }

    public void setScale(int scale) {
        int oldValue = scaleSlider.getValue();
        int value = scaleSlider.getValueForPercent(scale);
        if (oldValue != value) {
            scaleSlider.setValue(value);
        }
    }

    protected void fireItemStateChanged(ItemEvent e) {
        Object[] listeners = listenerList.getListenerList();
        for ( int i = listeners.length-2; i>=0; i-=2 ) {
            if ( listeners[i]==ItemListener.class ) {
                ((ItemListener)listeners[i+1]).itemStateChanged(e);
            }
        }
    }

    private void changeIcon() {
        textIcon.setText(Integer.toString(getScale()) + "%");
        setIcon(new ZJointIcon(new ZJointIcon(
                basicIcon, textIcon), dropdownIcon, 2));
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    private static final String NOT_CANCEL_POPUP = "doNotCancelPopup";

    /**
     * hack - извлекаю значение специального ключа, чтобы при
     * нажатии на кнопку не закрывался Popup 
     * @return значение ключа BasicComboBoxUI.HIDE_POPUP_KEY
     */
    public Object getHidePopupComboValue() {
        if (hidePopupComboValue == null) {
            JComboBox c = new JComboBox();
            hidePopupComboValue = c.getClientProperty(NOT_CANCEL_POPUP);
            c = null;
        }
        return hidePopupComboValue;
    }

    private class Handler implements ActionListener, MouseListener,
            MouseWheelListener, ChangeListener, PopupMenuListener {

        public void mouseClicked(MouseEvent e) {
            // nothing to implement
        }

        public void mousePressed(MouseEvent e) {
            if (e.getSource() == ScaleButton.this) {
                Dimension btnSz = ((JComponent) e.getSource()).getSize();
                System.out.println("pressed");
                if (scalePopup.isVisible()) {
                    System.out.println("visible->hide");
                    scalePopup.hidePopup();
                } else {
                    System.out.println("hidden->show");
                    scalePopup.show(0, btnSz.height);
                }    
            }    
        }

        public void mouseReleased(MouseEvent e) {
            // slider event
            if (e.getSource() == scaleSlider) {
                scalePopup.hidePopup();
            }
        }

        public void mouseEntered(MouseEvent e) {
            // nothing to implement
        }

        public void mouseExited(MouseEvent e) {
            // nothing to implement
        }

        public void actionPerformed(ActionEvent e) {
            // button event
        }

        public void mouseWheelMoved(MouseWheelEvent e) {
            doShift(e.getWheelRotation());
        }

        public void stateChanged(ChangeEvent e) {
            // slider event
            scaleChanged();
        }

        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

        }

        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

        }

        public void popupMenuCanceled(PopupMenuEvent e) {

        }
    }

    private void doShift(int wheelRotation) {
        scaleSlider.setValue(Math.max(scaleSlider.getMinimum(),
                Math.min(scaleSlider.getMaximum(),
                scaleSlider.getValue() + wheelRotation)));
    }


    private static int DEFAULT_SCALE_VALUE = 100;
    private static final int[] DEFAULT_SCALE_ROW = new int[] {
            10, 25, 50, 75, 100, 125, 150, 200, 300};

    private class ScaleSlider extends JSlider {

        private int[] scaleRow;
        private int defaultScale = DEFAULT_SCALE_VALUE;

        private ScaleSlider() {
            super(JSlider.HORIZONTAL);

            setMajorTickSpacing(1);
            setMinorTickSpacing(1);
            setPaintTicks(true);
            setSnapToTicks(true);
            setPaintLabels(true);
            setFocusable(false);

            setValues(DEFAULT_SCALE_ROW);
        }

        public Dimension getPreferredSize() {
            Dimension sz = super.getPreferredSize();
            sz.width += (scaleRow.length * 8);
            return sz;
        }

        private void setValues(int[] values) {
            Hashtable<Integer, Component> ht = new Hashtable<Integer, Component>();
            UIDefaults ud = UIManager.getLookAndFeel().getDefaults();
            Font f = ud.getFont("Menu.font");
            Font normalFont = f.deriveFont(((float) f.getSize() + fontModifier));
            Font boldFont = normalFont.deriveFont(Font.BOLD);
            this.scaleRow = values;


            int defaultIndex = 0;
            for (int i = 0; i < scaleRow.length; i++) {
                int v = scaleRow[i];
                Component c = new JLabel(Integer.toString(v), JLabel.CENTER);
                Font cf = normalFont;
                if (v == defaultScale) {
                    defaultIndex = i;
                    cf = boldFont;
                }
                c.setFont(cf);
                ht.put(i, c);
            }
            setModel(new DefaultBoundedRangeModel(defaultIndex, 0, 0, scaleRow.length - 1));
            setLabelTable(ht);
        }

        private int getValueForPercent(int percent) {
            int value = 0;
            for (int i = 1; i < scaleRow.length; i++) {
                if (scaleRow[i] > percent) {
                    break;
                } else {
                    value = i;
                }
            }
            return value;
        }

        private void setPercentValue(int percent) {
        }

        private int getPercentValue() {
            return scaleRow[getValue()];
        }
    }

    public class ScalePopup extends JPopupMenu {

        Component owner;

        public ScalePopup(Component owner, JComponent contents) {
            this.owner = owner;
            setLayout(new BorderLayout());
            setBorderPainted(true);
            setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
            setOpaque( false ); // true
            add(contents);
            setDoubleBuffered( true );
            setFocusable( false );

//            final Toolkit tk = Toolkit.getDefaultToolkit();
//            java.security.AccessController.doPrivileged(
//                new java.security.PrivilegedAction() {
//                    public Object run() {
//                        tk.addAWTEventListener(new AWTEventListener() {
//                            public void eventDispatched(AWTEvent event) {
//                                System.out.println("event: " + event.getID());
//                            }
//                        },
//                                AWTEvent.MOUSE_EVENT_MASK |
//                                AWTEvent.MOUSE_MOTION_EVENT_MASK |
//                                AWTEvent.MOUSE_WHEEL_EVENT_MASK |
//                                AWTEvent.WINDOW_EVENT_MASK | sun.awt.SunToolkit.GRAB_EVENT_MASK);
//                        return null;
//                    }
//                }
//            );

        }

        public boolean isVisible() {
            return super.isVisible();
        }

        public void show(int x, int y) {
            show(owner, x, y);
        }

        public void addNotify() {
            super.addNotify();
            requestFocus();
        }

        public void hidePopup() {
            MenuSelectionManager manager = MenuSelectionManager.defaultManager();
            MenuElement [] selection = manager.getSelectedPath();
            for ( int i = 0 ; i < selection.length ; i++ ) {
                if ( selection[i] == this ) {
                    manager.clearSelectedPath();
                    break;
                }
            }
            setVisible(false);
        }
    }
}
