package demo.tabs.buttons;

import com.zwing.ZPopupMenu;

import javax.swing.*;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import demo.tabs.buttons.JMenuItemList;

/**
 * Created: 24.10.2006 15:58:37
 *
 * @author Andrey Demin (ADemin@meshnetics.com)
 */
public class ValuePopup extends ZPopupMenu {

    public ValuePopup() {
        JMenuItem jmniNumber = new JMenuItem("Number");
        JMenu jmniBoolean = new JMenu("Boolean");
        JMenuItem jmniTrue = new JMenuItem("True");
        JMenuItem jmniFalse = new JMenuItem("False");
        jmniBoolean.add(jmniTrue);
        jmniBoolean.add(jmniFalse);
        JMenu jmniParam = new JMenuItemList("Parameters");

//        ListModel bigData = new AbstractListModel() {
//            public int getSize() { return 50; }
//            public Object getElementAt(int index) { return "Index " + index; }
//        };
//        JList jlstProps = new JList(bigData);
//        JScrollPane jscrProps = new JScrollPane(jlstProps) {
//            public Dimension getPreferredSize() {
//                return new Dimension(200, 80);
//            }
//        };
        setLayout(new GridBagLayout());
        add(jmniNumber, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0),
                0, 0));
        add(jmniBoolean, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0),
                0, 0));
        add(jmniParam, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0,0,0,0),
                0, 0));
//        add(jscrProps, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0,
//                GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0,0,0,0),
//                0, 0));

    }
}
