package com.helix.app

import java.util.{Comparator, Collections}
import javax.swing.{JFrame, UIManager, SwingUtilities}

import _root_.demo.MainFrame
import _root_.demo.laf.LookAndFeelManager
import com.helix.gui.HxMainFrame
import com.zwing.utils.ZUIManager

/**
  * Created by ademin on 21.12.2015.
  */
object HxHandsApp {
  def main(args: Array[String]) {
    SwingUtilities.invokeLater(new Runnable() {
      def run() {
        com.helix.gui.LookAndFeelManager.initialize()
        ZUIManager.initializeInstance()

        MainFrame.showFrame()

//        val mainFrame = new HxMainFrame
//
//        mainFrame.setBounds(100, 100, 600, 500)
//        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
//        mainFrame.setVisible(true)
      }
    })
  }
}
