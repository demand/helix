package com.helix.gui.boats

import javax.swing.{JComponent, JList, JScrollPane}

import com.zwing.{DockingCargo, CargoListener}

/**
  * Created by ademin on 22.12.2015.
  */
class CargoNavigator extends JScrollPane with DockingCargo {

  val prj = new JList[String]
  getViewport.add(prj, null)
  val vv = Array("abc", "def")
  prj.setListData(vv)

  def getVisualComponent: JComponent = this

  def getUniqueName: String = "Navigator"

  def getProperty(propName: String): AnyRef = null

  def addCargoListener(cargoListener: CargoListener): Unit = {}

  def removeCargoListener(cargoListener: CargoListener): Unit = {}
}
