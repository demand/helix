package com.helix.gui.boats

import java.awt.Insets

import com.zwing.docking.BoatEmpty
import com.zwing.utils.ResourceManager

/**
  * Created by ademin on 22.12.2015.
  */
object BasicBoats {
  class BoatDocs extends BoatEmpty(BoatEmpty.BST_CENTER, BoatEmpty.BST_CENTER, "Center") {
    setResizable(BoatEmpty.BST_RESIZABLE_NONE)
    setMovable(false)
    insets = new Insets(0, 0, 0, 0)

    setCargo(new CargoDocs)
  }

  class BoatNavigator extends BoatEmpty(
    BoatEmpty.BST_DOCK | BoatEmpty.BST_SLIDE | BoatEmpty.BST_FLOAT,
    BoatEmpty.BST_DOCK, ResourceManager.getString("boat.project.label")) {

    setIcon(ResourceManager.getImageIcon("boat.project.icon"))
    setCargo(new CargoNavigator)
  }
}
