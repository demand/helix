package com.helix.gui.boats

import javax.swing.{JPanel, JComponent, JTabbedPane}

import com.zwing.{CargoListener, DockingCargo}
import demo.tabs.{ButtonsTab, HtmlEditorTab}

/**
  * Created by ademin on 22.12.2015.
  */
class CargoDocs extends JTabbedPane with DockingCargo {

//  addTab("Tab 1", new ButtonsTab())
  addTab("Tab 1", new JPanel)
  addTab("Html Editor", new HtmlEditorTab())

  def getVisualComponent: JComponent = this

  def getUniqueName: String = "Documents"

  def getProperty(propName: String): AnyRef = null

  def addCargoListener(cargoListener: CargoListener): Unit = {}

  def removeCargoListener(cargoListener: CargoListener): Unit = {}
}
