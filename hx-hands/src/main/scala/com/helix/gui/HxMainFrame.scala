package com.helix.gui

import java.awt.{BorderLayout, FlowLayout}
import javax.swing._

/**
  * Created by ademin on 21.12.2015.
  */
class HxMainFrame extends JFrame {

  val dcp = new DockComponentProvider

  def init(): Unit = {
    val cp = getContentPane

    cp.setLayout(new BorderLayout)
    cp.add(dcp.component, BorderLayout.CENTER)
    dcp.build()

//    cp.setLayout(new FlowLayout)
//    cp.add(new JButton("Ok"))
//    cp.add(new JComboBox[String](Array("1", "abcd")))
//    cp.add(new JCheckBox("Option 1"))
//    cp.add(new JCheckBox("Option 2"))
//    cp.add(new JRadioButton("radio"))

  }

  init()

}
