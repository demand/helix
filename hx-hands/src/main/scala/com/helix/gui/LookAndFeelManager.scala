package com.helix.gui

import java.awt.Toolkit
import javax.swing.{ToolTipManager, JPopupMenu}

import com.alee.laf.WebLookAndFeel

object LookAndFeelManager {
  def initialize() {
    try {
        WebLookAndFeel.install()

        Toolkit.getDefaultToolkit.setDynamicLayout(false)
        JPopupMenu.setDefaultLightWeightPopupEnabled(true)
        ToolTipManager.sharedInstance().setLightWeightPopupEnabled(true);
    } catch {
        case e: Exception => e.printStackTrace()
    }
  }
}
