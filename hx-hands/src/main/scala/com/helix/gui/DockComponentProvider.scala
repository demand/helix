package com.helix.gui

import javax.swing.JComponent

import com.helix.gui.boats.BasicBoats.{BoatNavigator, BoatDocs}
import com.zwing.docking.{DockingConst, DockManager}

/**
  * Created by ademin on 22.12.2015.
  */
object DockComponentProvider {

}

class DockComponentProvider extends {
  val dockManager: DockManager = new DockManager

  def build(): Unit = {
    val navi = new BoatNavigator
    dockManager.getCargoManager.registerCargo(navi.getCargo)
    dockManager.attach(navi, DockingConst.DK_WEST)

    val center = new BoatDocs
    dockManager.getCargoManager.registerCargo(center.getCargo)
    dockManager.attach(center, DockingConst.DK_CENTER)
  }

  def component: JComponent = {
//    build()
    dockManager.getVisualComponent
  }
}
