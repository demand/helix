package com.helix.panels

import javax.swing._


class SamplePanel extends JPanel {
  setLayout(new BoxLayout(this, BoxLayout.X_AXIS))
  add(new JLabel("Test:"))
  add(Box.createHorizontalStrut(10))
  add(new JTextField(15))
}
