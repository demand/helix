package com.helix.actions

import java.awt.Frame
import java.awt.event.ActionEvent
import javax.swing.{JDialog, AbstractAction}

import com.helix.panels.SamplePanel
import com.helix.tops.ExampleTopComponent
import org.openide.util.{LookupEvent, LookupListener}
import org.openide.windows.WindowManager

class OpenExperimentAction extends AbstractAction("Привет") with LookupListener {

  val x = System.currentTimeMillis()

  override def resultChanged(lookupEvent: LookupEvent): Unit = {

  }

  override def actionPerformed(e: ActionEvent): Unit = {

    val tc = WindowManager.getDefault.findTopComponent("ExampleTopComponent")
    tc.openAtTabPosition(0)
    tc.requestActive()
//    val d: JDialog = new JDialog(null.asInstanceOf[Frame], "xxx", true)
//    d.setContentPane(new SamplePanel)
//    d.pack()
//    d.setLocation(100, 100)
//    d.setVisible(true)
  }
}
