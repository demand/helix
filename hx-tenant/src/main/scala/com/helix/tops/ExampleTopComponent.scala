package com.helix.tops

import java.awt.{Color, BorderLayout}
import javax.swing.JPanel

import org.openide.windows.{WindowManager, TopComponent}
import org.openide.windows.TopComponent
import org.openide.windows.TopComponent.Description
import org.openide.util.lookup.Lookups

/**
  * Created by ademin on 22.12.2015.
  */

object ExampleTopComponent {
  final val PreferredId = "ExampleTopComponent"
  lazy val default = new ExampleTopComponent
}

@Description(
  preferredID = ExampleTopComponent.PreferredId,
  persistenceType = TopComponent.PERSISTENCE_ALWAYS)
class ExampleTopComponent extends TopComponent {

  override def getName = "Editor"

  val content = new JPanel
  setLayout(new BorderLayout)
  content.setBackground(Color.WHITE)
  add(content, BorderLayout.CENTER)

  override def open() {
    val mode = WindowManager.getDefault.findMode("editor")
    if (mode != null) {
      mode.dockInto(this)
      super.open()
    }
  }
}
