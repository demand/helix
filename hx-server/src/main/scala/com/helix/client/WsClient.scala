package com.helix.client

import java.net.URI
import java.nio.ByteBuffer

import akka.actor.{ActorRef, Props, ActorSystem}
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import MessagesClientApp._


class WsClient(actorSystem: ActorSystem, address: String, actor: ActorRef)
  extends WebSocketClient(new URI(address)) {

  override def onOpen(handshake: ServerHandshake): Unit =
    actor ! MConnected(this)

  override def onError(e: Exception): Unit = {
    println("error")
    actor ! MError(e)
  }

  override def onMessage(message: String): Unit =
    actor ! MReceivedString(message)

  override def onMessage(buf: ByteBuffer): Unit =
    actor ! MReceivedBytes(buf)

  override def onClose(code: Int, reason: String, remote: Boolean): Unit = {
    println("closed")
    actor ! MDisconnected(None)
  }
}
