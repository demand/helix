package com.helix.client

import java.nio.ByteBuffer

import akka.actor.{ActorSystem, ActorRef, Actor}
import com.dyuproject.protostuff.{LinkedBuffer, ProtostuffIOUtil}
import com.helix.messages.HelloWorld
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

sealed trait ClientMessage
case class MConnected(c: WsClient) extends ClientMessage
case object MDoDisconnect extends ClientMessage
case class MError(x: Throwable) extends ClientMessage
case class MDoReconnect(c: Int) extends ClientMessage
case class MSendString(s: String) extends ClientMessage
case class MSendBytes(b: ByteBuffer) extends ClientMessage
case class MDisconnected(cause: Option[Throwable]) extends ClientMessage
case class MReceivedString(s: String) extends ClientMessage
case class MReceivedBytes(b: ByteBuffer) extends ClientMessage

class MessageHandler extends Actor {
  import MessagesClientApp._

  val reconnectStartCounter = 6
  var client: Option[WsClient] = None
  var working: Boolean = true

  var cn = 0

  Console.out.println("MainHandler created")

  def receive = {
    case MConnected(c) =>
      println("Connectedddddd")
      client = Some(c)
      println(client)

    case MDoDisconnect =>
      working = false
      client.foreach(_.closeBlocking())

    case MDoReconnect(c) =>
      working = true
      c match {
        case 0 =>
          updateStatus(s"Reconnecting")
          new WsClient(app.actorSystem, "ws://78.46.213.161:6081", self).connect()

        case x if x > 0 =>
          println(s"counter: $cn $x")
          cn += 1
          updateStatus(s"Will be reconnected in $x sec.")
          context.system.scheduler.scheduleOnce(1 seconds, self, MDoReconnect(x-1))
      }

    case MSendString(s) =>
      val sch = HelloWorld.getSchema
      val buffer = LinkedBuffer.allocate(1000)
      val hw1 = new HelloWorld()
        .setMessage(s)
        .setReverse("")
      val data = ProtostuffIOUtil.toByteArray(hw1, sch, buffer)
      buffer.clear()
      client.foreach(_.send(data))

    case MReceivedString(s) =>
      addText(s"RECV: $s")

    case MReceivedBytes(b) =>
      val sch = HelloWorld.getSchema
      val hw0 = new HelloWorld
      ProtostuffIOUtil.mergeFrom(b.array(), hw0, sch)
      val msg = hw0.getMessage
      addText(s"MSG: ${hw0.getMessage} (${hw0.getReverse})")

    case MError(x) =>

    case MDisconnected(cause) =>
      println("Disconnectedddddd " + cause)
      client = None
      if (working)
        context.system.scheduler.scheduleOnce(1 seconds, self, MDoReconnect(reconnectStartCounter))
  }
}
