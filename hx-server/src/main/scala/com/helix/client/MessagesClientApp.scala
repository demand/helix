package com.helix.client

import java.awt.BorderLayout
import java.awt.event.{WindowEvent, WindowAdapter, ActionEvent}
import java.net.URI
import javax.swing._

import akka.actor.{Props, ActorSystem}
import Messages._
import org.java_websocket.client.WebSocketClient
import org.java_websocket.drafts.{Draft_75, Draft}
import org.java_websocket.handshake.ServerHandshake


object MessagesClientApp {
  val app = new MessagesClientApp

  def updateStatus(s: String): Unit = app.txtStatus.setText(s)
  def addText(s: String): Unit = {
    val tx = MessagesClientApp.app.txtReceived
    tx.setText(tx.getText + s + "\n")
  }

  def main(args: Array[String]): Unit = app.go()
}

class MessagesClientApp {
  val actorSystem = ActorSystem("ClientActorSystem")

  val uiActor = actorSystem.actorOf(Props[ClientUIHandler], name = "uiActor")
  val msgActor = actorSystem.actorOf(Props[MessageHandler], name="msgActor")

  val frame = new JFrame
  val txtReceived = new JTextArea(40, 60)
  val txtSend = new JTextField(60)
  val txtStatus = new JTextField

  val actionSend = new AbstractAction("send") {
    override def actionPerformed(e: ActionEvent): Unit = {
      msgActor ! MSendString(txtSend.getText)
      txtSend.setText("")
    }
  }

  def go(): Unit = {
    val mainPanel = new JPanel(new BorderLayout(4, 4))
    val sendPanel = new JPanel(new BorderLayout(4, 0))
    sendPanel.add(txtSend, BorderLayout.CENTER)
    sendPanel.add(new JButton(actionSend), BorderLayout.EAST)
    val jscr = new JScrollPane(txtReceived,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
      ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER)
    mainPanel.add(jscr, BorderLayout.CENTER)
    mainPanel.add(sendPanel, BorderLayout.NORTH)
    mainPanel.add(txtStatus, BorderLayout.SOUTH)

    frame.setTitle("WS Client")
    frame.setContentPane(mainPanel)
    frame.setBounds(100, 100, 700, 600)
    frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE)
    frame.setVisible(true)

    frame.addWindowListener(new WindowAdapter {
      override def windowClosing(e: WindowEvent): Unit = {
        actorSystem.registerOnTermination {
          frame.setVisible(false)
          frame.dispose()
          System.exit(0)
        }
        actorSystem.shutdown()
      }
    })

    uiActor ! UICounter(0)
    msgActor ! MDoReconnect(0)
  }
}
