package com.helix.client

import akka.actor.{ActorSystem, ActorRef, Actor}
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

sealed trait Messages
object Messages {
  case class UICounter(v: Int) extends Messages
}

class ClientUIHandler extends Actor {
  import Messages._
  import MessagesClientApp._

  def receive = {
    case UICounter(v) =>
      updateStatus(s"Counter: $v")
      context.system.scheduler.scheduleOnce(1 seconds, self, UICounter(v + 1))
  }
}
