package com.helix.server

import java.io.File

import com.typesafe.config.{ConfigException, ConfigFactory}

import scala.util.Try

sealed trait HxConfig {
  def msg: Option[String]
}

object HxConfig {
  private val defaultWebfacePort = 6080
  private val defaultMessagePort = 6081
  private val defaultCommandShell = true

  val configFile = new File(System.getProperty("user.dir") + "/conf/hx-app.conf")

  case class HxConfigCorrupted(private val m: String) extends HxConfig {
    override def msg: Option[String] = Some(m)
  }
  case class HxConfigResolved(
    webfacePort: Int = defaultWebfacePort,
    messagesPort: Int = defaultMessagePort,
    commandShell: Boolean = true,
    msg: Option[String] = None) extends HxConfig

  val config: HxConfig = {
    if (configFile.exists()) {
      Try {
        val config = ConfigFactory.parseFile(configFile)
        def getValOrMessage[T](getter: String => T)(path: String, d: T): Either[T, String] = {
          Try {
            Left(getter(path))
          } recover {
            case _: ConfigException.WrongType => Right(path)
            case _: ConfigException.Missing => Left(d)
          } get
        }
        val getIntOrMessage = getValOrMessage { config.getInt } _
        val getBooleanOrMessage = getValOrMessage { config.getBoolean } _

        val webfacePort = getIntOrMessage("server.webface-port", defaultWebfacePort)
        val messagePort = getIntOrMessage("server.messages-port", defaultMessagePort)
        val commandShell = getBooleanOrMessage("server.command-shell", defaultCommandShell)

        if (webfacePort.isLeft && messagePort.isLeft && commandShell.isLeft)
          HxConfigResolved(webfacePort.left.get, messagePort.left.get, commandShell.left.get)
        else {
          val corrupted = Seq(webfacePort, messagePort, commandShell).flatMap(_.right.toOption).mkString(", ")
          HxConfigCorrupted(s"Wrong values found: $corrupted.")
        }
      } recover {
        case _: ConfigException.WrongType |
             _: ConfigException.Parse =>
        HxConfigCorrupted(s"Config at '${configFile.getPath}' has wrong format.")
      } get
    } else
      HxConfigResolved(msg = Some(
        s"Config at '${configFile.getPath}' is absent. Default settings are used."))
  }
}
