package com.helix.server

import akka.actor.{Props, ActorSystem}
import com.helix.server.HxConfig._


class HxServerApp(val config: HxConfigResolved) {

  import HxServerApp._

  val actorSystem = ActorSystem("HxServerActorSystem")
  val mainHandler = actorSystem.actorOf(Props[MainHandler])

  val websocket = new MessagesServer("Websocket", config.messagesPort, actorSystem)
  val webface = new WebFaceServer("Http", config.webfacePort, actorSystem)
  val servers = Seq(websocket, webface)

  var commandShell = if (config.commandShell) Some(new CommandShell(actorSystem)) else None

  def names(srv: Seq[VirginServer], escStart: String = "", escEnd: String = ""): String = srv
    .map(v => s"$escStart${v.name}@${v.port}$escEnd") match {
    case Seq() => ""
    case Seq(h) => h
    case s =>
      val (first, last) = s.splitAt(s.size - 1)
      first.mkString(", ") + " and " + last.head
  }

  def shutdownAll(): Unit = {
    servers.foreach(_.stop())
    actorSystem.shutdown()
    out(s"$escFgBlack$serverName closed$escNormal")
    Thread.sleep(500)
  }

  def start(): Unit = {
    Runtime.getRuntime.addShutdownHook(new Thread {
      override def run(): Unit = shutdownAll()
    })

    servers.foreach(_.start())
    Thread.sleep(500)

    servers.filterNot(_.isConnected) match {
      case failed if failed.nonEmpty =>
        out(s"$escFgBlack$serverName has been failed on ${names(failed, escFgRed, escFgBlack)}$escNormal")
        System.exit(1)
      case ok =>
        out(s"$escFgBlack$serverName is running with ${names(servers)}$escNormal")
        commandShell.foreach(_.start())
    }
  }
}

object HxServerApp {
  import HxConfig._

  val escBgGray = "\u001b[47m"
  val escFgBlack = "\u001b[30;47m"
  val escFgGreen = "\u001b[32;47m"
  val escFgRed = "\u001b[31;47m"
  val escNormal = "\u001b[0m"
  val escInvertedOn = "\u001b[7m"
  val escInvertedOff = "\u001b[27m"

  var app: HxServerApp = _
  val serverName = "Helix server"

  def out(s: String): Unit = Console.out.println(s)

  def main(args: Array[String]): Unit = {
    config.msg.foreach(out)
    LoggerTuner.tuneLogger()
    config match {
      case cfg: HxConfigResolved =>
        app = new HxServerApp(cfg)
        app.start()

      case cfg: HxConfigCorrupted =>
        out(s"$escFgBlack$serverName does not started$escNormal")
    }
  }
}
