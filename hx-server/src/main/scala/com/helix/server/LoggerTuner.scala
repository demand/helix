package com.helix.server

import java.io.File

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.joran.JoranConfigurator
import ch.qos.logback.core.joran.spi.JoranException
import ch.qos.logback.core.util.StatusPrinter
import org.slf4j.LoggerFactory

import scala.util.Try


object LoggerTuner {

  private val configFileProp = "logback.configurationFile"
  private val confFile = "/conf/logback.xml"

  def tuneLogger(): Unit = {
    // Use default behaviour if config file is defined
    if (System.getProperty(configFileProp) != null) return

    // try to get config file in $USER_DIR/conf/logback.xml
    val configFile = new File(System.getProperty("user.dir") + confFile)

    val rsc = LoggerTuner.getClass.getResource(confFile)

    // if file does not exists default file in app jar will be used
    val context = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
    Try {
      val configurator = new JoranConfigurator
      configurator.setContext(context)
      // Call context.reset() to clear any previous configuration, e.g. default
      // configuration. For multi-step configuration, omit calling context.reset().
      context.reset()
      if (configFile.exists) configurator.doConfigure(configFile)
      else configurator.doConfigure(rsc)
    } recover {
      case _: JoranException =>
    }

    StatusPrinter.printInCaseOfErrorsOrWarnings(context)
  }
}  
