package com.helix.server

import java.lang.management.ManagementFactory
import java.util.concurrent.atomic.AtomicReference
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global


object TestConsole {

  val esc = "\u001b"
  val escBgGray = "\u001b[47m"
  val escFgBlack = "\u001b[30;47m"
  val escFgGreen = "\u001b[32m"
  val escFgRed = "\u001b[31;47m"
  val escNormal = "\u001b[0m"
  val escInvertedOn = "\u001b[7m"
  val escInvertedOff = "\u001b[27m"

  def main (args: Array[String]) {

    val msg = new AtomicReference[String]("[nothing]")

    val initialHelix =
      """ O       o O       o O       o O       o O       o O       o O       o
        | | O   o | | O   o | | O   o | | O   o | | O   o | | O   o | | O   o |
        | | | O | | | | O | | | | O | | | | O | | | | O | | | | O | | | | O | |
        | | o   O | | o   O | | o   O | | o   O | | o   O | | o   O | | o   O |
        | o       O o       O o       O o       O o       O o       O o       O"""
        .stripMargin

    def drawNext(prevHelix: String, hasPrev: Boolean): Unit = {
      if (hasPrev) print(s"$esc[6A")
      println(prevHelix
        .replaceAll("o", s"${escFgGreen}o$escNormal")
        .replaceAll("O", s"${escFgGreen}O$escNormal") + "\n" + msg.get()
      )

      val nextHelix = prevHelix
        .split("\n")
        .map(s => s.drop(1) + s.head)
        .mkString("\n")
      Thread.sleep(200)
      drawNext(nextHelix, hasPrev = true)
    }

    val in = scala.io.Source.stdin.bufferedReader()
    Future { in.readLine() }
      .onComplete { case _ => System.exit(0) }

    drawNext(initialHelix, hasPrev = false)
  }
}
