package com.helix.server

import java.io.{IOException, InputStream, ByteArrayOutputStream}
import java.util.Scanner

import com.googlecode.lanterna.terminal.ansi.UnixTerminal
import jline.console.ConsoleReader

import scala.concurrent.Future


object TestCharConsole {

  def main(args: Array[String]): Unit = {
    println("xx")

    // jline
//    val cr = new ConsoleReader
//    cr.setPrompt("> ")
//    def read(): Unit = {
//      val l = cr.readLine()
//      cr.println(s"[$l]")
//      read()
//    }
//    read()

    // lanterna
//    val terminal = new UnixTerminal()
//    terminal.setCBreak(true)
//
//    def read(): Unit = {
//      val c = terminal.readInput()
//      if (c != null) print(s"[${c.getCharacter}]")
//      else Thread.sleep(100)
//      read()
//    }
//    read()

  }
}
