package com.helix.server

import akka.actor.{ActorSystem, Props}
import org.mashupbots.socko.events.{HttpRequestEvent, HttpResponseStatus}
import org.mashupbots.socko.routes._
import org.mashupbots.socko.webserver.{WebServerConfig, WebServer}

object WebFaceServer {
}

class WebFaceServer(val name: String, val port: Int, val actorSystem: ActorSystem)
  extends VirginServer {

  protected def routes = Routes {
    case HttpRequest(httpRequest) => httpRequest match {
      case Path("/favicon.ico") =>
        httpRequest.response.write(HttpResponseStatus.NOT_FOUND)

      case _ =>
        HxServerApp.app.mainHandler ! httpRequest
    }
 }
}
