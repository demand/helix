package com.helix.server

import akka.actor.{Props, ActorSystem}

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

case class ShellCmd(s: String)

class CommandShell(val actorSystem: ActorSystem) {

  def printHelp(): Unit = {
    x("Use \"exit\" for shutdown server.")
    prompt()
  }

  def x(s: String): Unit = Console.out.println(s)
  def prompt(): Unit = Console.out.print("> ")

  private[this] def goAhead(s: String): Boolean = s match {
      case null => false
      case ss => (ss, ss.toLowerCase.trim) match {
        case (_, "exit") => false
        case (_, "help") => printHelp(); true
        case _ => actorSystem.actorOf(Props[MainHandler]) ! ShellCmd(ss); true
      }
    }

  def start() = {
    x("Command shell launched. Try \"help\" in confusion.")
    prompt()

    val in = scala.io.Source.stdin.bufferedReader()
    def processLines(): Unit = if (goAhead(in.readLine())) processLines()

    Future { processLines() }
      .onComplete { case _ => System.exit(0) }
  }

}
