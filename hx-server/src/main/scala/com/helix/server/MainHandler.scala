package com.helix.server

import java.io.DataOutput
import java.text.SimpleDateFormat
import java.util.GregorianCalendar

import akka.actor.Actor
import akka.event.Logging
import com.dyuproject.protostuff.{LinkedBuffer, ProtostuffIOUtil}
import com.helix.messages.HelloWorld
import org.mashupbots.socko.events.{WebSocketHandshakeEvent, WebSocketFrameEvent, HttpRequestEvent}
import org.mashupbots.socko.routes.{GET, Path, WebSocketHandshake}
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global


class MainHandler extends Actor {
  val log = Logging(context.system, this)

  override def receive = {
    case event: HttpRequestEvent =>
      if (event.request.is100ContinueExpected)
        event.response.write100Continue()

      event match {
        case GET(Path("/about")) =>
          val buf =
            """<html><head><title>Helix Server Face</title></head>
              |<body>
              |<h1>Helix Server Face</h1>
              |<p>Hello</p>
              |</body>
              |</html>
            """.stripMargin
          event.response.write(buf, "text/html; charset=UTF-8")

        case _ =>
          val buf =
          """<html><head><title>Helix Server Face</title></head>
            |<body>
            |<h1>Not found</h1>
            |</body>
            |</html>
          """.stripMargin
          event.response.write(buf, "text/html; charset=UTF-8")

      }

    case event: WebSocketFrameEvent =>
      if (event.isBinary) {
        val bytes = event.readBinary()

        val sch = HelloWorld.getSchema
        val hw0 = new HelloWorld
        ProtostuffIOUtil.mergeFrom(bytes, hw0, sch)
        val msg = hw0.getMessage

        val buffer = LinkedBuffer.allocate(1000)
        val hw1 = new HelloWorld()
          .setMessage(msg)
          .setReverse(msg.reverse)
        val data = ProtostuffIOUtil.toByteArray(hw1, sch, buffer)
        buffer.clear()

        HxServerApp.app.websocket.writeBinary(data)
      } else if (event.isText) {
        writeWebSocketResponse(event)
      }


    case ShellCmd(c) if c == null =>
      HxServerApp.out("Command is null")

    case ShellCmd(c) =>
      HxServerApp.out(s"Command: $c")

    case mm => HxServerApp.out("Received unknown message of type: " + mm)
  }

  private def writeWebSocketResponse(event: WebSocketFrameEvent) {
    log.info("TextWebSocketFrame: " + event.readText)

    val dateFormatter = new SimpleDateFormat("HH:mm:ss")
    val time = new GregorianCalendar()
    val ts = dateFormatter.format(time.getTime)

    HxServerApp.app.websocket.writeText(ts + " " + event.readText, event.webSocketId)
  }

  //      context.stop(self)


}
