package com.helix.server

import akka.actor.ActorSystem
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.http.{HttpHeaders, HttpRequest}
import org.mashupbots.socko.events.SockoEvent
import org.mashupbots.socko.webserver.{RequestHandler, WebServer, WebServerConfig}

object VirginServer {

  class SafeRequestHandler(server: WebServer, routes: PartialFunction[SockoEvent, Unit])
    extends RequestHandler(server, routes) {

    override def channelRead(ctx: ChannelHandlerContext, e: AnyRef): Unit = {
      e match {
        case httpRequest: HttpRequest =>
          val failedConditions = Seq(
            "method" -> Option(httpRequest.getMethod).map(_.toString),
            "host"   -> Option(HttpHeaders.getHost(httpRequest)),
            "uri"    -> Option(httpRequest.getUri)
          ).filter(_._2.isEmpty).map(_._1)
          if (failedConditions.nonEmpty) {
            val fields = failedConditions.mkString(", ")
            log.warn(s"EndPoint $fields cannot be null or empty string")
          } else super.channelRead(ctx, e)
        case _ =>
          super.channelRead(ctx, e)
      }
    }
  }
}

trait VirginServer {
  type VirginRoutes = PartialFunction[SockoEvent, Unit]

  def port: Int
  def name: String

  protected def actorSystem: ActorSystem

  protected def routes: VirginRoutes

  protected val underlying = new WebServer(WebServerConfig(port = port, hostname = "0.0.0.0", serverName = name),
    actorSystem, { server => new VirginServer.SafeRequestHandler(server, routes) })

  def start(): VirginServer = { underlying.start(); this }

  def stop(): VirginServer = { underlying.stop(); this }

  def isConnected = !underlying.allChannels.isEmpty
}
