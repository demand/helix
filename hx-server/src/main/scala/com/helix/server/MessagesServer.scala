package com.helix.server

import com.helix.client.{MReceivedString, MReceivedBytes}
import org.mashupbots.socko.routes._
import akka.actor.{ActorSystem, Props, actorRef2Scala}
import org.mashupbots.socko.webserver.{WebServer, WebServerConfig}

class MessagesServer(val name: String, val port: Int, val actorSystem: ActorSystem)
  extends VirginServer {

  protected def routes = Routes {
    case WebSocketHandshake(wsHandshake) =>
      wsHandshake.authorize(
        onComplete = Some(onWebSocketHandshakeComplete),
        onClose = Some(onWebSocketClose))

    case WebSocketFrame(wsFrame) =>
      HxServerApp.app.mainHandler ! wsFrame
  }

  def writeText(s: String): Unit =
    underlying.webSocketConnections.writeText(s)

  def writeText(s: String, id: String): Unit =
    underlying.webSocketConnections.writeText(s, id)

  def writeBinary(b: Array[Byte]): Unit =
    underlying.webSocketConnections.writeBinary(b)

  def writeBinary(b: Array[Byte], id: String): Unit =
    underlying.webSocketConnections.writeBinary(b, id)


  private[this] def onWebSocketHandshakeComplete(webSocketId: String) {
    HxServerApp.out(s"Web Socket $webSocketId connected")
    HxServerApp.app.commandShell.foreach(_.prompt())
  }

  private[this] def onWebSocketClose(webSocketId: String) {
    Console.out.println(s"Web Socket $webSocketId closed")
  }

}
